// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:31 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FormulaFraminghamDTO.java

package infraestructura.formulas;


public class FormulaFraminghamDTO
{

    public FormulaFraminghamDTO()
    {
        P_EDAD = "Edad";
        P_FUMADOR = "Cu\341ntos cigarros fuma al d\355a";
        P_PRESION_ARTERIAL_SISTOLICA = "Presi\363n Arterial Sist\363lica";
        P_DIABETES = "\277Es diab\351tico?";
        P_HIPERTROFIA_V_I = "Ecocardiograf\355a - Hipertrofia del ventr\355culo izquierdo";
        P_COLESTEROL = "Colesterol Total";
        P_HDL = "HDL";
    }

    public String getS_DIABETICO()
    {
        return S_DIABETICO;
    }

    public void setS_DIABETICO(String S_DIABETICO)
    {
        if(S_DIABETICO.equalsIgnoreCase("S\355"))
            setDIABETES(1.0D);
        else
            setDIABETES(0.0D);
        this.S_DIABETICO = S_DIABETICO;
    }

    public String getS_HIPERTROFIA_V_I()
    {
        return S_HIPERTROFIA_V_I;
    }

    public String getS_SEXO()
    {
        return S_SEXO;
    }

    public void setS_SEXO(String S_SEXO)
    {
        if(S_SEXO.equalsIgnoreCase("M"))
            setSEXO(1.0D);
        else
            setSEXO(0.0D);
        this.S_SEXO = S_SEXO;
    }

    public void setS_HIPERTROFIA_V_I(String S_HIPERTROFIA_V_I)
    {
        if(S_HIPERTROFIA_V_I.equalsIgnoreCase("S\355"))
            setHIPERTROFIA_VENTRICULO_IZQ(1.0D);
        else
            setHIPERTROFIA_VENTRICULO_IZQ(0.0D);
        this.S_HIPERTROFIA_V_I = S_HIPERTROFIA_V_I;
    }

    public double getEDAD()
    {
        return EDAD;
    }

    public double getTIME_PERIOD()
    {
        return TIME_PERIOD;
    }

    public void setTIME_PERIOD(double TIME_PERIOD)
    {
        this.TIME_PERIOD = TIME_PERIOD;
    }

    public void setEDAD(double EDAD)
    {
        this.EDAD = EDAD;
    }

    public double getSEXO()
    {
        return SEXO;
    }

    private void setSEXO(double SEXO)
    {
        this.SEXO = SEXO;
    }

    public double getFUMADOR()
    {
        return FUMADOR;
    }

    public void setFUMADOR(double FUMADOR)
    {
        if(FUMADOR > 1.0D)
        {
            this.FUMADOR = 1.0D;
            return;
        }
        if(FUMADOR < 0.0D)
        {
            this.FUMADOR = 0.0D;
            return;
        } else
        {
            this.FUMADOR = FUMADOR;
            return;
        }
    }

    public double getCOLESTEROL_TOTAL()
    {
        return COLESTEROL_TOTAL;
    }

    public void setCOLESTEROL_TOTAL(double COLESTEROL_TOTAL)
    {
        this.COLESTEROL_TOTAL = COLESTEROL_TOTAL;
    }

    public double getDIABETES()
    {
        return DIABETES;
    }

    private void setDIABETES(double DIABETES)
    {
        this.DIABETES = DIABETES;
    }

    public double getHIPERTROFIA_VENTRICULO_IZQ()
    {
        return HIPERTROFIA_VENTRICULO_IZQ;
    }

    private void setHIPERTROFIA_VENTRICULO_IZQ(double HIPERTROFIA_VENTRICULO_IZQ)
    {
        this.HIPERTROFIA_VENTRICULO_IZQ = HIPERTROFIA_VENTRICULO_IZQ;
    }

    public double getPRESION_ARTERIAL_SISTOLICA()
    {
        return PRESION_ARTERIAL_SISTOLICA;
    }

    public void setPRESION_ARTERIAL_SISTOLICA(double PRESION_ARTERIAL_SISTOLICA)
    {
        this.PRESION_ARTERIAL_SISTOLICA = PRESION_ARTERIAL_SISTOLICA;
    }

    public double getHDL()
    {
        return HDL;
    }

    public void setHDL(double HDL)
    {
        this.HDL = HDL;
    }

    public double getResultado()
    {
        double resultado = 100D * (1.0D - Math.exp(-Math.exp((Math.log(TIME_PERIOD) - (18.814399999999999D + -1.2145999999999999D * (1.0D - SEXO) + -1.8443000000000001D * Math.log(EDAD) + 0.0D * Math.log(EDAD) * Math.log(EDAD) + 0.36680000000000001D * Math.log(EDAD) * (1.0D - SEXO) + 0.0D * Math.log(EDAD) * Math.log(EDAD) * (1.0D - SEXO) + -1.4032D * Math.log(PRESION_ARTERIAL_SISTOLICA) + -0.38990000000000002D * FUMADOR + -0.53900000000000003D * Math.log(COLESTEROL_TOTAL / HDL) + -0.30359999999999998D * DIABETES + -0.16969999999999999D * DIABETES * (1.0D - SEXO) + -0.3362D * HIPERTROFIA_VENTRICULO_IZQ + 0.0D * HIPERTROFIA_VENTRICULO_IZQ * SEXO)) / (Math.exp(0.65359999999999996D) * Math.exp(-0.2402D * (18.814399999999999D + -1.2145999999999999D * (1.0D - SEXO) + -1.8443000000000001D * Math.log(EDAD) + 0.0D * Math.log(EDAD) * Math.log(EDAD) + 0.36680000000000001D * Math.log(EDAD) * (1.0D - SEXO) + 0.0D * Math.log(EDAD) * Math.log(EDAD) * (1.0D - SEXO) + -1.4032D * Math.log(PRESION_ARTERIAL_SISTOLICA) + -0.38990000000000002D * FUMADOR + -0.53900000000000003D * Math.log(COLESTEROL_TOTAL / HDL) + -0.30359999999999998D * DIABETES + -0.16969999999999999D * DIABETES * (1.0D - SEXO) + -0.3362D * HIPERTROFIA_VENTRICULO_IZQ + 0.0D * HIPERTROFIA_VENTRICULO_IZQ * SEXO))))));
        return resultado;
    }

    public double getResultado(int redondear_x_decimales)
    {
        double resultado = 100D * (1.0D - Math.exp(-Math.exp((Math.log(TIME_PERIOD) - (18.814399999999999D + -1.2145999999999999D * (1.0D - SEXO) + -1.8443000000000001D * Math.log(EDAD) + 0.0D * Math.log(EDAD) * Math.log(EDAD) + 0.36680000000000001D * Math.log(EDAD) * (1.0D - SEXO) + 0.0D * Math.log(EDAD) * Math.log(EDAD) * (1.0D - SEXO) + -1.4032D * Math.log(PRESION_ARTERIAL_SISTOLICA) + -0.38990000000000002D * FUMADOR + -0.53900000000000003D * Math.log(COLESTEROL_TOTAL / HDL) + -0.30359999999999998D * DIABETES + -0.16969999999999999D * DIABETES * (1.0D - SEXO) + -0.3362D * HIPERTROFIA_VENTRICULO_IZQ + 0.0D * HIPERTROFIA_VENTRICULO_IZQ * SEXO)) / (Math.exp(0.65359999999999996D) * Math.exp(-0.2402D * (18.814399999999999D + -1.2145999999999999D * (1.0D - SEXO) + -1.8443000000000001D * Math.log(EDAD) + 0.0D * Math.log(EDAD) * Math.log(EDAD) + 0.36680000000000001D * Math.log(EDAD) * (1.0D - SEXO) + 0.0D * Math.log(EDAD) * Math.log(EDAD) * (1.0D - SEXO) + -1.4032D * Math.log(PRESION_ARTERIAL_SISTOLICA) + -0.38990000000000002D * FUMADOR + -0.53900000000000003D * Math.log(COLESTEROL_TOTAL / HDL) + -0.30359999999999998D * DIABETES + -0.16969999999999999D * DIABETES * (1.0D - SEXO) + -0.3362D * HIPERTROFIA_VENTRICULO_IZQ + 0.0D * HIPERTROFIA_VENTRICULO_IZQ * SEXO))))));
        return redondearNumero(resultado, redondear_x_decimales);
    }

    public double redondearNumero(double numero, int digitos)
    {
        int cifras = (int)Math.pow(10D, digitos);
        return Math.rint(numero * (double)cifras) / (double)cifras;
    }

    private double EDAD;
    private double SEXO;
    private double FUMADOR;
    private double COLESTEROL_TOTAL;
    private double DIABETES;
    private double HIPERTROFIA_VENTRICULO_IZQ;
    private double PRESION_ARTERIAL_SISTOLICA;
    private double HDL;
    private double TIME_PERIOD;
    private String S_DIABETICO;
    private String S_HIPERTROFIA_V_I;
    private String S_SEXO;
    public String P_EDAD;
    public String P_FUMADOR;
    public String P_PRESION_ARTERIAL_SISTOLICA;
    public String P_DIABETES;
    public String P_HIPERTROFIA_V_I;
    public String P_COLESTEROL;
    public String P_HDL;
}
