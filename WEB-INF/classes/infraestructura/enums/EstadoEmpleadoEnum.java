// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:29:24 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadoEmpleadoEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstadoEmpleadoEnum extends Enum
{

    public static EstadoEmpleadoEnum[] values()
    {
        return (EstadoEmpleadoEnum[])$VALUES.clone();
    }

    public static EstadoEmpleadoEnum valueOf(String name)
    {
        return (EstadoEmpleadoEnum)Enum.valueOf(infraestructura/enums/EstadoEmpleadoEnum, name);
    }

    private EstadoEmpleadoEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstadoEmpleadoEnum findByCode(String code)
    {
        EstadoEmpleadoEnum array[] = values();
        EstadoEmpleadoEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstadoEmpleadoEnum ACTIVO;
    public static final EstadoEmpleadoEnum INACTIVO;
    private String code;
    private String name;
    private static final EstadoEmpleadoEnum $VALUES[];

    static 
    {
        ACTIVO = new EstadoEmpleadoEnum("ACTIVO", 0, "A", "Activo");
        INACTIVO = new EstadoEmpleadoEnum("INACTIVO", 1, "I", "Inactivo");
        $VALUES = (new EstadoEmpleadoEnum[] {
            ACTIVO, INACTIVO
        });
    }
}
