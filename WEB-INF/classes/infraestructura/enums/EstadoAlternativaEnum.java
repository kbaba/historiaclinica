// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:29:18 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadoAlternativaEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstadoAlternativaEnum extends Enum
{

    public static EstadoAlternativaEnum[] values()
    {
        return (EstadoAlternativaEnum[])$VALUES.clone();
    }

    public static EstadoAlternativaEnum valueOf(String name)
    {
        return (EstadoAlternativaEnum)Enum.valueOf(infraestructura/enums/EstadoAlternativaEnum, name);
    }

    private EstadoAlternativaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstadoAlternativaEnum findByCode(String code)
    {
        EstadoAlternativaEnum array[] = values();
        EstadoAlternativaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstadoAlternativaEnum ACTIVA;
    public static final EstadoAlternativaEnum INACTIVA;
    private String code;
    private String name;
    private static final EstadoAlternativaEnum $VALUES[];

    static 
    {
        ACTIVA = new EstadoAlternativaEnum("ACTIVA", 0, "A", "Activa");
        INACTIVA = new EstadoAlternativaEnum("INACTIVA", 1, "I", "Inactiva");
        $VALUES = (new EstadoAlternativaEnum[] {
            ACTIVA, INACTIVA
        });
    }
}
