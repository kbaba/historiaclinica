// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:17 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstatutoSocialPersonaEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstatutoSocialPersonaEnum extends Enum
{

    public static EstatutoSocialPersonaEnum[] values()
    {
        return (EstatutoSocialPersonaEnum[])$VALUES.clone();
    }

    public static EstatutoSocialPersonaEnum valueOf(String name)
    {
        return (EstatutoSocialPersonaEnum)Enum.valueOf(infraestructura/enums/EstatutoSocialPersonaEnum, name);
    }

    private EstatutoSocialPersonaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstatutoSocialPersonaEnum findByCode(String code)
    {
        EstatutoSocialPersonaEnum array[] = values();
        EstatutoSocialPersonaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstatutoSocialPersonaEnum ADECUADO;
    public static final EstatutoSocialPersonaEnum MEDIO;
    public static final EstatutoSocialPersonaEnum INADECUADO;
    private String code;
    private String name;
    private static final EstatutoSocialPersonaEnum $VALUES[];

    static 
    {
        ADECUADO = new EstatutoSocialPersonaEnum("ADECUADO", 0, "A", "Condici\363n m\351dica adecuada");
        MEDIO = new EstatutoSocialPersonaEnum("MEDIO", 1, "B", "Condici\363n m\351dica media");
        INADECUADO = new EstatutoSocialPersonaEnum("INADECUADO", 2, "C", "Condici\363n m\351dica inadecuada");
        $VALUES = (new EstatutoSocialPersonaEnum[] {
            ADECUADO, MEDIO, INADECUADO
        });
    }
}
