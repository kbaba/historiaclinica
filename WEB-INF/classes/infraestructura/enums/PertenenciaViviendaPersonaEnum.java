// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:34 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PertenenciaViviendaPersonaEnum.java

package infraestructura.enums;

import java.util.*;

public final class PertenenciaViviendaPersonaEnum extends Enum
{

    public static PertenenciaViviendaPersonaEnum[] values()
    {
        return (PertenenciaViviendaPersonaEnum[])$VALUES.clone();
    }

    public static PertenenciaViviendaPersonaEnum valueOf(String name)
    {
        return (PertenenciaViviendaPersonaEnum)Enum.valueOf(infraestructura/enums/PertenenciaViviendaPersonaEnum, name);
    }

    private PertenenciaViviendaPersonaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static PertenenciaViviendaPersonaEnum findByCode(String code)
    {
        PertenenciaViviendaPersonaEnum array[] = values();
        PertenenciaViviendaPersonaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final PertenenciaViviendaPersonaEnum ALQUILADA;
    public static final PertenenciaViviendaPersonaEnum PROPIA;
    public static final PertenenciaViviendaPersonaEnum INVADIDA;
    private String code;
    private String name;
    private static final PertenenciaViviendaPersonaEnum $VALUES[];

    static 
    {
        ALQUILADA = new PertenenciaViviendaPersonaEnum("ALQUILADA", 0, "A", "Alquilada");
        PROPIA = new PertenenciaViviendaPersonaEnum("PROPIA", 1, "P", "Propia");
        INVADIDA = new PertenenciaViviendaPersonaEnum("INVADIDA", 2, "I", "Invaci\363n");
        $VALUES = (new PertenenciaViviendaPersonaEnum[] {
            ALQUILADA, PROPIA, INVADIDA
        });
    }
}
