// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:57 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TipoResidenciaPersonaEnum.java

package infraestructura.enums;

import java.util.*;

public final class TipoResidenciaPersonaEnum extends Enum
{

    public static TipoResidenciaPersonaEnum[] values()
    {
        return (TipoResidenciaPersonaEnum[])$VALUES.clone();
    }

    public static TipoResidenciaPersonaEnum valueOf(String name)
    {
        return (TipoResidenciaPersonaEnum)Enum.valueOf(infraestructura/enums/TipoResidenciaPersonaEnum, name);
    }

    private TipoResidenciaPersonaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static TipoResidenciaPersonaEnum findByCode(String code)
    {
        TipoResidenciaPersonaEnum array[] = values();
        TipoResidenciaPersonaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final TipoResidenciaPersonaEnum URBANO;
    public static final TipoResidenciaPersonaEnum MARGINAL;
    private String code;
    private String name;
    private static final TipoResidenciaPersonaEnum $VALUES[];

    static 
    {
        URBANO = new TipoResidenciaPersonaEnum("URBANO", 0, "U", "Urbano");
        MARGINAL = new TipoResidenciaPersonaEnum("MARGINAL", 1, "M", "Marginal");
        $VALUES = (new TipoResidenciaPersonaEnum[] {
            URBANO, MARGINAL
        });
    }
}
