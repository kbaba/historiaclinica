// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:43 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   RolEnum.java

package infraestructura.enums;

import java.util.*;

public final class RolEnum extends Enum
{

    public static RolEnum[] values()
    {
        return (RolEnum[])$VALUES.clone();
    }

    public static RolEnum valueOf(String name)
    {
        return (RolEnum)Enum.valueOf(infraestructura/enums/RolEnum, name);
    }

    private RolEnum(String s, int i, Integer code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public Integer getCode()
    {
        return code;
    }

    public void setCode(Integer code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static RolEnum findByCode(Integer code)
    {
        RolEnum array[] = values();
        RolEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final RolEnum ADMINISTRADOR;
    public static final RolEnum TECNICO_ENFERMERIA;
    public static final RolEnum LABORATORISTA;
    public static final RolEnum DOCTOR;
    private Integer code;
    private String name;
    private static final RolEnum $VALUES[];

    static 
    {
        ADMINISTRADOR = new RolEnum("ADMINISTRADOR", 0, Integer.valueOf(1), "Administrador");
        TECNICO_ENFERMERIA = new RolEnum("TECNICO_ENFERMERIA", 1, Integer.valueOf(2), "Enfermera");
        LABORATORISTA = new RolEnum("LABORATORISTA", 2, Integer.valueOf(3), "Laboratorista");
        DOCTOR = new RolEnum("DOCTOR", 3, Integer.valueOf(4), "Doctor");
        $VALUES = (new RolEnum[] {
            ADMINISTRADOR, TECNICO_ENFERMERIA, LABORATORISTA, DOCTOR
        });
    }
}
