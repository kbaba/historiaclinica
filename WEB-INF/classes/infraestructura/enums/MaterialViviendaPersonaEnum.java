// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:25 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   MaterialViviendaPersonaEnum.java

package infraestructura.enums;

import java.util.*;

public final class MaterialViviendaPersonaEnum extends Enum
{

    public static MaterialViviendaPersonaEnum[] values()
    {
        return (MaterialViviendaPersonaEnum[])$VALUES.clone();
    }

    public static MaterialViviendaPersonaEnum valueOf(String name)
    {
        return (MaterialViviendaPersonaEnum)Enum.valueOf(infraestructura/enums/MaterialViviendaPersonaEnum, name);
    }

    private MaterialViviendaPersonaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static MaterialViviendaPersonaEnum findByCode(String code)
    {
        MaterialViviendaPersonaEnum array[] = values();
        MaterialViviendaPersonaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final MaterialViviendaPersonaEnum NOBLE;
    public static final MaterialViviendaPersonaEnum SEMINOBLE;
    public static final MaterialViviendaPersonaEnum PRECARIO;
    private String code;
    private String name;
    private static final MaterialViviendaPersonaEnum $VALUES[];

    static 
    {
        NOBLE = new MaterialViviendaPersonaEnum("NOBLE", 0, "N", "Noble");
        SEMINOBLE = new MaterialViviendaPersonaEnum("SEMINOBLE", 1, "S", "Seminoble");
        PRECARIO = new MaterialViviendaPersonaEnum("PRECARIO", 2, "P", "Precario");
        $VALUES = (new MaterialViviendaPersonaEnum[] {
            NOBLE, SEMINOBLE, PRECARIO
        });
    }
}
