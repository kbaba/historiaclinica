// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:29:31 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadoPersonaEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstadoPersonaEnum extends Enum
{

    public static EstadoPersonaEnum[] values()
    {
        return (EstadoPersonaEnum[])$VALUES.clone();
    }

    public static EstadoPersonaEnum valueOf(String name)
    {
        return (EstadoPersonaEnum)Enum.valueOf(infraestructura/enums/EstadoPersonaEnum, name);
    }

    private EstadoPersonaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstadoPersonaEnum findByCode(String code)
    {
        EstadoPersonaEnum array[] = values();
        EstadoPersonaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstadoPersonaEnum ACTIVO;
    public static final EstadoPersonaEnum INACTIVO;
    private String code;
    private String name;
    private static final EstadoPersonaEnum $VALUES[];

    static 
    {
        ACTIVO = new EstadoPersonaEnum("ACTIVO", 0, "A", "Activo");
        INACTIVO = new EstadoPersonaEnum("INACTIVO", 1, "I", "Inactivo");
        $VALUES = (new EstadoPersonaEnum[] {
            ACTIVO, INACTIVO
        });
    }
}
