// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:29:49 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadosEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstadosEnum extends Enum
{

    public static EstadosEnum[] values()
    {
        return (EstadosEnum[])$VALUES.clone();
    }

    public static EstadosEnum valueOf(String name)
    {
        return (EstadosEnum)Enum.valueOf(infraestructura/enums/EstadosEnum, name);
    }

    private EstadosEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstadosEnum findByCode(String code)
    {
        EstadosEnum array[] = values();
        EstadosEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstadosEnum ACTIVA;
    public static final EstadosEnum INACTIVA;
    private String code;
    private String name;
    private static final EstadosEnum $VALUES[];

    static 
    {
        ACTIVA = new EstadosEnum("ACTIVA", 0, "A", "Activa");
        INACTIVA = new EstadosEnum("INACTIVA", 1, "I", "Inactiva");
        $VALUES = (new EstadosEnum[] {
            ACTIVA, INACTIVA
        });
    }
}
