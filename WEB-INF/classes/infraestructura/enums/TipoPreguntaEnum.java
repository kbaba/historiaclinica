// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:52 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TipoPreguntaEnum.java

package infraestructura.enums;

import java.util.*;

public final class TipoPreguntaEnum extends Enum
{

    public static TipoPreguntaEnum[] values()
    {
        return (TipoPreguntaEnum[])$VALUES.clone();
    }

    public static TipoPreguntaEnum valueOf(String name)
    {
        return (TipoPreguntaEnum)Enum.valueOf(infraestructura/enums/TipoPreguntaEnum, name);
    }

    private TipoPreguntaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static TipoPreguntaEnum findByCode(String code)
    {
        TipoPreguntaEnum array[] = values();
        TipoPreguntaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final TipoPreguntaEnum NO_DEFINIDO;
    private String code;
    private String name;
    private static final TipoPreguntaEnum $VALUES[];

    static 
    {
        NO_DEFINIDO = new TipoPreguntaEnum("NO_DEFINIDO", 0, "X", "No definido");
        $VALUES = (new TipoPreguntaEnum[] {
            NO_DEFINIDO
        });
    }
}
