// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:47 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SexoPersonaEnum.java

package infraestructura.enums;

import java.util.*;

public final class SexoPersonaEnum extends Enum
{

    public static SexoPersonaEnum[] values()
    {
        return (SexoPersonaEnum[])$VALUES.clone();
    }

    public static SexoPersonaEnum valueOf(String name)
    {
        return (SexoPersonaEnum)Enum.valueOf(infraestructura/enums/SexoPersonaEnum, name);
    }

    private SexoPersonaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static SexoPersonaEnum findByCode(String code)
    {
        SexoPersonaEnum array[] = values();
        SexoPersonaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final SexoPersonaEnum MASCULINO;
    public static final SexoPersonaEnum FEMENINO;
    private String code;
    private String name;
    private static final SexoPersonaEnum $VALUES[];

    static 
    {
        MASCULINO = new SexoPersonaEnum("MASCULINO", 0, "M", "Masculino");
        FEMENINO = new SexoPersonaEnum("FEMENINO", 1, "F", "Femenino");
        $VALUES = (new SexoPersonaEnum[] {
            MASCULINO, FEMENINO
        });
    }
}
