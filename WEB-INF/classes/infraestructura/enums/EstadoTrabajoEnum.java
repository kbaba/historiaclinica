// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:29:55 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadoTrabajoEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstadoTrabajoEnum extends Enum
{

    public static EstadoTrabajoEnum[] values()
    {
        return (EstadoTrabajoEnum[])$VALUES.clone();
    }

    public static EstadoTrabajoEnum valueOf(String name)
    {
        return (EstadoTrabajoEnum)Enum.valueOf(infraestructura/enums/EstadoTrabajoEnum, name);
    }

    private EstadoTrabajoEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstadoTrabajoEnum findByCode(String code)
    {
        EstadoTrabajoEnum array[] = values();
        EstadoTrabajoEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstadoTrabajoEnum ACTIVO;
    public static final EstadoTrabajoEnum INACTIVO;
    private String code;
    private String name;
    private static final EstadoTrabajoEnum $VALUES[];

    static 
    {
        ACTIVO = new EstadoTrabajoEnum("ACTIVO", 0, "A", "Activo");
        INACTIVO = new EstadoTrabajoEnum("INACTIVO", 1, "I", "Inactivo");
        $VALUES = (new EstadoTrabajoEnum[] {
            ACTIVO, INACTIVO
        });
    }
}
