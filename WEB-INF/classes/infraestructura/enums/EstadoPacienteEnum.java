// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:29:28 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadoPacienteEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstadoPacienteEnum extends Enum
{

    public static EstadoPacienteEnum[] values()
    {
        return (EstadoPacienteEnum[])$VALUES.clone();
    }

    public static EstadoPacienteEnum valueOf(String name)
    {
        return (EstadoPacienteEnum)Enum.valueOf(infraestructura/enums/EstadoPacienteEnum, name);
    }

    private EstadoPacienteEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstadoPacienteEnum findByCode(String code)
    {
        EstadoPacienteEnum array[] = values();
        EstadoPacienteEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstadoPacienteEnum ACTIVO;
    public static final EstadoPacienteEnum INACTIVO;
    public static final EstadoPacienteEnum FALLECIDO;
    private String code;
    private String name;
    private static final EstadoPacienteEnum $VALUES[];

    static 
    {
        ACTIVO = new EstadoPacienteEnum("ACTIVO", 0, "A", "Activo");
        INACTIVO = new EstadoPacienteEnum("INACTIVO", 1, "I", "Inactivo");
        FALLECIDO = new EstadoPacienteEnum("FALLECIDO", 2, "F", "Fallecido");
        $VALUES = (new EstadoPacienteEnum[] {
            ACTIVO, INACTIVO, FALLECIDO
        });
    }
}
