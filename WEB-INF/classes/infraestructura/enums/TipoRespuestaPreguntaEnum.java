// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:02 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TipoRespuestaPreguntaEnum.java

package infraestructura.enums;

import java.util.*;

public final class TipoRespuestaPreguntaEnum extends Enum
{

    public static TipoRespuestaPreguntaEnum[] values()
    {
        return (TipoRespuestaPreguntaEnum[])$VALUES.clone();
    }

    public static TipoRespuestaPreguntaEnum valueOf(String name)
    {
        return (TipoRespuestaPreguntaEnum)Enum.valueOf(infraestructura/enums/TipoRespuestaPreguntaEnum, name);
    }

    private TipoRespuestaPreguntaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static TipoRespuestaPreguntaEnum findByCode(String code)
    {
        TipoRespuestaPreguntaEnum array[] = values();
        TipoRespuestaPreguntaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final TipoRespuestaPreguntaEnum CHECKBOX;
    public static final TipoRespuestaPreguntaEnum CHECKBOX_OTRO;
    public static final TipoRespuestaPreguntaEnum RADIOBUTTON;
    public static final TipoRespuestaPreguntaEnum RADIOBUTTON_OTRO;
    public static final TipoRespuestaPreguntaEnum INPUT;
    public static final TipoRespuestaPreguntaEnum INPUTNUM;
    public static final TipoRespuestaPreguntaEnum TEXTAREA;
    private String code;
    private String name;
    private static final TipoRespuestaPreguntaEnum $VALUES[];

    static 
    {
        CHECKBOX = new TipoRespuestaPreguntaEnum("CHECKBOX", 0, "C", "Check");
        CHECKBOX_OTRO = new TipoRespuestaPreguntaEnum("CHECKBOX_OTRO", 1, "Z", "Check con alternativa: \"otro\"");
        RADIOBUTTON = new TipoRespuestaPreguntaEnum("RADIOBUTTON", 2, "G", "Grupo");
        RADIOBUTTON_OTRO = new TipoRespuestaPreguntaEnum("RADIOBUTTON_OTRO", 3, "O", "Grupo con alternativa: \"otro\"");
        INPUT = new TipoRespuestaPreguntaEnum("INPUT", 4, "T", "Campo de Texto Alfab\351tico");
        INPUTNUM = new TipoRespuestaPreguntaEnum("INPUTNUM", 5, "N", "Campo de Texto N\372merico");
        TEXTAREA = new TipoRespuestaPreguntaEnum("TEXTAREA", 6, "A", "\301rea de Texto");
        $VALUES = (new TipoRespuestaPreguntaEnum[] {
            CHECKBOX, CHECKBOX_OTRO, RADIOBUTTON, RADIOBUTTON_OTRO, INPUT, INPUTNUM, TEXTAREA
        });
    }
}
