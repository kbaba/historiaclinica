// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:30:39 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   QuienPreguntaEnum.java

package infraestructura.enums;

import java.util.*;

public final class QuienPreguntaEnum extends Enum
{

    public static QuienPreguntaEnum[] values()
    {
        return (QuienPreguntaEnum[])$VALUES.clone();
    }

    public static QuienPreguntaEnum valueOf(String name)
    {
        return (QuienPreguntaEnum)Enum.valueOf(infraestructura/enums/QuienPreguntaEnum, name);
    }

    private QuienPreguntaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static QuienPreguntaEnum findByCode(String code)
    {
        QuienPreguntaEnum array[] = values();
        QuienPreguntaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final QuienPreguntaEnum ENFERMERIA;
    public static final QuienPreguntaEnum TECNICO_LABORATORIO;
    public static final QuienPreguntaEnum MEDICO;
    private String code;
    private String name;
    private static final QuienPreguntaEnum $VALUES[];

    static 
    {
        ENFERMERIA = new QuienPreguntaEnum("ENFERMERIA", 0, "T", "Enfermera");
        TECNICO_LABORATORIO = new QuienPreguntaEnum("TECNICO_LABORATORIO", 1, "L", "Laboratorista");
        MEDICO = new QuienPreguntaEnum("MEDICO", 2, "M", "M\351dico");
        $VALUES = (new QuienPreguntaEnum[] {
            ENFERMERIA, TECNICO_LABORATORIO, MEDICO
        });
    }
}
