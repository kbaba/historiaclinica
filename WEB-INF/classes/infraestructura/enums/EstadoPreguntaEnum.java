// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:29:38 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadoPreguntaEnum.java

package infraestructura.enums;

import java.util.*;

public final class EstadoPreguntaEnum extends Enum
{

    public static EstadoPreguntaEnum[] values()
    {
        return (EstadoPreguntaEnum[])$VALUES.clone();
    }

    public static EstadoPreguntaEnum valueOf(String name)
    {
        return (EstadoPreguntaEnum)Enum.valueOf(infraestructura/enums/EstadoPreguntaEnum, name);
    }

    private EstadoPreguntaEnum(String s, int i, String code, String name)
    {
        super(s, i);
        this.code = code;
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static EstadoPreguntaEnum findByCode(String code)
    {
        EstadoPreguntaEnum array[] = values();
        EstadoPreguntaEnum status = null;
        int i = 0;
        do
        {
            if(i >= array.length)
                break;
            if(array[i].getCode().equals(code))
            {
                status = array[i];
                break;
            }
            i++;
        } while(true);
        return status;
    }

    public static List findAll()
    {
        List list = new ArrayList();
        list.addAll(Arrays.asList(values()));
        return list;
    }

    public static final EstadoPreguntaEnum ACTIVA;
    public static final EstadoPreguntaEnum INACTIVA;
    private String code;
    private String name;
    private static final EstadoPreguntaEnum $VALUES[];

    static 
    {
        ACTIVA = new EstadoPreguntaEnum("ACTIVA", 0, "A", "Activa");
        INACTIVA = new EstadoPreguntaEnum("INACTIVA", 1, "I", "Inactiva");
        $VALUES = (new EstadoPreguntaEnum[] {
            ACTIVA, INACTIVA
        });
    }
}
