// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:26:25 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ApplicationDTO.java

package infraestructura.dto;

import java.util.Date;

// Referenced classes of package infraestructura.dto:
//            PersonaDTO, UsuarioDTO, AtencionDTO

public class ApplicationDTO
{

    public ApplicationDTO()
    {
    }

    public ApplicationDTO(PersonaDTO paciente, Date horaSeparacion)
    {
        pacienteSeparado = paciente;
        this.horaSeparacion = horaSeparacion;
        setSeEncuentraEnAtencion(Boolean.valueOf(false));
    }

    public ApplicationDTO(PersonaDTO paciente, Date horaSeparacion, Boolean seEncuentraEnAtencion)
    {
        pacienteSeparado = paciente;
        this.horaSeparacion = horaSeparacion;
        this.seEncuentraEnAtencion = seEncuentraEnAtencion;
    }

    public ApplicationDTO(PersonaDTO pacienteSeparado, UsuarioDTO empleado, AtencionDTO atencion, Date horaSeparacion, Boolean seEncuentraEnAtencion)
    {
        this.pacienteSeparado = pacienteSeparado;
        this.empleado = empleado;
        atention = atencion;
        this.horaSeparacion = horaSeparacion;
        this.seEncuentraEnAtencion = seEncuentraEnAtencion;
    }

    public ApplicationDTO(PersonaDTO pacienteSeparado, UsuarioDTO empleado, AtencionDTO atencion, Date horaSeparacion)
    {
        this.pacienteSeparado = pacienteSeparado;
        this.empleado = empleado;
        atention = atencion;
        this.horaSeparacion = horaSeparacion;
        setSeEncuentraEnAtencion(Boolean.valueOf(false));
    }

    public PersonaDTO getPacienteSeparado()
    {
        return pacienteSeparado;
    }

    public void setPacienteSeparado(PersonaDTO pacienteSeparado)
    {
        this.pacienteSeparado = pacienteSeparado;
    }

    public Date getHoraSeparacion()
    {
        return horaSeparacion;
    }

    public void setHoraSeparacion(Date horaSeparacion)
    {
        this.horaSeparacion = horaSeparacion;
    }

    public Boolean getSeEncuentraEnAtencion()
    {
        return seEncuentraEnAtencion;
    }

    public void setSeEncuentraEnAtencion(Boolean seEncuentraEnAtencion)
    {
        this.seEncuentraEnAtencion = seEncuentraEnAtencion;
    }

    public UsuarioDTO getEmpleado()
    {
        return empleado;
    }

    public void setEmpleado(UsuarioDTO empleado)
    {
        this.empleado = empleado;
    }

    public AtencionDTO getAtention()
    {
        return atention;
    }

    public void setAtention(AtencionDTO atention)
    {
        this.atention = atention;
    }

    private PersonaDTO pacienteSeparado;
    private UsuarioDTO empleado;
    private AtencionDTO atention;
    private Date horaSeparacion;
    private Boolean seEncuentraEnAtencion;
}
