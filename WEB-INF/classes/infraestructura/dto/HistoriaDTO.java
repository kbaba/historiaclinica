// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:22 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriaDTO.java

package infraestructura.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

// Referenced classes of package infraestructura.dto:
//            AtencionDTO, PacienteDTO

public class HistoriaDTO
    implements Serializable
{

    public HistoriaDTO()
    {
        conjuntoAtenciones = new HashSet();
    }

    public Integer getPacienteId()
    {
        return pacienteId;
    }

    public void setPacienteId(Integer pacienteId)
    {
        this.pacienteId = pacienteId;
    }

    public Integer getIdHistoria()
    {
        return idHistoria;
    }

    public void setIdHistoria(Integer idHistoria)
    {
        this.idHistoria = idHistoria;
    }

    public PacienteDTO getPaciente()
    {
        return paciente;
    }

    public void setPaciente(PacienteDTO paciente)
    {
        this.paciente = paciente;
    }

    public Set getConjuntoAtenciones()
    {
        return conjuntoAtenciones;
    }

    public void setConjuntoAtenciones(Set conjuntoAtenciones)
    {
        this.conjuntoAtenciones = conjuntoAtenciones;
    }

    public transient void agregarAtencion(AtencionDTO atenciones[])
    {
        AtencionDTO arr$[] = atenciones;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            AtencionDTO atencion = arr$[i$];
            atencion.setHistoria(this);
            conjuntoAtenciones.add(atencion);
        }

    }

    private Integer idHistoria;
    private PacienteDTO paciente;
    private Set conjuntoAtenciones;
    private Integer pacienteId;
}
