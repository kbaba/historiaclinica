// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 06:06:34 a.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadDTO.java

package infraestructura.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class EnfermedadDTO
    implements Serializable
{

    public EnfermedadDTO()
    {
        conjuntoAtenciones = new HashSet();
    }

    public EnfermedadDTO(String idEnfermedad, String nombreEnfermedad)
    {
        conjuntoAtenciones = new HashSet();
        this.idEnfermedad = idEnfermedad;
        this.nombreEnfermedad = nombreEnfermedad;
    }

    public EnfermedadDTO(String idEnfermedad, String nombreEnfermedad, String grupoEnfermedad)
    {
        conjuntoAtenciones = new HashSet();
        this.idEnfermedad = idEnfermedad;
        this.nombreEnfermedad = nombreEnfermedad;
        this.grupoEnfermedad = grupoEnfermedad;
    }

    public String getIdEnfermedad()
    {
        return idEnfermedad;
    }

    public void setIdEnfermedad(String idEnfermedad)
    {
        this.idEnfermedad = idEnfermedad;
    }

    public String getNombreEnfermedad()
    {
        return nombreEnfermedad;
    }

    public void setNombreEnfermedad(String nombreEnfermedad)
    {
        this.nombreEnfermedad = nombreEnfermedad;
    }

    public String getGrupoEnfermedad()
    {
        return grupoEnfermedad;
    }

    public void setGrupoEnfermedad(String grupoEnfermedad)
    {
        this.grupoEnfermedad = grupoEnfermedad;
    }

    public Set getConjuntoAtenciones()
    {
        return conjuntoAtenciones;
    }

    public void setConjuntoAtenciones(Set conjuntoAtenciones)
    {
        this.conjuntoAtenciones = conjuntoAtenciones;
    }

    public Boolean getEsCapitulo()
    {
        return esCapitulo;
    }

    public void setEsCapitulo(Boolean esCapitulo)
    {
        this.esCapitulo = esCapitulo;
    }

    public String getGrupoString()
    {
        return grupoString;
    }

    public void setGrupoString(String grupoString)
    {
        this.grupoString = grupoString;
    }

    public String getCapituloString()
    {
        return capituloString;
    }

    public void setCapituloString(String capituloString)
    {
        this.capituloString = capituloString;
    }

    private String idEnfermedad;
    private String nombreEnfermedad;
    private String grupoEnfermedad;
    private String grupoString;
    private String capituloString;
    private Boolean esCapitulo;
    public Set conjuntoAtenciones;
}
