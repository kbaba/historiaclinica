// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:28 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadDTO.java

package infraestructura.dto;

import infraestructura.enums.EstadosEnum;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            ProcedimientoDTO, PreguntaDTO, EspecialidadDTO

public class SubEspecialidadDTO
    implements Serializable
{

    public SubEspecialidadDTO()
    {
        conjuntoProcedimientos = new HashSet();
        conjuntoPreguntas = new HashSet();
    }

    public Integer getIdSubEspecialidad()
    {
        return idSubEspecialidad;
    }

    public void setIdSubEspecialidad(Integer idSubEspecialidad)
    {
        this.idSubEspecialidad = idSubEspecialidad;
    }

    public String getNombreSubEspecialidad()
    {
        return nombreSubEspecialidad;
    }

    public void setNombreSubEspecialidad(String nombreSubEspecialidad)
    {
        this.nombreSubEspecialidad = nombreSubEspecialidad;
    }

    public EspecialidadDTO getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(EspecialidadDTO especialidad)
    {
        this.especialidad = especialidad;
    }

    public Set getConjuntoProcedimientos()
    {
        return conjuntoProcedimientos;
    }

    public void setConjuntoProcedimientos(Set conjuntoProcedimientos)
    {
        this.conjuntoProcedimientos = conjuntoProcedimientos;
    }

    public String getEstadoSubEspecialidad()
    {
        return estadoSubEspecialidad;
    }

    public void setEstadoSubEspecialidad(String estadoSubEspecialidad)
    {
        if(StringUtils.isNotBlank(estadoSubEspecialidad))
            setEstadoEnum(EstadosEnum.findByCode(estadoSubEspecialidad));
        this.estadoSubEspecialidad = estadoSubEspecialidad;
    }

    public String getNumeroProcedimnientos()
    {
        return numeroProcedimnientos;
    }

    public void setNumeroProcedimnientos(String numeroProcedimnientos)
    {
        this.numeroProcedimnientos = numeroProcedimnientos;
    }

    public EstadosEnum getEstadoEnum()
    {
        return estadoEnum;
    }

    public void setEstadoEnum(EstadosEnum estadoEnum)
    {
        this.estadoEnum = estadoEnum;
    }

    public Set getConjuntoPreguntas()
    {
        return conjuntoPreguntas;
    }

    public void setConjuntoPreguntas(Set conjuntoPreguntas)
    {
        this.conjuntoPreguntas = conjuntoPreguntas;
    }

    public void agregarProcedimiento(ProcedimientoDTO procedimiento)
    {
        procedimiento.setSubEspecialidad(this);
        conjuntoProcedimientos.add(procedimiento);
    }

    public void agregarPregunta(PreguntaDTO pregunta)
    {
        pregunta.setSubEspecialidad(this);
        conjuntoPreguntas.add(pregunta);
    }

    private Integer idSubEspecialidad;
    private String nombreSubEspecialidad;
    private EspecialidadDTO especialidad;
    private Set conjuntoProcedimientos;
    private Set conjuntoPreguntas;
    private String numeroProcedimnientos;
    private String estadoSubEspecialidad;
    private EstadosEnum estadoEnum;
}
