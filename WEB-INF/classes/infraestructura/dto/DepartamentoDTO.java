// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:26:43 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   DepartamentoDTO.java

package infraestructura.dto;

import java.util.HashSet;
import java.util.Set;

public class DepartamentoDTO
{

    public DepartamentoDTO()
    {
        provincias = new HashSet();
    }

    public String getIdDepartamento()
    {
        return idDepartamento;
    }

    public void setIdDepartamento(String idDepartamento)
    {
        this.idDepartamento = idDepartamento;
    }

    public String getNombreDepartamento()
    {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento)
    {
        this.nombreDepartamento = nombreDepartamento;
    }

    public String getPaisId()
    {
        return paisId;
    }

    public void setPaisId(String paisId)
    {
        this.paisId = paisId;
    }

    public DepartamentoDTO getTodo()
    {
        return this;
    }

    public String getIdCompuestoDepartamento()
    {
        return (new StringBuilder()).append(paisId).append(",").append(idDepartamento).toString();
    }

    public void setIdCompuestoDepartamento(String idCompuestoDepartamento)
    {
        this.idCompuestoDepartamento = idCompuestoDepartamento;
    }

    private String paisId;
    private String idDepartamento;
    private String nombreDepartamento;
    private Set provincias;
    private String idCompuestoDepartamento;
}
