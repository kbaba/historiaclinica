// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:01 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntaDTO.java

package infraestructura.dto;

import infraestructura.enums.*;
import java.io.Serializable;
import java.util.*;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            AlternativaDTO, EspecialidadDTO, SubEspecialidadDTO, ProcedimientoDTO

public class PreguntaDTO
    implements Serializable
{

    public PreguntaDTO()
    {
        conjuntoAlternativas = new HashSet();
        listaAlternativas = new ArrayList();
    }

    public Integer getIdPregunta()
    {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta)
    {
        this.idPregunta = idPregunta;
    }

    public String getDescripcionPregunta()
    {
        return descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta)
    {
        this.descripcionPregunta = descripcionPregunta;
    }

    public String getTipoPregunta()
    {
        return tipoPregunta;
    }

    public void setTipoPregunta(String tipoPregunta)
    {
        if(StringUtils.isNotBlank(tipoPregunta))
            setTipoPreguntaEnum(TipoPreguntaEnum.findByCode(tipoPregunta));
        this.tipoPregunta = tipoPregunta;
    }

    public String getTipoRespuesta()
    {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(String tipoRespuesta)
    {
        if(StringUtils.isNotBlank(tipoRespuesta))
            setTipoRespuestaPreguntaEnum(TipoRespuestaPreguntaEnum.findByCode(tipoRespuesta));
        this.tipoRespuesta = tipoRespuesta;
    }

    public String getQuienPregunta()
    {
        return quienPregunta;
    }

    public void setQuienPregunta(String quienPregunta)
    {
        if(StringUtils.isNotBlank(quienPregunta))
            setQuienPreguntaEnum(QuienPreguntaEnum.findByCode(quienPregunta));
        this.quienPregunta = quienPregunta;
    }

    public String getEstadoPregunta()
    {
        return estadoPregunta;
    }

    public void setEstadoPregunta(String estadoPregunta)
    {
        if(StringUtils.isNotBlank(estadoPregunta))
            setEstadoPreguntaEnum(EstadoPreguntaEnum.findByCode(estadoPregunta));
        this.estadoPregunta = estadoPregunta;
    }

    public ProcedimientoDTO getProcedimiento()
    {
        return procedimiento;
    }

    public void setProcedimiento(ProcedimientoDTO procedimiento)
    {
        this.procedimiento = procedimiento;
    }

    public Set getConjuntoAlternativas()
    {
        return conjuntoAlternativas;
    }

    public void setConjuntoAlternativas(Set conjuntoAlternativas)
    {
        this.conjuntoAlternativas = conjuntoAlternativas;
        addAlternativas(conjuntoAlternativas);
    }

    public EspecialidadDTO getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(EspecialidadDTO especialidad)
    {
        this.especialidad = especialidad;
    }

    public SubEspecialidadDTO getSubEspecialidad()
    {
        return subEspecialidad;
    }

    public void setSubEspecialidad(SubEspecialidadDTO subEspecialidad)
    {
        this.subEspecialidad = subEspecialidad;
    }

    public TipoPreguntaEnum getTipoPreguntaEnum()
    {
        return tipoPreguntaEnum;
    }

    public void setTipoPreguntaEnum(TipoPreguntaEnum tipoPreguntaEnum)
    {
        this.tipoPreguntaEnum = tipoPreguntaEnum;
    }

    public TipoRespuestaPreguntaEnum getTipoRespuestaPreguntaEnum()
    {
        return tipoRespuestaPreguntaEnum;
    }

    public void setTipoRespuestaPreguntaEnum(TipoRespuestaPreguntaEnum tipoRespuestaPreguntaEnum)
    {
        this.tipoRespuestaPreguntaEnum = tipoRespuestaPreguntaEnum;
    }

    public QuienPreguntaEnum getQuienPreguntaEnum()
    {
        return quienPreguntaEnum;
    }

    public void setQuienPreguntaEnum(QuienPreguntaEnum quienPreguntaEnum)
    {
        this.quienPreguntaEnum = quienPreguntaEnum;
    }

    public EstadoPreguntaEnum getEstadoPreguntaEnum()
    {
        return estadoPreguntaEnum;
    }

    public void setEstadoPreguntaEnum(EstadoPreguntaEnum estadoPreguntaEnum)
    {
        this.estadoPreguntaEnum = estadoPreguntaEnum;
    }

    public void addAlternativa(AlternativaDTO alternativa)
    {
        alternativa.setPregunta(this);
        conjuntoAlternativas.add(alternativa);
        listaAlternativas.add(alternativa);
    }

    public void deleteAlternativa(AlternativaDTO alternativa)
    {
        if(!contieneAlternativa(alternativa))
        {
            return;
        } else
        {
            conjuntoAlternativas.remove(alternativa);
            return;
        }
    }

    public void addAlternativas(Set alternativas)
    {
        AlternativaDTO entrada;
        for(Iterator i$ = alternativas.iterator(); i$.hasNext(); addAlternativa(entrada))
            entrada = (AlternativaDTO)i$.next();

    }

    public boolean contieneAlternativa(AlternativaDTO alternativa)
    {
        ArrayList conjunto = new ArrayList();
        conjunto.addAll(conjuntoAlternativas);
        for(int i = 0; i < conjunto.size(); i++)
        {
            AlternativaDTO dto = (AlternativaDTO)conjunto.get(i);
            if(dto.getDescripcionAlternativa().equalsIgnoreCase(alternativa.getDescripcionAlternativa()))
                return true;
        }

        return false;
    }

    public Object getCantidadAlternativas()
    {
        int t = conjuntoAlternativas.size();
        if(t > 0)
            return Integer.valueOf(t);
        else
            return "No posee alternativas";
    }

    public String getNombreEspecialidad()
    {
        if(especialidad != null)
            return especialidad.getNombreEspecialidad();
        else
            return "Pregunta General";
    }

    public String getNombreSubEspecialidad()
    {
        if(subEspecialidad != null)
            return subEspecialidad.getNombreSubEspecialidad();
        else
            return "-";
    }

    public String getNombreProcedimiento()
    {
        if(procedimiento != null)
            return procedimiento.getNombreProcedimiento();
        else
            return "-";
    }

    public boolean isTieneAlternativas()
    {
        return !conjuntoAlternativas.isEmpty();
    }

    public void setTieneAlternativas(boolean tieneAlternativas)
    {
        this.tieneAlternativas = tieneAlternativas;
    }

    public List getListaAlternativas()
    {
        return listaAlternativas;
    }

    public void setListaAlternativas(List listaAlternativas)
    {
        this.listaAlternativas = listaAlternativas;
    }

    public List getRespuestasSeleccionadas()
    {
        return respuestasSeleccionadas;
    }

    public void setRespuestasSeleccionadas(List RespuestasSeleccionadas)
    {
        respuestasSeleccionadas = RespuestasSeleccionadas;
    }

    public String getRespuestaOTRO()
    {
        return respuestaOTRO;
    }

    public void setRespuestaOTRO(String respuestaOTRO)
    {
        this.respuestaOTRO = respuestaOTRO;
    }

    private Integer idPregunta;
    private String descripcionPregunta;
    private String tipoPregunta;
    private String tipoRespuesta;
    private String quienPregunta;
    private String estadoPregunta;
    private EspecialidadDTO especialidad;
    private SubEspecialidadDTO subEspecialidad;
    private ProcedimientoDTO procedimiento;
    private Set conjuntoAlternativas;
    private List respuestasSeleccionadas;
    private String respuestaOTRO;
    private List listaAlternativas;
    private TipoPreguntaEnum tipoPreguntaEnum;
    private TipoRespuestaPreguntaEnum tipoRespuestaPreguntaEnum;
    private QuienPreguntaEnum quienPreguntaEnum;
    private EstadoPreguntaEnum estadoPreguntaEnum;
    private boolean tieneAlternativas;
}
