// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:15 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   GrupoEnfermedadDTO.java

package infraestructura.dto;

import java.io.Serializable;

public class GrupoEnfermedadDTO
    implements Serializable
{

    public GrupoEnfermedadDTO()
    {
    }

    public String getIdCapitulo()
    {
        return idCapitulo;
    }

    public void setIdCapitulo(String idCapitulo)
    {
        this.idCapitulo = idCapitulo;
    }

    public String getNombreCapitulo()
    {
        return nombreCapitulo;
    }

    public void setNombreCapitulo(String nombreCapitulo)
    {
        this.nombreCapitulo = nombreCapitulo;
    }

    public String getIdGrupo()
    {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo)
    {
        this.idGrupo = idGrupo;
    }

    public String getNombreGrupo()
    {
        return nombreGrupo;
    }

    public void setNombreGrupo(String nombreGrupo)
    {
        this.nombreGrupo = nombreGrupo;
    }

    public String getIdSubGrupo()
    {
        return idSubGrupo;
    }

    public void setIdSubGrupo(String idSubGrupo)
    {
        this.idSubGrupo = idSubGrupo;
    }

    public String getNombreSubGrupo()
    {
        return nombreSubGrupo;
    }

    public void setNombreSubGrupo(String nombreSubGrupo)
    {
        this.nombreSubGrupo = nombreSubGrupo;
    }

    public String getIdEnfermedad()
    {
        return idEnfermedad;
    }

    public void setIdEnfermedad(String idEnfermedad)
    {
        this.idEnfermedad = idEnfermedad;
    }

    public String getNombreEnfermedad()
    {
        return nombreEnfermedad;
    }

    public void setNombreEnfermedad(String nombreEnfermedad)
    {
        this.nombreEnfermedad = nombreEnfermedad;
    }

    private String idCapitulo;
    private String nombreCapitulo;
    private String idGrupo;
    private String nombreGrupo;
    private String idSubGrupo;
    private String nombreSubGrupo;
    private String idEnfermedad;
    private String nombreEnfermedad;
}
