// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:07 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoDTO.java

package infraestructura.dto;

import infraestructura.enums.EstadosEnum;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            SubEspecialidadDTO, EspecialidadDTO, PreguntaDTO

public class ProcedimientoDTO
    implements Serializable
{

    public ProcedimientoDTO()
    {
        conjuntoPreguntas = new HashSet();
    }

    public Integer getIdProcedimiento()
    {
        return idProcedimiento;
    }

    public void setIdProcedimiento(Integer idProcedimiento)
    {
        this.idProcedimiento = idProcedimiento;
    }

    public String getNombreProcedimiento()
    {
        return nombreProcedimiento;
    }

    public void setNombreProcedimiento(String nombreProcedimiento)
    {
        this.nombreProcedimiento = nombreProcedimiento;
    }

    public SubEspecialidadDTO getSubEspecialidad()
    {
        return subEspecialidad;
    }

    public void setSubEspecialidad(SubEspecialidadDTO subEspecialidad)
    {
        if(subEspecialidad != null && StringUtils.isNotBlank(subEspecialidad.getNombreSubEspecialidad()))
            setEspecialidadRelacionada(subEspecialidad.getEspecialidad().getNombreEspecialidad());
        this.subEspecialidad = subEspecialidad;
    }

    public String getEstadoProcedimiento()
    {
        return estadoProcedimiento;
    }

    public void setEstadoProcedimiento(String estadoProcedimiento)
    {
        if(StringUtils.isNotBlank(estadoProcedimiento))
            setEstadoEnum(EstadosEnum.findByCode(estadoProcedimiento));
        this.estadoProcedimiento = estadoProcedimiento;
    }

    public Set getConjuntoPreguntas()
    {
        return conjuntoPreguntas;
    }

    public void setConjuntoPreguntas(Set conjuntoPreguntas)
    {
        this.conjuntoPreguntas = conjuntoPreguntas;
    }

    public EstadosEnum getEstadoEnum()
    {
        return estadoEnum;
    }

    public void setEstadoEnum(EstadosEnum estadoEnum)
    {
        this.estadoEnum = estadoEnum;
    }

    public String getEspecialidadRelacionada()
    {
        return especialidadRelacionada;
    }

    public void setEspecialidadRelacionada(String especialidadRelacionada)
    {
        this.especialidadRelacionada = especialidadRelacionada;
    }

    public void agregarPregunta(PreguntaDTO pregunta)
    {
        pregunta.setProcedimiento(this);
        conjuntoPreguntas.add(pregunta);
    }

    private Integer idProcedimiento;
    private String nombreProcedimiento;
    private SubEspecialidadDTO subEspecialidad;
    private String especialidadRelacionada;
    private String estadoProcedimiento;
    private Set conjuntoPreguntas;
    private EstadosEnum estadoEnum;
}
