// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:58 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TrabajoDTO.java

package infraestructura.dto;

import infraestructura.enums.EstadoTrabajoEnum;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            AtencionDTO, EspecialidadDTO, EmpleadoDTO

public class TrabajoDTO
{

    public TrabajoDTO()
    {
        conjuntoAtenciones = new HashSet();
        ponerleIdNulo = "NO";
        NULO_SI = "SI";
    }

    public Integer getIdTrabajo()
    {
        return idTrabajo;
    }

    public Integer getEspecialidadId()
    {
        return especialidadId;
    }

    public void setEspecialidadId(Integer especialidadId)
    {
        this.especialidadId = especialidadId;
    }

    public Integer getEmpleadoId()
    {
        return empleadoId;
    }

    public void setEmpleadoId(Integer empleadoId)
    {
        this.empleadoId = empleadoId;
    }

    public void setIdTrabajo(Integer idTrabajo)
    {
        this.idTrabajo = idTrabajo;
    }

    public EspecialidadDTO getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(EspecialidadDTO especialidad)
    {
        this.especialidad = especialidad;
    }

    public EmpleadoDTO getEmpleado()
    {
        return empleado;
    }

    public void setEmpleado(EmpleadoDTO empleado)
    {
        this.empleado = empleado;
    }

    public Set getConjuntoAtenciones()
    {
        return conjuntoAtenciones;
    }

    public void setConjuntoAtenciones(Set conjuntoAtenciones)
    {
        this.conjuntoAtenciones = conjuntoAtenciones;
    }

    public void agregarAtencion(AtencionDTO atencion)
    {
        atencion.setTrabajo(this);
        conjuntoAtenciones.add(atencion);
    }

    public String getEstadoTrabajo()
    {
        return estadoTrabajo;
    }

    public void setEstadoTrabajo(String estadoTrabajo)
    {
        if(StringUtils.isNotBlank(estadoTrabajo))
            setEstadoTrabajoEnum(EstadoTrabajoEnum.findByCode(estadoTrabajo));
        this.estadoTrabajo = estadoTrabajo;
    }

    public EstadoTrabajoEnum getEstadoTrabajoEnum()
    {
        return estadoTrabajoEnum;
    }

    public void setEstadoTrabajoEnum(EstadoTrabajoEnum estadoTrabajoEnum)
    {
        this.estadoTrabajoEnum = estadoTrabajoEnum;
    }

    public String getPonerleIdNulo()
    {
        return ponerleIdNulo;
    }

    public void setPonerleIdNulo(String ponerleIdNulo)
    {
        this.ponerleIdNulo = ponerleIdNulo;
    }

    private Integer idTrabajo;
    private EspecialidadDTO especialidad;
    private EmpleadoDTO empleado;
    private Set conjuntoAtenciones;
    private Integer especialidadId;
    private Integer empleadoId;
    private String estadoTrabajo;
    private EstadoTrabajoEnum estadoTrabajoEnum;
    private String ponerleIdNulo;
    public String NULO_SI;
}
