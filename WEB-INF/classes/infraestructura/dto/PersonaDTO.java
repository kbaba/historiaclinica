// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:54 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PersonaDTO.java

package infraestructura.dto;

import infraestructura.enums.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            PacienteDTO, EmpleadoDTO, PaisDTO, DepartamentoDTO, 
//            ProvinciaDTO, DistritoDTO, RolDTO

public class PersonaDTO
    implements Serializable
{

    public PersonaDTO()
    {
    }

    public String getApenom()
    {
        apenom = (new StringBuilder()).append(apellidoPaternoPersona).append(" ").append(apellidoMaternoPersona).append(", ").append(nombrePersona).toString();
        return apenom;
    }

    public Integer getIdPersona()
    {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona)
    {
        this.idPersona = idPersona;
    }

    public String getTipoDocumentoPersona()
    {
        return tipoDocumentoPersona;
    }

    public void setTipoDocumentoPersona(String tipoDocumentoPersona)
    {
        this.tipoDocumentoPersona = tipoDocumentoPersona;
    }

    public String getDocumentoPersona()
    {
        return documentoPersona;
    }

    public void setDocumentoPersona(String documentoPersona)
    {
        this.documentoPersona = documentoPersona;
    }

    public String getNombrePersona()
    {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona)
    {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPaternoPersona()
    {
        return apellidoPaternoPersona;
    }

    public void setApellidoPaternoPersona(String apellidoPaternoPersona)
    {
        this.apellidoPaternoPersona = apellidoPaternoPersona;
    }

    public String getApellidoMaternoPersona()
    {
        return apellidoMaternoPersona;
    }

    public void setApellidoMaternoPersona(String apellidoMaternoPersona)
    {
        this.apellidoMaternoPersona = apellidoMaternoPersona;
    }

    public Date getFechaNacimientoPersona()
    {
        return fechaNacimientoPersona;
    }

    public void setFechaNacimientoPersona(Date fechaNacimientoPersona)
    {
        this.fechaNacimientoPersona = fechaNacimientoPersona;
    }

    public String getTelefonoFijoPersona()
    {
        return telefonoFijoPersona;
    }

    public void setTelefonoFijoPersona(String telefonoFijoPersona)
    {
        this.telefonoFijoPersona = telefonoFijoPersona;
    }

    public String getTelefonoCelularPersona()
    {
        return telefonoCelularPersona;
    }

    public void setTelefonoCelularPersona(String telefonoCelularPersona)
    {
        this.telefonoCelularPersona = telefonoCelularPersona;
    }

    public String getEmailPersona()
    {
        return emailPersona;
    }

    public void setEmailPersona(String emailPersona)
    {
        this.emailPersona = emailPersona;
    }

    public String getDireccionPersona()
    {
        return direccionPersona;
    }

    public void setDireccionPersona(String direccionPersona)
    {
        this.direccionPersona = direccionPersona;
    }

    public String getIdResidenciaDistrito()
    {
        return idResidenciaDistrito;
    }

    public void setIdResidenciaDistrito(String idResidenciaDistrito)
    {
        this.idResidenciaDistrito = idResidenciaDistrito;
    }

    public String getStringTipoResidenciaPersona()
    {
        return stringTipoResidenciaPersona;
    }

    public void setStringTipoResidenciaPersona(String stringTipoResidenciaPersona)
    {
        this.stringTipoResidenciaPersona = stringTipoResidenciaPersona;
    }

    public String getStringPertenenciaViviendaPersona()
    {
        return stringPertenenciaViviendaPersona;
    }

    public void setStringPertenenciaViviendaPersona(String stringPertenenciaViviendaPersona)
    {
        this.stringPertenenciaViviendaPersona = stringPertenenciaViviendaPersona;
    }

    public String getStringMaterialViviendaPersona()
    {
        return stringMaterialViviendaPersona;
    }

    public void setStringMaterialViviendaPersona(String stringMaterialViviendaPersona)
    {
        this.stringMaterialViviendaPersona = stringMaterialViviendaPersona;
    }

    public String getStringEstatutoSocialPersona()
    {
        return stringEstatutoSocialPersona;
    }

    public void setStringEstatutoSocialPersona(String stringEstatutoSocialPersona)
    {
        this.stringEstatutoSocialPersona = stringEstatutoSocialPersona;
    }

    public PacienteDTO getPaciente()
    {
        return paciente;
    }

    public void setPaciente(PacienteDTO paciente)
    {
        if(paciente != null)
            paciente.setPersona(this);
        this.paciente = paciente;
    }

    public EmpleadoDTO getEmpleado()
    {
        return empleado;
    }

    public void setEmpleado(EmpleadoDTO empleado)
    {
        if(empleado != null)
            empleado.setPersona(this);
        this.empleado = empleado;
    }

    public String getIdPaisNacionalidad()
    {
        return idPaisNacionalidad;
    }

    public void setIdPaisNacionalidad(String idPaisNacionalidad)
    {
        this.idPaisNacionalidad = idPaisNacionalidad;
    }

    public int getEdad()
    {
        return calculaEdad();
    }

    public void setEdad(int edad)
    {
        this.edad = edad;
    }

    private int calculaEdad()
    {
        int years = 0;
        Calendar actual = Calendar.getInstance();
        years = (1900 + actual.getTime().getYear()) - (1900 + fechaNacimientoPersona.getYear());
        return calculaEdadRefinado(years, fechaNacimientoPersona.getMonth(), fechaNacimientoPersona.getDate());
    }

    public String getFechaNac()
    {
        String aF1o = "";
        String mes = "";
        String dia = "";
        aF1o = (new StringBuilder()).append(1900 + fechaNacimientoPersona.getYear()).append("").toString();
        if((new StringBuilder()).append("").append(fechaNacimientoPersona.getMonth() + 1).toString().length() == 1)
            mes = (new StringBuilder()).append("0").append(fechaNacimientoPersona.getMonth() + 1).toString();
        else
            mes = (new StringBuilder()).append(fechaNacimientoPersona.getMonth() + 1).append("").toString();
        if((new StringBuilder()).append("").append(fechaNacimientoPersona.getDate()).toString().length() == 1)
            dia = (new StringBuilder()).append("0").append(fechaNacimientoPersona.getDate()).toString();
        else
            dia = (new StringBuilder()).append(fechaNacimientoPersona.getDate()).append("").toString();
        fechaNac = (new StringBuilder()).append(aF1o).append("/").append(mes).append("/").append(dia).toString();
        return fechaNac;
    }

    public void setFechaNac(String fechaNac)
    {
        this.fechaNac = fechaNac;
    }

    private int calculaEdadRefinado(int diferencia, int month, int day)
    {
        int resultado = diferencia;
        Calendar calendar1 = Calendar.getInstance();
        int dia_actual = calendar1.getTime().getDate();
        int mes_actual = calendar1.getTime().getMonth();
        if(mes_actual < month)
            return resultado - 1;
        if(mes_actual < month)
            return resultado;
        if(month == mes_actual && dia_actual < day)
            return resultado - 1;
        else
            return resultado;
    }

    public DepartamentoDTO getDepartamentoPersona()
    {
        return departamentoPersona;
    }

    public void setDepartamentoPersona(DepartamentoDTO departamentoPersona)
    {
        this.departamentoPersona = departamentoPersona;
    }

    public DistritoDTO getDistritoPersona()
    {
        return distritoPersona;
    }

    public void setDistritoPersona(DistritoDTO distritoPersona)
    {
        this.distritoPersona = distritoPersona;
    }

    public PaisDTO getPaisPersona()
    {
        return paisPersona;
    }

    public void setPaisPersona(PaisDTO paisPersona)
    {
        this.paisPersona = paisPersona;
    }

    public ProvinciaDTO getProvinciaPersona()
    {
        return provinciaPersona;
    }

    public void setProvinciaPersona(ProvinciaDTO provinciaPersona)
    {
        this.provinciaPersona = provinciaPersona;
    }

    public Integer getCantidadPersonasViviendaPersona()
    {
        return cantidadPersonasViviendaPersona;
    }

    public void setCantidadPersonasViviendaPersona(Integer cantidadPersonasViviendaPersona)
    {
        this.cantidadPersonasViviendaPersona = cantidadPersonasViviendaPersona;
    }

    public Integer getDormitoriosViviendaPersona()
    {
        return dormitoriosViviendaPersona;
    }

    public void setDormitoriosViviendaPersona(Integer dormitoriosViviendaPersona)
    {
        this.dormitoriosViviendaPersona = dormitoriosViviendaPersona;
    }

    public String getEstatutoSocialPersona()
    {
        return estatutoSocialPersona;
    }

    public void setEstatutoSocialPersona(String estatutoSocialPersona)
    {
        if(StringUtils.isNotBlank(estatutoSocialPersona))
            setEstatutoSocialPersonaEnum(EstatutoSocialPersonaEnum.findByCode(estatutoSocialPersona));
        this.estatutoSocialPersona = estatutoSocialPersona;
    }

    public String getMaterialViviendaPersona()
    {
        return materialViviendaPersona;
    }

    public void setMaterialViviendaPersona(String materialViviendaPersona)
    {
        if(StringUtils.isNotBlank(materialViviendaPersona))
            setMaterialViviendaPersonaEnum(MaterialViviendaPersonaEnum.findByCode(materialViviendaPersona));
        this.materialViviendaPersona = materialViviendaPersona;
    }

    public String getPertenenciaViviendaPersona()
    {
        return pertenenciaViviendaPersona;
    }

    public void setPertenenciaViviendaPersona(String pertenenciaViviendaPersona)
    {
        if(StringUtils.isNotBlank(pertenenciaViviendaPersona))
            setPertenenciaViviendaPersonaPersonaEnum(PertenenciaViviendaPersonaEnum.findByCode(pertenenciaViviendaPersona));
        this.pertenenciaViviendaPersona = pertenenciaViviendaPersona;
    }

    public String getTipoResidenciaPersona()
    {
        return tipoResidenciaPersona;
    }

    public void setTipoResidenciaPersona(String tipoResidenciaPersona)
    {
        if(StringUtils.isNotBlank(tipoResidenciaPersona))
            setTipoResidenciaPersonaEnum(TipoResidenciaPersonaEnum.findByCode(tipoResidenciaPersona));
        this.tipoResidenciaPersona = tipoResidenciaPersona;
    }

    public TipoResidenciaPersonaEnum getTipoResidenciaPersonaEnum()
    {
        return tipoResidenciaPersonaEnum;
    }

    public void setTipoResidenciaPersonaEnum(TipoResidenciaPersonaEnum tipoResidenciaPersonaEnum)
    {
        this.tipoResidenciaPersonaEnum = tipoResidenciaPersonaEnum;
    }

    public PertenenciaViviendaPersonaEnum getPertenenciaViviendaPersonaPersonaEnum()
    {
        return pertenenciaViviendaPersonaPersonaEnum;
    }

    public void setPertenenciaViviendaPersonaPersonaEnum(PertenenciaViviendaPersonaEnum pertenenciaViviendaPersonaPersonaEnum)
    {
        this.pertenenciaViviendaPersonaPersonaEnum = pertenenciaViviendaPersonaPersonaEnum;
    }

    public MaterialViviendaPersonaEnum getMaterialViviendaPersonaEnum()
    {
        return materialViviendaPersonaEnum;
    }

    public void setMaterialViviendaPersonaEnum(MaterialViviendaPersonaEnum materialViviendaPersonaEnum)
    {
        this.materialViviendaPersonaEnum = materialViviendaPersonaEnum;
    }

    public EstatutoSocialPersonaEnum getEstatutoSocialPersonaEnum()
    {
        return estatutoSocialPersonaEnum;
    }

    public void setEstatutoSocialPersonaEnum(EstatutoSocialPersonaEnum estatutoSocialPersonaEnum)
    {
        this.estatutoSocialPersonaEnum = estatutoSocialPersonaEnum;
    }

    public String getSexoPersona()
    {
        return sexoPersona;
    }

    public void setSexoPersona(String sexoPersona)
    {
        if(StringUtils.isNotBlank(sexoPersona))
            setSexoPersonaEnum(SexoPersonaEnum.findByCode(sexoPersona));
        this.sexoPersona = sexoPersona;
    }

    public SexoPersonaEnum getSexoPersonaEnum()
    {
        return sexoPersonaEnum;
    }

    public void setSexoPersonaEnum(SexoPersonaEnum sexoPersonaEnum)
    {
        this.sexoPersonaEnum = sexoPersonaEnum;
    }

    public RolDTO getRol()
    {
        return rol;
    }

    public void setRol(RolDTO rol)
    {
        this.rol = rol;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    private Integer idPersona;
    private String tipoDocumentoPersona;
    private String documentoPersona;
    private String idPaisNacionalidad;
    private String nombrePersona;
    private String apellidoPaternoPersona;
    private String apellidoMaternoPersona;
    private Date fechaNacimientoPersona;
    private String telefonoFijoPersona;
    private String telefonoCelularPersona;
    private String emailPersona;
    private String direccionPersona;
    private String idResidenciaDistrito;
    private PacienteDTO paciente;
    private EmpleadoDTO empleado;
    private String tipoResidenciaPersona;
    private Integer dormitoriosViviendaPersona;
    private Integer cantidadPersonasViviendaPersona;
    private String pertenenciaViviendaPersona;
    private String materialViviendaPersona;
    private String estatutoSocialPersona;
    private String stringTipoResidenciaPersona;
    private String stringPertenenciaViviendaPersona;
    private String stringMaterialViviendaPersona;
    private String stringEstatutoSocialPersona;
    private int edad;
    private String fechaNac;
    private PaisDTO paisPersona;
    private DepartamentoDTO departamentoPersona;
    private ProvinciaDTO provinciaPersona;
    private DistritoDTO distritoPersona;
    private TipoResidenciaPersonaEnum tipoResidenciaPersonaEnum;
    private PertenenciaViviendaPersonaEnum pertenenciaViviendaPersonaPersonaEnum;
    private MaterialViviendaPersonaEnum materialViviendaPersonaEnum;
    private EstatutoSocialPersonaEnum estatutoSocialPersonaEnum;
    private String sexoPersona;
    private SexoPersonaEnum sexoPersonaEnum;
    private RolDTO rol;
    private String usuario;
    private String clave;
    private String apenom;
}
