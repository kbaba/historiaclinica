// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:26:54 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpleadoDTO.java

package infraestructura.dto;

import infraestructura.enums.EstadoEmpleadoEnum;
import infraestructura.enums.QuienPreguntaEnum;
import java.io.Serializable;
import java.util.*;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            TrabajoDTO, PersonaDTO

public class EmpleadoDTO
    implements Serializable
{

    public EmpleadoDTO()
    {
        conjuntoTrabajos = new HashSet();
    }

    public Integer getIdEmpleado()
    {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado)
    {
        this.idEmpleado = idEmpleado;
    }

    public String getTipoEmpleado()
    {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(String tipoEmpleado)
    {
        if(StringUtils.isNotBlank(tipoEmpleado))
            setQuienPreguntaEnum(QuienPreguntaEnum.findByCode(tipoEmpleado));
        this.tipoEmpleado = tipoEmpleado;
    }

    public String getEstadoEmpleado()
    {
        return estadoEmpleado;
    }

    public void setEstadoEmpleado(String estadoEmpleado)
    {
        if(StringUtils.isNotBlank(estadoEmpleado))
            setEstadoEmpleadoEnum(EstadoEmpleadoEnum.findByCode(estadoEmpleado));
        this.estadoEmpleado = estadoEmpleado;
    }

    public Set getConjuntoTrabajos()
    {
        return conjuntoTrabajos;
    }

    public void setConjuntoTrabajos(Set conjuntoTrabajos)
    {
        this.conjuntoTrabajos = conjuntoTrabajos;
    }

    public PersonaDTO getPersona()
    {
        return persona;
    }

    public void setPersona(PersonaDTO persona)
    {
        this.persona = persona;
    }

    public void agregarTrabajo(TrabajoDTO trabajo)
    {
        if(trabajo != null)
            trabajo.setEmpleado(this);
        conjuntoTrabajos.add(trabajo);
    }

    public void vaciaAgregaTrabajo(TrabajoDTO trabajo)
    {
        conjuntoTrabajos.clear();
        agregarTrabajo(trabajo);
    }

    public void agregarTrabajos(List trabajo)
    {
        TrabajoDTO trab;
        for(Iterator i$ = trabajo.iterator(); i$.hasNext(); agregarTrabajo(trab))
            trab = (TrabajoDTO)i$.next();

    }

    public EstadoEmpleadoEnum getEstadoEmpleadoEnum()
    {
        return estadoEmpleadoEnum;
    }

    public void setEstadoEmpleadoEnum(EstadoEmpleadoEnum estadoEmpleadoEnum)
    {
        this.estadoEmpleadoEnum = estadoEmpleadoEnum;
    }

    public QuienPreguntaEnum getQuienPreguntaEnum()
    {
        return quienPreguntaEnum;
    }

    public void setQuienPreguntaEnum(QuienPreguntaEnum quienPreguntaEnum)
    {
        this.quienPreguntaEnum = quienPreguntaEnum;
    }

    private Integer idEmpleado;
    private PersonaDTO persona;
    private String tipoEmpleado;
    private String estadoEmpleado;
    private EstadoEmpleadoEnum estadoEmpleadoEnum;
    private QuienPreguntaEnum quienPreguntaEnum;
    private Set conjuntoTrabajos;
}
