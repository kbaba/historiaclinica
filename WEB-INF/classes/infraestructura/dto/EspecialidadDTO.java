// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:04 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EspecialidadDTO.java

package infraestructura.dto;

import infraestructura.enums.EstadosEnum;
import java.io.Serializable;
import java.util.*;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            TrabajoDTO, SubEspecialidadDTO, PreguntaDTO

public class EspecialidadDTO
    implements Serializable
{

    public EspecialidadDTO(String nombreEspecialidad, String estadoEspecialidad)
    {
        conjuntoSubEspecialidades = new HashSet();
        conjuntoPreguntas = new HashSet();
        conjuntoTrabajos = new HashSet();
        this.nombreEspecialidad = nombreEspecialidad;
        this.estadoEspecialidad = estadoEspecialidad;
    }

    public EspecialidadDTO()
    {
        conjuntoSubEspecialidades = new HashSet();
        conjuntoPreguntas = new HashSet();
        conjuntoTrabajos = new HashSet();
    }

    public Integer getIdEspecialidad()
    {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad)
    {
        this.idEspecialidad = idEspecialidad;
    }

    public String getNombreEspecialidad()
    {
        return nombreEspecialidad;
    }

    public void setNombreEspecialidad(String nombreEspecialidad)
    {
        this.nombreEspecialidad = nombreEspecialidad;
    }

    public String getEstadoEspecialidad()
    {
        return estadoEspecialidad;
    }

    public void setEstadoEspecialidad(String estadoEspecialidad)
    {
        if(StringUtils.isNotBlank(estadoEspecialidad))
            setEstadoEnum(EstadosEnum.findByCode(estadoEspecialidad));
        this.estadoEspecialidad = estadoEspecialidad;
    }

    public Set getConjuntoSubEspecialidades()
    {
        return conjuntoSubEspecialidades;
    }

    public void setConjuntoSubEspecialidades(Set conjuntoSubEspecialidades)
    {
        this.conjuntoSubEspecialidades = conjuntoSubEspecialidades;
    }

    public String getNumeroDeSubEspecialidades()
    {
        return numeroDeSubEspecialidades;
    }

    public void setNumeroDeSubEspecialidades(String numeroDeEspecialidades)
    {
        numeroDeSubEspecialidades = numeroDeEspecialidades.equals("0") ? "No posee subespecialidades" : numeroDeEspecialidades.equals("1") ? (new StringBuilder()).append(numeroDeEspecialidades).append(" subespecialidad").toString() : (new StringBuilder()).append(numeroDeEspecialidades).append(" subespecialidades ").toString();
    }

    public String getNumeroDeProcedimientos()
    {
        return numeroDeProcedimientos;
    }

    public void setNumeroDeProcedimientos(String numeroDeProcedimientos)
    {
        this.numeroDeProcedimientos = numeroDeProcedimientos.equals("0") ? "No posee procedimientos" : numeroDeProcedimientos.equals("1") ? (new StringBuilder()).append(numeroDeProcedimientos).append(" procedimiento").toString() : (new StringBuilder()).append(numeroDeProcedimientos).append(" procedimientos ").toString();
    }

    public Set getConjuntoPreguntas()
    {
        return conjuntoPreguntas;
    }

    public void setConjuntoPreguntas(Set conjuntoPreguntas)
    {
        this.conjuntoPreguntas = conjuntoPreguntas;
    }

    public EstadosEnum getEstadoEnum()
    {
        return estadoEnum;
    }

    public void setEstadoEnum(EstadosEnum estadoEnum)
    {
        this.estadoEnum = estadoEnum;
    }

    public void agregarSubEspecialidad(SubEspecialidadDTO subEspecialidad)
    {
        subEspecialidad.setEspecialidad(this);
        conjuntoSubEspecialidades.add(subEspecialidad);
    }

    public void agregarPregunta(PreguntaDTO pregunta)
    {
        pregunta.setEspecialidad(this);
        conjuntoPreguntas.add(pregunta);
    }

    public Set getConjuntoTrabajos()
    {
        return conjuntoTrabajos;
    }

    public void setConjuntoTrabajos(Set conjuntoTrabajos)
    {
        this.conjuntoTrabajos = conjuntoTrabajos;
    }

    public void addTrabajo(TrabajoDTO trabajo)
    {
        trabajo.setEspecialidad(this);
        conjuntoTrabajos.add(trabajo);
    }

    public void addTrabajos(List trabajo)
    {
        TrabajoDTO trab;
        for(Iterator i$ = trabajo.iterator(); i$.hasNext(); addTrabajo(trab))
            trab = (TrabajoDTO)i$.next();

    }

    public void setLowerCaseDescripcion(String lowerCaseDescripcion)
    {
        this.lowerCaseDescripcion = lowerCaseDescripcion;
    }

    public String getLowerCaseDescripcion()
    {
        lowerCaseDescripcion = getNombreEspecialidad().toLowerCase().substring(0, 6);
        return lowerCaseDescripcion;
    }

    private Integer idEspecialidad;
    private String nombreEspecialidad;
    private String estadoEspecialidad;
    private Set conjuntoSubEspecialidades;
    private String numeroDeSubEspecialidades;
    private String numeroDeProcedimientos;
    private Set conjuntoPreguntas;
    private Set conjuntoTrabajos;
    EstadosEnum estadoEnum;
    private String lowerCaseDescripcion;
}
