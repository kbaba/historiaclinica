// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:39 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PacienteDTO.java

package infraestructura.dto;

import infraestructura.enums.EstadoPacienteEnum;
import java.io.Serializable;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            HistoriaDTO, PersonaDTO

public class PacienteDTO
    implements Serializable
{

    public PacienteDTO()
    {
    }

    public Integer getIdPaciente()
    {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente)
    {
        this.idPaciente = idPaciente;
    }

    public String getEstadoPaciente()
    {
        return estadoPaciente;
    }

    public String getEstadoString()
    {
        return estadoString;
    }

    public void setEstadoString(String estadoString)
    {
        this.estadoString = estadoString;
    }

    public void setEstadoPaciente(String estadoPaciente)
    {
        if(StringUtils.isNotBlank(estadoPaciente))
            setEstadoPacienteEnum(EstadoPacienteEnum.findByCode(estadoPaciente));
        this.estadoPaciente = estadoPaciente;
    }

    public PersonaDTO getPersona()
    {
        return persona;
    }

    public void setPersona(PersonaDTO persona)
    {
        this.persona = persona;
    }

    public HistoriaDTO getHistoria()
    {
        return historia;
    }

    public void setHistoria(HistoriaDTO historia)
    {
        if(historia != null)
            historia.setPaciente(this);
        this.historia = historia;
    }

    public EstadoPacienteEnum getEstadoPacienteEnum()
    {
        return estadoPacienteEnum;
    }

    public void setEstadoPacienteEnum(EstadoPacienteEnum estadoPacienteEnum)
    {
        this.estadoPacienteEnum = estadoPacienteEnum;
    }

    private Integer idPaciente;
    private PersonaDTO persona;
    private String estadoPaciente;
    private String estadoString;
    private HistoriaDTO historia;
    private EstadoPacienteEnum estadoPacienteEnum;
}
