// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:18 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   RespuestaDTO.java

package infraestructura.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package infraestructura.dto:
//            AtencionDTO, EspecialidadDTO, SubEspecialidadDTO, ProcedimientoDTO

public class RespuestaDTO
    implements Serializable
{

    public RespuestaDTO()
    {
        listaRespuestas = new ArrayList();
    }

    public String getEspecialidadDerivados()
    {
        return especialidadDerivados;
    }

    public void setEspecialidadDerivados(String especialidadDerivados)
    {
        this.especialidadDerivados = especialidadDerivados;
    }

    public List getListaRespuestas()
    {
        return listaRespuestas;
    }

    public void setListaRespuestas(List listaRespuestas)
    {
        this.listaRespuestas = listaRespuestas;
    }

    public Integer getIdRespuesta()
    {
        return idRespuesta;
    }

    public Integer getEspecialidadId()
    {
        return especialidadId;
    }

    public void setEspecialidadId(Integer especialidadId)
    {
        this.especialidadId = especialidadId;
    }

    public Integer getSubEspecialidadId()
    {
        return subEspecialidadId;
    }

    public void setSubEspecialidadId(Integer subEspecialidadId)
    {
        this.subEspecialidadId = subEspecialidadId;
    }

    public Integer getProcedimientoId()
    {
        return procedimientoId;
    }

    public void setProcedimientoId(Integer procedimientoId)
    {
        this.procedimientoId = procedimientoId;
    }

    public void setIdRespuesta(Integer idRespuesta)
    {
        this.idRespuesta = idRespuesta;
    }

    public String getDescripcionPregunta()
    {
        return descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta)
    {
        this.descripcionPregunta = descripcionPregunta;
    }

    public String getDetalleRespuesta()
    {
        return detalleRespuesta;
    }

    public void setDetalleRespuesta(String detalleRespuesta)
    {
        this.detalleRespuesta = detalleRespuesta;
    }

    public AtencionDTO getAtencion()
    {
        return atencion;
    }

    public void setAtencion(AtencionDTO atencion)
    {
        this.atencion = atencion;
    }

    public EspecialidadDTO getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(EspecialidadDTO especialidad)
    {
        this.especialidad = especialidad;
    }

    public SubEspecialidadDTO getSubEspecialidad()
    {
        return subEspecialidad;
    }

    public void setSubEspecialidad(SubEspecialidadDTO subEspecialidad)
    {
        this.subEspecialidad = subEspecialidad;
    }

    public ProcedimientoDTO getProcedimiento()
    {
        return procedimiento;
    }

    public void setProcedimiento(ProcedimientoDTO procedimiento)
    {
        this.procedimiento = procedimiento;
    }

    private Integer idRespuesta;
    private String descripcionPregunta;
    private String detalleRespuesta;
    private AtencionDTO atencion;
    private EspecialidadDTO especialidad;
    private SubEspecialidadDTO subEspecialidad;
    private ProcedimientoDTO procedimiento;
    private Integer especialidadId;
    private Integer subEspecialidadId;
    private Integer procedimientoId;
    List listaRespuestas;
    private String especialidadDerivados;
}
