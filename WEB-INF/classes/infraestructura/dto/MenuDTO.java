// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:26 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   MenuDTO.java

package infraestructura.dto;

import java.util.ArrayList;
import java.util.List;

public class MenuDTO
    implements Comparable
{

    public MenuDTO()
    {
        options = new ArrayList();
    }

    public Integer getIdMenu()
    {
        return idMenu;
    }

    public void setIdMenu(Integer idMenu)
    {
        this.idMenu = idMenu;
    }

    public String getNombreMenu()
    {
        return nombreMenu;
    }

    public void setNombreMenu(String nombreMenu)
    {
        this.nombreMenu = nombreMenu;
    }

    public Integer getPosicionMenu()
    {
        return posicionMenu;
    }

    public void setPosicionMenu(Integer posicionMenu)
    {
        this.posicionMenu = posicionMenu;
    }

    public List getOptions()
    {
        return options;
    }

    public void setOptions(List options)
    {
        this.options = options;
    }

    public int compareTo(MenuDTO o)
    {
        return posicionMenu.compareTo(o.getPosicionMenu());
    }

    public boolean equals(Object o)
    {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        MenuDTO menuDTO = (MenuDTO)o;
        return idMenu == null ? menuDTO.idMenu == null : idMenu.equals(menuDTO.idMenu);
    }

    public int hashCode()
    {
        return idMenu == null ? 0 : idMenu.hashCode();
    }

    public volatile int compareTo(Object x0)
    {
        return compareTo((MenuDTO)x0);
    }

    private Integer idMenu;
    private String nombreMenu;
    private Integer posicionMenu;
    private List options;
}
