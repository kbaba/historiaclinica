// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:26:49 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   DistritoDTO.java

package infraestructura.dto;


public class DistritoDTO
{

    public DistritoDTO()
    {
    }

    public String getDepartamentoId()
    {
        return departamentoId;
    }

    public void setDepartamentoId(String departamentoId)
    {
        this.departamentoId = departamentoId;
    }

    public String getIdDistrito()
    {
        return idDistrito;
    }

    public void setIdDistrito(String idDistrito)
    {
        this.idDistrito = idDistrito;
    }

    public String getNombreDistrito()
    {
        return nombreDistrito;
    }

    public void setNombreDistrito(String nombreDistrito)
    {
        this.nombreDistrito = nombreDistrito;
    }

    public String getPaisId()
    {
        return paisId;
    }

    public void setPaisId(String paisId)
    {
        this.paisId = paisId;
    }

    public String getProvinciaId()
    {
        return provinciaId;
    }

    public void setProvinciaId(String provinciaId)
    {
        this.provinciaId = provinciaId;
    }

    public String getIdCompuestoDistrito()
    {
        return (new StringBuilder()).append(paisId).append(",").append(departamentoId).append(",").append(provinciaId).append(",").append(idDistrito).toString();
    }

    public void setIdCompuestoDistrito(String idCompuestoDistrito)
    {
        this.idCompuestoDistrito = idCompuestoDistrito;
    }

    private String paisId;
    private String departamentoId;
    private String provinciaId;
    private String idDistrito;
    private String nombreDistrito;
    private String idCompuestoDistrito;
}
