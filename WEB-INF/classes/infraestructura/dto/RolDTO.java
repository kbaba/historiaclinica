// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:24 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   RolDTO.java

package infraestructura.dto;

import java.io.Serializable;
import java.util.*;

// Referenced classes of package infraestructura.dto:
//            OpcionDTO

public class RolDTO
    implements Serializable
{

    public RolDTO()
    {
        options = new HashSet();
    }

    public Integer getIdRol()
    {
        return idRol;
    }

    public void setIdRol(Integer idRol)
    {
        this.idRol = idRol;
    }

    public String getNombreRol()
    {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol)
    {
        this.nombreRol = nombreRol;
    }

    public Set getOptions()
    {
        return options;
    }

    public void setOptions(Set options)
    {
        this.options = options;
    }

    public void addOpciones(Set options)
    {
        OpcionDTO opcion;
        for(Iterator i$ = options.iterator(); i$.hasNext(); addOpcion(opcion))
            opcion = (OpcionDTO)i$.next();

    }

    public void addOpcion(OpcionDTO opcion)
    {
        options.add(opcion);
    }

    private Integer idRol;
    private String nombreRol;
    private Set options;
}
