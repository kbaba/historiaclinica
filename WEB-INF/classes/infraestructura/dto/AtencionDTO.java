// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:26:36 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AtencionDTO.java

package infraestructura.dto;

import java.io.Serializable;
import java.util.*;
import org.apache.commons.lang.StringUtils;

// Referenced classes of package infraestructura.dto:
//            RespuestaDTO, HistoriaDTO, TrabajoDTO, EnfermedadDTO

public class AtencionDTO
    implements Serializable
{

    public AtencionDTO()
    {
        conjuntoRespuestas = new HashSet();
        conjuntoEnfermedades = new HashSet();
        listaEnfermedades = new ArrayList();
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public List getListaEnfermedades()
    {
        return listaEnfermedades;
    }

    public void setListaEnfermedades(List listaEnfermedades)
    {
        this.listaEnfermedades = listaEnfermedades;
    }

    public boolean isTieneEnfermedades()
    {
        return tieneEnfermedades;
    }

    public void setTieneEnfermedades(boolean tieneEnfermedades)
    {
        this.tieneEnfermedades = tieneEnfermedades;
    }

    public String getFechaAtenFormato()
    {
        if(fechaAtencion == null)
            return "";
        String aF1o = "";
        String mes = "";
        String dia = "";
        aF1o = (new StringBuilder()).append(1900 + fechaAtencion.getYear()).append("").toString();
        if((new StringBuilder()).append("").append(fechaAtencion.getMonth() + 1).toString().length() == 1)
            mes = (new StringBuilder()).append("0").append(fechaAtencion.getMonth() + 1).toString();
        else
            mes = (new StringBuilder()).append(fechaAtencion.getMonth() + 1).append("").toString();
        if((new StringBuilder()).append("").append(fechaAtencion.getDate()).toString().length() == 1)
            dia = (new StringBuilder()).append("0").append(fechaAtencion.getDate()).toString();
        else
            dia = (new StringBuilder()).append(fechaAtencion.getDate()).append("").toString();
        return (new StringBuilder()).append(aF1o).append("/").append(mes).append("/").append(dia).toString();
    }

    public String getFechaSiguienteAtenFormato()
    {
        if(fechaAproxSiguienteAtencion == null)
            return "";
        String aF1o = "";
        String mes = "";
        String dia = "";
        aF1o = (new StringBuilder()).append(1900 + fechaAproxSiguienteAtencion.getYear()).append("").toString();
        if((new StringBuilder()).append("").append(fechaAproxSiguienteAtencion.getMonth() + 1).toString().length() == 1)
            mes = (new StringBuilder()).append("0").append(fechaAproxSiguienteAtencion.getMonth() + 1).toString();
        else
            mes = (new StringBuilder()).append(fechaAproxSiguienteAtencion.getMonth() + 1).append("").toString();
        if((new StringBuilder()).append("").append(fechaAproxSiguienteAtencion.getDate()).toString().length() == 1)
            dia = (new StringBuilder()).append("0").append(fechaAproxSiguienteAtencion.getDate()).toString();
        else
            dia = (new StringBuilder()).append(fechaAproxSiguienteAtencion.getDate()).append("").toString();
        return (new StringBuilder()).append(aF1o).append("/").append(mes).append("/").append(dia).toString();
    }

    public Integer getIdAtencion()
    {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion)
    {
        this.idAtencion = idAtencion;
    }

    public Date getFechaAtencion()
    {
        return fechaAtencion;
    }

    public void setFechaAtencion(Date fechaAtencion)
    {
        this.fechaAtencion = fechaAtencion;
    }

    public String getDiagnosticoAtencion()
    {
        return diagnosticoAtencion;
    }

    public void setDiagnosticoAtencion(String diagnosticoAtencion)
    {
        this.diagnosticoAtencion = diagnosticoAtencion;
    }

    public Date getFechaAproxSiguienteAtencion()
    {
        return fechaAproxSiguienteAtencion;
    }

    public void setFechaAproxSiguienteAtencion(Date fechaAproxSiguienteAtencion)
    {
        this.fechaAproxSiguienteAtencion = fechaAproxSiguienteAtencion;
    }

    public Set getConjuntoRespuestas()
    {
        return conjuntoRespuestas;
    }

    public void setConjuntoRespuestas(Set conjuntoRespuestas)
    {
        this.conjuntoRespuestas = conjuntoRespuestas;
    }

    public Set getConjuntoEnfermedades()
    {
        return conjuntoEnfermedades;
    }

    public void setConjuntoEnfermedades(Set conjuntoEnfermedades)
    {
        this.conjuntoEnfermedades = conjuntoEnfermedades;
    }

    public HistoriaDTO getHistoria()
    {
        return historia;
    }

    public void setHistoria(HistoriaDTO historia)
    {
        this.historia = historia;
    }

    public TrabajoDTO getTrabajo()
    {
        return trabajo;
    }

    public void setTrabajo(TrabajoDTO trabajo)
    {
        this.trabajo = trabajo;
    }

    public String getFechaAproxSiguienteAtencionString()
    {
        return fechaAproxSiguienteAtencionString;
    }

    public void setFechaAproxSiguienteAtencionString(String fechaAproxSiguienteAtencionString)
    {
        if(StringUtils.isNotBlank(diagnosticoAtencion))
            setFechaAtencion(new Date(fechaAproxSiguienteAtencionString));
        this.fechaAproxSiguienteAtencionString = fechaAproxSiguienteAtencionString;
    }

    public transient void agregarRespuestas(RespuestaDTO respuestas[])
    {
        RespuestaDTO arr$[] = respuestas;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            RespuestaDTO respuesta = arr$[i$];
            respuesta.setAtencion(this);
            conjuntoRespuestas.add(respuesta);
        }

    }

    public void agregarRespuestas(List respuestas)
    {
        RespuestaDTO respuesta;
        for(Iterator i$ = respuestas.iterator(); i$.hasNext(); conjuntoRespuestas.add(respuesta))
        {
            respuesta = (RespuestaDTO)i$.next();
            respuesta.setAtencion(this);
        }

    }

    public transient void agregarEnfermedades(EnfermedadDTO enfermedades[])
    {
        EnfermedadDTO arr$[] = enfermedades;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            EnfermedadDTO enfermedad = arr$[i$];
            conjuntoEnfermedades.add(enfermedad);
        }

    }

    private Integer idAtencion;
    private Date fechaAtencion;
    private String diagnosticoAtencion;
    private Date fechaAproxSiguienteAtencion;
    private String fechaAproxSiguienteAtencionString;
    private HistoriaDTO historia;
    private Set conjuntoRespuestas;
    private TrabajoDTO trabajo;
    public Set conjuntoEnfermedades;
    public List listaEnfermedades;
    private boolean tieneEnfermedades;
    private int index;
}
