// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:49 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   UsuarioDTO.java

package infraestructura.dto;

import infraestructura.enums.RolEnum;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package infraestructura.dto:
//            EmpleadoDTO

public class UsuarioDTO
{

    public UsuarioDTO()
    {
        menus = new ArrayList();
    }

    public Integer getIdUsuario()
    {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario)
    {
        this.idUsuario = idUsuario;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getApellido()
    {
        return apellido;
    }

    public void setApellido(String apellido)
    {
        this.apellido = apellido;
    }

    public String getNombreCompleto()
    {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto)
    {
        this.nombreCompleto = nombreCompleto;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    public Integer getCodigoRol()
    {
        return codigoRol;
    }

    public void setCodigoRol(Integer codigoRol)
    {
        if(codigoRol != null)
        {
            setEsAdministrador(RolEnum.ADMINISTRADOR.getCode().equals(codigoRol));
            setEsEnfermeria(RolEnum.TECNICO_ENFERMERIA.getCode().equals(codigoRol));
            setEsLaboratorista(RolEnum.LABORATORISTA.getCode().equals(codigoRol));
            setEsMedico(RolEnum.DOCTOR.getCode().equals(codigoRol));
        }
        this.codigoRol = codigoRol;
    }

    public String getNombreRol()
    {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol)
    {
        this.nombreRol = nombreRol;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public List getMenus()
    {
        return menus;
    }

    public void setMenus(List menus)
    {
        this.menus = menus;
    }

    public EmpleadoDTO getEmpleado()
    {
        return empleado;
    }

    public void setEmpleado(EmpleadoDTO empleado)
    {
        this.empleado = empleado;
    }

    public boolean isEsAdministrador()
    {
        return esAdministrador;
    }

    public void setEsAdministrador(boolean esAdministrador)
    {
        this.esAdministrador = esAdministrador;
    }

    public boolean isEsEnfermeria()
    {
        return esEnfermeria;
    }

    public void setEsEnfermeria(boolean esEnfermeria)
    {
        this.esEnfermeria = esEnfermeria;
    }

    public boolean isEsLaboratorista()
    {
        return esLaboratorista;
    }

    public void setEsLaboratorista(boolean esLaboratorista)
    {
        this.esLaboratorista = esLaboratorista;
    }

    public boolean isEsMedico()
    {
        return esMedico;
    }

    public void setEsMedico(boolean esMedico)
    {
        this.esMedico = esMedico;
    }

    private Integer idUsuario;
    private String nombre;
    private String apellido;
    private String nombreCompleto;
    private String login;
    private String clave;
    private Integer codigoRol;
    private String nombreRol;
    private String email;
    private EmpleadoDTO empleado;
    private List menus;
    private boolean esAdministrador;
    private boolean esEnfermeria;
    private boolean esLaboratorista;
    private boolean esMedico;
}
