// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:28:12 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProvinciaDTO.java

package infraestructura.dto;

import java.util.HashSet;
import java.util.Set;

public class ProvinciaDTO
{

    public ProvinciaDTO()
    {
        provincias = new HashSet();
    }

    public String getDepartamentoId()
    {
        return departamentoId;
    }

    public void setDepartamentoId(String departamentoId)
    {
        this.departamentoId = departamentoId;
    }

    public String getPaisId()
    {
        return paisId;
    }

    public void setPaisId(String paisId)
    {
        this.paisId = paisId;
    }

    public String getIdProvincia()
    {
        return idProvincia;
    }

    public void setIdProvincia(String idProvincia)
    {
        this.idProvincia = idProvincia;
    }

    public String getNombreProvincia()
    {
        return nombreProvincia;
    }

    public void setNombreProvincia(String nombreProvincia)
    {
        this.nombreProvincia = nombreProvincia;
    }

    public ProvinciaDTO getTodo()
    {
        return this;
    }

    public String getIdCompuestoProvincia()
    {
        return (new StringBuilder()).append(paisId).append(",").append(departamentoId).append(",").append(idProvincia).toString();
    }

    public void setIdCompuestoProvincia(String idCompuestoProvincia)
    {
        this.idCompuestoProvincia = idCompuestoProvincia;
    }

    private String paisId;
    private String departamentoId;
    private String idProvincia;
    private String nombreProvincia;
    private Set provincias;
    private String idCompuestoProvincia;
}
