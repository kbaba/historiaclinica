// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 05:07:21 a.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AlternativaDTO.java

package infraestructura.dto;

import infraestructura.enums.EstadoAlternativaEnum;
import java.io.Serializable;
import org.apache.commons.lang3.StringUtils;

// Referenced classes of package infraestructura.dto:
//            PreguntaDTO

public class AlternativaDTO
    implements Serializable
{

    public AlternativaDTO()
    {
    }

    public Integer getIdAlternativa()
    {
        return idAlternativa;
    }

    public void setIdAlternativa(Integer idAlternativa)
    {
        this.idAlternativa = idAlternativa;
    }

    public String getDescripcionAlternativa()
    {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa)
    {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public String getEstadoAlternativa()
    {
        return estadoAlternativa;
    }

    public void setEstadoAlternativa(String estadoAlternativa)
    {
        if(StringUtils.isNotBlank(estadoAlternativa))
            setEstadoAlternativaEnum(EstadoAlternativaEnum.findByCode(estadoAlternativa));
        this.estadoAlternativa = estadoAlternativa;
    }

    public PreguntaDTO getPregunta()
    {
        return pregunta;
    }

    public void setPregunta(PreguntaDTO pregunta)
    {
        this.pregunta = pregunta;
    }

    public EstadoAlternativaEnum getEstadoAlternativaEnum()
    {
        return estadoAlternativaEnum;
    }

    public void setEstadoAlternativaEnum(EstadoAlternativaEnum estadoAlternativaEnum)
    {
        this.estadoAlternativaEnum = estadoAlternativaEnum;
    }

    private Integer idAlternativa;
    private String descripcionAlternativa;
    private PreguntaDTO pregunta;
    private String estadoAlternativa;
    private EstadoAlternativaEnum estadoAlternativaEnum;
}
