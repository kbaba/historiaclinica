// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:46 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PaisDTO.java

package infraestructura.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class PaisDTO
    implements Serializable
{

    public PaisDTO()
    {
        departamentos = new HashSet();
    }

    public Set getDepartamentos()
    {
        return departamentos;
    }

    public void setDepartamentos(Set departamentos)
    {
        this.departamentos = departamentos;
    }

    public String getIdPais()
    {
        return idPais;
    }

    public void setIdPais(String idPais)
    {
        this.idPais = idPais;
    }

    public String getNombrePais()
    {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais)
    {
        this.nombrePais = nombrePais;
    }

    private String idPais;
    private String nombrePais;
    private Set departamentos;
}
