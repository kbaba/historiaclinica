// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:27:34 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   OpcionDTO.java

package infraestructura.dto;


public class OpcionDTO
    implements Comparable
{

    public OpcionDTO()
    {
        showMenu = true;
    }

    public Integer getIdOpcion()
    {
        return idOpcion;
    }

    public void setIdOpcion(Integer idOpcion)
    {
        this.idOpcion = idOpcion;
    }

    public String getNombreOpcion()
    {
        return nombreOpcion;
    }

    public void setNombreOpcion(String nombreOpcion)
    {
        this.nombreOpcion = nombreOpcion;
    }

    public String getDescripcionOpcion()
    {
        return descripcionOpcion;
    }

    public void setDescripcionOpcion(String descripcionOpcion)
    {
        this.descripcionOpcion = descripcionOpcion;
    }

    public Integer getPosicionOpcion()
    {
        return posicionOpcion;
    }

    public void setPosicionOpcion(Integer posicionOpcion)
    {
        this.posicionOpcion = posicionOpcion;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public boolean isShowMenu()
    {
        return showMenu;
    }

    public void setShowMenu(boolean showMenu)
    {
        this.showMenu = showMenu;
    }

    public int compareTo(OpcionDTO o)
    {
        return posicionOpcion.compareTo(o.getPosicionOpcion());
    }

    public Boolean getMostrarEnMobile()
    {
        return mostrarEnMobile;
    }

    public void setMostrarEnMobile(Boolean mostrarEnMobile)
    {
        this.mostrarEnMobile = mostrarEnMobile;
    }

    public boolean equals(Object o)
    {
        if(this == o)
            return true;
        if(o == null || getClass() != o.getClass())
            return false;
        OpcionDTO optionDTO = (OpcionDTO)o;
        return idOpcion == null ? optionDTO.getIdOpcion() == null : idOpcion.equals(optionDTO.getIdOpcion());
    }

    public int hashCode()
    {
        return idOpcion == null ? 0 : idOpcion.hashCode();
    }

    public volatile int compareTo(Object x0)
    {
        return compareTo((OpcionDTO)x0);
    }

    private Integer idOpcion;
    private String nombreOpcion;
    private String descripcionOpcion;
    private Integer posicionOpcion;
    private String url;
    private boolean showMenu;
    private Boolean mostrarEnMobile;
}
