// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:14 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ApplicationException.java

package infraestructura.exception;


public class ApplicationException extends RuntimeException
{

    public ApplicationException(String message, Throwable cause)
    {
        super(message, cause);
        this.message = message;
    }

    public ApplicationException(String message)
    {
        super(message);
        this.message = message;
    }

    public ApplicationException(String message, Object parameters[])
    {
        super(message);
        this.message = message;
        this.parameters = parameters;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Object[] getParameters()
    {
        return parameters;
    }

    public void setParameters(Object parameters[])
    {
        this.parameters = parameters;
    }

    private String message;
    private Object parameters[];
}
