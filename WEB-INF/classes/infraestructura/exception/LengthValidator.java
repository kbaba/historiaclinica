// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:25 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   LengthValidator.java

package infraestructura.exception;

import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class LengthValidator
    implements Validator
{

    public LengthValidator()
    {
        ot_alarma = "";
        txtAlarma = "";
    }

    public void validate(FacesContext fc, UIComponent component, Object o)
        throws ValidatorException
    {
        boolean isOK = true;
        initProps(component, fc);
        FacesMessage message = new FacesMessage();
        if(ot_alarma.length() == 0)
        {
            message.setDetail((new StringBuilder()).append("Ingrese una ").append(txtAlarma).append(" porfavor.").toString());
            message.setSummary((new StringBuilder()).append("Ingrese una ").append(txtAlarma).append(" porfavor.").toString());
            isOK = false;
        }
        if(!isOK)
        {
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        } else
        {
            return;
        }
    }

    private void initProps(UIComponent component, FacesContext context)
    {
        Object txtAlarma = component.getAttributes().get("txtAlarma");
        HtmlInputText ht = (HtmlInputText)context.getViewRoot().findComponent(txtAlarma.toString());
        if(ht != null)
            txtAlarma = ht.getValue();
        ot_alarma = (String)component.getAttributes().get("outputAlarma");
        if(txtAlarma != null)
            try
            {
                this.txtAlarma = txtAlarma.toString();
            }
            catch(Exception e) { }
    }

    private String ot_alarma;
    private String txtAlarma;
}
