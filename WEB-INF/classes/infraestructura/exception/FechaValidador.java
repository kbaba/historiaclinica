// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:18 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FechaValidador.java

package infraestructura.exception;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import util.FormatUtil;

public class FechaValidador
    implements Validator
{

    public FechaValidador()
    {
        opLog = "";
        fecAComparar = new Date();
        nombreCampoAComparar = "";
        nombreCampoComparado = "";
    }

    public void validate(FacesContext context, UIComponent component, Object value)
        throws ValidatorException
    {
        boolean isOK = true;
        initProps(component, context);
        FacesMessage message = new FacesMessage();
        Date thisDate = null;
        try
        {
            thisDate = (Date)value;
        }
        catch(Exception e)
        {
            System.out.println((new StringBuilder()).append("Error al validar el metodo de FechaValidador :").append(e).toString());
        }
        if(thisDate == null)
            thisDate = new Date();
        int result = thisDate.compareTo(fecAComparar);
        String compareData = "";
        if(nombreCampoAComparar.trim().length() != 0)
            compareData = nombreCampoAComparar;
        else
            compareData = (new SimpleDateFormat("dd/MM/yyyy")).format(fecAComparar);
        if(result < 0 && opLog.equalsIgnoreCase("ge"))
        {
            message.setDetail((new StringBuilder()).append(compareData).append(" no puede ser mayor a la ").append(nombreCampoComparado).toString());
            message.setSummary((new StringBuilder()).append(compareData).append(" no puede ser mayor a la ").append(nombreCampoComparado).toString());
            isOK = false;
        } else
        if(result > 0 && opLog.equalsIgnoreCase("le"))
        {
            message.setDetail((new StringBuilder()).append("This date should <= ").append(compareData).toString());
            message.setSummary((new StringBuilder()).append("This date should <= ").append(compareData).toString());
            isOK = false;
        } else
        if(result != 0 && opLog.equalsIgnoreCase("eq"))
        {
            message.setDetail((new StringBuilder()).append("This date should == ").append(compareData).toString());
            message.setSummary((new StringBuilder()).append("This date should == ").append(compareData).toString());
            isOK = false;
        } else
        if(result <= 0 && opLog.equalsIgnoreCase("gt"))
        {
            message.setDetail((new StringBuilder()).append("This date should > ").append(compareData).toString());
            message.setSummary((new StringBuilder()).append("This date should > ").append(compareData).toString());
            isOK = false;
        } else
        if(result >= 0 && opLog.equalsIgnoreCase("lt"))
        {
            message.setDetail((new StringBuilder()).append("This date should < ").append(compareData).toString());
            message.setSummary((new StringBuilder()).append("This date should < ").append(compareData).toString());
            isOK = false;
        } else
        if(result == 0 && opLog.equalsIgnoreCase("nt"))
        {
            message.setDetail((new StringBuilder()).append("This date should not = ").append(compareData).toString());
            message.setSummary((new StringBuilder()).append("This date should not = ").append(compareData).toString());
            isOK = false;
        }
        if(!isOK)
        {
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        } else
        {
            return;
        }
    }

    private void initProps(UIComponent component, FacesContext context)
    {
        Object fecAComparar = component.getAttributes().get("fecAComparar");
        HtmlInputText ht = (HtmlInputText)context.getViewRoot().findComponent(fecAComparar.toString());
        if(ht != null)
            fecAComparar = ht.getValue();
        opLog = (String)component.getAttributes().get("opLog");
        if(opLog == null)
            opLog = ">";
        nombreCampoAComparar = (String)component.getAttributes().get("nombreCampoAComparar");
        nombreCampoComparado = (String)component.getAttributes().get("nombreCampoComparado");
        if(fecAComparar != null)
            try
            {
                this.fecAComparar = FormatUtil.getDateTime(fecAComparar);
            }
            catch(Exception e) { }
    }

    private String opLog;
    private Date fecAComparar;
    private String nombreCampoAComparar;
    private String nombreCampoComparado;
}
