// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:56 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   GrupoEnfermedadRowMapper.java

package infraestructura.mapper;

import infraestructura.dto.GrupoEnfermedadDTO;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class GrupoEnfermedadRowMapper
    implements RowMapper
{

    public GrupoEnfermedadRowMapper()
    {
    }

    public Object mapRow(ResultSet rs, int i)
        throws SQLException
    {
        GrupoEnfermedadDTO grupoEnfermedadDTO = new GrupoEnfermedadDTO();
        grupoEnfermedadDTO.setIdCapitulo(rs.getString("idCapitulo"));
        grupoEnfermedadDTO.setNombreCapitulo(rs.getString("nombreCapitulo"));
        grupoEnfermedadDTO.setIdGrupo(rs.getString("idGrupo"));
        grupoEnfermedadDTO.setNombreGrupo(rs.getString("nombreGrupo"));
        grupoEnfermedadDTO.setIdSubGrupo(rs.getString("idSubGrupo"));
        grupoEnfermedadDTO.setNombreSubGrupo(rs.getString("nombreSubGrupo"));
        grupoEnfermedadDTO.setIdEnfermedad(rs.getString("idEnfermedad"));
        grupoEnfermedadDTO.setNombreEnfermedad(rs.getString("nombreEnfermedad"));
        return grupoEnfermedadDTO;
    }
}
