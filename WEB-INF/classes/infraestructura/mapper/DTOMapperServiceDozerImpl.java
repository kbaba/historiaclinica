// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:42 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   DTOMapperServiceDozerImpl.java

package infraestructura.mapper;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

// Referenced classes of package infraestructura.mapper:
//            DTOMapperService

@Component
public class DTOMapperServiceDozerImpl
    implements DTOMapperService
{

    public DTOMapperServiceDozerImpl()
    {
        mapper = new DozerBeanMapper();
    }

    public Object map(Object o, Class tClass)
    {
        return mapper.map(o, tClass);
    }

    Mapper mapper;
}
