// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:53 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EspecialidadRowMapper.java

package infraestructura.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import paquete.entities.Especialidad;

public class EspecialidadRowMapper
    implements RowMapper
{

    public EspecialidadRowMapper()
    {
    }

    public Object mapRow(ResultSet resultSet, int i)
        throws SQLException
    {
        Especialidad especialidad = new Especialidad();
        especialidad.setIdEspecialidad(Integer.valueOf(resultSet.getInt("id_especialidad")));
        especialidad.setNombreEspecialidad(resultSet.getString("nombre_especialidad"));
        especialidad.setEstadoEspecialidad(resultSet.getString("estado_especialidad"));
        return especialidad;
    }
}
