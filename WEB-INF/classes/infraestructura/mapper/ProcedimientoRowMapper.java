// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:04 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoRowMapper.java

package infraestructura.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import paquete.entities.Procedimiento;
import paquete.entities.SubEspecialidad;

public class ProcedimientoRowMapper
    implements RowMapper
{

    public ProcedimientoRowMapper()
    {
    }

    public Object mapRow(ResultSet rs, int i)
        throws SQLException
    {
        Procedimiento procedimiento = new Procedimiento();
        procedimiento.setIdProcedimiento(Integer.valueOf(rs.getInt("id_procedimiento")));
        procedimiento.setNombreProcedimiento(rs.getString("nombre_procedimiento"));
        procedimiento.setEstadoProcedimiento(rs.getString("estado_procedimiento"));
        SubEspecialidad subEspecialidad = new SubEspecialidad();
        subEspecialidad.setIdSubEspecialidad(Integer.valueOf(rs.getInt("sub_especialidad_id")));
        procedimiento.setSubEspecialidad(subEspecialidad);
        return procedimiento;
    }
}
