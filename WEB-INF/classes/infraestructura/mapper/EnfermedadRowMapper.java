// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:49 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadRowMapper.java

package infraestructura.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import paquete.entities.Enfermedad;

public class EnfermedadRowMapper
    implements RowMapper
{

    public EnfermedadRowMapper()
    {
    }

    public Enfermedad mapRow(ResultSet rs, int i)
        throws SQLException
    {
        Enfermedad enfermedad = new Enfermedad();
        enfermedad.setIdEnfermedad(rs.getString("id_enfermedad"));
        enfermedad.setNombreEnfermedad(rs.getString("nombre_enfermedad"));
        enfermedad.setGrupoEnfermedad(rs.getString("grupo_enfermedad"));
        enfermedad.setEsCapitulo(Boolean.valueOf(rs.getBoolean("es_capitulo")));
        return enfermedad;
    }

    public volatile Object mapRow(ResultSet x0, int x1)
        throws SQLException
    {
        return mapRow(x0, x1);
    }
}
