// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:00 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntaRowMapper.java

package infraestructura.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import org.springframework.jdbc.core.RowMapper;
import paquete.entities.*;
import paquete.repositorios.*;

@Component
public class PreguntaRowMapper
    implements RowMapper
{

    public PreguntaRowMapper()
    {
    }

    public ProcedimientoRepository getProcedimientoRepository()
    {
        return procedimientoRepository;
    }

    public void setProcedimientoRepository(ProcedimientoRepository procedimientoRepository)
    {
        this.procedimientoRepository = procedimientoRepository;
    }

    public void setEspecialidadRepository(EspecialidadRepository especialidadRepository)
    {
        this.especialidadRepository = especialidadRepository;
    }

    public void setSubEspecialidadRepository(SubEspecialidadRepository subEspecialidadRepository)
    {
        this.subEspecialidadRepository = subEspecialidadRepository;
    }

    public AlternativaRepository getAlternativaRepository()
    {
        return alternativaRepository;
    }

    public void setAlternativaRepository(AlternativaRepository alternativaRepository)
    {
        this.alternativaRepository = alternativaRepository;
    }

    public Object mapRow(ResultSet resultSet, int i)
        throws SQLException
    {
        Pregunta pregunta = new Pregunta();
        pregunta.setIdPregunta(Integer.valueOf(resultSet.getInt("P.id_pregunta")));
        pregunta.setDescripcionPregunta(resultSet.getString("P.descripcion_pregunta"));
        pregunta.setTipoPregunta(resultSet.getString("P.tipo_pregunta"));
        pregunta.setTipoRespuesta(resultSet.getString("P.tipo_respuesta"));
        pregunta.setQuienPregunta(resultSet.getString("P.quien_pregunta"));
        pregunta.setEstadoPregunta(resultSet.getString("P.estado_pregunta"));
        Integer idProcedimiento = new Integer(resultSet.getInt("P.procedimiento_id"));
        Integer idSubEspecialidad = new Integer(resultSet.getInt("P.sub_especialidad_id"));
        Integer idEspecialidad = new Integer(resultSet.getInt("P.especialidad_id"));
        if(idProcedimiento != null && idProcedimiento.intValue() > 0)
        {
            Procedimiento proce = (Procedimiento)procedimientoRepository.find(idProcedimiento);
            pregunta.setProcedimiento(proce);
            idSubEspecialidad = proce.getSubEspecialidad().getIdSubEspecialidad();
        }
        if(idSubEspecialidad != null && idSubEspecialidad.intValue() > 0)
        {
            SubEspecialidad sub = (SubEspecialidad)subEspecialidadRepository.find(idSubEspecialidad);
            pregunta.setSubEspecialidad(sub);
            idEspecialidad = sub.getEspecialidad().getIdEspecialidad();
        }
        if(idEspecialidad != null && idEspecialidad.intValue() > 0)
        {
            Especialidad espe = (Especialidad)especialidadRepository.find(idEspecialidad);
            pregunta.setEspecialidad(espe);
        }
        java.util.List listaAlternativas = alternativaRepository.getAlternativasXPreguntaId(resultSet.getInt("P.id_pregunta"));
        Set alternativas = new HashSet();
        alternativas.addAll(listaAlternativas);
        pregunta.setAlternativas(alternativas);
        return pregunta;
    }

    ProcedimientoRepository procedimientoRepository;
    AlternativaRepository alternativaRepository;
    EspecialidadRepository especialidadRepository;
    SubEspecialidadRepository subEspecialidadRepository;
}
