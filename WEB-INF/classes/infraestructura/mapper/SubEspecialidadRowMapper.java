// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:08 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadRowMapper.java

package infraestructura.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import paquete.entities.Especialidad;
import paquete.entities.SubEspecialidad;

public class SubEspecialidadRowMapper
    implements RowMapper
{

    public SubEspecialidadRowMapper()
    {
    }

    public Object mapRow(ResultSet rs, int i)
        throws SQLException
    {
        SubEspecialidad subEspecialidad = new SubEspecialidad();
        subEspecialidad.setIdSubEspecialidad(Integer.valueOf(rs.getInt("id_sub_especialidad")));
        subEspecialidad.setNombreSubEspecialidad(rs.getString("nombre_sub_especialidad"));
        Especialidad especialidad = new Especialidad();
        especialidad.setIdEspecialidad(Integer.valueOf(rs.getInt("especialidad_id")));
        subEspecialidad.setEspecialidad(especialidad);
        subEspecialidad.setEstadoSubEspecialidad(rs.getString("estado_sub_especialidad"));
        return subEspecialidad;
    }
}
