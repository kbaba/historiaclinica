// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:31:46 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpleadoRowMapper.java

package infraestructura.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import paquete.entities.Empleado;
import paquete.entities.Persona;
import paquete.repositorios.*;

public class EmpleadoRowMapper
    implements RowMapper
{

    public EmpleadoRowMapper()
    {
    }

    public ProcedimientoRepository getProcedimientoRepository()
    {
        return procedimientoRepository;
    }

    public void setProcedimientoRepository(ProcedimientoRepository procedimientoRepository)
    {
        this.procedimientoRepository = procedimientoRepository;
    }

    public void setEspecialidadRepository(EspecialidadRepository especialidadRepository)
    {
        this.especialidadRepository = especialidadRepository;
    }

    public void setSubEspecialidadRepository(SubEspecialidadRepository subEspecialidadRepository)
    {
        this.subEspecialidadRepository = subEspecialidadRepository;
    }

    public Persona mapRow(ResultSet rs, int i)
        throws SQLException
    {
        Persona persona = new Persona();
        persona.setIdPersona(Integer.valueOf(rs.getInt("P.id_persona")));
        persona.setTipoDocumentoPersona(rs.getString("P.tipo_documento_persona"));
        persona.setDocumentoPersona(rs.getString("P.documento_persona"));
        persona.setIdPaisNacionalidad(rs.getString("P.nacionalidad_pais_id"));
        persona.setNombrePersona(rs.getString("P.nombre_persona"));
        persona.setApellidoPaternoPersona(rs.getString("P.apellido_paterno_persona"));
        persona.setApellidoMaternoPersona(rs.getString("P.apellido_materno_persona"));
        persona.setFechaNacimientoPersona(rs.getDate("P.fecha_nacimiento_persona"));
        persona.setTelefonoFijoPersona(rs.getString("P.telefono_fijo_persona"));
        persona.setTelefonoCelularPersona(rs.getString("P.telefono_celular_persona"));
        persona.setEmailPersona(rs.getString("P.email_persona"));
        persona.setDireccionPersona(rs.getString("P.direccion_persona"));
        persona.setIdResidenciaDistrito(rs.getString("P.residencia_distrito_id"));
        persona.setSexoPersona(rs.getString("P.sexo_persona"));
        persona.setUsuario(rs.getString("P.usuario"));
        persona.setClave(rs.getString("P.clave"));
        Empleado empleado = new Empleado();
        empleado.setIdEmpleado(Integer.valueOf(rs.getInt("E.id_empleado")));
        empleado.setTipoEmpleado(rs.getString("E.tipo_empleado"));
        empleado.setEstadoEmpleado(rs.getString("E.estado_empleado"));
        persona.setEmpleado(empleado);
        return persona;
    }

    public volatile Object mapRow(ResultSet x0, int x1)
        throws SQLException
    {
        return mapRow(x0, x1);
    }

    ProcedimientoRepository procedimientoRepository;
    EspecialidadRepository especialidadRepository;
    SubEspecialidadRepository subEspecialidadRepository;
}
