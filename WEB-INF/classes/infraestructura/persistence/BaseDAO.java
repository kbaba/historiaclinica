// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:14 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   BaseDAO.java

package infraestructura.persistence;

import com.googlecode.genericdao.dao.jpa.GenericDAOImpl;
import com.googlecode.genericdao.search.jpa.JPASearchProcessor;
import javax.persistence.EntityManager;

public class BaseDAO extends GenericDAOImpl
{

    public BaseDAO()
    {
    }

    @PersistenceContext
    public void setEntityManager(EntityManager entityManager)
    {
        super.setEntityManager(entityManager);
    }

    @Autowired
    public void setSearchProcessor(JPASearchProcessor searchProcessor)
    {
        super.setSearchProcessor(searchProcessor);
    }

    public transient void persist(Object entities[])
    {
        super.persist(entities);
    }

    public Object merge(Object entity)
    {
        return _merge(entity);
    }

    public transient Object[] merge(Object entities[])
    {
        return _merge(persistentClass, entities);
    }
}
