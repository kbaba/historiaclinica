// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:21 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   JpaHbmBeanResultTransformer.java

package infraestructura.persistence;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

public class JpaHbmBeanResultTransformer
    implements ResultTransformer
{

    public JpaHbmBeanResultTransformer(Class resultClass)
    {
        ignoreNonWritableProperty = false;
        this.resultClass = resultClass;
    }

    public JpaHbmBeanResultTransformer setIgnoreNonWritableProperty(boolean ignoreNonWritableProperty)
    {
        this.ignoreNonWritableProperty = ignoreNonWritableProperty;
        return this;
    }

    public Object transformTuple(Object tuple[], String aliases[])
    {
        Object result;
        try
        {
            result = resultClass.newInstance();
            BeanWrapper wrap = new BeanWrapperImpl(result);
            for(int i = 0; i < aliases.length; i++)
            {
                String alias = aliases[i];
                if(!ignoreNonWritableProperty || wrap.isWritableProperty(alias))
                    wrap.setPropertyValue(alias, tuple[i]);
            }

        }
        catch(InstantiationException e)
        {
            throw new HibernateException((new StringBuilder()).append("Could not instantiate resultclass: ").append(resultClass.getName()).toString());
        }
        catch(IllegalAccessException e)
        {
            throw new HibernateException((new StringBuilder()).append("Could not instantiate resultclass: ").append(resultClass.getName()).toString());
        }
        return result;
    }

    public List transformList(List collection)
    {
        return collection;
    }

    private final Class resultClass;
    boolean ignoreNonWritableProperty;
}
