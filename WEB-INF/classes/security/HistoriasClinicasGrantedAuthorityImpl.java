// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:48:01 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasGrantedAuthorityImpl.java

package security;

import org.springframework.security.core.authority.GrantedAuthorityImpl;

public class HistoriasClinicasGrantedAuthorityImpl extends GrantedAuthorityImpl
{

    public HistoriasClinicasGrantedAuthorityImpl()
    {
        super("INIT");
    }

    public HistoriasClinicasGrantedAuthorityImpl(String role)
    {
        super(role);
    }
}
