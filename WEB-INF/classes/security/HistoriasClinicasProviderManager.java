// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:48:18 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasProviderManager.java

package security;

import java.util.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.security.authentication.*;
import org.springframework.security.core.*;
import org.springframework.util.Assert;

// Referenced classes of package security:
//            HistoriasClinicasAuthenticationToken

public class HistoriasClinicasProviderManager extends AbstractAuthenticationManager
    implements MessageSourceAware, InitializingBean
{
    private static final class NullEventPublisher
        implements AuthenticationEventPublisher
    {

        public void publishAuthenticationFailure(AuthenticationException authenticationexception, Authentication authentication1)
        {
        }

        public void publishAuthenticationSuccess(Authentication authentication1)
        {
        }

        private NullEventPublisher()
        {
        }

    }


    public HistoriasClinicasProviderManager()
    {
        eventPublisher = new NullEventPublisher();
        providers = Collections.emptyList();
        messages = SpringSecurityMessageSource.getAccessor();
        eraseCredentialsAfterAuthentication = false;
    }

    public void afterPropertiesSet()
        throws Exception
    {
        if(parent == null && providers.isEmpty())
            throw new IllegalArgumentException("A parent AuthenticationManager or a list of AuthenticationProviders is required");
        else
            return;
    }

    public Authentication doAuthentication(Authentication authentication)
        throws AuthenticationException
    {
        Class toTest = authentication.getClass();
        AuthenticationException lastException = null;
        Authentication result = null;
        Iterator i$ = getProviders().iterator();
        do
        {
            if(!i$.hasNext())
                break;
            AuthenticationProvider provider = (AuthenticationProvider)i$.next();
            logger.debug((new StringBuilder()).append("Authentication attempt using ").append(provider.getClass().getName()).toString());
            try
            {
                if(provider instanceof AnonymousAuthenticationProvider)
                {
                    result = provider.authenticate(authentication);
                } else
                {
                    HistoriasClinicasAuthenticationToken authOld = (HistoriasClinicasAuthenticationToken)authentication;
                    HistoriasClinicasAuthenticationToken token = new HistoriasClinicasAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(), authOld.isMobile());
                    result = provider.authenticate(token);
                }
                if(result == null)
                    continue;
                copyDetails(authentication, result);
                break;
            }
            catch(AccountStatusException e)
            {
                eventPublisher.publishAuthenticationFailure(e, authentication);
                throw e;
            }
            catch(AuthenticationException e)
            {
                lastException = e;
            }
        } while(true);
        if(result == null && parent != null)
            try
            {
                result = parent.authenticate(authentication);
            }
            catch(ProviderNotFoundException e) { }
            catch(AuthenticationException e)
            {
                lastException = e;
            }
        if(result != null)
        {
            if(eraseCredentialsAfterAuthentication && (result instanceof CredentialsContainer))
                ((CredentialsContainer)result).eraseCredentials();
            eventPublisher.publishAuthenticationSuccess(result);
            return result;
        }
        if(lastException == null)
            lastException = new ProviderNotFoundException(messages.getMessage("ProviderManager.providerNotFound", new Object[] {
                toTest.getName()
            }, "No AuthenticationProvider found for {0}"));
        eventPublisher.publishAuthenticationFailure(lastException, authentication);
        throw lastException;
    }

    private void copyDetails(Authentication source, Authentication dest)
    {
        if((dest instanceof AbstractAuthenticationToken) && dest.getDetails() == null)
        {
            AbstractAuthenticationToken token = (AbstractAuthenticationToken)dest;
            token.setDetails(source.getDetails());
        }
    }

    public List getProviders()
    {
        return providers;
    }

    public void setMessageSource(MessageSource messageSource)
    {
        messages = new MessageSourceAccessor(messageSource);
    }

    public void setParent(AuthenticationManager parent)
    {
        this.parent = parent;
    }

    public void setAuthenticationEventPublisher(AuthenticationEventPublisher eventPublisher)
    {
        Assert.notNull(eventPublisher, "AuthenticationEventPublisher cannot be null");
        this.eventPublisher = eventPublisher;
    }

    public void setEraseCredentialsAfterAuthentication(boolean eraseSecretData)
    {
        eraseCredentialsAfterAuthentication = eraseSecretData;
    }

    public boolean isEraseCredentialsAfterAuthentication()
    {
        return eraseCredentialsAfterAuthentication;
    }

    public void setProviders(List providers)
    {
        Assert.notNull(providers, "Providers list cannot be null");
        Object currentObject;
        for(Iterator i$ = providers.iterator(); i$.hasNext(); Assert.isInstanceOf(org/springframework/security/authentication/AuthenticationProvider, currentObject, "Can only provide AuthenticationProvider instances"))
            currentObject = i$.next();

        this.providers = providers;
    }

    private static final Log logger = LogFactory.getLog(security/HistoriasClinicasProviderManager);
    private AuthenticationEventPublisher eventPublisher;
    private List providers;
    protected MessageSourceAccessor messages;
    private AuthenticationManager parent;
    private boolean eraseCredentialsAfterAuthentication;

}
