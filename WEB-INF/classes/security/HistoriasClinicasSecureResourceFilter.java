// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:48:28 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasSecureResourceFilter.java

package security;

import java.util.Collection;
import java.util.Properties;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

public class HistoriasClinicasSecureResourceFilter
    implements FilterInvocationSecurityMetadataSource
{

    public HistoriasClinicasSecureResourceFilter()
    {
    }

    public Collection getAllConfigAttributes()
    {
        return null;
    }

    public Collection getAttributes(Object filter)
    {
        FilterInvocation filterInvocation = (FilterInvocation)filter;
        String url = filterInvocation.getRequestUrl();
        if(url.indexOf("public") < 0 && url.endsWith(".jsf") && url.indexOf("login") < 0 && url.indexOf("main") < 0)
        {
            int lastSlashIndex = url.lastIndexOf("/");
            url = (new StringBuilder()).append("ROLE_").append(url.substring(lastSlashIndex + 1, url.length() - 4)).toString();
            return SecurityConfig.createListFromCommaDelimitedString(url);
        } else
        {
            return getAttributesEstandar(filter);
        }
    }

    public Collection getAttributesEstandar(Object filter)
        throws IllegalArgumentException
    {
        FilterInvocation filterInvocation = (FilterInvocation)filter;
        String url = filterInvocation.getRequestUrl();
        String urlPropsValue = urlProperties.getProperty(url);
        StringBuilder rolesStringBuilder = new StringBuilder();
        if(urlPropsValue != null)
            rolesStringBuilder.append(urlPropsValue).append(",");
        if(!url.endsWith("/"))
        {
            int lastSlashIndex = url.lastIndexOf("/");
            url = url.substring(0, lastSlashIndex + 1);
        }
        String urlParts[] = url.split("/");
        StringBuilder urlBuilder = new StringBuilder();
        String arr$[] = urlParts;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            String urlPart = arr$[i$];
            if(urlPart.trim().length() == 0)
                continue;
            urlBuilder.append("/").append(urlPart);
            urlPropsValue = urlProperties.getProperty((new StringBuilder()).append(urlBuilder.toString()).append("/**").toString());
            if(urlPropsValue != null)
                rolesStringBuilder.append(urlPropsValue).append(",");
        }

        if(rolesStringBuilder.toString().endsWith(","))
            rolesStringBuilder.deleteCharAt(rolesStringBuilder.length() - 1);
        if(rolesStringBuilder.length() == 0)
            return null;
        else
            return SecurityConfig.createListFromCommaDelimitedString(rolesStringBuilder.toString());
    }

    public boolean supports(Class arg0)
    {
        return true;
    }

    public void setUrlProperties(Properties urlProperties)
    {
        this.urlProperties = urlProperties;
    }

    public Properties getUrlProperties()
    {
        return urlProperties;
    }

    private Properties urlProperties;
}
