// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:48:22 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasRedirectStrategy.java

package security;

import infraestructura.dto.*;
import infraestructura.exception.HistoriasClinicasSecurityException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;

// Referenced classes of package security:
//            HistoriasClinicasAuthenticationToken, HistoriasClinicasSecurityInfo

public class HistoriasClinicasRedirectStrategy extends DefaultRedirectStrategy
    implements RedirectStrategy
{

    public HistoriasClinicasRedirectStrategy()
    {
    }

    public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url)
        throws IOException
    {
        String urlToRedirect = null;
        String carpetaPages = "/pages/";
        try
        {
            HistoriasClinicasAuthenticationToken token = (HistoriasClinicasAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
            String login = (String)token.getPrincipal();
            String password = (String)token.getCredentials();
            boolean mobile = token.isMobile();
            UsuarioDTO usuarioDTO = historiasClinicasSecurityInfo.obtenerUsuario(login, password, mobile);
            boolean found = false;
            Iterator i$ = usuarioDTO.getMenus().iterator();
label0:
            do
            {
                if(!i$.hasNext())
                    break;
                MenuDTO menu = (MenuDTO)i$.next();
                Iterator i$ = menu.getOptions().iterator();
                OpcionDTO opcion;
                do
                {
                    if(!i$.hasNext())
                        continue label0;
                    opcion = (OpcionDTO)i$.next();
                    if(opcion.getMostrarEnMobile().booleanValue())
                    {
                        urlToRedirect = opcion.getUrl();
                        found = true;
                        continue label0;
                    }
                } while(!opcion.isShowMenu());
                urlToRedirect = opcion.getUrl();
                found = true;
            } while(!found);
            if(urlToRedirect != null)
                urlToRedirect = (new StringBuilder()).append(carpetaPages).append(urlToRedirect).toString();
        }
        catch(HistoriasClinicasSecurityException e)
        {
            LOG.warn("Revise los siguientes detalles en configuracin seguridad", e);
            urlToRedirect = (new StringBuilder()).append(carpetaPages).append("main.jsf").toString();
        }
        if(urlToRedirect == null)
        {
            throw new HistoriasClinicasSecurityException("Configure adecuadamente el URL de la opcion por defecto: ");
        } else
        {
            LOG.info((new StringBuilder()).append("Abriendo directamente la pgina: ").append(urlToRedirect).toString());
            super.sendRedirect(request, response, urlToRedirect);
            return;
        }
    }

    public void setHistoriasClinicasSecurityInfo(HistoriasClinicasSecurityInfo historiasClinicasSecurityInfo)
    {
        this.historiasClinicasSecurityInfo = historiasClinicasSecurityInfo;
    }

    private static final Logger LOG = LoggerFactory.getLogger(security/HistoriasClinicasRedirectStrategy);
    HistoriasClinicasSecurityInfo historiasClinicasSecurityInfo;

}
