// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:47:51 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasAuthenticationToken.java

package security;

import java.util.Collection;
import org.springframework.security.authentication.AbstractAuthenticationToken;

public class HistoriasClinicasAuthenticationToken extends AbstractAuthenticationToken
{

    public HistoriasClinicasAuthenticationToken(Object principal, Object credentials, boolean mobile)
    {
        super(null);
        this.principal = principal;
        this.credentials = credentials;
        this.mobile = mobile;
        setAuthenticated(false);
    }

    /**
     * @deprecated Method HistoriasClinicasAuthenticationToken is deprecated
     */

    public HistoriasClinicasAuthenticationToken(Object principal, Object credentials, boolean mobile, Collection authorities)
    {
        super(authorities);
        this.principal = principal;
        this.credentials = credentials;
        this.mobile = mobile;
        super.setAuthenticated(true);
    }

    public Object getCredentials()
    {
        return credentials;
    }

    public Object getPrincipal()
    {
        return principal;
    }

    public boolean isMobile()
    {
        return mobile;
    }

    public void setMobile(boolean mobile)
    {
        this.mobile = mobile;
    }

    public void setAuthenticated(boolean isAuthenticated)
        throws IllegalArgumentException
    {
        if(isAuthenticated)
        {
            throw new IllegalArgumentException("Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        } else
        {
            super.setAuthenticated(false);
            return;
        }
    }

    public void eraseCredentials()
    {
        super.eraseCredentials();
        credentials = null;
    }

    private final Object principal;
    private Object credentials;
    private boolean mobile;
}
