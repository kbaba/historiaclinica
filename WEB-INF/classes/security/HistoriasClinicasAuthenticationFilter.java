// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:47:23 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasAuthenticationFilter.java

package security;

import java.util.StringTokenizer;
import javax.servlet.http.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.TextEscapeUtils;
import org.springframework.util.Assert;

// Referenced classes of package security:
//            HistoriasClinicasAuthenticationToken

public class HistoriasClinicasAuthenticationFilter extends AbstractAuthenticationProcessingFilter
{

    public HistoriasClinicasAuthenticationFilter()
    {
        super("/j_spring_security_check");
        usernameParameter = "j_username";
        passwordParameter = "j_password";
        mobileParameter = "j_mobileCheck_input";
        cryptParameter = "j_token";
        postOnly = true;
    }

    private String token(String param, String paramName)
    {
        for(StringTokenizer tokenizer = new StringTokenizer(param, "&"); tokenizer.hasMoreTokens();)
        {
            String token = tokenizer.nextToken();
            String patron = (new StringBuilder()).append(paramName).append("=").toString();
            int index = token.indexOf(patron);
            if(index != -1)
                return token.substring(index + patron.length());
        }

        return null;
    }

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
        throws AuthenticationException
    {
        if(postOnly && !request.getMethod().equals("POST"))
            throw new AuthenticationServiceException((new StringBuilder()).append("Authentication method not supported: ").append(request.getMethod()).toString());
        String username = obtainUsername(request);
        String password = obtainPassword(request);
        boolean mobile = obtainMobile(request);
        if(username == null)
            username = "";
        if(password == null)
            password = "";
        username = username.trim();
        HistoriasClinicasAuthenticationToken authRequest = new HistoriasClinicasAuthenticationToken(username, password, mobile);
        HttpSession session = request.getSession(false);
        if(session != null || getAllowSessionCreation())
            request.getSession().setAttribute("SPRING_SECURITY_LAST_USERNAME", TextEscapeUtils.escapeEntities(username));
        setDetails(request, authRequest);
        Authentication authenticate = getAuthenticationManager().authenticate(authRequest);
        return authenticate;
    }

    protected String obtainPassword(HttpServletRequest request)
    {
        return getParameter(passwordParameter, request);
    }

    protected String obtainUsername(HttpServletRequest request)
    {
        return getParameter(usernameParameter, request);
    }

    protected boolean obtainMobile(HttpServletRequest request)
    {
        String mobile = getParameter(mobileParameter, request);
        return StringUtils.isNotBlank(mobile) ? mobile.equals("on") : false;
    }

    protected void setDetails(HttpServletRequest request, HistoriasClinicasAuthenticationToken authRequest)
    {
        authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
    }

    public void setUsernameParameter(String usernameParameter)
    {
        Assert.hasText(usernameParameter, "Username parameter must not be empty or null");
        this.usernameParameter = usernameParameter;
    }

    public void setPasswordParameter(String passwordParameter)
    {
        Assert.hasText(passwordParameter, "Password parameter must not be empty or null");
        this.passwordParameter = passwordParameter;
    }

    public void setPostOnly(boolean postOnly)
    {
        this.postOnly = postOnly;
    }

    public final String getUsernameParameter()
    {
        return usernameParameter;
    }

    public final String getPasswordParameter()
    {
        return passwordParameter;
    }

    private String getParameter(String paramName, HttpServletRequest request)
    {
        return request.getParameter(paramName);
    }

    public static final String SPRING_SECURITY_FORM_USERNAME_KEY = "j_username";
    public static final String SPRING_SECURITY_FORM_PASSWORD_KEY = "j_password";
    public static final String SPRING_SECURITY_FORM_ROL_KEY = "j_mobileCheck_input";
    public static final String SPRING_SECURITY_CRYPT = "j_token";
    public static final String SPRING_SECURITY_LAST_USERNAME_KEY = "SPRING_SECURITY_LAST_USERNAME";
    private String usernameParameter;
    private String passwordParameter;
    private String mobileParameter;
    private String cryptParameter;
    private boolean postOnly;
}
