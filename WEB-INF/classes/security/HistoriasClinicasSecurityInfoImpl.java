// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:48:41 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasSecurityInfoImpl.java

package security;

import infraestructura.dto.*;
import infraestructura.enums.EstadoTrabajoEnum;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.*;
import paquete.repositorios.PersonaRepository;

// Referenced classes of package security:
//            HistoriasClinicasSecurityInfo

@Service
public class HistoriasClinicasSecurityInfoImpl
    implements HistoriasClinicasSecurityInfo
{

    public HistoriasClinicasSecurityInfoImpl()
    {
    }

    public UsuarioDTO obtenerUsuario(String login, String password, boolean mobile)
    {
        UsuarioDTO usuarioDTO = null;
        boolean found = false;
        List options = new ArrayList();
        Set menus = new HashSet();
        Persona persona = personaRepository.acceder(login, password);
        if(persona != null)
        {
            usuarioDTO = new UsuarioDTO();
            usuarioDTO.setNombreCompleto((new StringBuilder()).append(persona.getNombrePersona()).append(" ").append(persona.getApellidoPaternoPersona()).append(" ").append(persona.getApellidoMaternoPersona()).toString());
            usuarioDTO.setCodigoRol(persona.getRol().getIdRol());
            usuarioDTO.setNombreRol(persona.getRol().getNombreRol());
            Iterator i$;
            if(persona.getEmpleado() != null)
            {
                usuarioDTO.setEmpleado((EmpleadoDTO)mapperService.map(persona.getEmpleado(), infraestructura/dto/EmpleadoDTO));
                i$ = persona.getEmpleado().getTrabajos().iterator();
                do
                {
                    if(!i$.hasNext())
                        break;
                    Trabajo trabajo = (Trabajo)i$.next();
                    if(trabajo.getEstadoTrabajo().equals(EstadoTrabajoEnum.ACTIVO.getCode()))
                        usuarioDTO.getEmpleado().agregarTrabajo((TrabajoDTO)mapperService.map(trabajo, infraestructura/dto/TrabajoDTO));
                } while(true);
            }
            Opcion option;
            for(i$ = persona.getRol().getOptions().iterator(); i$.hasNext(); menus.add(option.getMenu()))
            {
                option = (Opcion)i$.next();
                options.add(option);
            }

            i$ = menus.iterator();
            do
            {
                if(!i$.hasNext())
                    break;
                Menu menu = (Menu)i$.next();
                List menuOptions = new ArrayList();
                Iterator i$ = options.iterator();
                do
                {
                    if(!i$.hasNext())
                        break;
                    Opcion option = (Opcion)i$.next();
                    if(!menu.getOpciones().contains(option))
                        continue;
                    OpcionDTO opcionDTO;
                    if(mobile && option.getMostrarEnMobile().booleanValue())
                    {
                        menuOptions.clear();
                        opcionDTO = (OpcionDTO)mapperService.map(option, infraestructura/dto/OpcionDTO);
                        menuOptions.add(opcionDTO);
                        found = true;
                        break;
                    }
                    opcionDTO = (OpcionDTO)mapperService.map(option, infraestructura/dto/OpcionDTO);
                    if(!opcionDTO.getMostrarEnMobile().booleanValue())
                    {
                        opcionDTO.setShowMenu(option.getMostrarEnMenu().booleanValue());
                        menuOptions.add(opcionDTO);
                    }
                } while(true);
                if(!menuOptions.isEmpty())
                {
                    MenuDTO menuDto = new MenuDTO();
                    menuDto.setIdMenu(menu.getIdMenu());
                    menuDto.setNombreMenu(menu.getNombreMenu());
                    menuDto.setPosicionMenu(menu.getPosicionMenu());
                    Collections.sort(menuOptions);
                    menuDto.getOptions().addAll(menuOptions);
                    usuarioDTO.getMenus().add(menuDto);
                }
            } while(true);
            if(mobile && !found)
                return null;
            Collections.sort(usuarioDTO.getMenus());
        }
        return usuarioDTO;
    }

    private static final Logger LOG = LoggerFactory.getLogger(security/HistoriasClinicasSecurityInfoImpl);
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private DTOMapperService mapperService;

}
