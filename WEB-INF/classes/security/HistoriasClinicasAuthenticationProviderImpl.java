// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:47:45 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasClinicasAuthenticationProviderImpl.java

package security;

import infraestructura.dto.*;
import java.util.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

// Referenced classes of package security:
//            HistoriasClinicasAuthenticationToken, HistoriasClinicasGrantedAuthorityImpl, HistoriasClinicasAuthenticationProvider, HistoriasClinicasSecurityInfo

public class HistoriasClinicasAuthenticationProviderImpl
    implements HistoriasClinicasAuthenticationProvider
{

    public HistoriasClinicasAuthenticationProviderImpl()
    {
    }

    @Transactional
    public Authentication authenticate(Authentication authentication)
        throws AuthenticationException
    {
        HistoriasClinicasAuthenticationToken token = (HistoriasClinicasAuthenticationToken)authentication;
        List authorities = new ArrayList();
        String username = (String)token.getPrincipal();
        String password = (String)token.getCredentials();
        boolean mobile = token.isMobile();
        UsuarioDTO usuario = historiasClinicasSecurityInfo.obtenerUsuario(username, password, mobile);
        if(usuario == null)
            return null;
        for(Iterator i$ = usuario.getMenus().iterator(); i$.hasNext();)
        {
            MenuDTO menu = (MenuDTO)i$.next();
            Iterator i$ = menu.getOptions().iterator();
            while(i$.hasNext()) 
            {
                OpcionDTO opcion = (OpcionDTO)i$.next();
                authorities.add(new HistoriasClinicasGrantedAuthorityImpl((new StringBuilder()).append("ROLE_").append(opcion.getNombreOpcion()).toString()));
            }
        }

        authorities.add(new HistoriasClinicasGrantedAuthorityImpl("ROLE_HISTORIAS_CLINICAS_USER"));
        return new HistoriasClinicasAuthenticationToken(token.getName(), token.getCredentials(), token.isMobile(), authorities);
    }

    public boolean supports(Class authentication)
    {
        return authentication.equals(security/HistoriasClinicasAuthenticationToken);
    }

    @Autowired
    HistoriasClinicasSecurityInfo historiasClinicasSecurityInfo;
}
