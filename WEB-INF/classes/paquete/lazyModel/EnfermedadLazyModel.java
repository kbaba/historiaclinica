// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:03 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadLazyModel.java

package paquete.lazyModel;

import infraestructura.dto.EnfermedadDTO;
import infraestructura.dto.GrupoEnfermedadDTO;
import java.util.*;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import paquete.entities.Enfermedad;
import paquete.service.EnfermedadService;

public class EnfermedadLazyModel extends LazyDataModel
{

    public EnfermedadLazyModel(EnfermedadDTO enfermedadFiltro, Enfermedad capituloFiltro, EnfermedadService enfermedadService, Boolean realizarBusqueda)
    {
        this.enfermedadFiltro = enfermedadFiltro;
        this.capituloFiltro = capituloFiltro;
        this.enfermedadService = enfermedadService;
        this.realizarBusqueda = realizarBusqueda;
    }

    public List load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map filters)
    {
        if(!realizarBusqueda.booleanValue())
        {
            enfermedades = new ArrayList();
            setRowCount(0);
        } else
        {
            enfermedades = enfermedadService.findByFilter(enfermedadFiltro, capituloFiltro, startingAt, maxPerPage);
            int dataSize = enfermedadService.cuentaRegistros(enfermedadFiltro, capituloFiltro, startingAt, maxPerPage);
            setRowCount(dataSize);
        }
        return enfermedades;
    }

    public Object getRowKey(GrupoEnfermedadDTO enfermedad)
    {
        return enfermedad.getIdEnfermedad();
    }

    public GrupoEnfermedadDTO getRowData(String enfermedadId)
    {
        Integer id = Integer.valueOf(enfermedadId);
        for(Iterator i$ = enfermedades.iterator(); i$.hasNext();)
        {
            GrupoEnfermedadDTO enfermedadDTO = (GrupoEnfermedadDTO)i$.next();
            if(id.equals(enfermedadDTO.getIdEnfermedad()))
                return enfermedadDTO;
        }

        return null;
    }

    public void setRowIndex(int rowIndex)
    {
        if(rowIndex == -1 || getPageSize() == 0)
            super.setRowIndex(-1);
        else
            super.setRowIndex(rowIndex % getPageSize());
    }

    public volatile Object getRowKey(Object x0)
    {
        return getRowKey((GrupoEnfermedadDTO)x0);
    }

    public volatile Object getRowData(String x0)
    {
        return getRowData(x0);
    }

    EnfermedadService enfermedadService;
    private EnfermedadDTO enfermedadFiltro;
    private Enfermedad capituloFiltro;
    private static final long serialVersionUID = 1L;
    private List enfermedades;
    private Boolean realizarBusqueda;
}
