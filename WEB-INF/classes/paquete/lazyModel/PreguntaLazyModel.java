// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:10 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntaLazyModel.java

package paquete.lazyModel;

import infraestructura.dto.AlternativaDTO;
import infraestructura.dto.PreguntaDTO;
import java.util.*;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import paquete.service.PreguntaService;

public class PreguntaLazyModel extends LazyDataModel
{

    public PreguntaLazyModel(PreguntaDTO preguntaFiltro, PreguntaService preguntaService, boolean realizarBusqueda)
    {
        this.preguntaFiltro = preguntaFiltro;
        this.preguntaService = preguntaService;
        this.realizarBusqueda = realizarBusqueda;
    }

    public PreguntaLazyModel(PreguntaDTO preguntaFiltro, PreguntaService preguntaService, boolean realizarBusqueda, AlternativaDTO alternativaDTO)
    {
        this.preguntaFiltro = preguntaFiltro;
        this.preguntaService = preguntaService;
        this.realizarBusqueda = realizarBusqueda;
        alternativa = alternativaDTO;
    }

    public List load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map filters)
    {
        if(!realizarBusqueda)
        {
            preguntas = new ArrayList();
            setRowCount(0);
            return preguntas;
        }
        if(alternativa != null)
        {
            preguntas = preguntaService.findByFilter(preguntaFiltro, alternativa, startingAt, maxPerPage);
            int dataSize = preguntaService.cuentaRegistrosFilterLazy(preguntaFiltro, alternativa);
            setRowCount(dataSize);
        } else
        {
            preguntas = preguntaService.findByFilter(preguntaFiltro, startingAt, maxPerPage, true);
            int dataSize = preguntaService.cuentaRegistrosFilterLazy(preguntaFiltro);
            setRowCount(dataSize);
        }
        return preguntas;
    }

    public Object getRowKey(PreguntaDTO pregunta)
    {
        return pregunta.getIdPregunta();
    }

    public PreguntaDTO getRowData(String preguntaId)
    {
        Integer id = Integer.valueOf(preguntaId);
        for(Iterator i$ = preguntas.iterator(); i$.hasNext();)
        {
            PreguntaDTO preguntaDTO = (PreguntaDTO)i$.next();
            if(id.equals(preguntaDTO.getIdPregunta()))
                return preguntaDTO;
        }

        return null;
    }

    public void setRowIndex(int rowIndex)
    {
        if(rowIndex == -1 || getPageSize() == 0)
            super.setRowIndex(-1);
        else
            super.setRowIndex(rowIndex % getPageSize());
    }

    public volatile Object getRowKey(Object x0)
    {
        return getRowKey((PreguntaDTO)x0);
    }

    public volatile Object getRowData(String x0)
    {
        return getRowData(x0);
    }

    PreguntaService preguntaService;
    private PreguntaDTO preguntaFiltro;
    private static final long serialVersionUID = 1L;
    private List preguntas;
    private boolean realizarBusqueda;
    private AlternativaDTO alternativa;
}
