// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:07 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PacienteNuevoLazyModel.java

package paquete.lazyModel;

import infraestructura.dto.PersonaDTO;
import java.util.*;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import paquete.service.PersonaService;

public class PacienteNuevoLazyModel extends LazyDataModel
{

    public PacienteNuevoLazyModel(PersonaDTO personaFiltro, PersonaService personaService, boolean realizarBusqueda)
    {
        this.personaFiltro = personaFiltro;
        this.personaService = personaService;
        this.realizarBusqueda = realizarBusqueda;
    }

    public List load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map filters)
    {
        if(!realizarBusqueda)
        {
            personas = new ArrayList();
            setRowCount(0);
            return personas;
        } else
        {
            personas = personaService.findByFilterLazy(personaFiltro, startingAt, maxPerPage);
            int dataSize = personaService.cuentaRegistrosFilterLazy(personaFiltro);
            setRowCount(dataSize);
            return personas;
        }
    }

    public Object getRowKey(PersonaDTO persona)
    {
        return persona.getIdPersona();
    }

    public PersonaDTO getRowData(String personaId)
    {
        Integer id = Integer.valueOf(personaId);
        for(Iterator i$ = personas.iterator(); i$.hasNext();)
        {
            PersonaDTO personaDTO = (PersonaDTO)i$.next();
            if(id.equals(personaDTO.getIdPersona()))
                return personaDTO;
        }

        return null;
    }

    public void setRowIndex(int rowIndex)
    {
        if(rowIndex == -1 || getPageSize() == 0)
            super.setRowIndex(-1);
        else
            super.setRowIndex(rowIndex % getPageSize());
    }

    public volatile Object getRowKey(Object x0)
    {
        return getRowKey((PersonaDTO)x0);
    }

    public volatile Object getRowData(String x0)
    {
        return getRowData(x0);
    }

    PersonaService personaService;
    private PersonaDTO personaFiltro;
    private static final long serialVersionUID = 1L;
    private List personas;
    private boolean realizarBusqueda;
}
