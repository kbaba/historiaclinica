// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:56 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AreasJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import infraestructura.enums.EstadosEnum;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import org.apache.commons.lang.StringUtils;
import org.primefaces.event.*;
import paquete.entities.*;
import paquete.repositorios.EspecialidadRepository;
import paquete.repositorios.SubEspecialidadRepository;
import paquete.service.*;
import util.FindCrudBeanBase;
import util.Validaciones;

@ManagedBean
@Scope(value="session")
@Component
public class AreasJSFBean extends FindCrudBeanBase
{

    public AreasJSFBean()
    {
        listaEstados = new ArrayList();
    }

    @PostConstruct
    public void init()
    {
        especialidadFiltro = new Especialidad();
        subEspecialidadFiltro = new SubEspecialidad();
        procedimientoFiltro = new Procedimiento();
        validar = new Validaciones();
        nuevaEspecialidad = new EspecialidadDTO();
        especialidadReferenciada = new Especialidad();
        nuevaSubEspecialidad = new SubEspecialidadDTO();
        subEspecialidadReferenciada = new SubEspecialidad();
        nuevaProcedimiento = new ProcedimientoDTO();
        listaEspecialidads = especialidadService.buscarEspecialidad(especialidadFiltro);
        listaAuxEspecialidadsValidacion = copiarListaEspecialidads(listaEspecialidads);
        listaSubEspecialidads = new ArrayList();
        listaAuxSubEspecialidadValidacion = new ArrayList();
        listaProcedimientos = new ArrayList();
        listaEstados = EstadosEnum.findAll();
        mostrarLista(true, false, false);
        buscarPorEspecialidad = Boolean.valueOf(false);
        buscarPorSubEspecialidad = Boolean.valueOf(false);
    }

    public void setCursorTableNull()
    {
        especialidadSeleccionada = new EspecialidadDTO();
        subEspecialidadSeleccionada = new SubEspecialidadDTO();
        procedimientoSeleccionado = new ProcedimientoDTO();
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionEvent)
    {
        tipoEntidad = getParam(actionEvent, "tipoEntidad");
        if(especialidadFiltro == null)
        {
            setBuscarPorEspecialidad(Boolean.valueOf(false));
            especialidadFiltro = new Especialidad();
        }
        if(subEspecialidadFiltro == null)
        {
            setBuscarPorSubEspecialidad(Boolean.valueOf(false));
            subEspecialidadFiltro = new SubEspecialidad();
        }
        if(procedimientoFiltro == null)
            procedimientoFiltro = new Procedimiento();
        if(estadoEnum != null)
        {
            especialidadFiltro.setEstadoEspecialidad(estadoEnum.getCode());
            subEspecialidadFiltro.setEstadoSubEspecialidad(estadoEnum.getCode());
            procedimientoFiltro.setEstadoProcedimiento(estadoEnum.getCode());
        }
        if(StringUtils.isNotBlank(tipoEntidad))
        {
            if(tipoEntidad.equals("area"))
            {
                listaEspecialidads = especialidadService.buscarEspecialidad(especialidadFiltro);
                listaAuxEspecialidadsValidacion = copiarListaEspecialidads(listaEspecialidads);
                mostrarLista(true, false, false);
            }
            if(tipoEntidad.equals("subarea"))
            {
                subEspecialidadFiltro.setEspecialidad(especialidadFiltro);
                listaSubEspecialidads = subEspecialidadService.buscarSubEspecialidad((SubEspecialidadDTO)mapperService.map(subEspecialidadFiltro, infraestructura/dto/SubEspecialidadDTO));
                listaAuxSubEspecialidadValidacion = copiarListaSubEspecialidads(listaSubEspecialidads);
                mostrarLista(false, true, false);
            }
            if(tipoEntidad.equals("especialidad"))
            {
                procedimientoFiltro.setSubEspecialidad(subEspecialidadFiltro);
                listaProcedimientos = procedimientoService.findByFilter((ProcedimientoDTO)mapperService.map(procedimientoFiltro, infraestructura/dto/ProcedimientoDTO));
                mostrarLista(false, false, true);
            }
        } else
        {
            if(StringUtils.isNotBlank(procedimientoFiltro.getNombreProcedimiento()))
            {
                listaProcedimientos = procedimientoService.findByFilter((ProcedimientoDTO)mapperService.map(procedimientoFiltro, infraestructura/dto/ProcedimientoDTO));
                mostrarLista(false, false, true);
                return;
            }
            if(!buscarPorEspecialidad.booleanValue() && !buscarPorSubEspecialidad.booleanValue())
            {
                listaEspecialidads = especialidadService.buscarEspecialidad(especialidadFiltro);
                listaAuxEspecialidadsValidacion = copiarListaEspecialidads(listaEspecialidads);
                mostrarLista(true, false, false);
            }
            if(buscarPorEspecialidad.booleanValue() && buscarPorSubEspecialidad.booleanValue())
            {
                procedimientoFiltro.setSubEspecialidad(subEspecialidadFiltro);
                listaProcedimientos = procedimientoService.findByFilter((ProcedimientoDTO)mapperService.map(procedimientoFiltro, infraestructura/dto/ProcedimientoDTO));
                mostrarLista(false, false, true);
                return;
            }
            if(buscarPorSubEspecialidad.booleanValue())
            {
                procedimientoFiltro.setSubEspecialidad(subEspecialidadFiltro);
                listaProcedimientos = procedimientoService.findByFilter((ProcedimientoDTO)mapperService.map(procedimientoFiltro, infraestructura/dto/ProcedimientoDTO));
                mostrarLista(false, false, true);
                return;
            }
            if(buscarPorEspecialidad.booleanValue())
            {
                subEspecialidadFiltro.setEspecialidad(especialidadFiltro);
                listaSubEspecialidads = subEspecialidadService.buscarSubEspecialidad((SubEspecialidadDTO)mapperService.map(subEspecialidadFiltro, infraestructura/dto/SubEspecialidadDTO));
                listaAuxSubEspecialidadValidacion = copiarListaSubEspecialidads(listaSubEspecialidads);
                mostrarLista(false, true, false);
            }
        }
    }

    public void onClean(ActionEvent actionEvent)
    {
        listaEspecialidads = new ArrayList();
        listaAuxEspecialidadsValidacion.clear();
        listaSubEspecialidads = new ArrayList();
        listaAuxEspecialidadsValidacion.clear();
        listaProcedimientos = new ArrayList();
        especialidadFiltro = new Especialidad();
        subEspecialidadFiltro = new SubEspecialidad();
        procedimientoFiltro = new Procedimiento();
        nuevaEspecialidad = new EspecialidadDTO();
        especialidadReferenciada = new Especialidad();
        nuevaSubEspecialidad = new SubEspecialidadDTO();
        subEspecialidadReferenciada = new SubEspecialidad();
        nuevaProcedimiento = new ProcedimientoDTO();
        setBuscarPorEspecialidad(Boolean.valueOf(false));
        setBuscarPorSubEspecialidad(Boolean.valueOf(false));
    }

    public void onPersist(ActionEvent actionEvent)
    {
        tipoEntidad = getParam(actionEvent, "tipoEntidad");
        String mensaje[] = {
            ""
        };
        if(tipoEntidad.equals("area"))
        {
            boolean estaOk = validarDatosEspecialidad(nuevaEspecialidad, mensaje);
            if(!estaOk)
            {
                setSuccess(false);
                addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
                return;
            }
            especialidadFiltro = new Especialidad();
            subEspecialidadFiltro = new SubEspecialidad();
            procedimientoFiltro = new Procedimiento();
            nuevaEspecialidad.setEstadoEspecialidad(nuevaEspecialidad.getEstadoEnum().getCode());
            especialidadService.guardarEspecialidad(nuevaEspecialidad);
            setSuccess(true);
            especialidadSeleccionada = nuevaEspecialidad;
        }
        if(tipoEntidad.equals("subarea"))
        {
            boolean estaOkSubEspecialidad = validarDatosSubEspecialidad(nuevaSubEspecialidad, mensaje, especialidadReferenciada);
            if(!estaOkSubEspecialidad)
            {
                setSuccess(false);
                addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
                return;
            }
            EspecialidadDTO areaDTO = (EspecialidadDTO)mapperService.map(especialidadReferenciada, infraestructura/dto/EspecialidadDTO);
            especialidadFiltro = especialidadReferenciada;
            subEspecialidadFiltro = new SubEspecialidad();
            procedimientoFiltro = new Procedimiento();
            nuevaSubEspecialidad.setEstadoSubEspecialidad(nuevaSubEspecialidad.getEstadoEnum().getCode());
            areaDTO.agregarSubEspecialidad(nuevaSubEspecialidad);
            especialidadService.guardarEspecialidad(areaDTO);
            setSuccess(true);
            subEspecialidadSeleccionada = nuevaSubEspecialidad;
        }
        if(tipoEntidad.equals("especialidad"))
        {
            boolean estaOkProcedimiento = validarDatosProcedimiento(nuevaProcedimiento, mensaje, especialidadReferenciada, subEspecialidadReferenciada);
            if(!estaOkProcedimiento)
            {
                setSuccess(false);
                addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
                return;
            }
            procedimientoFiltro = new Procedimiento();
            Procedimiento nuevoProcedimiento = new Procedimiento();
            nuevoProcedimiento.setEstadoProcedimiento(nuevaProcedimiento.getEstadoEnum().getCode());
            nuevoProcedimiento.setNombreProcedimiento(nuevaProcedimiento.getNombreProcedimiento());
            subEspecialidadReferenciada.agregarProcedimientos(nuevoProcedimiento);
            especialidadRepository.merge(especialidadReferenciada);
            setSuccess(true);
            procedimientoSeleccionado = nuevaProcedimiento;
            subEspecialidadFiltro = subEspecialidadReferenciada;
            especialidadFiltro = especialidadReferenciada;
        }
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent event)
    {
        especialidadReferenciada = new Especialidad();
        subEspecialidadReferenciada = new SubEspecialidad();
        nuevaEspecialidad = new EspecialidadDTO();
        nuevaSubEspecialidad = new SubEspecialidadDTO();
        nuevaProcedimiento = new ProcedimientoDTO();
    }

    private void mostrarLista(boolean muestraListaEspecialidads, boolean muestraListaSubEspecialidads, boolean muestraListaProcedimientoes)
    {
        mostrarListaEspecialidads = muestraListaEspecialidads;
        mostrarListaSubEspecialidads = muestraListaSubEspecialidads;
        mostrarListaProcedimientoes = muestraListaProcedimientoes;
    }

    public List completeEspecialidad(String query)
    {
        List items = new ArrayList();
        query = query.trim();
        if(query.length() > 0)
        {
            items = especialidadRepository.completeEspecialidad(query.toUpperCase());
            if(items.isEmpty())
                items = especialidadRepository.completeEspecialidad(null);
        }
        return items;
    }

    public List completeSubEspecialidad(String query)
    {
        List items = new ArrayList();
        query = query.trim();
        if(query.length() > 0)
            if(especialidadFiltro != null && especialidadFiltro.getIdEspecialidad() != null)
            {
                items = subEspecialidadRepository.completeSubEspecialidad(query.toUpperCase(), especialidadFiltro.getIdEspecialidad());
                if(items.isEmpty())
                    items = subEspecialidadRepository.completeSubEspecialidad(null, especialidadFiltro.getIdEspecialidad());
            } else
            {
                items = subEspecialidadRepository.completeSubEspecialidad(query.toUpperCase(), null);
                if(items.isEmpty())
                    items = subEspecialidadRepository.completeSubEspecialidad(null, null);
            }
        return items;
    }

    public void limpiarFiltrosPorEspecialidad(SelectEvent event)
    {
        String quehacer = getParam(event, "quehacer");
        if(StringUtils.isNotBlank(quehacer))
        {
            especialidadReferenciada = (Especialidad)event.getObject();
            especialidadFiltro = especialidadReferenciada;
            setBuscarPorEspecialidad(Boolean.valueOf(true));
        } else
        {
            especialidadFiltro = (Especialidad)event.getObject();
            subEspecialidadFiltro = new SubEspecialidad();
            procedimientoFiltro = new Procedimiento();
            setBuscarPorEspecialidad(Boolean.valueOf(true));
        }
    }

    public void limpiarFiltrosPorSubEspecialidad(SelectEvent event)
    {
        if(subEspecialidadReferenciada != null && StringUtils.isNotBlank(subEspecialidadReferenciada.getNombreSubEspecialidad()))
        {
            subEspecialidadReferenciada = (SubEspecialidad)event.getObject();
            subEspecialidadFiltro = subEspecialidadReferenciada;
            setBuscarPorSubEspecialidad(Boolean.valueOf(true));
        } else
        {
            SubEspecialidad subEspecialidad = (SubEspecialidad)event.getObject();
            subEspecialidadFiltro = subEspecialidad;
            procedimientoFiltro = new Procedimiento();
            setBuscarPorSubEspecialidad(Boolean.valueOf(true));
        }
    }

    public void onModify(RowEditEvent event)
    {
        if(event.getObject() instanceof EspecialidadDTO)
        {
            EspecialidadDTO object = (EspecialidadDTO)event.getObject();
            object.setEstadoEspecialidad(object.getEstadoEnum().getCode());
            String mensaje[] = new String[1];
            if(!validarDatosEspecialidad(object, mensaje))
            {
                int index = listaEspecialidads.indexOf(object);
                object.setNombreEspecialidad(((EspecialidadDTO)listaAuxEspecialidadsValidacion.get(index)).getNombreEspecialidad());
                addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
                return;
            } else
            {
                especialidadService.guardarEspecialidad(object);
                addMessage("El registro se actualiz\363 correctamente", FacesMessage.SEVERITY_INFO, new Object[0]);
                return;
            }
        }
        if(event.getObject() instanceof SubEspecialidadDTO)
        {
            SubEspecialidadDTO object = (SubEspecialidadDTO)event.getObject();
            String mensaje[] = new String[1];
            if(!validarDatosSubEspecialidad(object, mensaje, (Especialidad)mapperService.map(object.getEspecialidad(), paquete/entities/Especialidad)))
            {
                int index = listaSubEspecialidads.indexOf(object);
                object.setNombreSubEspecialidad(((SubEspecialidadDTO)listaAuxSubEspecialidadValidacion.get(index)).getNombreSubEspecialidad());
                addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
                return;
            } else
            {
                object.setEstadoSubEspecialidad(object.getEstadoEnum().getCode());
                subEspecialidadService.guardarSubEspecialidad(object);
                addMessage("El registro se actualiz\363 correctamente", FacesMessage.SEVERITY_INFO, new Object[0]);
                return;
            }
        }
        if(event.getObject() instanceof ProcedimientoDTO)
        {
            ProcedimientoDTO object = (ProcedimientoDTO)event.getObject();
            object.setEstadoProcedimiento(object.getEstadoEnum().getCode());
            procedimientoService.guardarProcedimiento(object);
        }
    }

    private boolean validarDatosEspecialidad(EspecialidadDTO area, String mensaje[])
    {
        if(area == null)
        {
            mensaje[0] = "Falta rellenar alg\372n campo obligatorio";
            return false;
        }
        String nombre = area.getNombreEspecialidad();
        if(!validar.chekeoPalabras(nombre))
        {
            mensaje[0] = "Para el \"nombre\" de la especialidad solo est\341n permitidos caracteres de aA-zZ y n\372meros.No s\355mbolos. Campo obligatorio";
            return false;
        }
        if(area.getIdEspecialidad() != null)
        {
            if(especialidadService.existeEspecialidadRepetida(nombre, (new StringBuilder()).append(area.getIdEspecialidad()).append("").toString()))
            {
                mensaje[0] = "El nombre de la especialidad especificado ya existe, por favor proporcione uno nuevo";
                return false;
            }
        } else
        if(especialidadService.existeEspecialidadRepetida(nombre))
        {
            mensaje[0] = "El nombre de especialidad especificado ya existe, por favor proporcione uno nuevo";
            return false;
        }
        if(subEspecialidadService.existeSubEspecialidadRepetida(nombre))
        {
            mensaje[0] = "El nombre de especialidad especificado ya existe en alguna SUBESPECIALIDAD, por favor proporcione uno nuevo";
            return false;
        }
        if(procedimientoService.existeProcedimientoRepetido(nombre))
        {
            mensaje[0] = "El nombre de especialidad especificado ya existe en alg\372n PROCEDOMIENTO, por favor proporcione uno nuevo";
            return false;
        } else
        {
            return true;
        }
    }

    private boolean validarDatosSubEspecialidad(SubEspecialidadDTO subEspecialidad, String mensaje[], Especialidad especialidadReferenciada)
    {
        if(subEspecialidad == null || especialidadReferenciada == null)
        {
            mensaje[0] = "Falta rellenar alg\372n campo obligatorio";
            return false;
        }
        String nombre = subEspecialidad.getNombreSubEspecialidad();
        if(!especialidadService.existeEspecialidadRepetida(especialidadReferenciada.getNombreEspecialidad()))
        {
            mensaje[0] = "El nombre de la especialidad especificada NO existe, por favor proporcione uno nuevo";
            return false;
        }
        if(!validar.chekeoPalabras(nombre))
        {
            mensaje[0] = "Para \"nombre\" de la subespecialidad solo est\341n permitidos caracteres de aA-zZ y n\372meros.No s\355mbolos. Campo obligatorio";
            return false;
        }
        if(subEspecialidad.getIdSubEspecialidad() != null)
        {
            if(subEspecialidadService.existeSubEspecialidadRepetida(nombre, (new StringBuilder()).append(subEspecialidad.getIdSubEspecialidad().intValue()).append("").toString()))
            {
                mensaje[0] = "El nombre de la subespecialidad especificado ya existe, por favor proporcione uno nuevo";
                return false;
            }
        } else
        if(subEspecialidadService.existeSubEspecialidadRepetida(nombre))
        {
            mensaje[0] = "El nombre de subespecialidad especificado ya existe, por favor proporcione uno nuevo";
            return false;
        }
        if(especialidadService.existeEspecialidadRepetida(nombre))
        {
            mensaje[0] = "El nombre de subespecialidad especificado ya existe en alguna ESPECIALIDAD, por favor proporcione uno nuevo";
            return false;
        }
        if(procedimientoService.existeProcedimientoRepetido(nombre))
        {
            mensaje[0] = "El nombre de subespecialidad especificado ya existe en alg\372n PROCEDIMIENTO, por favor proporcione uno nuevo";
            return false;
        } else
        {
            return true;
        }
    }

    private boolean validarDatosProcedimiento(ProcedimientoDTO especialidad, String mensaje[], Especialidad especialidadReferenciada, SubEspecialidad subEspecialidadReferenciada)
    {
        if(subEspecialidadReferenciada == null || especialidadReferenciada == null)
        {
            mensaje[0] = "Falta rellenar alg\372n campo obligatorio especialidad";
            return false;
        }
        String nombre = especialidad.getNombreProcedimiento();
        if(!especialidadService.existeEspecialidadRepetida(especialidadReferenciada.getNombreEspecialidad()))
        {
            mensaje[0] = "El nombre del procedimiento especificado NO existe, por favor proporcione uno nuevo";
            return false;
        }
        if(subEspecialidadReferenciada.getNombreSubEspecialidad() != null && !subEspecialidadService.existeSubEspecialidadRepetida(subEspecialidadReferenciada.getNombreSubEspecialidad()))
        {
            mensaje[0] = "El nombre del procedimiento especificado NO existe, por favor proporcione uno nuevo";
            return false;
        }
        if(!validar.chekeoPalabras(nombre))
        {
            mensaje[0] = "Para \"nombre\" del procedimiento solo est\341n permitidos caracteres de aA-zZ y n\372meros.No s\355mbolos. Campo obligatorio";
            return false;
        }
        if(especialidad.getIdProcedimiento() != null)
        {
            if(procedimientoService.existeProcedimientoRepetido(nombre, (new StringBuilder()).append(especialidad.getIdProcedimiento().intValue()).append("").toString()))
            {
                mensaje[0] = "El nombre del procedimiento especificado ya existe, por favor proporcione uno nuevo";
                return false;
            }
        } else
        if(procedimientoService.existeProcedimientoRepetido(nombre))
        {
            mensaje[0] = "El nombre del procedimiento especificado ya existe, por favor proporcione uno nuevo";
            return false;
        }
        if(especialidadService.existeEspecialidadRepetida(nombre))
        {
            mensaje[0] = "El nombre del procedimiento especificado ya existe en alguna ESPECIALIDAD, por favor proporcione uno nuevo";
            return false;
        }
        if(subEspecialidadService.existeSubEspecialidadRepetida(nombre))
        {
            mensaje[0] = "El nombre del procedimiento especificado ya existe en alguna SUBESPECIALIDAD, por favor proporcione uno nuevo";
            return false;
        } else
        {
            return true;
        }
    }

    private List copiarListaEspecialidads(List listaEspecialidads)
    {
        List listaCopiada = new ArrayList();
        EspecialidadDTO nuevoElemento;
        for(Iterator i$ = listaEspecialidads.iterator(); i$.hasNext(); listaCopiada.add(nuevoElemento))
        {
            EspecialidadDTO dto = (EspecialidadDTO)i$.next();
            nuevoElemento = new EspecialidadDTO();
            nuevoElemento.setIdEspecialidad(dto.getIdEspecialidad());
            nuevoElemento.setNombreEspecialidad(dto.getNombreEspecialidad());
        }

        return listaCopiada;
    }

    private List copiarListaSubEspecialidads(List listaSubEspecialidads)
    {
        List listaCopiada = new ArrayList();
        SubEspecialidadDTO nuevoElemento;
        for(Iterator i$ = listaSubEspecialidads.iterator(); i$.hasNext(); listaCopiada.add(nuevoElemento))
        {
            SubEspecialidadDTO dto = (SubEspecialidadDTO)i$.next();
            nuevoElemento = new SubEspecialidadDTO();
            nuevoElemento.setIdSubEspecialidad(dto.getIdSubEspecialidad());
            nuevoElemento.setNombreSubEspecialidad(dto.getNombreSubEspecialidad());
        }

        return listaCopiada;
    }

    public void toIndex(ActionEvent e)
    {
        redireccionar("../index.jsf");
    }

    public Especialidad getEspecialidadFiltro()
    {
        return especialidadFiltro;
    }

    public void setEspecialidadFiltro(Especialidad areaFiltro)
    {
        especialidadFiltro = areaFiltro;
    }

    public SubEspecialidad getSubEspecialidadFiltro()
    {
        return subEspecialidadFiltro;
    }

    public void setSubEspecialidadFiltro(SubEspecialidad subEspecialidadFiltro)
    {
        this.subEspecialidadFiltro = subEspecialidadFiltro;
    }

    public Procedimiento getProcedimientoFiltro()
    {
        return procedimientoFiltro;
    }

    public void setProcedimientoFiltro(Procedimiento procedimientoFiltro)
    {
        this.procedimientoFiltro = procedimientoFiltro;
    }

    public EspecialidadDTO getNuevaEspecialidad()
    {
        return nuevaEspecialidad;
    }

    public void setNuevaEspecialidad(EspecialidadDTO nuevaEspecialidad)
    {
        this.nuevaEspecialidad = nuevaEspecialidad;
    }

    public SubEspecialidadDTO getNuevaSubEspecialidad()
    {
        return nuevaSubEspecialidad;
    }

    public void setNuevaSubEspecialidad(SubEspecialidadDTO nuevaSubEspecialidad)
    {
        this.nuevaSubEspecialidad = nuevaSubEspecialidad;
    }

    public ProcedimientoDTO getNuevaProcedimiento()
    {
        return nuevaProcedimiento;
    }

    public void setNuevaProcedimiento(ProcedimientoDTO nuevaProcedimiento)
    {
        this.nuevaProcedimiento = nuevaProcedimiento;
    }

    public List getListaEspecialidads()
    {
        return listaEspecialidads;
    }

    public void setListaEspecialidads(List listaEspecialidads)
    {
        this.listaEspecialidads = listaEspecialidads;
    }

    public List getListaSubEspecialidads()
    {
        return listaSubEspecialidads;
    }

    public void setListaSubEspecialidads(List listaSubEspecialidads)
    {
        this.listaSubEspecialidads = listaSubEspecialidads;
    }

    public List getListaProcedimientoes()
    {
        return listaProcedimientos;
    }

    public void setListaProcedimientoes(List listaProcedimientoes)
    {
        listaProcedimientos = listaProcedimientoes;
    }

    public boolean isMostrarListaEspecialidads()
    {
        return mostrarListaEspecialidads;
    }

    public void setMostrarListaEspecialidads(boolean mostrarListaEspecialidads)
    {
        this.mostrarListaEspecialidads = mostrarListaEspecialidads;
    }

    public boolean isMostrarListaSubEspecialidads()
    {
        return mostrarListaSubEspecialidads;
    }

    public void setMostrarListaSubEspecialidads(boolean mostrarListaSubEspecialidads)
    {
        this.mostrarListaSubEspecialidads = mostrarListaSubEspecialidads;
    }

    public boolean isMostrarListaProcedimientoes()
    {
        return mostrarListaProcedimientoes;
    }

    public void setMostrarListaProcedimientoes(boolean mostrarListaProcedimientoes)
    {
        this.mostrarListaProcedimientoes = mostrarListaProcedimientoes;
    }

    public Especialidad getEspecialidadReferenciada()
    {
        return especialidadReferenciada;
    }

    public void setEspecialidadReferenciada(Especialidad areaReferenciada)
    {
        especialidadReferenciada = areaReferenciada;
    }

    public SubEspecialidad getSubEspecialidadReferenciada()
    {
        return subEspecialidadReferenciada;
    }

    public void setSubEspecialidadReferenciada(SubEspecialidad subEspecialidadReferenciada)
    {
        this.subEspecialidadReferenciada = subEspecialidadReferenciada;
    }

    public List getListaEstados()
    {
        return listaEstados;
    }

    public void setListaEstados(List listaEstados)
    {
        this.listaEstados = listaEstados;
    }

    public void setTipoEntidad(String tipoEntidad)
    {
        this.tipoEntidad = tipoEntidad;
    }

    public EspecialidadDTO getEspecialidadSeleccionada()
    {
        return especialidadSeleccionada;
    }

    public void setEspecialidadSeleccionada(EspecialidadDTO areaSeleccionada)
    {
        especialidadSeleccionada = areaSeleccionada;
    }

    public SubEspecialidadDTO getSubEspecialidadSeleccionada()
    {
        return subEspecialidadSeleccionada;
    }

    public void setSubEspecialidadSeleccionada(SubEspecialidadDTO subEspecialidadSeleccionada)
    {
        this.subEspecialidadSeleccionada = subEspecialidadSeleccionada;
    }

    public ProcedimientoDTO getProcedimientoSeleccionada()
    {
        return procedimientoSeleccionado;
    }

    public void setProcedimientoSeleccionada(ProcedimientoDTO especialidadSeleccionada)
    {
        procedimientoSeleccionado = especialidadSeleccionada;
    }

    public Boolean getBuscarPorEspecialidad()
    {
        return buscarPorEspecialidad;
    }

    public void setBuscarPorEspecialidad(Boolean buscarPorEspecialidad)
    {
        this.buscarPorEspecialidad = buscarPorEspecialidad;
    }

    public Boolean getBuscarPorSubEspecialidad()
    {
        return buscarPorSubEspecialidad;
    }

    public void setBuscarPorSubEspecialidad(Boolean buscarPorSubEspecialidad)
    {
        this.buscarPorSubEspecialidad = buscarPorSubEspecialidad;
    }

    public EstadosEnum getEstadoEnum()
    {
        return estadoEnum;
    }

    public void setEstadoEnum(EstadosEnum estadoEnum)
    {
        this.estadoEnum = estadoEnum;
    }

    @Autowired
    EspecialidadService especialidadService;
    @Autowired
    SubEspecialidadService subEspecialidadService;
    @Autowired
    EspecialidadRepository especialidadRepository;
    @Autowired
    SubEspecialidadRepository subEspecialidadRepository;
    @Autowired
    ProcedimientoService procedimientoService;
    @Autowired
    private DTOMapperService mapperService;
    private Especialidad especialidadFiltro;
    private SubEspecialidad subEspecialidadFiltro;
    private Procedimiento procedimientoFiltro;
    private EspecialidadDTO nuevaEspecialidad;
    private Especialidad especialidadReferenciada;
    private SubEspecialidadDTO nuevaSubEspecialidad;
    private SubEspecialidad subEspecialidadReferenciada;
    private ProcedimientoDTO nuevaProcedimiento;
    private List listaEspecialidads;
    private List listaAuxEspecialidadsValidacion;
    private EspecialidadDTO especialidadSeleccionada;
    private List listaSubEspecialidads;
    private List listaAuxSubEspecialidadValidacion;
    private SubEspecialidadDTO subEspecialidadSeleccionada;
    private List listaProcedimientos;
    private ProcedimientoDTO procedimientoSeleccionado;
    private boolean mostrarListaEspecialidads;
    private boolean mostrarListaSubEspecialidads;
    private boolean mostrarListaProcedimientoes;
    private List listaEstados;
    private EstadosEnum estadoEnum;
    private String tipoEntidad;
    private Boolean buscarPorEspecialidad;
    private Boolean buscarPorSubEspecialidad;
    private Validaciones validar;
}
