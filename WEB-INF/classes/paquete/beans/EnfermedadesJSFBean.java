// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:33:31 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadesJSFBean.java

package paquete.beans;

import infraestructura.dto.EnfermedadDTO;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.ActionEvent;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import paquete.entities.Enfermedad;
import paquete.lazyModel.EnfermedadLazyModel;
import paquete.repositorios.EnfermedadRepository;
import paquete.service.EnfermedadService;
import util.FindCrudBeanBase;

@ManagedBean
@Scope(value="session")
@Component
public class EnfermedadesJSFBean extends FindCrudBeanBase
{

    public EnfermedadesJSFBean()
    {
    }

    @PostConstruct
    public void init()
    {
        enfermedadFiltro = new EnfermedadDTO();
        capituloFiltro = new Enfermedad();
        enfermedadLazyModel = new EnfermedadLazyModel(enfermedadFiltro, capituloFiltro, enfermedadService, Boolean.valueOf(true));
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionEvent)
    {
        enfermedadLazyModel = new EnfermedadLazyModel(enfermedadFiltro, capituloFiltro, enfermedadService, Boolean.valueOf(true));
    }

    public void onClean(ActionEvent actionEvent)
    {
        enfermedadLazyModel = new EnfermedadLazyModel(enfermedadFiltro, capituloFiltro, enfermedadService, Boolean.valueOf(false));
        enfermedadFiltro = new EnfermedadDTO();
        capituloFiltro = new Enfermedad();
    }

    public void onPersist(ActionEvent actionEvent)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent event)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void handleSelect(SelectEvent event)
    {
        enfermedadFiltro = (EnfermedadDTO)event.getObject();
    }

    public void handleEnfermedades(SelectEvent event)
    {
        enfermedadFiltro = (EnfermedadDTO)event.getObject();
    }

    public List comboEnfermedades(String query)
    {
        List items = new ArrayList();
        query = query.trim();
        Enfermedad filtro = new Enfermedad();
        filtro.setGrupoEnfermedad(query);
        if(query.length() > 0)
            items = enfermedadRepository.findByFilter(filtro);
        return items;
    }

    public List completeEnfermedad(String query)
    {
        List items = new ArrayList();
        query = query.trim();
        Enfermedad filtro = new Enfermedad();
        filtro.setIdEnfermedad(query);
        filtro.setNombreEnfermedad(query);
        if(query.length() > 0)
            items = enfermedadRepository.findByFilter(filtro);
        return items;
    }

    public Enfermedad getCapituloFiltro()
    {
        return capituloFiltro;
    }

    public void setCapituloFiltro(Enfermedad capituloFiltro)
    {
        this.capituloFiltro = capituloFiltro;
    }

    public void toIndex(ActionEvent e)
    {
        redireccionar("../index.jsf");
    }

    public EnfermedadDTO getEnfermedadFiltro()
    {
        return enfermedadFiltro;
    }

    public void setEnfermedadFiltro(EnfermedadDTO enfermedadFiltro)
    {
        this.enfermedadFiltro = enfermedadFiltro;
    }

    public LazyDataModel getEnfermedadLazyModel()
    {
        return enfermedadLazyModel;
    }

    public void setEnfermedadLazyModel(LazyDataModel enfermedadLazyModel)
    {
        this.enfermedadLazyModel = enfermedadLazyModel;
    }

    @Autowired
    EnfermedadService enfermedadService;
    @Autowired
    EnfermedadRepository enfermedadRepository;
    private LazyDataModel enfermedadLazyModel;
    private EnfermedadDTO enfermedadFiltro;
    private Enfermedad capituloFiltro;
}
