// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:33:41 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriasJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import infraestructura.enums.*;
import java.io.PrintStream;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import paquete.repositorios.*;
import paquete.service.*;
import util.FindCrudBeanBase;
import util.Validaciones;

@ManagedBean
@Scope(value="session")
@Component
public class HistoriasJSFBean extends FindCrudBeanBase
{

    public HistoriasJSFBean()
    {
        listaEstadosPaciente = new ArrayList();
        listaSexoPersonaEnum = new ArrayList();
        listaRespuestas = new ArrayList();
        medico = "M";
        tecnica = "T";
        laboratorista = "L";
    }

    @PostConstruct
    public void init()
    {
        mostrarDetalle = true;
        validar = new Validaciones();
        listaPersonas = new ArrayList();
        listaEstadosPaciente = EstadoPacienteEnum.findAll();
        listaSexoPersonaEnum = SexoPersonaEnum.findAll();
        idPersonaSeleccionada = null;
        inicializaPersonaSeleccionada();
        inicializaFiltro();
        listaAtenciones = new ArrayList();
        atencionSeleccionada = new AtencionDTO();
        MAX = -1;
        ACTUAL = -1;
        listaTrabajos = new ArrayList();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map sessionMap = externalContext.getSessionMap();
        Boolean mostrarDialogDeTrabajo = (Boolean)sessionMap.get("mostrarDialogTrabajo");
        if(getUsuario().isEsEnfermeria() || getUsuario().isEsLaboratorista() || getUsuario().isEsMedico())
            if(getUsuario().getEmpleado().getConjuntoTrabajos().size() > 1 && mostrarDialogDeTrabajo == null)
            {
                Iterator i$ = getUsuario().getEmpleado().getConjuntoTrabajos().iterator();
                do
                {
                    if(!i$.hasNext())
                        break;
                    TrabajoDTO trabajo = (TrabajoDTO)i$.next();
                    if(trabajo.getEstadoTrabajo().equals(EstadoTrabajoEnum.ACTIVO.getCode()))
                    {
                        listaTrabajos.add(trabajo);
                        setMostrarDialogTrabajo(true);
                    }
                } while(true);
            } else
            {
                setMostrarDialogTrabajo(false);
            }
        if(getUsuario().isEsMedico() && getUsuario().getEmpleado().getConjuntoTrabajos().size() >= 1)
        {
            TrabajoDTO trabajo = (TrabajoDTO)getUsuario().getEmpleado().getConjuntoTrabajos().toArray()[0];
            if(trabajo.getEspecialidad().getNombreEspecialidad().substring(0, 6).toLowerCase().equals("cardio"))
                sessionMap.put("mostrarBoton", Boolean.valueOf(true));
            else
                sessionMap.put("mostrarBoton", Boolean.valueOf(false));
        }
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionevent)
    {
    }

    public void onClean(ActionEvent actionEvent)
    {
        inicializaFiltro();
        inicializaPersonaSeleccionada();
        listaAtenciones.clear();
        listaPersonas.clear();
        idPersonaSeleccionada = null;
        listaPersonas.clear();
        inicializaFiltro();
        listaAtenciones = new ArrayList();
        listaRespuestas = new ArrayList();
        personaSeleccionada = new PersonaDTO();
        atencionSeleccionada = new AtencionDTO();
        MAX = -1;
        ACTUAL = -1;
    }

    public void onPersist(ActionEvent actionevent)
    {
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent event)
    {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map sessionMap = externalContext.getSessionMap();
        String accion = sessionMap.get("accion").toString();
        if(StringUtils.isNotBlank(accion) && accion.equals("buscar"))
        {
            realizaBusquedaAtenciones(null);
            atencionSeleccionada = (AtencionDTO)sessionMap.get("atencionGuardada");
            sessionMap.put("accion", "");
            sessionMap.put("atencionGuardada", null);
        }
    }

    public List separaIdsCompuesto(String compuesto, String separador)
    {
        List lista = new ArrayList();
        for(StringTokenizer st = new StringTokenizer(compuesto, separador); st.hasMoreTokens(); lista.add(st.nextToken()));
        return lista;
    }

    private void inicializaPersonaSeleccionada()
    {
        personaSeleccionada = new PersonaDTO();
        pacienteSeleccionado = new PacienteDTO();
        personaSeleccionada.setPaciente(pacienteSeleccionado);
        personaSeleccionada.setTipoDocumentoPersona("1");
    }

    public void inicializaFiltro()
    {
        personaFiltro = new PersonaDTO();
        pacienteFiltro = new PacienteDTO();
        pacienteFiltro.setEstadoPaciente("A");
        historiaFiltro = new HistoriaDTO();
        pacienteFiltro.setHistoria(historiaFiltro);
        personaFiltro.setPaciente(pacienteFiltro);
    }

    public void realizaBusquedaAtenciones(ActionEvent actionEvent)
    {
        if(idPersonaSeleccionada == null || idPersonaSeleccionada.intValue() < 1)
        {
            addMessage("Debe seleccionar un paciente sugerido de \"Coincidencias\".", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        }
        if(cantidadAtencionesSeleccionada.equalsIgnoreCase("-1"))
        {
            addMessage("Debe especificar una cantidad de atenciones a buscar.", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        } else
        {
            HistoriaDTO historia = historiaRepository.traeHistoriaPorPersonaId(idPersonaSeleccionada);
            listaAtenciones = atencionService.traeAtencionesHistoria(historia.getIdHistoria(), cantidadAtencionesSeleccionada);
            MAX = listaAtenciones.size();
            return;
        }
    }

    public void realizaBusquedaPacientes(ActionEvent actionEvent)
    {
        if(personaFiltro.getPaciente().getEstadoPacienteEnum() == null)
            personaFiltro.getPaciente().setEstadoPaciente("");
        else
            personaFiltro.getPaciente().setEstadoPaciente(personaFiltro.getPaciente().getEstadoPacienteEnum().getCode());
        if(personaFiltro.getSexoPersonaEnum() == null)
            personaFiltro.setSexoPersona("");
        else
            personaFiltro.setSexoPersona(personaFiltro.getSexoPersonaEnum().getCode());
        listaPersonas = personaService.findByFilter(personaFiltro);
        if(listaPersonas.isEmpty())
            addMessage("No se encontraron resultados", FacesMessage.SEVERITY_INFO, new Object[0]);
    }

    public void depurar(String mensaje)
    {
        System.out.println((new StringBuilder()).append("####").append(mensaje).toString());
    }

    public void cargaDetallesDeAtencion(ActionEvent event)
    {
        if(atencionSeleccionada == null || atencionSeleccionada.getIdAtencion() == null)
        {
            mostrarDetalle = false;
            addMessage("Debe seleccionar una atenci\363n", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        } else
        {
            PersonaDTO pfiltro = new PersonaDTO();
            pfiltro.setIdPersona(idPersonaSeleccionada);
            personaSeleccionada = (PersonaDTO)personaService.findByFilter(pfiltro).get(0);
            listaRespuestas = atencionService.traeRespuestasDeAtencion(atencionSeleccionada.getIdAtencion());
            List enfermedades = enfermedadRepository.traeEnfermedadesAtencion(atencionSeleccionada.getIdAtencion());
            atencionSeleccionada.setListaEnfermedades(enfermedades);
            atencionSeleccionada.setTieneEnfermedades(enfermedades.size() > 0);
            mostrarDetalle = true;
            ACTUAL = listaAtenciones.indexOf(atencionSeleccionada);
            executeModal("detailDialog", mostrarDetalle);
            showNewDialog(event);
            return;
        }
    }

    public void toIndex(ActionEvent e)
    {
        redireccionar("../index.jsf");
    }

    public void muestroEjemplo(AjaxBehaviorEvent evento)
    {
        addMessage((new StringBuilder()).append("ola k ase esto es un ejemplo o k ase: ").append(personaFiltro.getDocumentoPersona()).toString(), FacesMessage.SEVERITY_FATAL, new Object[0]);
    }

    public void cleanBusquedaPaciente(ActionEvent ae)
    {
        listaPersonas.clear();
        inicializaFiltro();
        listaAtenciones = new ArrayList();
        listaRespuestas = new ArrayList();
        personaSeleccionada = new PersonaDTO();
    }

    public void seteaActual(SelectEvent selectevent, int i)
    {
    }

    public void navegaIzquierda(ActionEvent e)
    {
        if(ACTUAL == 0)
        {
            addMessage("No se puede retroceder m\341s. Es la primera atenci\363n", FacesMessage.SEVERITY_INFO, new Object[0]);
            return;
        } else
        {
            ACTUAL = ACTUAL - 1;
            muestraAtencion((AtencionDTO)listaAtenciones.get(ACTUAL));
            showNewDialog(e);
            return;
        }
    }

    public void navegaDerecha(ActionEvent e)
    {
        if(ACTUAL == listaAtenciones.size() - 1)
        {
            addMessage("No hay m\341s atenciones", FacesMessage.SEVERITY_INFO, new Object[0]);
            return;
        } else
        {
            ACTUAL = ACTUAL + 1;
            muestraAtencion((AtencionDTO)listaAtenciones.get(ACTUAL));
            showNewDialog(e);
            return;
        }
    }

    private void muestraAtencion(AtencionDTO nuevaAtencionSeleccionada)
    {
        atencionSeleccionada = nuevaAtencionSeleccionada;
        listaRespuestas = atencionService.traeRespuestasDeAtencion(nuevaAtencionSeleccionada.getIdAtencion());
        List enfermedades = enfermedadRepository.traeEnfermedadesAtencion(nuevaAtencionSeleccionada.getIdAtencion());
        nuevaAtencionSeleccionada.setListaEnfermedades(enfermedades);
        nuevaAtencionSeleccionada.setTieneEnfermedades(enfermedades.size() > 0);
        mostrarDetalle = true;
    }

    public int getMAX()
    {
        return MAX;
    }

    public void setMAX(int MAX)
    {
        this.MAX = MAX;
    }

    public int getACTUAL()
    {
        return ACTUAL;
    }

    public void setACTUAL(int ACTUAL)
    {
        this.ACTUAL = ACTUAL;
    }

    public boolean isMostrarDetalle()
    {
        return mostrarDetalle;
    }

    public void setMostrarDetalle(boolean mostrarDetalle)
    {
        this.mostrarDetalle = mostrarDetalle;
    }

    public List getListaRespuestas()
    {
        return listaRespuestas;
    }

    public void setListaRespuestas(List listaRespuestas)
    {
        this.listaRespuestas = listaRespuestas;
    }

    public String getMedico()
    {
        return medico;
    }

    public void setMedico(String medico)
    {
        this.medico = medico;
    }

    public String getTecnica()
    {
        return tecnica;
    }

    public void setTecnica(String tecnica)
    {
        this.tecnica = tecnica;
    }

    public String getLaboratorista()
    {
        return laboratorista;
    }

    public void setLaboratorista(String laboratorista)
    {
        this.laboratorista = laboratorista;
    }

    public AtencionDTO getAtencionSeleccionada()
    {
        return atencionSeleccionada;
    }

    public void setAtencionSeleccionada(AtencionDTO atencionSeleccionada)
    {
        this.atencionSeleccionada = atencionSeleccionada;
    }

    public Integer getIdPersonaSeleccionada()
    {
        return idPersonaSeleccionada;
    }

    public void setIdPersonaSeleccionada(Integer idPersonaSeleccionada)
    {
        this.idPersonaSeleccionada = idPersonaSeleccionada;
    }

    public List getListaSexoPersonaEnum()
    {
        return listaSexoPersonaEnum;
    }

    public void setListaSexoPersonaEnum(List listaSexoPersonaEnum)
    {
        this.listaSexoPersonaEnum = listaSexoPersonaEnum;
    }

    public PacienteDTO getPacienteSeleccionado()
    {
        return pacienteSeleccionado;
    }

    public void setPacienteSeleccionado(PacienteDTO pacienteSeleccionado)
    {
        this.pacienteSeleccionado = pacienteSeleccionado;
    }

    public HistoriaDTO getNuevaHistoria()
    {
        return nuevaHistoria;
    }

    public void setNuevaHistoria(HistoriaDTO nuevaHistoria)
    {
        this.nuevaHistoria = nuevaHistoria;
    }

    public String getCantidadAtencionesSeleccionada()
    {
        return cantidadAtencionesSeleccionada;
    }

    public void setCantidadAtencionesSeleccionada(String cantidadAtencionesSeleccionada)
    {
        this.cantidadAtencionesSeleccionada = cantidadAtencionesSeleccionada;
    }

    public HistoriaDTO getHistoriaFiltro()
    {
        return historiaFiltro;
    }

    public void setHistoriaFiltro(HistoriaDTO historiaFiltro)
    {
        this.historiaFiltro = historiaFiltro;
    }

    public List getListaPersonas()
    {
        return listaPersonas;
    }

    public void setListaPersonas(List listaPersonas)
    {
        this.listaPersonas = listaPersonas;
    }

    public PersonaDTO getPersonaFiltro()
    {
        return personaFiltro;
    }

    public void setPersonaFiltro(PersonaDTO personaFiltro)
    {
        this.personaFiltro = personaFiltro;
    }

    public PacienteDTO getPacienteFiltro()
    {
        return pacienteFiltro;
    }

    public void setPacienteFiltro(PacienteDTO pacienteFiltro)
    {
        this.pacienteFiltro = pacienteFiltro;
    }

    public PersonaDTO getPersonaSeleccionada()
    {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(PersonaDTO personaSeleccionada)
    {
        this.personaSeleccionada = personaSeleccionada;
    }

    public List getListaEstadosPaciente()
    {
        return listaEstadosPaciente;
    }

    public void setListaEstadosPaciente(List listaEstadosPaciente)
    {
        this.listaEstadosPaciente = listaEstadosPaciente;
    }

    public List getListaTrabajos()
    {
        return listaTrabajos;
    }

    public void setListaTrabajos(List listaTrabajos)
    {
        this.listaTrabajos = listaTrabajos;
    }

    public boolean isMostrarDialogTrabajo()
    {
        return mostrarDialogTrabajo;
    }

    public List getListaAtenciones()
    {
        return listaAtenciones;
    }

    public void setListaAtenciones(List listaAtenciones)
    {
        this.listaAtenciones = listaAtenciones;
    }

    public void setMostrarDialogTrabajo(boolean mostrarDialogTrabajo)
    {
        this.mostrarDialogTrabajo = mostrarDialogTrabajo;
    }

    public TrabajoDTO getTrabajoDTO()
    {
        return trabajoDTO;
    }

    public void setTrabajoDTO(TrabajoDTO trabajoDTO)
    {
        this.trabajoDTO = trabajoDTO;
    }

    @Autowired
    DataSource dataSource;
    @Autowired
    PersonaService personaService;
    @Autowired
    AtencionService atencionService;
    @Autowired
    PacienteService pacienteService;
    @Autowired
    PaisRepository paisRepository;
    @Autowired
    EnfermedadRepository enfermedadRepository;
    @Autowired
    AtencionRepository atencionRepository;
    @Autowired
    HistoriaRepository historiaRepository;
    @Autowired
    DepartamentoRepository departamentoRepository;
    @Autowired
    ProvinciaRepository provinciaRepository;
    @Autowired
    DistritoRepository distritoRepository;
    private PersonaDTO personaFiltro;
    private PersonaDTO personaSeleccionada;
    private PacienteDTO pacienteFiltro;
    private PacienteDTO pacienteSeleccionado;
    private HistoriaDTO historiaFiltro;
    private List listaEstadosPaciente;
    private List listaSexoPersonaEnum;
    private List listaRespuestas;
    private List listaTrabajos;
    private List listaAtenciones;
    private List listaPersonas;
    private boolean mostrarDetalle;
    private boolean mostrarDialogTrabajo;
    private Validaciones validar;
    private Integer idPersonaSeleccionada;
    private AtencionDTO atencionSeleccionada;
    private String cantidadAtencionesSeleccionada;
    private TrabajoDTO trabajoDTO;
    private HistoriaDTO nuevaHistoria;
    private String medico;
    private String tecnica;
    private String laboratorista;
    private int MAX;
    private int ACTUAL;
}
