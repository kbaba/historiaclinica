// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:33:51 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   MenuJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import java.io.IOException;
import java.util.*;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import security.HistoriasClinicasAuthenticationToken;
import security.HistoriasClinicasSecurityInfo;
import util.FindCrudBeanBase;

@ManagedBean
@Scope(value="session")
@Component
public class MenuJSFBean
{

    public MenuJSFBean()
    {
    }

    @PostConstruct
    public void inicializarMenu()
    {
        LOG.info("Inicializando Menu con roles asignados al usuario");
        HistoriasClinicasAuthenticationToken authentication = (HistoriasClinicasAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
        String login = (String)authentication.getPrincipal();
        String password = (String)authentication.getCredentials();
        boolean mobile = authentication.isMobile();
        usuario = historiasClinicasSecutiryInfo.obtenerUsuario(login, password, mobile);
        menus = new ArrayList();
        MenuDTO menu;
label0:
        for(Iterator i$ = usuario.getMenus().iterator(); i$.hasNext(); menus.add(menu))
        {
            menu = (MenuDTO)i$.next();
            List options = new ArrayList(menu.getOptions());
            Iterator i$ = options.iterator();
            do
            {
                if(!i$.hasNext())
                    continue label0;
                OpcionDTO option = (OpcionDTO)i$.next();
                if(!option.isShowMenu())
                    menu.getOptions().remove(option);
            } while(true);
        }

    }

    public void pageAction(ActionEvent event)
        throws IOException, ServletException
    {
        cleanSession();
        String page = (String)event.getComponent().getAttributes().get("page");
        FacesContext.getCurrentInstance().getExternalContext().redirect(page);
    }

    private void cleanSession()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession)facesContext.getExternalContext().getSession(false);
        Enumeration e = session.getAttributeNames();
        do
        {
            if(!e.hasMoreElements())
                break;
            String key = (String)e.nextElement();
            Object object = session.getAttribute(key);
            if(object != null && (object instanceof FindCrudBeanBase))
                session.removeAttribute(key);
        } while(true);
    }

    public UsuarioDTO getUsuario()
    {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario)
    {
        this.usuario = usuario;
    }

    public List getMenus()
    {
        return menus;
    }

    public void setMenus(List menus)
    {
        this.menus = menus;
    }

    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    HistoriasClinicasSecurityInfo historiasClinicasSecutiryInfo;
    private UsuarioDTO usuario;
    private List menus;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/beans/MenuJSFBean);

}
