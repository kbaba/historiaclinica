// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:33:36 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadisticosJSFBean.java

package paquete.beans;

import javax.faces.event.ActionEvent;
import org.primefaces.event.CloseEvent;
import util.FindCrudBeanBase;

@ManagedBean
@Scope(value="session")
@Component
public class EstadisticosJSFBean extends FindCrudBeanBase
{

    public EstadisticosJSFBean()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionevent)
    {
    }

    public void onClean(ActionEvent actionevent)
    {
    }

    public void onPersist(ActionEvent actionevent)
    {
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent closeevent)
    {
    }
}
