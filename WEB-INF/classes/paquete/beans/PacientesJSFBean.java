// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:34:02 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PacientesJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import infraestructura.enums.*;
import java.io.PrintStream;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import paquete.lazyModel.PacienteNuevoLazyModel;
import paquete.repositorios.*;
import paquete.service.PacienteService;
import paquete.service.PersonaService;
import util.FindCrudBeanBase;
import util.Validaciones;

@ManagedBean
@Scope(value="session")
@Component
public class PacientesJSFBean extends FindCrudBeanBase
{

    public PacientesJSFBean()
    {
        listaEstadosPaciente = new ArrayList();
        listaEstatutoSocialPersona = new ArrayList();
        listaMaterialViviendaPersona = new ArrayList();
        listaPertenenciaViviendaPersona = new ArrayList();
        listaTipoResidenciaPersona = new ArrayList();
        listaSexoPersonaEnum = new ArrayList();
    }

    @PostConstruct
    public void init()
    {
        validar = new Validaciones();
        cantidadPersonasVivienda = "";
        cantidadDormitoriosVivienda = "";
        listaEstadosPaciente = EstadoPacienteEnum.findAll();
        listaEstatutoSocialPersona = EstatutoSocialPersonaEnum.findAll();
        listaMaterialViviendaPersona = MaterialViviendaPersonaEnum.findAll();
        listaPertenenciaViviendaPersona = PertenenciaViviendaPersonaEnum.findAll();
        listaTipoResidenciaPersona = TipoResidenciaPersonaEnum.findAll();
        listaSexoPersonaEnum = SexoPersonaEnum.findAll();
        inicializaPersonaSeleccionada();
        inicializaFiltro();
        inicializaNuevaPersona();
        inicializaLocalizacion();
        listaPais = paisRepository.getAllPaises();
        personaSeleccionada.setDepartamentoPersona(new DepartamentoDTO());
        personaSeleccionada.setProvinciaPersona(provinciaSeleccionada);
        personaSeleccionada.setDistritoPersona(distritoSeleccionado);
        personaLazyModel = new PacienteNuevoLazyModel(personaFiltro, personaService, true);
        mostrarDetalle = true;
        nuevoEstadoPersonaSeleccionada = "-1";
        listaTrabajos = new ArrayList();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map sessionMap = externalContext.getSessionMap();
        Boolean mostrarDialogDeTrabajo = (Boolean)sessionMap.get("mostrarDialogTrabajo");
        if(getUsuario().isEsEnfermeria() || getUsuario().isEsLaboratorista() || getUsuario().isEsMedico())
            if(getUsuario().getEmpleado().getConjuntoTrabajos().size() > 1 && mostrarDialogDeTrabajo == null)
            {
                Iterator i$ = getUsuario().getEmpleado().getConjuntoTrabajos().iterator();
                do
                {
                    if(!i$.hasNext())
                        break;
                    TrabajoDTO trabajo = (TrabajoDTO)i$.next();
                    if(trabajo.getEstadoTrabajo().equals(EstadoTrabajoEnum.ACTIVO.getCode()))
                    {
                        listaTrabajos.add(trabajo);
                        setMostrarDialogTrabajo(true);
                    }
                } while(true);
            } else
            {
                setMostrarDialogTrabajo(false);
            }
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionevent)
    {
    }

    public void onClean(ActionEvent actionEvent)
    {
        personaLazyModel = new PacienteNuevoLazyModel(personaFiltro, personaService, false);
        inicializaFiltro();
        inicializaPersonaSeleccionada();
        cantidadPersonasVivienda = "";
        cantidadDormitoriosVivienda = "";
    }

    public void onPersist(ActionEvent actionevent)
    {
    }

    public void realizaBusqueda()
    {
        if(personaFiltro.getPaciente().getEstadoPacienteEnum() == null)
            personaFiltro.getPaciente().setEstadoPaciente("");
        else
            personaFiltro.getPaciente().setEstadoPaciente(personaFiltro.getPaciente().getEstadoPacienteEnum().getCode());
        if(personaFiltro.getTipoResidenciaPersonaEnum() == null)
            personaFiltro.setTipoResidenciaPersona("");
        else
            personaFiltro.setTipoResidenciaPersona(personaFiltro.getTipoResidenciaPersonaEnum().getCode());
        if(personaFiltro.getPertenenciaViviendaPersonaPersonaEnum() == null)
            personaFiltro.setPertenenciaViviendaPersona("");
        else
            personaFiltro.setPertenenciaViviendaPersona(personaFiltro.getPertenenciaViviendaPersonaPersonaEnum().getCode());
        if(personaFiltro.getMaterialViviendaPersonaEnum() == null)
            personaFiltro.setMaterialViviendaPersona("");
        else
            personaFiltro.setMaterialViviendaPersona(personaFiltro.getMaterialViviendaPersonaEnum().getCode());
        if(personaFiltro.getEstatutoSocialPersonaEnum() == null)
            personaFiltro.setEstatutoSocialPersona("");
        else
            personaFiltro.setEstatutoSocialPersona(personaFiltro.getEstatutoSocialPersonaEnum().getCode());
        if(personaFiltro.getSexoPersonaEnum() == null)
            personaFiltro.setSexoPersona("");
        else
            personaFiltro.setSexoPersona(personaFiltro.getSexoPersonaEnum().getCode());
        personaLazyModel = new PacienteNuevoLazyModel(personaFiltro, personaService, true);
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent closeevent)
    {
    }

    public void guardaPaciente(ActionEvent actionEvent)
    {
        String mensaje[] = {
            ""
        };
        boolean estaOk = validarDatosPersona(nuevaPersona, mensaje);
        if(!estaOk)
        {
            addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        if(!validar.chekeoNumero(cantidadDormitoriosVivienda))
        {
            addMessage("\"Cantidad de Dormitorios \" de persona s\363lo se aceptan n\372meros.Dato Obligatorio", FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        if(!validar.chekeoNumero(cantidadPersonasVivienda))
        {
            addMessage("Dato \"Cantidad de Personas en la Vivienda\" de persona s\363lo se aceptan n\372meros.Dato Obligatorio", FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        } else
        {
            nuevaPersona.setDormitoriosViviendaPersona(new Integer(cantidadDormitoriosVivienda));
            nuevaPersona.setCantidadPersonasViviendaPersona(new Integer(cantidadPersonasVivienda));
            nuevaPersona.setSexoPersona(nuevaPersona.getSexoPersonaEnum().getCode());
            escribeEnumsEstadoSocial(nuevaPersona);
            personaService.guardarPersona(nuevaPersona);
            addMessage("Se guard\363 de manera correcta la informaci\363n del paciente", FacesMessage.SEVERITY_INFO, new Object[0]);
            inicializaNuevaPersona();
            inicializaLocalizacion();
            cantidadPersonasVivienda = "";
            cantidadDormitoriosVivienda = "";
            executeModal("persistDialog", false);
            realizaBusqueda();
            return;
        }
    }

    public void actualizaData(ActionEvent actionEvent)
    {
        String mensaje[] = {
            ""
        };
        boolean estaOk = validarDatosPersona(personaSeleccionada, mensaje);
        if(!estaOk)
        {
            addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        if(!validar.chekeoNumero(cantidadDormitoriosVivienda))
        {
            addMessage("\"Cantidad de Dormitorios \" de persona s\363lo se aceptan n\372meros.Dato Obligatorio", FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        if(!validar.chekeoNumero(cantidadPersonasVivienda))
        {
            addMessage("Dato \"Cantidad de Personas en la Vivienda\" de persona s\363lo se aceptan n\372meros.Dato Obligatorio", FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        } else
        {
            personaSeleccionada.getPaciente().setEstadoPaciente(personaSeleccionada.getPaciente().getEstadoPacienteEnum().getCode());
            personaSeleccionada.setIdResidenciaDistrito(idCompuestoDistritoPersonaSeleccionada);
            personaSeleccionada.setSexoPersona(personaSeleccionada.getSexoPersonaEnum().getCode());
            personaSeleccionada.setDormitoriosViviendaPersona(new Integer(cantidadDormitoriosVivienda));
            personaSeleccionada.setCantidadPersonasViviendaPersona(new Integer(cantidadPersonasVivienda));
            escribeEnumsEstadoSocial(personaSeleccionada);
            personaService.guardarPersona(personaSeleccionada);
            addMessage("Se actualiz\363 de manera correcta la informaci\363n del paciente", FacesMessage.SEVERITY_INFO, new Object[0]);
            inicializaPersonaSeleccionada();
            ocultaDialogModificar();
            inicializaLocalizacion();
            cantidadPersonasVivienda = "";
            cantidadDormitoriosVivienda = "";
            realizaBusqueda();
            return;
        }
    }

    public void cargaDetallesDePaciente(ActionEvent event)
    {
        if(personaSeleccionada == null || StringUtils.isBlank(personaSeleccionada.getDocumentoPersona()))
        {
            addMessage("Debe seleccionar antes un paciente", FacesMessage.SEVERITY_WARN, new Object[0]);
            mostrarDetalle = false;
            executeModal("detailDialog", mostrarDetalle);
            return;
        } else
        {
            mostrarDetalle = true;
            executeModal("detailDialog", mostrarDetalle);
            String idCompuesto = personaSeleccionada.getIdResidenciaDistrito();
            List lista = separaIdsCompuesto(idCompuesto, ",");
            String idPais = (String)lista.get(0);
            String idDepartamento = (String)lista.get(1);
            String idProvincia = (String)lista.get(2);
            String idDistrito = (String)lista.get(3);
            listaDepartamento = departamentoRepository.findByPaisId(idPais);
            listaProvincia = provinciaRepository.findByDepartamento(idPais, idDepartamento);
            listaDistrito = distritoRepository.findByProvincia(idPais, idDepartamento, idProvincia);
            idCompuestoDepartamentoPersonaSeleccionada = (new StringBuilder()).append(idPais).append(",").append(idDepartamento).toString();
            idCompuestoProvinciaPersonaSeleccionada = (new StringBuilder()).append(idPais).append(",").append(idDepartamento).append(",").append(idProvincia).toString();
            idCompuestoDistritoPersonaSeleccionada = (new StringBuilder()).append(idPais).append(",").append(idDepartamento).append(",").append(idProvincia).append(",").append(idDistrito).toString();
            cantidadDormitoriosVivienda = (new StringBuilder()).append(personaSeleccionada.getDormitoriosViviendaPersona().intValue()).append("").toString();
            cantidadPersonasVivienda = (new StringBuilder()).append(personaSeleccionada.getCantidadPersonasViviendaPersona().intValue()).append("").toString();
            showNewDialog(event);
            return;
        }
    }

    private void inicializaPersonaSeleccionada()
    {
        personaSeleccionada = new PersonaDTO();
        pacienteSeleccionado = new PacienteDTO();
        personaSeleccionada.setPaciente(pacienteSeleccionado);
        personaSeleccionada.setTipoDocumentoPersona("1");
    }

    private void inicializaLocalizacion()
    {
        idCompuestoDepartamento = "";
        idCompuestoProvincia = "";
        idCompuestoDistrito = "";
        idCompuestoDepartamentoPersonaSeleccionada = "";
        idCompuestoProvinciaPersonaSeleccionada = "";
        idCompuestoDistritoPersonaSeleccionada = "";
        provinciaSeleccionada = new ProvinciaDTO();
        distritoSeleccionado = new DistritoDTO();
        listaDepartamento = new ArrayList();
        listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void inicializaFiltro()
    {
        personaFiltro = new PersonaDTO();
        pacienteFiltro = new PacienteDTO();
        pacienteFiltro.setEstadoPaciente("A");
        historiaFiltro = new HistoriaDTO();
        pacienteFiltro.setHistoria(historiaFiltro);
        personaFiltro.setPaciente(pacienteFiltro);
    }

    public void inicializaNuevaPersona()
    {
        nuevaPersona = new PersonaDTO();
        nuevoPaciente = new PacienteDTO();
        nuevoPaciente.setEstadoPaciente("A");
        nuevaHistoria = new HistoriaDTO();
        nuevoPaciente.setHistoria(nuevaHistoria);
        nuevaPersona.setPaciente(nuevoPaciente);
        cantidadPersonasVivienda = "";
        cantidadDormitoriosVivienda = "";
    }

    private void ocultaDialogModificar()
    {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("detailDialog.hide()");
    }

    private boolean validarDatosPersona(PersonaDTO persona, String mensaje[])
    {
        String nombres = persona.getNombrePersona();
        String apeP = persona.getApellidoPaternoPersona();
        String apeM = persona.getApellidoMaternoPersona();
        String documento = persona.getDocumentoPersona();
        if(!validar.chekeoPalabrasSinNFAmero(nombres))
        {
            mensaje[0] = "Para \"nombre\" de persona solo est\341n permitidos caracteres de aA-zZ, no n\372meros, ni s\355mbolos. Campo obligatorio";
            return false;
        }
        if(!validar.chekeoPalabrasSinNFAmero(apeP))
        {
            mensaje[0] = "Para \"apellido paterno\" de persona solo est\341n permitidos caracteres de aA-zZ, no n\372meros, ni s\355mbolos. Campo obligatorio";
            return false;
        }
        if(!validar.chekeoPalabrasSinNFAmero(apeM))
        {
            mensaje[0] = "Para \"apellido materno\" de persona solo est\341n permitidos caracteres de aA-zZ, no n\372meros, ni s\355mbolos. Campo obligatorio";
            return false;
        }
        if(!validar.chekeoNumero(persona.getTipoDocumentoPersona()))
        {
            mensaje[0] = (new StringBuilder()).append("\"Tipo de Documento\" de persona Campo obligatorio: ").append(persona.getTipoDocumentoPersona()).toString();
            return false;
        }
        if(persona.getFechaNacimientoPersona() == null)
        {
            mensaje[0] = "Error \"Fecha de Nacimiento\" de persona es obligatorio";
            return false;
        }
        if(!esMenorIgual(persona.getFechaNacimientoPersona()))
        {
            mensaje[0] = "La fecha de nacimiento seleccionada debe ser menor o igual a la actual";
            return false;
        }
        if(persona.getTipoDocumentoPersona().equals("1"))
        {
            if(!validar.chekeoNumero(documento))
            {
                mensaje[0] = "Para \"DNI\" de persona solo est\341n permitidos caracteres num\351ricos.Campo obligatorio";
                return false;
            }
            if(!validar.chekeoPalabraCantidadCaracteres(documento, 8))
            {
                mensaje[0] = "Para \"DNI\" de persona la cantidad de caracteres debe ser exactamente 08.Campo obligatorio";
                return false;
            }
        }
        if(persona.getIdPersona() != null)
        {
            if(personaService.esDocumentoRepetido(documento, persona.getIdPersona().intValue(), persona.getTipoDocumentoPersona()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Documento\" de persona. Documento ya en uso.";
                return false;
            }
            if(personaService.esUsuarioRepetido(persona.getUsuario(), persona.getIdPersona().intValue()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Usuario\" de persona. Usuario ya en uso.";
                return false;
            }
        } else
        {
            if(personaService.esDocumentoRepetido(documento, persona.getTipoDocumentoPersona()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Documento\" de persona. Documento ya en uso.";
                return false;
            }
            if(personaService.esUsuarioRepetido(persona.getUsuario()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Usuario\" de persona. Usuario ya en uso.";
                return false;
            }
        }
        if(persona.getSexoPersonaEnum() == null)
        {
            mensaje[0] = "\"Sexo\" de la persona  es un dato necesario.";
            return false;
        }
        if(StringUtils.isBlank(persona.getIdResidenciaDistrito()))
        {
            mensaje[0] = "\"Distrito de residencia\" de la persona  es un dato necesario.";
            return false;
        }
        if(StringUtils.isBlank(persona.getDireccionPersona()))
        {
            mensaje[0] = "\"Direcci\363n\" de persona es un dato necesario.";
            return false;
        }
        if(persona.getTipoResidenciaPersonaEnum() == null)
        {
            mensaje[0] = "\"Tipo residencia\" de persona es un dato necesario.";
            return false;
        }
        if(persona.getPertenenciaViviendaPersonaPersonaEnum() == null)
        {
            mensaje[0] = "\"Pertenencia Vivienda\" de persona es un dato necesario.";
            return false;
        }
        if(persona.getMaterialViviendaPersonaEnum() == null)
        {
            mensaje[0] = "\"Material Vivienda\" de persona es un dato necesario.";
            return false;
        }
        if(persona.getUsuario() != null)
        {
            if(persona.getUsuario().length() < 5)
            {
                mensaje[0] = "\"Usuario\" de persona debe tener al menos 5 caracteres.";
                return false;
            }
            if(persona.getIdPersona() != null)
            {
                if(personaService.esUsuarioRepetido(persona.getUsuario(), persona.getIdPersona().intValue()))
                {
                    mensaje[0] = "Error ya existe un registro en el sistema con ese \"Usuario\" de persona. Usuario ya en uso.";
                    return false;
                }
            } else
            if(personaService.esUsuarioRepetido(persona.getUsuario()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Usuario \" de persona. Usuario ya en uso.";
                return false;
            }
            if(persona.getClave() == null)
            {
                mensaje[0] = "\"Usuario\" de persona necesita contrase\361a.";
                return false;
            }
            if(persona.getClave().length() < 5)
            {
                mensaje[0] = "\"Contrase\361a\" de persona necesita al menos 5 caracteres.";
                return false;
            }
        }
        return true;
    }

    public void persistManejaCambioPais()
    {
        String idPais = nuevaPersona.getIdPaisNacionalidad();
        depurar((new StringBuilder()).append("el id de pais es: ").append(idPais).toString());
        if(StringUtils.isNotBlank(idPais))
        {
            listaDepartamento = departamentoRepository.findByPaisId(idPais);
            depurar((new StringBuilder()).append("tam\361o de departamentos es: ").append(listaDepartamento.size()).toString());
        } else
        {
            listaDepartamento = new ArrayList();
        }
        listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void manejaCambioDepartamento()
    {
        List compuesto = separaIdsCompuesto(idCompuestoDepartamento, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId))
            listaProvincia = provinciaRepository.findByDepartamento(idPais, departamentoId);
        else
            listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void manejaCambioProvincia()
    {
        List compuesto = separaIdsCompuesto(idCompuestoProvincia, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        String provinciaId = (String)compuesto.get(2);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId) && StringUtils.isNotBlank(provinciaId))
            listaDistrito = distritoRepository.findByProvincia(idPais, departamentoId, provinciaId);
        else
            listaDistrito = new ArrayList();
    }

    public void detailManejaCambioPais()
    {
        String idPais = personaSeleccionada.getIdPaisNacionalidad();
        depurar((new StringBuilder()).append("el id de pais es: ").append(idPais).toString());
        if(StringUtils.isNotBlank(idPais))
        {
            listaDepartamento = departamentoRepository.findByPaisId(idPais);
            depurar((new StringBuilder()).append("tam\361o de departamentos es: ").append(listaDepartamento.size()).toString());
        } else
        {
            listaDepartamento = new ArrayList();
        }
        listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void manejaCambioDepartamentoDetail()
    {
        List compuesto = separaIdsCompuesto(idCompuestoDepartamentoPersonaSeleccionada, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId))
            listaProvincia = provinciaRepository.findByDepartamento(idPais, departamentoId);
        else
            listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void manejaCambioProvinciaDetail()
    {
        List compuesto = separaIdsCompuesto(idCompuestoProvinciaPersonaSeleccionada, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        String provinciaId = (String)compuesto.get(2);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId) && StringUtils.isNotBlank(provinciaId))
            listaDistrito = distritoRepository.findByProvincia(idPais, departamentoId, provinciaId);
        else
            listaDistrito = new ArrayList();
    }

    public List separaIdsCompuesto(String compuesto, String separador)
    {
        List lista = new ArrayList();
        for(StringTokenizer st = new StringTokenizer(compuesto, separador); st.hasMoreTokens(); lista.add(st.nextToken()));
        return lista;
    }

    public void depurar(String mensaje)
    {
        System.out.println((new StringBuilder()).append("####").append(mensaje).toString());
    }

    public void toIndex(ActionEvent e)
    {
        redireccionar("../index.jsf");
    }

    private void escribeEnumsEstadoSocial(PersonaDTO persona)
    {
        persona.setTipoResidenciaPersona(persona.getTipoResidenciaPersonaEnum().getCode());
        persona.setPertenenciaViviendaPersona(persona.getPertenenciaViviendaPersonaPersonaEnum().getCode());
        persona.setMaterialViviendaPersona(persona.getMaterialViviendaPersonaEnum().getCode());
        String estatuto_social = personaService.determinaEstatutoSocial(persona);
        persona.setEstatutoSocialPersona(estatuto_social);
    }

    private boolean esMenorIgual(Date fecha)
    {
        Calendar hoy = Calendar.getInstance();
        Date actual = hoy.getTime();
        int diff = actual.compareTo(fecha);
        return diff > 0 || diff == 0;
    }

    public List getListaTrabajos()
    {
        return listaTrabajos;
    }

    public void setListaTrabajos(List listaTrabajos)
    {
        this.listaTrabajos = listaTrabajos;
    }

    public boolean isMostrarDialogTrabajo()
    {
        return mostrarDialogTrabajo;
    }

    public void setMostrarDialogTrabajo(boolean mostrarDialogTrabajo)
    {
        this.mostrarDialogTrabajo = mostrarDialogTrabajo;
    }

    public String getTipoResidenciaPersona()
    {
        return tipoResidenciaPersona;
    }

    public void setTipoResidenciaPersona(String tipoResidenciaPersona)
    {
        this.tipoResidenciaPersona = tipoResidenciaPersona;
    }

    public String getNuevoEstadoPersonaSeleccionada()
    {
        return nuevoEstadoPersonaSeleccionada;
    }

    public void setNuevoEstadoPersonaSeleccionada(String nuevoEstadoPersonaSeleccionada)
    {
        this.nuevoEstadoPersonaSeleccionada = nuevoEstadoPersonaSeleccionada;
    }

    public boolean isMostrarDetalle()
    {
        return mostrarDetalle;
    }

    public List getListaSexoPersonaEnum()
    {
        return listaSexoPersonaEnum;
    }

    public void setListaSexoPersonaEnum(List listaSexoPersonaEnum)
    {
        this.listaSexoPersonaEnum = listaSexoPersonaEnum;
    }

    public void setMostrarDetalle(boolean mostrarDetalle)
    {
        this.mostrarDetalle = mostrarDetalle;
    }

    public String getIdCompuestoDistrito()
    {
        return idCompuestoDistrito;
    }

    public void setIdCompuestoDistrito(String idCompuestoDistrito)
    {
        this.idCompuestoDistrito = idCompuestoDistrito;
    }

    public void setarSeleccionado(SelectEvent event)
    {
        personaSeleccionada = (PersonaDTO)event.getObject();
    }

    public String getIdCompuestoProvincia()
    {
        return idCompuestoProvincia;
    }

    public void setIdCompuestoProvincia(String idCompuestoProvincia)
    {
        this.idCompuestoProvincia = idCompuestoProvincia;
    }

    public PersonaDTO getPersonaFiltro()
    {
        return personaFiltro;
    }

    public void setPersonaFiltro(PersonaDTO personaFiltro)
    {
        this.personaFiltro = personaFiltro;
    }

    public List getListaDepartamento()
    {
        return listaDepartamento;
    }

    public String getIdCompuestoDepartamento()
    {
        return idCompuestoDepartamento;
    }

    public void setIdCompuestoDepartamento(String idCompuestoDepartamento)
    {
        this.idCompuestoDepartamento = idCompuestoDepartamento;
    }

    public void setListaDepartamento(List listaDepartamento)
    {
        this.listaDepartamento = listaDepartamento;
    }

    public List getListaDistrito()
    {
        return listaDistrito;
    }

    public void setListaDistrito(List listaDistrito)
    {
        this.listaDistrito = listaDistrito;
    }

    public List getListaProvincia()
    {
        return listaProvincia;
    }

    public void setListaProvincia(List listaProvincia)
    {
        this.listaProvincia = listaProvincia;
    }

    public PacienteDTO getPacienteFiltro()
    {
        return pacienteFiltro;
    }

    public void setPacienteFiltro(PacienteDTO pacienteFiltro)
    {
        this.pacienteFiltro = pacienteFiltro;
    }

    public String getIdCompuestoDepartamentoPersonaSeleccionada()
    {
        return idCompuestoDepartamentoPersonaSeleccionada;
    }

    public void setIdCompuestoDepartamentoPersonaSeleccionada(String idCompuestoDepartamentoPersonaSeleccionada)
    {
        this.idCompuestoDepartamentoPersonaSeleccionada = idCompuestoDepartamentoPersonaSeleccionada;
    }

    public String getIdCompuestoDistritoPersonaSeleccionada()
    {
        return idCompuestoDistritoPersonaSeleccionada;
    }

    public void setIdCompuestoDistritoPersonaSeleccionada(String idCompuestoDistritoPersonaSeleccionada)
    {
        this.idCompuestoDistritoPersonaSeleccionada = idCompuestoDistritoPersonaSeleccionada;
    }

    public String getIdCompuestoProvinciaPersonaSeleccionada()
    {
        return idCompuestoProvinciaPersonaSeleccionada;
    }

    public void setIdCompuestoProvinciaPersonaSeleccionada(String idCompuestoProvinciaPersonaSeleccionada)
    {
        this.idCompuestoProvinciaPersonaSeleccionada = idCompuestoProvinciaPersonaSeleccionada;
    }

    public PersonaDTO getPersonaSeleccionada()
    {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(PersonaDTO personaSeleccionada)
    {
        this.personaSeleccionada = personaSeleccionada;
    }

    public List getListaPais()
    {
        return listaPais;
    }

    public void setListaPais(List listaPais)
    {
        this.listaPais = listaPais;
    }

    public Date getHoy()
    {
        Calendar calendar1 = Calendar.getInstance();
        hoy = calendar1.getTime();
        return hoy;
    }

    public void setHoy(Date hoy)
    {
        this.hoy = hoy;
    }

    public List getListaEstadosPaciente()
    {
        return listaEstadosPaciente;
    }

    public void setListaEstadosPaciente(List listaEstadosPaciente)
    {
        this.listaEstadosPaciente = listaEstadosPaciente;
    }

    public List getListaEstatutoSocialPersona()
    {
        return listaEstatutoSocialPersona;
    }

    public void setListaEstatutoSocialPersona(List listaEstatutoSocialPersona)
    {
        this.listaEstatutoSocialPersona = listaEstatutoSocialPersona;
    }

    public List getListaMaterialViviendaPersona()
    {
        return listaMaterialViviendaPersona;
    }

    public void setListaMaterialViviendaPersona(List listaMaterialViviendaPersona)
    {
        this.listaMaterialViviendaPersona = listaMaterialViviendaPersona;
    }

    public List getListaPertenenciaViviendaPersona()
    {
        return listaPertenenciaViviendaPersona;
    }

    public void setListaPertenenciaViviendaPersona(List listaPertenenciaViviendaPersona)
    {
        this.listaPertenenciaViviendaPersona = listaPertenenciaViviendaPersona;
    }

    public List getListaTipoResidenciaPersona()
    {
        return listaTipoResidenciaPersona;
    }

    public void setListaTipoResidenciaPersona(List listaTipoResidenciaPersona)
    {
        this.listaTipoResidenciaPersona = listaTipoResidenciaPersona;
    }

    public PersonaDTO getNuevaPersona()
    {
        return nuevaPersona;
    }

    public void setNuevaPersona(PersonaDTO nuevaPersona)
    {
        this.nuevaPersona = nuevaPersona;
    }

    public PacienteDTO getNuevoPaciente()
    {
        return nuevoPaciente;
    }

    public void setNuevoPaciente(PacienteDTO nuevoPaciente)
    {
        this.nuevoPaciente = nuevoPaciente;
    }

    public DistritoDTO getDistritoSeleccionado()
    {
        return distritoSeleccionado;
    }

    public void setDistritoSeleccionado(DistritoDTO distritoSeleccionado)
    {
        this.distritoSeleccionado = distritoSeleccionado;
    }

    public ProvinciaDTO getProvinciaSeleccionada()
    {
        return provinciaSeleccionada;
    }

    public void setProvinciaSeleccionada(ProvinciaDTO provinciaSeleccionada)
    {
        this.provinciaSeleccionada = provinciaSeleccionada;
    }

    public LazyDataModel getPersonaLazyModel()
    {
        return personaLazyModel;
    }

    public String getCantidadDormitoriosVivienda()
    {
        return cantidadDormitoriosVivienda;
    }

    public void setCantidadDormitoriosVivienda(String cantidadDormitoriosVivienda)
    {
        this.cantidadDormitoriosVivienda = cantidadDormitoriosVivienda;
    }

    public String getCantidadPersonasVivienda()
    {
        return cantidadPersonasVivienda;
    }

    public void setCantidadPersonasVivienda(String cantidadPersonasVivienda)
    {
        this.cantidadPersonasVivienda = cantidadPersonasVivienda;
    }

    public void setPersonaLazyModel(LazyDataModel personaLazyModel)
    {
        this.personaLazyModel = personaLazyModel;
    }

    @Autowired
    DataSource dataSource;
    @Autowired
    PersonaService personaService;
    @Autowired
    PacienteService pacienteService;
    @Autowired
    PaisRepository paisRepository;
    @Autowired
    DepartamentoRepository departamentoRepository;
    @Autowired
    ProvinciaRepository provinciaRepository;
    @Autowired
    DistritoRepository distritoRepository;
    private PersonaDTO personaFiltro;
    private PersonaDTO personaSeleccionada;
    private PersonaDTO nuevaPersona;
    private PacienteDTO pacienteFiltro;
    private PacienteDTO nuevoPaciente;
    private PacienteDTO pacienteSeleccionado;
    private ProvinciaDTO provinciaSeleccionada;
    private DistritoDTO distritoSeleccionado;
    private List listaPais;
    private List listaDepartamento;
    private List listaProvincia;
    private List listaDistrito;
    private List listaTrabajos;
    private List listaEstadosPaciente;
    private List listaEstatutoSocialPersona;
    private List listaMaterialViviendaPersona;
    private List listaPertenenciaViviendaPersona;
    private List listaTipoResidenciaPersona;
    private List listaSexoPersonaEnum;
    private String cantidadDormitoriosVivienda;
    private String cantidadPersonasVivienda;
    private Date hoy;
    private Validaciones validar;
    private String idCompuestoDepartamento;
    private String idCompuestoProvincia;
    private String idCompuestoDistrito;
    private String idCompuestoDepartamentoPersonaSeleccionada;
    private String idCompuestoProvinciaPersonaSeleccionada;
    private String idCompuestoDistritoPersonaSeleccionada;
    private LazyDataModel personaLazyModel;
    private boolean mostrarDetalle;
    private String nuevoEstadoPersonaSeleccionada;
    private String tipoResidenciaPersona;
    private HistoriaDTO nuevaHistoria;
    private HistoriaDTO historiaFiltro;
    private boolean mostrarDialogTrabajo;
}
