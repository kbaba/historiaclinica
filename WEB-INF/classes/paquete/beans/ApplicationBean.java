// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:52 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ApplicationBean.java

package paquete.beans;

import java.util.HashMap;
import java.util.Map;

@Scope(value="singleton")
@Component
public class ApplicationBean
{

    public ApplicationBean()
    {
    }

    @PostConstruct
    public void init()
    {
        separaciones = new HashMap();
    }

    public Map getSeparaciones()
    {
        return separaciones;
    }

    public void setSeparaciones(Map separaciones)
    {
        this.separaciones = separaciones;
    }

    private Map separaciones;
}
