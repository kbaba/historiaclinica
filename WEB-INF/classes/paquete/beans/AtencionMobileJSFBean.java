// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:33:17 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AtencionMobileJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import infraestructura.enums.TipoRespuestaPreguntaEnum;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import javax.faces.component.UIComponent;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import org.apache.commons.lang.StringUtils;
import org.primefaces.event.CloseEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.*;
import paquete.repositorios.*;
import paquete.service.*;
import util.FindCrudBeanBase;

// Referenced classes of package paquete.beans:
//            ApplicationBean

@ManagedBean
@Scope(value="session")
@Component
public class AtencionMobileJSFBean extends FindCrudBeanBase
{

    public AtencionMobileJSFBean()
    {
    }

    @PostConstruct
    public void init()
    {
        preguntasGenerales = new ArrayList();
        preguntasEspecialidad = new ArrayList();
        preguntasSubEspecialidad = new ArrayList();
        preguntasProcedimiento = new ArrayList();
        respuestasGenerales = new ArrayList();
        respuestasEspecialidad = new ArrayList();
        respuestasSubEspecialidad = new ArrayList();
        respuestasProcedimiento = new ArrayList();
        pintarGeneral = Boolean.valueOf(true);
        pintarEspecialidad = Boolean.valueOf(true);
        pintarSubEspecialidad = Boolean.valueOf(true);
        pintarProcedimiento = Boolean.valueOf(true);
        pintarEnfermedades = Boolean.valueOf(true);
        listaEnfermedades = new ArrayList();
        listaEnfermedadesSeleccionadas = new ArrayList();
        setAtencionSeparada(Boolean.valueOf(false));
        setContBusquedaSeparaciones(new Integer(1));
        LOG.debug((new StringBuilder()).append("\n\n\n\n Iniciando atenci\363n en mobile, se procede a buscar una atenci\363n para el empleado").append(getUsuario().getEmpleado().getIdEmpleado()).append("\n\n\n\n").toString());
        if(applicationBean.getSeparaciones().containsKey(getUsuario().getEmpleado().getIdEmpleado()))
        {
            setAtencionSeparada(Boolean.valueOf(true));
            separacion = (ApplicationDTO)applicationBean.getSeparaciones().get(getUsuario().getEmpleado().getIdEmpleado());
        }
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionevent)
    {
    }

    public void onClean(ActionEvent actionEvent)
    {
        String accion = (String)actionEvent.getComponent().getAttributes().get("accion");
        if(StringUtils.isNotBlank(accion))
        {
            if(accion.equals("cancelarSeleccionEnfermedad"))
            {
                criterioBusquedaEnfermedades = null;
                listaEnfermedades.clear();
            }
            if(accion.equals("cancelarVerEnfermedades"))
                listaEnfermedadesSeleccionadas.clear();
            if(accion.equals("cancelarPreguntasGenerales"))
            {
                preguntasGenerales.clear();
                respuestasGenerales.clear();
            }
            if(accion.equals("cancelarPreguntasEspecialidad"))
            {
                preguntasEspecialidad.clear();
                respuestasEspecialidad.clear();
                idSubEspecialidad = null;
                subEspecialidadSeleccionada = null;
            }
            if(accion.equals("cancelarPreguntasSubEspecialidad"))
            {
                preguntasSubEspecialidad.clear();
                respuestasSubEspecialidad.clear();
                idProcedimiento = null;
                procedimientoSeleccionado = null;
            }
            if(accion.equals("cancelarPreguntasProcedimiento"))
            {
                preguntasProcedimiento.clear();
                respuestasProcedimiento.clear();
            }
        } else
        {
            criterioBusquedaEnfermedades = null;
            listaEnfermedades.clear();
            listaEnfermedadesSeleccionadas.clear();
            preguntasGenerales.clear();
            respuestasGenerales.clear();
            preguntasEspecialidad.clear();
            respuestasEspecialidad.clear();
            idSubEspecialidad = null;
            subEspecialidadSeleccionada = null;
            preguntasSubEspecialidad.clear();
            respuestasSubEspecialidad.clear();
            idProcedimiento = null;
            procedimientoSeleccionado = null;
            preguntasProcedimiento.clear();
            respuestasProcedimiento.clear();
        }
    }

    public void onPersist(ActionEvent actionEvent)
    {
        organizarPreguntasYRespuestas(preguntasGenerales, respuestasGenerales, Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), null, null, null);
        organizarPreguntasYRespuestas(preguntasEspecialidad, respuestasEspecialidad, Boolean.valueOf(true), Boolean.valueOf(false), Boolean.valueOf(false), separacion.getAtention().getTrabajo().getEspecialidad(), null, null);
        organizarPreguntasYRespuestas(preguntasSubEspecialidad, respuestasSubEspecialidad, Boolean.valueOf(false), Boolean.valueOf(true), Boolean.valueOf(false), null, subEspecialidadSeleccionada, null);
        organizarPreguntasYRespuestas(preguntasProcedimiento, respuestasProcedimiento, Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(true), null, null, procedimientoSeleccionado);
        separacion.getAtention().setHistoria(separacion.getPacienteSeparado().getPaciente().getHistoria());
        separacion.getAtention().setFechaAtencion(Calendar.getInstance().getTime());
        separacion.setAtention(atencionService.persistOrMerge(separacion.getAtention()));
        if(!listaEnfermedadesSeleccionadas.isEmpty())
        {
            Enfermedad enfermedad;
            for(Iterator i$ = listaEnfermedadesSeleccionadas.iterator(); i$.hasNext(); separacion.getAtention().agregarEnfermedades(new EnfermedadDTO[] {
    (EnfermedadDTO)mapperService.map(enfermedad, infraestructura/dto/EnfermedadDTO)
}))
                enfermedad = (Enfermedad)i$.next();

            atencionService.persistOrMerge(separacion.getAtention());
        }
        ((ApplicationDTO)applicationBean.getSeparaciones().get(separacion.getEmpleado().getEmpleado().getIdEmpleado())).setAtention(null);
        applicationBean.getSeparaciones().remove(separacion.getEmpleado().getIdUsuario());
        separacion = null;
        onClean(actionEvent);
        setAtencionSeparada(Boolean.valueOf(false));
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent closeevent)
    {
    }

    public void verificarAtencion()
    {
        if(!atencionSeparada.booleanValue() && applicationBean.getSeparaciones().containsKey(getUsuario().getEmpleado().getIdEmpleado()))
        {
            LOG.debug((new StringBuilder()).append("\n\n\n\n Se encontr\363 separaci\363n para el paciente ").append(((ApplicationDTO)applicationBean.getSeparaciones().get(getUsuario().getEmpleado().getIdEmpleado())).getPacienteSeparado().getIdPersona()).append("\n\n\n\n").toString());
            setAtencionSeparada(Boolean.valueOf(true));
            ((ApplicationDTO)applicationBean.getSeparaciones().get(getUsuario().getEmpleado().getIdEmpleado())).setSeEncuentraEnAtencion(Boolean.valueOf(true));
            separacion = (ApplicationDTO)applicationBean.getSeparaciones().get(getUsuario().getEmpleado().getIdEmpleado());
        }
    }

    private void organizarPreguntasYRespuestas(List listaPreguntas, List listaRespuestas, Boolean esEspecialidad, Boolean esSubEspecialidad, Boolean esProcedimiento, EspecialidadDTO especialidadRelacionada, SubEspecialidad subEspecialidadRelacionada, 
            Procedimiento procedimientoRelacionado)
    {
        for(int index = 0; index < listaPreguntas.size(); index++)
        {
            PreguntaDTO pregunta = (PreguntaDTO)listaPreguntas.get(index);
            if(TipoRespuestaPreguntaEnum.CHECKBOX.getCode().equals(pregunta.getTipoRespuesta()) || TipoRespuestaPreguntaEnum.CHECKBOX_OTRO.getCode().equals(pregunta.getTipoRespuesta()))
            {
                StringBuilder respuestas = new StringBuilder();
                int contRespuestas = 1;
                List aux = pregunta.getRespuestasSeleccionadas();
                for(Iterator i$ = aux.iterator(); i$.hasNext();)
                {
                    String string = (String)i$.next();
                    if(contRespuestas == aux.size())
                        respuestas.append(string);
                    else
                        respuestas.append(string).append(";");
                    contRespuestas++;
                }

                if(StringUtils.isNotBlank(pregunta.getRespuestaOTRO()))
                    if(respuestas.length() > 0)
                    {
                        respuestas.append(";");
                        respuestas.append(pregunta.getRespuestaOTRO());
                    } else
                    {
                        respuestas.append(pregunta.getRespuestaOTRO());
                    }
                ((RespuestaDTO)listaRespuestas.get(index)).setDetalleRespuesta(respuestas.toString());
            }
            if(TipoRespuestaPreguntaEnum.RADIOBUTTON_OTRO.getCode().equals(pregunta.getTipoRespuesta()))
            {
                StringBuilder respuestas = new StringBuilder();
                String rpta = ((RespuestaDTO)listaRespuestas.get(index)).getDetalleRespuesta();
                if(StringUtils.isNotBlank(rpta))
                    respuestas.append(rpta);
                if(StringUtils.isNotBlank(pregunta.getRespuestaOTRO()))
                    if(respuestas.length() > 0)
                    {
                        respuestas.append(";");
                        respuestas.append(pregunta.getRespuestaOTRO());
                    } else
                    {
                        respuestas.append(pregunta.getRespuestaOTRO());
                    }
                ((RespuestaDTO)listaRespuestas.get(index)).setDetalleRespuesta(respuestas.toString());
            }
            if(esEspecialidad.booleanValue() && especialidadRelacionada != null)
                ((RespuestaDTO)listaRespuestas.get(index)).setEspecialidad(especialidadRelacionada);
            if(esSubEspecialidad.booleanValue() && subEspecialidadRelacionada != null)
                ((RespuestaDTO)listaRespuestas.get(index)).setSubEspecialidad((SubEspecialidadDTO)mapperService.map(subEspecialidadRelacionada, infraestructura/dto/SubEspecialidadDTO));
            if(esProcedimiento.booleanValue() && procedimientoRelacionado != null)
                ((RespuestaDTO)listaRespuestas.get(index)).setProcedimiento((ProcedimientoDTO)mapperService.map(procedimientoRelacionado, infraestructura/dto/ProcedimientoDTO));
            separacion.getAtention().agregarRespuestas(new RespuestaDTO[] {
                (RespuestaDTO)listaRespuestas.get(index)
            });
        }

    }

    public String addEnfermedad(Enfermedad enfermedad)
    {
        if(!listaEnfermedadesSeleccionadas.contains(enfermedad))
        {
            listaEnfermedadesSeleccionadas.add(enfermedad);
            listaEnfermedades.clear();
            criterioBusquedaEnfermedades = null;
            return "pm:verEnfermedades";
        } else
        {
            return "";
        }
    }

    public void removeEnfermedad(Enfermedad enfermedad)
    {
        listaEnfermedadesSeleccionadas.remove(enfermedad);
    }

    public void cargarPreguntasGenerales(ActionEvent event)
    {
        preguntasGenerales.clear();
        respuestasGenerales.clear();
        PreguntaDTO preguntaFiltro = new PreguntaDTO();
        preguntaFiltro.setEstadoPregunta("A");
        preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
        preguntasGenerales = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
        respuestasGenerales.clear();
        RespuestaDTO respuesta;
        for(Iterator i$ = preguntasGenerales.iterator(); i$.hasNext(); respuestasGenerales.add(respuesta))
        {
            PreguntaDTO preguntaGeneral = (PreguntaDTO)i$.next();
            respuesta = new RespuestaDTO();
            respuesta.setDescripcionPregunta(preguntaGeneral.getDescripcionPregunta());
        }

        if(preguntasGenerales.isEmpty())
            setPintarGeneral(Boolean.valueOf(false));
        else
            setPintarGeneral(Boolean.valueOf(true));
    }

    public void cargarPreguntasDeEspecialidad(ActionEvent event)
    {
        PreguntaDTO preguntaFiltro = new PreguntaDTO();
        preguntaFiltro.setEstadoPregunta("A");
        preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
        preguntaFiltro.setEspecialidad((EspecialidadDTO)mapperService.map(separacion.getAtention().getTrabajo().getEspecialidad(), infraestructura/dto/EspecialidadDTO));
        preguntasEspecialidad = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
        respuestasEspecialidad.clear();
        RespuestaDTO respuesta;
        for(Iterator i$ = preguntasEspecialidad.iterator(); i$.hasNext(); respuestasEspecialidad.add(respuesta))
        {
            PreguntaDTO preguntaEspecialidad = (PreguntaDTO)i$.next();
            respuesta = new RespuestaDTO();
            respuesta.setDescripcionPregunta(preguntaEspecialidad.getDescripcionPregunta());
        }

        if(preguntasEspecialidad.isEmpty())
            setPintarEspecialidad(Boolean.valueOf(false));
        else
            setPintarEspecialidad(Boolean.valueOf(true));
        Especialidad especialidad = new Especialidad();
        especialidad.setIdEspecialidad(separacion.getAtention().getTrabajo().getEspecialidad().getIdEspecialidad());
        SubEspecialidad subEspecialidadFiltro = new SubEspecialidad();
        subEspecialidadFiltro.setEspecialidad(especialidad);
        listaSubEspecialidades = subEspecialidadRepository.findByFilter(subEspecialidadFiltro);
    }

    public void cargarPreguntasSubEspecialidad(ActionEvent event)
    {
        PreguntaDTO preguntaFiltro = new PreguntaDTO();
        preguntaFiltro.setEstadoPregunta("A");
        preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
        subEspecialidadSeleccionada = (SubEspecialidad)subEspecialidadRepository.find(Integer.valueOf(Integer.parseInt(idSubEspecialidad)));
        preguntaFiltro.setSubEspecialidad((SubEspecialidadDTO)mapperService.map(subEspecialidadSeleccionada, infraestructura/dto/SubEspecialidadDTO));
        preguntasSubEspecialidad = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
        respuestasSubEspecialidad.clear();
        RespuestaDTO respuesta;
        for(Iterator i$ = preguntasSubEspecialidad.iterator(); i$.hasNext(); respuestasSubEspecialidad.add(respuesta))
        {
            PreguntaDTO preguntaSubEspecialidad = (PreguntaDTO)i$.next();
            respuesta = new RespuestaDTO();
            respuesta.setDescripcionPregunta(preguntaSubEspecialidad.getDescripcionPregunta());
        }

        if(preguntasSubEspecialidad.isEmpty())
            setPintarSubEspecialidad(Boolean.valueOf(false));
        else
            setPintarSubEspecialidad(Boolean.valueOf(true));
        Procedimiento procedimiento = new Procedimiento();
        procedimiento.setSubEspecialidad(subEspecialidadSeleccionada);
        listaProcedimientos = procedimientoRepository.findByFilter(procedimiento);
    }

    public void cargarPreguntasProcedimiento(ActionEvent event)
    {
        PreguntaDTO preguntaFiltro = new PreguntaDTO();
        preguntaFiltro.setEstadoPregunta("A");
        preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
        procedimientoSeleccionado = (Procedimiento)procedimientoRepository.find(Integer.valueOf(Integer.parseInt(idProcedimiento)));
        preguntaFiltro.setProcedimiento((ProcedimientoDTO)mapperService.map(procedimientoSeleccionado, infraestructura/dto/ProcedimientoDTO));
        preguntasProcedimiento = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
        respuestasProcedimiento.clear();
        RespuestaDTO respuesta;
        for(Iterator i$ = preguntasProcedimiento.iterator(); i$.hasNext(); respuestasProcedimiento.add(respuesta))
        {
            PreguntaDTO preguntaProcedimiento = (PreguntaDTO)i$.next();
            respuesta = new RespuestaDTO();
            respuesta.setDescripcionPregunta(preguntaProcedimiento.getDescripcionPregunta());
        }

        if(preguntasProcedimiento.isEmpty())
            setPintarProcedimiento(Boolean.valueOf(false));
        else
            setPintarProcedimiento(Boolean.valueOf(true));
    }

    public void cancelarPreguntasGenerales(ActionEvent actionevent)
    {
    }

    public void cancelarPreguntasEspecialidad(ActionEvent actionevent)
    {
    }

    public void cancelarPreguntasSubEspecialidad(ActionEvent actionevent)
    {
    }

    public void cancelarPreguntasProcedimiento(ActionEvent actionevent)
    {
    }

    public void completeEnfermedad(AjaxBehaviorEvent ajaxBehaviorEvent)
    {
        if(StringUtils.isNotBlank(criterioBusquedaEnfermedades))
        {
            criterioBusquedaEnfermedades = criterioBusquedaEnfermedades.trim();
            Enfermedad filtro = new Enfermedad();
            filtro.setIdEnfermedad(criterioBusquedaEnfermedades);
            filtro.setNombreEnfermedad(criterioBusquedaEnfermedades);
            if(criterioBusquedaEnfermedades.length() > 0)
                listaEnfermedades = enfermedadRepository.findByFilterNoCap(filtro);
        }
    }

    public Boolean getAtencionSeparada()
    {
        return atencionSeparada;
    }

    public void setAtencionSeparada(Boolean atencionSeparada)
    {
        this.atencionSeparada = atencionSeparada;
    }

    public ApplicationDTO getSeparacion()
    {
        return separacion;
    }

    public void setSeparacion(ApplicationDTO separacion)
    {
        this.separacion = separacion;
    }

    public Integer getContBusquedaSeparaciones()
    {
        return contBusquedaSeparaciones;
    }

    public void setContBusquedaSeparaciones(Integer contBusquedaSeparaciones)
    {
        this.contBusquedaSeparaciones = contBusquedaSeparaciones;
    }

    public List getPreguntasGenerales()
    {
        return preguntasGenerales;
    }

    public void setPreguntasGenerales(List preguntasGenerales)
    {
        this.preguntasGenerales = preguntasGenerales;
    }

    public List getPreguntasEspecialidad()
    {
        return preguntasEspecialidad;
    }

    public void setPreguntasEspecialidad(List preguntasEspecialidad)
    {
        this.preguntasEspecialidad = preguntasEspecialidad;
    }

    public List getPreguntasSubEspecialidad()
    {
        return preguntasSubEspecialidad;
    }

    public void setPreguntasSubEspecialidad(List preguntasSubEspecialidad)
    {
        this.preguntasSubEspecialidad = preguntasSubEspecialidad;
    }

    public List getPreguntasProcedimiento()
    {
        return preguntasProcedimiento;
    }

    public void setPreguntasProcedimiento(List preguntasProcedimiento)
    {
        this.preguntasProcedimiento = preguntasProcedimiento;
    }

    public List getRespuestasGenerales()
    {
        return respuestasGenerales;
    }

    public void setRespuestasGenerales(List respuestasGenerales)
    {
        this.respuestasGenerales = respuestasGenerales;
    }

    public List getRespuestasEspecialidad()
    {
        return respuestasEspecialidad;
    }

    public void setRespuestasEspecialidad(List respuestasEspecialidad)
    {
        this.respuestasEspecialidad = respuestasEspecialidad;
    }

    public List getRespuestasSubEspecialidad()
    {
        return respuestasSubEspecialidad;
    }

    public void setRespuestasSubEspecialidad(List respuestasSubEspecialidad)
    {
        this.respuestasSubEspecialidad = respuestasSubEspecialidad;
    }

    public List getRespuestasProcedimiento()
    {
        return respuestasProcedimiento;
    }

    public void setRespuestasProcedimiento(List respuestasProcedimiento)
    {
        this.respuestasProcedimiento = respuestasProcedimiento;
    }

    public List getListaSubEspecialidades()
    {
        return listaSubEspecialidades;
    }

    public void setListaSubEspecialidades(List listaSubEspecialidades)
    {
        this.listaSubEspecialidades = listaSubEspecialidades;
    }

    public List getListaProcedimientos()
    {
        return listaProcedimientos;
    }

    public void setListaProcedimientos(List listaProcedimientos)
    {
        this.listaProcedimientos = listaProcedimientos;
    }

    public SubEspecialidad getSubEspecialidadSeleccionada()
    {
        return subEspecialidadSeleccionada;
    }

    public void setSubEspecialidadSeleccionada(SubEspecialidad subEspecialidadSeleccionada)
    {
        this.subEspecialidadSeleccionada = subEspecialidadSeleccionada;
    }

    public Procedimiento getProcedimientoSeleccionado()
    {
        return procedimientoSeleccionado;
    }

    public void setProcedimientoSeleccionado(Procedimiento procedimientoSeleccionado)
    {
        this.procedimientoSeleccionado = procedimientoSeleccionado;
    }

    public String getIdSubEspecialidad()
    {
        return idSubEspecialidad;
    }

    public void setIdSubEspecialidad(String idSubEspecialidad)
    {
        this.idSubEspecialidad = idSubEspecialidad;
    }

    public String getIdProcedimiento()
    {
        return idProcedimiento;
    }

    public void setIdProcedimiento(String idProcedimiento)
    {
        this.idProcedimiento = idProcedimiento;
    }

    public Boolean getPintarGeneral()
    {
        return pintarGeneral;
    }

    public void setPintarGeneral(Boolean pintarGeneral)
    {
        this.pintarGeneral = pintarGeneral;
    }

    public Boolean getPintarEspecialidad()
    {
        return pintarEspecialidad;
    }

    public void setPintarEspecialidad(Boolean pintarEspecialidad)
    {
        this.pintarEspecialidad = pintarEspecialidad;
    }

    public Boolean getPintarSubEspecialidad()
    {
        return pintarSubEspecialidad;
    }

    public void setPintarSubEspecialidad(Boolean pintarSubEspecialidad)
    {
        this.pintarSubEspecialidad = pintarSubEspecialidad;
    }

    public Boolean getPintarProcedimiento()
    {
        return pintarProcedimiento;
    }

    public void setPintarProcedimiento(Boolean pintarProcedimiento)
    {
        this.pintarProcedimiento = pintarProcedimiento;
    }

    public List getListaEnfermedades()
    {
        return listaEnfermedades;
    }

    public void setListaEnfermedades(List listaEnfermedades)
    {
        this.listaEnfermedades = listaEnfermedades;
    }

    public Enfermedad getEnfermedadSeleccionada()
    {
        return enfermedadSeleccionada;
    }

    public void setEnfermedadSeleccionada(Enfermedad enfermedadSeleccionada)
    {
        this.enfermedadSeleccionada = enfermedadSeleccionada;
    }

    public Boolean getPintarEnfermedades()
    {
        return pintarEnfermedades;
    }

    public void setPintarEnfermedades(Boolean pintarEnfermedades)
    {
        this.pintarEnfermedades = pintarEnfermedades;
    }

    public String getCriterioBusquedaEnfermedades()
    {
        return criterioBusquedaEnfermedades;
    }

    public void setCriterioBusquedaEnfermedades(String criterioBusquedaEnfermedades)
    {
        this.criterioBusquedaEnfermedades = criterioBusquedaEnfermedades;
    }

    public List getListaEnfermedadesSeleccionadas()
    {
        return listaEnfermedadesSeleccionadas;
    }

    public void setListaEnfermedadesSeleccionadas(List listaEnfermedadesSeleccionadas)
    {
        this.listaEnfermedadesSeleccionadas = listaEnfermedadesSeleccionadas;
    }

    @Autowired
    ApplicationBean applicationBean;
    @Autowired
    AtencionService atencionService;
    @Autowired
    EspecialidadService especialidadService;
    @Autowired
    SubEspecialidadService subEspecialidadService;
    @Autowired
    ProcedimientoService procedimientoService;
    @Autowired
    PreguntaService preguntaService;
    @Autowired
    FormulaService formulaService;
    @Autowired
    PersonaService personaService;
    @Autowired
    DTOMapperService mapperService;
    @Autowired
    EspecialidadRepository especialidadRepository;
    @Autowired
    SubEspecialidadRepository subEspecialidadRepository;
    @Autowired
    ProcedimientoRepository procedimientoRepository;
    @Autowired
    EnfermedadRepository enfermedadRepository;
    private List preguntasGenerales;
    private List preguntasEspecialidad;
    private List preguntasSubEspecialidad;
    private List preguntasProcedimiento;
    private List respuestasGenerales;
    private List respuestasEspecialidad;
    private List respuestasSubEspecialidad;
    private List respuestasProcedimiento;
    private List listaSubEspecialidades;
    private List listaProcedimientos;
    private SubEspecialidad subEspecialidadSeleccionada;
    private String idSubEspecialidad;
    private Procedimiento procedimientoSeleccionado;
    private String idProcedimiento;
    private Enfermedad enfermedadSeleccionada;
    private Boolean atencionSeparada;
    private ApplicationDTO separacion;
    private Integer contBusquedaSeparaciones;
    private Boolean pintarGeneral;
    private Boolean pintarEspecialidad;
    private Boolean pintarSubEspecialidad;
    private Boolean pintarProcedimiento;
    private Boolean pintarEnfermedades;
    private List listaEnfermedades;
    private List listaEnfermedadesSeleccionadas;
    private String criterioBusquedaEnfermedades;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/beans/AtencionMobileJSFBean);

}
