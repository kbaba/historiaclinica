// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:34:11 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntasJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import infraestructura.enums.*;
import infraestructura.exception.ApplicationException;
import java.io.PrintStream;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.sql.DataSource;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.model.LazyDataModel;
import paquete.lazyModel.PreguntaLazyModel;
import paquete.service.*;
import util.FindCrudBeanBase;
import util.Validaciones;

@ManagedBean
@Scope(value="session")
@Component
public class PreguntasJSFBean extends FindCrudBeanBase
{

    public PreguntasJSFBean()
    {
        listaQuienPregunta = new ArrayList();
        listaTipoRespuestaPregunta = new ArrayList();
        listaEstadoPregunta = new ArrayList();
        listaEstadoAlternativa = new ArrayList();
        listaAlternativas = new ArrayList();
        listaAlternativasNuevaPregunta = new ArrayList();
    }

    @PostConstruct
    public void init()
    {
        validar = new Validaciones();
        mostrarDetalle = true;
        listaQuienPregunta = QuienPreguntaEnum.findAll();
        listaTipoRespuestaPregunta = TipoRespuestaPreguntaEnum.findAll();
        listaEstadoPregunta = EstadoPreguntaEnum.findAll();
        listaEstadoAlternativa = EstadoAlternativaEnum.findAll();
        preguntaSeleccionada = new PreguntaDTO();
        preguntaFiltro = new PreguntaDTO();
        inicializaNuevaPregunta();
        inicializaEspecialidadSubEspecialidadProcedimiento();
        preguntaLazyModel = new PreguntaLazyModel(preguntaFiltro, preguntaService, true);
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionevent)
    {
    }

    public void onClean(ActionEvent actionEvent)
    {
        preguntaLazyModel = new PreguntaLazyModel(preguntaFiltro, preguntaService, false);
        preguntaFiltro = new PreguntaDTO();
        preguntaSeleccionada = new PreguntaDTO();
        listaAlternativasNuevaPregunta = new ArrayList();
        listaAlternativas = new ArrayList();
        inicializaEspecialidadSubEspecialidadProcedimiento();
        inicializaNuevaPregunta();
    }

    public void onPersist(ActionEvent actionevent)
    {
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent closeevent)
    {
    }

    private void ocultaDialogModificar()
    {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("detailDialog.hide()");
    }

    public void realizaBusqueda()
    {
        if(especialidadSeleccionadaId != null && especialidadSeleccionadaId.intValue() != -1)
        {
            EspecialidadDTO especialidad = especialidadService.find(especialidadSeleccionadaId);
            preguntaFiltro.setEspecialidad(especialidad);
        } else
        {
            preguntaFiltro.setEspecialidad(null);
        }
        if(subEspecialidadSeleccionadaId != null && subEspecialidadSeleccionadaId.intValue() != -1)
        {
            SubEspecialidadDTO subEspecialidad = subEspecialidadService.findById(subEspecialidadSeleccionadaId);
            preguntaFiltro.setSubEspecialidad(subEspecialidad);
        } else
        {
            preguntaFiltro.setSubEspecialidad(null);
        }
        if(procedimientoSeleccionadoId != null && procedimientoSeleccionadoId.intValue() != -1)
        {
            ProcedimientoDTO procedimiento = procedimientoService.findById(procedimientoSeleccionadoId.intValue());
            preguntaFiltro.setProcedimiento(procedimiento);
        } else
        {
            preguntaFiltro.setProcedimiento(null);
        }
        if(preguntaFiltro.getEstadoPreguntaEnum() != null)
            preguntaFiltro.setEstadoPregunta(preguntaFiltro.getEstadoPreguntaEnum().getCode());
        else
            preguntaFiltro.setEstadoPregunta(null);
        if(preguntaFiltro.getQuienPreguntaEnum() != null)
            preguntaFiltro.setQuienPregunta(preguntaFiltro.getQuienPreguntaEnum().getCode());
        else
            preguntaFiltro.setQuienPregunta(null);
        if(preguntaFiltro.getTipoPreguntaEnum() != null)
            preguntaFiltro.setTipoPregunta(preguntaFiltro.getTipoPreguntaEnum().getCode());
        else
            preguntaFiltro.setTipoPregunta(null);
        if(preguntaFiltro.getTipoRespuestaPreguntaEnum() != null)
            preguntaFiltro.setTipoRespuesta(preguntaFiltro.getTipoRespuestaPreguntaEnum().getCode());
        else
            preguntaFiltro.setTipoRespuesta(null);
        preguntaLazyModel = new PreguntaLazyModel(preguntaFiltro, preguntaService, true);
    }

    public void guardaPregunta(ActionEvent actionEvent)
    {
        if(nuevaPregunta.getQuienPreguntaEnum() != null)
            nuevaPregunta.setQuienPregunta(nuevaPregunta.getQuienPreguntaEnum().getCode());
        else
            nuevaPregunta.setQuienPregunta(null);
        if(nuevaPregunta.getTipoPreguntaEnum() != null)
            nuevaPregunta.setTipoPregunta(nuevaPregunta.getTipoPreguntaEnum().getCode());
        else
            nuevaPregunta.setTipoPregunta(null);
        if(nuevaPregunta.getTipoRespuestaPreguntaEnum() != null)
            nuevaPregunta.setTipoRespuesta(nuevaPregunta.getTipoRespuestaPreguntaEnum().getCode());
        else
            nuevaPregunta.setTipoRespuesta(null);
        String mensaje[] = {
            ""
        };
        boolean estaOk = validarDatosPreguntaGrabarNuevo(nuevaPregunta, mensaje);
        if(!estaOk)
        {
            addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        if(especialidadSeleccionadaId != null && especialidadSeleccionadaId.intValue() != -1)
        {
            EspecialidadDTO especialidad = especialidadService.find(especialidadSeleccionadaId);
            nuevaPregunta.setEspecialidad(especialidad);
        } else
        {
            nuevaPregunta.setEspecialidad(null);
        }
        if(subEspecialidadSeleccionadaId != null && subEspecialidadSeleccionadaId.intValue() != -1)
        {
            SubEspecialidadDTO subEspecialidad = subEspecialidadService.findById(subEspecialidadSeleccionadaId);
            nuevaPregunta.setSubEspecialidad(subEspecialidad);
        } else
        {
            nuevaPregunta.setSubEspecialidad(null);
        }
        if(procedimientoSeleccionadoId != null && procedimientoSeleccionadoId.intValue() != -1)
        {
            ProcedimientoDTO procedimiento = procedimientoService.findById(procedimientoSeleccionadoId.intValue());
            nuevaPregunta.setProcedimiento(procedimiento);
        } else
        {
            nuevaPregunta.setProcedimiento(null);
        }
        nuevaPregunta.setTipoPregunta("X");
        if(listaAlternativasNuevaPregunta.size() > 1)
        {
            Set conjunto = new HashSet();
            conjunto.addAll(listaAlternativasNuevaPregunta);
            nuevaPregunta.addAlternativas(conjunto);
        }
        preguntaService.guardarPregunta(nuevaPregunta);
        addMessage("Se guard\363 de manera correcta la nueva pregunta", FacesMessage.SEVERITY_INFO, new Object[0]);
        inicializaNuevaPregunta();
        inicializaEspecialidadSubEspecialidadProcedimiento();
        executeModal("persistDialog", false);
        realizaBusqueda();
    }

    public void actualizaData(ActionEvent actionEvent)
    {
        if(preguntaSeleccionada.getQuienPreguntaEnum() != null)
            preguntaSeleccionada.setQuienPregunta(preguntaSeleccionada.getQuienPreguntaEnum().getCode());
        else
            preguntaSeleccionada.setQuienPregunta(null);
        if(preguntaSeleccionada.getTipoPreguntaEnum() != null)
            preguntaSeleccionada.setTipoPregunta(preguntaSeleccionada.getTipoPreguntaEnum().getCode());
        else
            preguntaSeleccionada.setTipoPregunta(null);
        if(preguntaSeleccionada.getTipoRespuestaPreguntaEnum() != null)
            preguntaSeleccionada.setTipoRespuesta(preguntaSeleccionada.getTipoRespuestaPreguntaEnum().getCode());
        else
            preguntaSeleccionada.setTipoRespuesta(null);
        String mensaje[] = {
            ""
        };
        boolean estaOk = validarDatosPreguntaActualizar(preguntaSeleccionada, mensaje);
        if(!estaOk)
        {
            addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        if(especialidadSeleccionadaId != null && especialidadSeleccionadaId.intValue() != -1)
        {
            EspecialidadDTO especialidad = especialidadService.find(especialidadSeleccionadaId);
            preguntaSeleccionada.setEspecialidad(especialidad);
        } else
        {
            preguntaSeleccionada.setEspecialidad(null);
        }
        if(subEspecialidadSeleccionadaId != null && subEspecialidadSeleccionadaId.intValue() != -1)
        {
            SubEspecialidadDTO subEspecialidad = subEspecialidadService.findById(subEspecialidadSeleccionadaId);
            preguntaSeleccionada.setSubEspecialidad(subEspecialidad);
        } else
        {
            preguntaSeleccionada.setSubEspecialidad(null);
        }
        if(procedimientoSeleccionadoId != null && procedimientoSeleccionadoId.intValue() != -1)
        {
            ProcedimientoDTO procedimiento = procedimientoService.findById(procedimientoSeleccionadoId.intValue());
            preguntaSeleccionada.setProcedimiento(procedimiento);
        } else
        {
            preguntaSeleccionada.setProcedimiento(null);
        }
        preguntaSeleccionada.setTipoPregunta("X");
        if(listaAlternativas.size() > 1)
        {
            Set conjunto = actualizaEstadosAlternativaYTraeConjunto();
            conjunto.addAll(listaAlternativas);
            preguntaSeleccionada.setConjuntoAlternativas(conjunto);
        }
        preguntaService.guardarPregunta(preguntaSeleccionada);
        addMessage("Se actualiz\363 de manera correcta la informaci\363n de la pregunta", FacesMessage.SEVERITY_INFO, new Object[0]);
        preguntaSeleccionada = new PreguntaDTO();
        listaAlternativas = new ArrayList();
        ocultaDialogModificar();
        realizaBusqueda();
    }

    public void cargaDetallesDePregunta(ActionEvent event)
    {
        if(preguntaSeleccionada == null || preguntaSeleccionada.getIdPregunta() == null)
        {
            addMessage("Debe seleccionar antes una pregunta", FacesMessage.SEVERITY_WARN, new Object[0]);
            mostrarDetalle = false;
            executeModal("detailDialog", mostrarDetalle);
            return;
        }
        mostrarDetalle = true;
        if(preguntaSeleccionada.getEspecialidad() != null)
            especialidadSeleccionadaId = preguntaSeleccionada.getEspecialidad().getIdEspecialidad();
        else
            especialidadSeleccionadaId = Integer.valueOf(-1);
        if(preguntaSeleccionada.getSubEspecialidad() != null)
            subEspecialidadSeleccionadaId = preguntaSeleccionada.getSubEspecialidad().getIdSubEspecialidad();
        else
            subEspecialidadSeleccionadaId = Integer.valueOf(-1);
        if(preguntaSeleccionada.getProcedimiento() != null)
            procedimientoSeleccionadoId = preguntaSeleccionada.getProcedimiento().getIdProcedimiento();
        else
            procedimientoSeleccionadoId = Integer.valueOf(-1);
        listaAlternativas = new ArrayList();
        listaAlternativas.addAll(alternativaService.getAlternativasXPreguntaId(preguntaSeleccionada.getIdPregunta().intValue()));
        executeModal("detailDialog", mostrarDetalle);
        showNewDialog(event);
    }

    public void inicializaNuevaPregunta()
    {
        nuevaPregunta = new PreguntaDTO();
        nuevaPregunta.setEstadoPregunta("A");
        listaAlternativas = new ArrayList();
        listaAlternativasNuevaPregunta = new ArrayList();
    }

    private void inicializaEspecialidadSubEspecialidadProcedimiento()
    {
        listaEspecialidad = especialidadService.getAll();
        listaSubEspecialidad = new ArrayList();
        listaProcedimiento = new ArrayList();
        especialidadSeleccionadaId = new Integer(-1);
        subEspecialidadSeleccionadaId = new Integer(-1);
        procedimientoSeleccionadoId = new Integer(-1);
    }

    private boolean validarDatosPreguntaGrabarNuevo(PreguntaDTO pregunta, String mensaje[])
    {
        if(pregunta.getIdPregunta() != null)
        {
            if(preguntaService.esPreguntaRepetida(pregunta.getDescripcionPregunta(), pregunta.getIdPregunta().intValue()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con esa \"Descripci\363n\" de pregunta.Pregunta ya existente";
                return false;
            }
        } else
        if(preguntaService.esPreguntaRepetida(pregunta.getDescripcionPregunta()))
        {
            mensaje[0] = "Error ya existe un registro en el sistema con esa \"Descripci\363n\" de pregunta.Pregunta ya existente";
            return false;
        }
        if(pregunta.getQuienPregunta() == null)
        {
            mensaje[0] = "Error debe especificar  \"Qui\351n Pregunta\" la pregunta.Qui\351n Pregunta no puede estar vac\355o";
            return false;
        }
        if(pregunta.getTipoRespuesta() == null)
        {
            mensaje[0] = "Error debe especificar  \"Tipo Respuesta Pregunta\" la pregunta.Tipo Respuesta Pregunta no puede estar vac\355o";
            return false;
        }
        if(!pregunta.getTipoRespuesta().equalsIgnoreCase("G") && !pregunta.getTipoRespuesta().equalsIgnoreCase("O") && !pregunta.getTipoRespuesta().equalsIgnoreCase("C") && !pregunta.getTipoRespuesta().equalsIgnoreCase("Z") && !listaAlternativasNuevaPregunta.isEmpty())
        {
            mensaje[0] = "Error los tipos de preguntas que no sean  \"Grupo\", \"Check\" no pueden tener alternativas";
            return false;
        }
        if(pregunta.getTipoRespuesta().equalsIgnoreCase("G") || pregunta.getTipoRespuesta().equalsIgnoreCase("O") || pregunta.getTipoRespuesta().equalsIgnoreCase("C") || pregunta.getTipoRespuesta().equalsIgnoreCase("Z"))
        {
            if(listaAlternativasNuevaPregunta.size() < 2)
            {
                mensaje[0] = "Error el tipo de respuesta depregunta \"Grupo\", \"Check\" necesita al menos 02 alternativas.";
                return false;
            }
            if(contieneAlternativaRepetida(listaAlternativasNuevaPregunta, mensaje))
                return false;
        }
        return true;
    }

    private boolean validarDatosPreguntaActualizar(PreguntaDTO pregunta, String mensaje[])
    {
        if(pregunta.getIdPregunta() != null)
        {
            if(preguntaService.esPreguntaRepetida(pregunta.getDescripcionPregunta(), pregunta.getIdPregunta().intValue()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con esa \"Descripci\363n\" de pregunta.Pregunta ya existente";
                return false;
            }
        } else
        if(preguntaService.esPreguntaRepetida(pregunta.getDescripcionPregunta()))
        {
            mensaje[0] = "Error ya existe un registro en el sistema con esa \"Descripci\363n\" de pregunta.Pregunta ya existente";
            return false;
        }
        if(pregunta.getQuienPregunta() == null)
        {
            mensaje[0] = "Error debe especificar  \"Qui\351n Pregunta\" la pregunta.Qui\351n Pregunta no puede estar vac\355o";
            return false;
        }
        if(pregunta.getTipoRespuesta() == null)
        {
            mensaje[0] = "Error debe especificar  \"Tipo Respuesta Pregunta\" la pregunta.Tipo Respuesta Pregunta no puede estar vac\355o";
            return false;
        }
        if(!pregunta.getTipoRespuesta().equalsIgnoreCase("G") && !pregunta.getTipoRespuesta().equalsIgnoreCase("O") && !pregunta.getTipoRespuesta().equalsIgnoreCase("C") && !pregunta.getTipoRespuesta().equalsIgnoreCase("Z") && !listaAlternativas.isEmpty())
        {
            mensaje[0] = "Error los tipos de preguntas que no sean  \"Grupo\", \"Check\" no pueden tener alternativas";
            return false;
        }
        if(pregunta.getTipoRespuesta().equalsIgnoreCase("G") || pregunta.getTipoRespuesta().equalsIgnoreCase("O") || pregunta.getTipoRespuesta().equalsIgnoreCase("C") || pregunta.getTipoRespuesta().equalsIgnoreCase("Z"))
        {
            if(listaAlternativas.size() < 2)
            {
                mensaje[0] = "Error el tipo de respuesta depregunta \"Grupo\", \"Check\" necesita al menos 02 alternativas.";
                return false;
            }
            if(contieneAlternativaRepetida(listaAlternativas, mensaje))
                return false;
        }
        return true;
    }

    private boolean contieneAlternativaRepetida(List listaAlternativas, String mensaje[])
    {
        for(int i = 0; i < listaAlternativas.size(); i++)
        {
            AlternativaDTO item = (AlternativaDTO)listaAlternativas.get(i);
            for(int j = i + 1; j < listaAlternativas.size(); j++)
            {
                AlternativaDTO item2 = (AlternativaDTO)listaAlternativas.get(j);
                if(item.getDescripcionAlternativa().equalsIgnoreCase(item2.getDescripcionAlternativa()))
                {
                    mensaje[0] = (new StringBuilder()).append("Error alternativa repetida: ").append(item.getDescripcionAlternativa()).toString();
                    return true;
                }
            }

        }

        return false;
    }

    private HashSet actualizaEstadosAlternativaYTraeConjunto()
    {
        HashSet conjunto = new HashSet();
        for(int i = 0; i < listaAlternativas.size(); i++)
        {
            AlternativaDTO alternativa = (AlternativaDTO)listaAlternativas.get(i);
            alternativa.setEstadoAlternativa(alternativa.getEstadoAlternativaEnum().getCode());
        }

        return conjunto;
    }

    public void manejaCambioEspecialidad()
    {
        if(especialidadSeleccionadaId != null && especialidadSeleccionadaId.intValue() != -1)
        {
            listaSubEspecialidad = subEspecialidadService.findByEspecialidadId(especialidadSeleccionadaId);
        } else
        {
            listaSubEspecialidad = new ArrayList();
            subEspecialidadSeleccionadaId = new Integer(-1);
        }
        listaProcedimiento = new ArrayList();
        procedimientoSeleccionadoId = new Integer(-1);
    }

    public void manejaCambioSubEspecialidad()
    {
        if(subEspecialidadSeleccionadaId != null && especialidadSeleccionadaId.intValue() != -1)
        {
            listaProcedimiento = procedimientoService.findBySubEspecialidad(subEspecialidadSeleccionadaId);
        } else
        {
            listaProcedimiento = new ArrayList();
            procedimientoSeleccionadoId = new Integer(-1);
        }
    }

    public void addAlternativa(ActionEvent event)
    {
        try
        {
            AlternativaDTO nuevaAlternativa = new AlternativaDTO();
            nuevaAlternativa.setEstadoAlternativa("A");
            listaAlternativasNuevaPregunta.add(nuevaAlternativa);
        }
        catch(ApplicationException e)
        {
            addMessage(e.getMessage(), FacesMessage.SEVERITY_WARN, e.getParameters());
        }
    }

    public void addAlternativaDetail(ActionEvent event)
    {
        try
        {
            AlternativaDTO nuevaAlternativa = new AlternativaDTO();
            nuevaAlternativa.setEstadoAlternativa("A");
            listaAlternativas.add(nuevaAlternativa);
        }
        catch(ApplicationException e)
        {
            addMessage(e.getMessage(), FacesMessage.SEVERITY_WARN, e.getParameters());
        }
    }

    public void removeAlternativa(ActionEvent event)
    {
        if(!listaAlternativasNuevaPregunta.isEmpty())
        {
            listaAlternativasNuevaPregunta.remove(listaAlternativasNuevaPregunta.size() - 1);
            return;
        } else
        {
            addMessage("No hay m\341s alternativas que borrar", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        }
    }

    public void removeAlternativaDetail(ActionEvent event)
    {
        if(!listaAlternativas.isEmpty())
        {
            listaAlternativas.remove(listaAlternativas.size() - 1);
            return;
        } else
        {
            addMessage("No hay m\341s alternativas que borrar", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        }
    }

    public void depurar(String mensaje)
    {
        System.out.println((new StringBuilder()).append("####").append(mensaje).toString());
    }

    public void toIndex(ActionEvent e)
    {
        redireccionar("../index.jsf");
    }

    public boolean isMostrarDetalle()
    {
        return mostrarDetalle;
    }

    public void setMostrarDetalle(boolean mostrarDetalle)
    {
        this.mostrarDetalle = mostrarDetalle;
    }

    public List getListaAlternativasNuevaPregunta()
    {
        return listaAlternativasNuevaPregunta;
    }

    public void setListaAlternativasNuevaPregunta(List listaAlternativasNuevaPregunta)
    {
        this.listaAlternativasNuevaPregunta = listaAlternativasNuevaPregunta;
    }

    public PreguntaDTO getPreguntaFiltro()
    {
        return preguntaFiltro;
    }

    public void setPreguntaFiltro(PreguntaDTO preguntaFiltro)
    {
        this.preguntaFiltro = preguntaFiltro;
    }

    public PreguntaDTO getPreguntaSeleccionada()
    {
        return preguntaSeleccionada;
    }

    public void setPreguntaSeleccionada(PreguntaDTO preguntaSeleccionada)
    {
        this.preguntaSeleccionada = preguntaSeleccionada;
    }

    public AlternativaService getAlternativaService()
    {
        return alternativaService;
    }

    public void setAlternativaService(AlternativaService alternativaService)
    {
        this.alternativaService = alternativaService;
    }

    public PreguntaDTO getNuevaPregunta()
    {
        return nuevaPregunta;
    }

    public void setNuevaPregunta(PreguntaDTO nuevaPregunta)
    {
        this.nuevaPregunta = nuevaPregunta;
    }

    public AlternativaDTO getNuevaAlternativa()
    {
        return nuevaAlternativa;
    }

    public void setNuevaAlternativa(AlternativaDTO nuevaAlternativa)
    {
        this.nuevaAlternativa = nuevaAlternativa;
    }

    public List getListaAlternativas()
    {
        return listaAlternativas;
    }

    public void setListaAlternativas(List listaAlternativas)
    {
        this.listaAlternativas = listaAlternativas;
    }

    public LazyDataModel getPreguntaLazyModel()
    {
        return preguntaLazyModel;
    }

    public void setPreguntaLazyModel(LazyDataModel preguntaLazyModel)
    {
        this.preguntaLazyModel = preguntaLazyModel;
    }

    public Date getHoy()
    {
        Calendar calendar1 = Calendar.getInstance();
        hoy = calendar1.getTime();
        return hoy;
    }

    public EspecialidadService getEspecialidadService()
    {
        return especialidadService;
    }

    public void setEspecialidadService(EspecialidadService especialidadService)
    {
        this.especialidadService = especialidadService;
    }

    public List getListaEspecialidad()
    {
        return listaEspecialidad;
    }

    public void setListaEspecialidad(List listaEspecialidad)
    {
        this.listaEspecialidad = listaEspecialidad;
    }

    public List getListaSubEspecialidad()
    {
        return listaSubEspecialidad;
    }

    public void setListaSubEspecialidad(List listaSubEspecialidad)
    {
        this.listaSubEspecialidad = listaSubEspecialidad;
    }

    public List getListaProcedimiento()
    {
        return listaProcedimiento;
    }

    public void setListaProcedimiento(List listaProcedimiento)
    {
        this.listaProcedimiento = listaProcedimiento;
    }

    public Integer getEspecialidadSeleccionadaId()
    {
        return especialidadSeleccionadaId;
    }

    public void setEspecialidadSeleccionadaId(Integer especialidadSeleccionadaId)
    {
        this.especialidadSeleccionadaId = especialidadSeleccionadaId;
    }

    public Integer getSubEspecialidadSeleccionadaId()
    {
        return subEspecialidadSeleccionadaId;
    }

    public void setSubEspecialidadSeleccionadaId(Integer subEspecialidadSeleccionadaId)
    {
        this.subEspecialidadSeleccionadaId = subEspecialidadSeleccionadaId;
    }

    public Integer getProcedimientoSeleccionadoId()
    {
        return procedimientoSeleccionadoId;
    }

    public void setProcedimientoSeleccionadoId(Integer procedimientoSeleccionadoId)
    {
        this.procedimientoSeleccionadoId = procedimientoSeleccionadoId;
    }

    public List getListaQuienPregunta()
    {
        return listaQuienPregunta;
    }

    public void setListaQuienPregunta(List listaQuienPregunta)
    {
        this.listaQuienPregunta = listaQuienPregunta;
    }

    public List getListaTipoRespuestaPregunta()
    {
        return listaTipoRespuestaPregunta;
    }

    public void setListaTipoRespuestaPregunta(List listaTipoRespuestaPregunta)
    {
        this.listaTipoRespuestaPregunta = listaTipoRespuestaPregunta;
    }

    public List getListaEstadoPregunta()
    {
        return listaEstadoPregunta;
    }

    public void setListaEstadoPregunta(List listaEstadoPregunta)
    {
        this.listaEstadoPregunta = listaEstadoPregunta;
    }

    public List getListaEstadoAlternativa()
    {
        return listaEstadoAlternativa;
    }

    public void setListaEstadoAlternativa(List listaEstadoAlternativa)
    {
        this.listaEstadoAlternativa = listaEstadoAlternativa;
    }

    public void setHoy(Date hoy)
    {
        this.hoy = hoy;
    }

    @Autowired
    DataSource dataSource;
    @Autowired
    PreguntaService preguntaService;
    @Autowired
    AlternativaService alternativaService;
    @Autowired
    EspecialidadService especialidadService;
    @Autowired
    SubEspecialidadService subEspecialidadService;
    @Autowired
    ProcedimientoService procedimientoService;
    private PreguntaDTO preguntaFiltro;
    private PreguntaDTO preguntaSeleccionada;
    private PreguntaDTO nuevaPregunta;
    private AlternativaDTO nuevaAlternativa;
    private Date hoy;
    private Validaciones validar;
    private LazyDataModel preguntaLazyModel;
    private boolean mostrarDetalle;
    private List listaEspecialidad;
    private List listaSubEspecialidad;
    private List listaProcedimiento;
    private Integer especialidadSeleccionadaId;
    private Integer subEspecialidadSeleccionadaId;
    private Integer procedimientoSeleccionadoId;
    private List listaQuienPregunta;
    private List listaTipoRespuestaPregunta;
    private List listaEstadoPregunta;
    private List listaEstadoAlternativa;
    private List listaAlternativas;
    private List listaAlternativasNuevaPregunta;
}
