// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:33:03 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AtencionJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import infraestructura.enums.TipoRespuestaPreguntaEnum;
import infraestructura.formulas.FormulaFraminghamDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.primefaces.component.selectonelistbox.SelectOneListbox;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.*;
import paquete.repositorios.*;
import paquete.service.*;
import util.FindCrudBeanBase;

// Referenced classes of package paquete.beans:
//            ApplicationBean

@ManagedBean
@Scope(value="session")
@Component
public class AtencionJSFBean extends FindCrudBeanBase
{

    public AtencionJSFBean()
    {
        separacion = false;
        correcto = false;
    }

    @PostConstruct
    public void init()
    {
        especialidadFiltro = new Especialidad();
        subEspecialidadFiltro = new SubEspecialidad();
        procedimientoFiltro = new Procedimiento();
        mostrarTabGeneral = true;
        mostrarTabEspecialidad = false;
        mostrarTabSubEspecialidad = false;
        mostrarTabProcedimiento = false;
        preguntasGenerales = new ArrayList();
        preguntasEspecialidad = new ArrayList();
        preguntasSubEspecialidad = new ArrayList();
        preguntasProcedimiento = new ArrayList();
        respuestasGenerales = new ArrayList();
        respuestasEspecialidad = new ArrayList();
        respuestasSubEspecialidad = new ArrayList();
        respuestasProcedimiento = new ArrayList();
        atencion = new AtencionDTO();
        atencion.setFechaAtencion(new Date());
        enfermedadFiltro = new Enfermedad();
        listaEnfermedades = new ArrayList();
        selectedIndex = Integer.valueOf(0);
        idPersonaSeleccionada = Integer.valueOf(-1);
        fechaLuego = DateUtils.addDays(new Date(), 1);
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionEvent)
    {
        atencion = new AtencionDTO();
        especialidadFiltro = new Especialidad();
        subEspecialidadFiltro = new SubEspecialidad();
        procedimientoFiltro = new Procedimiento();
        enfermedadFiltro = new Enfermedad();
        enfermedadSeleccionada = new Enfermedad();
        preguntasGenerales = new ArrayList();
        preguntasEspecialidad = new ArrayList();
        preguntasSubEspecialidad = new ArrayList();
        preguntasProcedimiento = new ArrayList();
        respuestasGenerales = new ArrayList();
        respuestasEspecialidad = new ArrayList();
        respuestasSubEspecialidad = new ArrayList();
        respuestasProcedimiento = new ArrayList();
        mostrarTabGeneral = true;
        mostrarTabEspecialidad = false;
        mostrarTabSubEspecialidad = false;
        mostrarTabProcedimiento = false;
        listaEnfermedades.clear();
        setSelectedIndex(new Integer(0));
        resetForm(actionEvent.getComponent());
    }

    public void onClean(ActionEvent actionEvent)
    {
        especialidadFiltro = new Especialidad();
        subEspecialidadFiltro = new SubEspecialidad();
        procedimientoFiltro = new Procedimiento();
        personaSeleccionada = new PersonaDTO();
        preguntasGenerales.clear();
        respuestasGenerales.clear();
        preguntasEspecialidad.clear();
        respuestasEspecialidad.clear();
        preguntasSubEspecialidad.clear();
        respuestasSubEspecialidad.clear();
        preguntasProcedimiento.clear();
        respuestasProcedimiento.clear();
        listaEnfermedades.clear();
        atencion = new AtencionDTO();
        idPersonaSeleccionada = Integer.valueOf(-1);
    }

    public void onPersist(ActionEvent actionEvent)
    {
        dialogNameASE = (String)actionEvent.getComponent().getAttributes().get("dialogName");
        if(!isSeparacion());
        boolean encuentra = false;
        if(mostrarTabGeneral)
        {
            Iterator i$ = respuestasGenerales.iterator();
            do
            {
                if(!i$.hasNext())
                    break;
                RespuestaDTO respuestaGeneral = (RespuestaDTO)i$.next();
                if(StringUtils.isNotBlank(respuestaGeneral.getDetalleRespuesta()))
                    encuentra = true;
            } while(true);
            if(!encuentra)
            {
                addMessage("Rebe responder por lo menos una pregunta general", FacesMessage.SEVERITY_WARN, new Object[0]);
                setSuccess(false);
                return;
            }
        }
        if(mostrarTabEspecialidad)
        {
            Iterator i$ = respuestasEspecialidad.iterator();
            do
            {
                if(!i$.hasNext())
                    break;
                RespuestaDTO respuestaEspecialidad = (RespuestaDTO)i$.next();
                if(StringUtils.isNotBlank(respuestaEspecialidad.getDetalleRespuesta()))
                    encuentra = true;
            } while(true);
            if(!encuentra)
            {
                addMessage("Rebe responder por lo menos una pregunta de especialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
                setSuccess(false);
                return;
            }
        }
        if(mostrarTabSubEspecialidad)
        {
            Iterator i$ = respuestasSubEspecialidad.iterator();
            do
            {
                if(!i$.hasNext())
                    break;
                RespuestaDTO respuestaSubEspecialidad = (RespuestaDTO)i$.next();
                if(StringUtils.isNotBlank(respuestaSubEspecialidad.getDetalleRespuesta()))
                    encuentra = true;
            } while(true);
            if(!encuentra)
            {
                addMessage("Rebe responder por lo menos una pregunta de subespecialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
                setSuccess(false);
                return;
            }
        }
        if(mostrarTabProcedimiento)
        {
            Iterator i$ = respuestasProcedimiento.iterator();
            do
            {
                if(!i$.hasNext())
                    break;
                RespuestaDTO respuestaProcedimiento = (RespuestaDTO)i$.next();
                if(StringUtils.isNotBlank(respuestaProcedimiento.getDetalleRespuesta()))
                    encuentra = true;
            } while(true);
            if(!encuentra)
            {
                addMessage("Rebe responder por lo menos una pregunta de pocedimiento", FacesMessage.SEVERITY_WARN, new Object[0]);
                setSuccess(false);
                return;
            }
        }
        if(trabajoDTO != null)
        {
            atencion.setTrabajo(trabajoDTO);
        } else
        {
            List auxList = new ArrayList(getUsuario().getEmpleado().getConjuntoTrabajos());
            atencion.setTrabajo((TrabajoDTO)auxList.get(0));
        }
        organizarPreguntasYRespuestas(preguntasGenerales, respuestasGenerales, Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(false), null, null, null);
        organizarPreguntasYRespuestas(preguntasEspecialidad, respuestasEspecialidad, Boolean.valueOf(true), Boolean.valueOf(false), Boolean.valueOf(false), especialidadFiltro, null, null);
        organizarPreguntasYRespuestas(preguntasSubEspecialidad, respuestasSubEspecialidad, Boolean.valueOf(false), Boolean.valueOf(true), Boolean.valueOf(false), null, subEspecialidadFiltro, null);
        organizarPreguntasYRespuestas(preguntasProcedimiento, respuestasProcedimiento, Boolean.valueOf(false), Boolean.valueOf(false), Boolean.valueOf(true), null, null, procedimientoFiltro);
        atencion.setHistoria(personaSeleccionada.getPaciente().getHistoria());
        atencion.setFechaAtencion(Calendar.getInstance().getTime());
        atencion = atencionService.persistOrMerge(atencion);
        if(!listaEnfermedades.isEmpty())
        {
            Enfermedad enfermedad;
            for(Iterator i$ = listaEnfermedades.iterator(); i$.hasNext(); atencion.agregarEnfermedades(new EnfermedadDTO[] {
    (EnfermedadDTO)mapperService.map(enfermedad, infraestructura/dto/EnfermedadDTO)
}))
                enfermedad = (Enfermedad)i$.next();

            atencionService.persistOrMerge(atencion);
        }
        setSuccess(true);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map sessionMap = externalContext.getSessionMap();
        sessionMap.put("accion", "buscar");
        sessionMap.put("atencionGuardada", atencion);
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent closeevent)
    {
    }

    private void organizarPreguntasYRespuestas(List listaPreguntas, List listaRespuestas, Boolean esEspecialidad, Boolean esSubEspecialidad, Boolean esProcedimiento, Especialidad especialidadRelacionada, SubEspecialidad subEspecialidadRelacionada, 
            Procedimiento procedimientoRelacionado)
    {
        for(int index = 0; index < listaPreguntas.size(); index++)
        {
            PreguntaDTO pregunta = (PreguntaDTO)listaPreguntas.get(index);
            if(TipoRespuestaPreguntaEnum.CHECKBOX.getCode().equals(pregunta.getTipoRespuesta()) || TipoRespuestaPreguntaEnum.CHECKBOX_OTRO.getCode().equals(pregunta.getTipoRespuesta()))
            {
                StringBuilder respuestas = new StringBuilder();
                int contRespuestas = 1;
                List aux = pregunta.getRespuestasSeleccionadas();
                for(Iterator i$ = aux.iterator(); i$.hasNext();)
                {
                    String string = (String)i$.next();
                    if(contRespuestas == aux.size())
                        respuestas.append(string);
                    else
                        respuestas.append(string).append(";");
                    contRespuestas++;
                }

                if(StringUtils.isNotBlank(pregunta.getRespuestaOTRO()))
                    if(respuestas.length() > 0)
                    {
                        respuestas.append(";");
                        respuestas.append(pregunta.getRespuestaOTRO());
                    } else
                    {
                        respuestas.append(pregunta.getRespuestaOTRO());
                    }
                ((RespuestaDTO)listaRespuestas.get(index)).setDetalleRespuesta(respuestas.toString());
            }
            if(TipoRespuestaPreguntaEnum.RADIOBUTTON_OTRO.getCode().equals(pregunta.getTipoRespuesta()))
            {
                StringBuilder respuestas = new StringBuilder();
                String rpta = ((RespuestaDTO)listaRespuestas.get(index)).getDetalleRespuesta();
                if(StringUtils.isNotBlank(rpta))
                    respuestas.append(rpta);
                if(StringUtils.isNotBlank(pregunta.getRespuestaOTRO()))
                    if(respuestas.length() > 0)
                    {
                        respuestas.append(";");
                        respuestas.append(pregunta.getRespuestaOTRO());
                    } else
                    {
                        respuestas.append(pregunta.getRespuestaOTRO());
                    }
                ((RespuestaDTO)listaRespuestas.get(index)).setDetalleRespuesta(respuestas.toString());
            }
            if(esEspecialidad.booleanValue() && especialidadRelacionada != null)
                ((RespuestaDTO)listaRespuestas.get(index)).setEspecialidad((EspecialidadDTO)mapperService.map(especialidadRelacionada, infraestructura/dto/EspecialidadDTO));
            if(esSubEspecialidad.booleanValue() && subEspecialidadRelacionada != null)
                ((RespuestaDTO)listaRespuestas.get(index)).setSubEspecialidad((SubEspecialidadDTO)mapperService.map(subEspecialidadRelacionada, infraestructura/dto/SubEspecialidadDTO));
            if(esProcedimiento.booleanValue() && procedimientoRelacionado != null)
                ((RespuestaDTO)listaRespuestas.get(index)).setProcedimiento((ProcedimientoDTO)mapperService.map(procedimientoRelacionado, infraestructura/dto/ProcedimientoDTO));
            atencion.agregarRespuestas(new RespuestaDTO[] {
                (RespuestaDTO)listaRespuestas.get(index)
            });
        }

    }

    public void realizaCalculoRiesgoCardioVascular()
    {
        int id_paciente = personaSeleccionada.getPaciente().getIdPaciente().intValue();
        FormulaFraminghamDTO fra = new FormulaFraminghamDTO();
        Object objetos[] = new Object[6];
        String busqueda_pregunta[] = {
            fra.P_FUMADOR, fra.P_DIABETES, fra.P_HIPERTROFIA_V_I, fra.P_PRESION_ARTERIAL_SISTOLICA, fra.P_COLESTEROL, fra.P_HDL
        };
        String estado = rellaObjetosCV(objetos, busqueda_pregunta, id_paciente);
        if(!estado.equalsIgnoreCase("ok"))
        {
            addMessage(estado, FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        fra.setTIME_PERIOD(10D);
        fra.setS_SEXO(personaSeleccionada.getSexoPersona());
        fra.setEDAD(Double.parseDouble((new StringBuilder()).append(personaSeleccionada.getEdad()).append("").toString()));
        fra.setFUMADOR(Double.parseDouble(objetos[0].toString()));
        fra.setS_DIABETICO(objetos[1].toString());
        fra.setS_HIPERTROFIA_V_I(objetos[2].toString());
        fra.setPRESION_ARTERIAL_SISTOLICA(Double.parseDouble(objetos[3].toString()));
        fra.setCOLESTEROL_TOTAL(Double.parseDouble(objetos[4].toString()));
        fra.setHDL(Double.parseDouble(objetos[5].toString()));
        String resultado = (new StringBuilder()).append("El riesgo cardiovascular a (").append(fra.getTIME_PERIOD()).append(") a\361os es ").append(fra.getResultado(2)).append("%").toString();
        if(StringUtils.isNotBlank(atencion.getDiagnosticoAtencion()))
            atencion.setDiagnosticoAtencion((new StringBuilder()).append(atencion.getDiagnosticoAtencion()).append("\n").append(resultado).toString());
        else
            atencion.setDiagnosticoAtencion(resultado);
        addMessage((new StringBuilder()).append(resultado).append("\nSe ha guardado en el \341rea de texto diagn\363stico.").toString(), FacesMessage.SEVERITY_INFO, new Object[0]);
    }

    private String rellaObjetosCV(Object objetos[], String busqueda_pregunta[], int idPaciente)
    {
        for(int i = 0; i < objetos.length; i++)
        {
            objetos[i] = formulaService.findValorRespuesta(busqueda_pregunta[i], Integer.valueOf(idPaciente));
            if(objetos[i] == null || StringUtils.isBlank(objetos[i].toString()))
                return (new StringBuilder()).append("Error falta dato: ").append(busqueda_pregunta[i]).toString();
        }

        return "ok";
    }

    public void cerrarDialog(ActionEvent event)
    {
        String dialogName = (String)event.getComponent().getAttributes().get("dialogName");
        if(dialogName.equals("atencionDialog"))
        {
            atencion = new AtencionDTO();
            especialidadFiltro = new Especialidad();
            subEspecialidadFiltro = new SubEspecialidad();
            procedimientoFiltro = new Procedimiento();
            preguntasGenerales.clear();
            respuestasGenerales.clear();
            preguntasEspecialidad.clear();
            respuestasEspecialidad.clear();
            preguntasSubEspecialidad.clear();
            respuestasSubEspecialidad.clear();
            preguntasProcedimiento.clear();
            respuestasProcedimiento.clear();
            mostrarTabGeneral = true;
            mostrarTabEspecialidad = false;
            mostrarTabSubEspecialidad = false;
            mostrarTabProcedimiento = false;
            setSelectedIndex(new Integer(0));
        }
        if(dialogName.equals("entrevistarDialog"))
        {
            especialidadFiltro = new Especialidad();
            subEspecialidadFiltro = new SubEspecialidad();
            procedimientoFiltro = new Procedimiento();
            preguntasGenerales.clear();
            respuestasGenerales.clear();
            preguntasEspecialidad.clear();
            respuestasEspecialidad.clear();
            preguntasSubEspecialidad.clear();
            respuestasSubEspecialidad.clear();
            preguntasProcedimiento.clear();
            respuestasProcedimiento.clear();
            mostrarTabGeneral = true;
            mostrarTabEspecialidad = false;
            mostrarTabSubEspecialidad = false;
            mostrarTabProcedimiento = false;
            setSelectedIndex(new Integer(0));
        }
        if(dialogName.equals("enfermedadesDialog"))
        {
            listaEnfermedades.clear();
            enfermedadSeleccionada = new Enfermedad();
        }
        executeModal(dialogName, false);
        resetForm(event.getComponent());
    }

    public void metodoQueNoHaceNada(ActionEvent event)
    {
        String dialogName = getParam(event, "dialogName");
        executeModal(dialogName, false);
        resetForm(event.getComponent());
    }

    public String getLowerCaseDescripcionEspecialidad()
    {
        if(especialidadFiltro == null || StringUtils.isBlank(especialidadFiltro.getNombreEspecialidad()))
        {
            return "";
        } else
        {
            String lowerCaseDescripcion = especialidadFiltro.getNombreEspecialidad().toLowerCase().substring(0, 6);
            return lowerCaseDescripcion;
        }
    }

    public void separarAtenciF3n(ActionEvent actionEvent)
    {
        setSeparacion(true);
        verificarPersonaSeleccionada(actionEvent);
        if(correcto)
        {
            atencion.setFechaAtencion(DateUtils.addDays(new Date(), 0));
            if(trabajoDTO != null)
            {
                atencion.setTrabajo(trabajoDTO);
            } else
            {
                List auxList = new ArrayList(getUsuario().getEmpleado().getConjuntoTrabajos());
                atencion.setTrabajo((TrabajoDTO)auxList.get(0));
            }
            if(!applicationBean.getSeparaciones().containsKey(getUsuario().getEmpleado().getIdEmpleado()))
            {
                applicationBean.getSeparaciones().put(getUsuario().getEmpleado().getIdEmpleado(), new ApplicationDTO(personaSeleccionada, getUsuario(), atencion, new Date(), Boolean.valueOf(false)));
                StringBuilder builder = new StringBuilder();
                builder.append("Se ha separado una atenci\363n m\363vil para el paciente: ").append(personaSeleccionada.getNombrePersona()).append(" ").append(personaSeleccionada.getApellidoPaternoPersona()).append(" ").append(personaSeleccionada.getApellidoMaternoPersona());
                addMessage(builder.toString(), FacesMessage.SEVERITY_INFO, new Object[0]);
                addMessage("Dispone de 10 minutos para atenderlo v\355a m\363vil, luego esta ser\341 eliminada autom\341ticamente", FacesMessage.SEVERITY_WARN, new Object[0]);
            } else
            {
                ApplicationDTO antiguaSeparacion = (ApplicationDTO)applicationBean.getSeparaciones().get(getUsuario().getEmpleado().getIdEmpleado());
                if(((ApplicationDTO)applicationBean.getSeparaciones().get(getUsuario().getEmpleado().getIdEmpleado())).getPacienteSeparado().getIdPersona().equals(personaSeleccionada.getIdPersona()))
                {
                    if(antiguaSeparacion.getSeEncuentraEnAtencion().booleanValue())
                    {
                        addMessage("La separaci\363n solicitada ya se encuentra siendo atendida", FacesMessage.SEVERITY_ERROR, new Object[0]);
                    } else
                    {
                        Date horarioSeparacion = antiguaSeparacion.getHoraSeparacion();
                        Date horarioActual = new Date();
                        long difference = horarioActual.getTime() - horarioSeparacion.getTime();
                        int minutes = (int)((difference / 60000L) % 60L);
                        StringBuilder builder = new StringBuilder();
                        if(minutes == 0)
                            builder.append("No se puede realizar dos separaciones para un mismo paciente");
                        else
                        if(minutes == 1)
                            builder.append("La separaci\363n del paciente seleccionado ya fue realizada hace ").append(minutes).append(" minuto");
                        else
                            builder.append("La separaci\363n del paciente seleccionado ya fue realizada hace ").append(minutes).append(" minutos");
                        addMessage(builder.toString(), FacesMessage.SEVERITY_WARN, new Object[0]);
                    }
                } else
                if(antiguaSeparacion.getSeEncuentraEnAtencion().booleanValue())
                {
                    addMessage("No se puede realizar ninguna otra separaci\363n mientras se est\351 atendiendo la separaci\363n solicitada anteriormente", FacesMessage.SEVERITY_ERROR, new Object[0]);
                } else
                {
                    PersonaDTO pacienteSeparado = antiguaSeparacion.getPacienteSeparado();
                    applicationBean.getSeparaciones().put(getUsuario().getEmpleado().getIdEmpleado(), new ApplicationDTO(personaSeleccionada, getUsuario(), atencion, new Date(), Boolean.valueOf(false)));
                    StringBuilder builder = new StringBuilder();
                    builder.append("Se ha reemplazado la separaci\363n hecha al paciente: ").append(pacienteSeparado.getNombrePersona()).append(" ").append(pacienteSeparado.getApellidoPaternoPersona()).append(" ").append(pacienteSeparado.getApellidoMaternoPersona()).append(" ");
                    builder.append("\npor la nueva separaci\363n hecha al paciente: ").append(personaSeleccionada.getNombrePersona()).append(" ").append(personaSeleccionada.getApellidoPaternoPersona()).append(" ").append(personaSeleccionada.getApellidoMaternoPersona());
                    addMessage(builder.toString(), FacesMessage.SEVERITY_INFO, new Object[0]);
                    addMessage("Dispone de 10 minutos para atender esta nueva separaci\363n, luego esta ser\341 eliminada autom\341ticamente", FacesMessage.SEVERITY_WARN, new Object[0]);
                }
            }
        }
    }

    public List completeEspecialidad(String query)
    {
        List items = new ArrayList();
        query = query.trim();
        if(query.length() > 0)
        {
            if(trabajoDTO != null)
            {
                query = trabajoDTO.getEspecialidad().getNombreEspecialidad();
            } else
            {
                List auxList = new ArrayList(getUsuario().getEmpleado().getConjuntoTrabajos());
                query = ((TrabajoDTO)auxList.get(0)).getEspecialidad().getNombreEspecialidad();
            }
            items = especialidadRepository.completeEspecialidad(query.toUpperCase());
            if(items.isEmpty())
                items = especialidadRepository.completeEspecialidad(null);
        }
        return items;
    }

    public List completeSubEspecialidad(String query)
    {
        if(especialidadFiltro.getIdEspecialidad() != null)
        {
            subEspecialidadFiltro.setEspecialidad(especialidadFiltro);
            List items = new ArrayList();
            query = query.trim();
            if(query.length() > 0)
            {
                items = subEspecialidadRepository.completeSubEspecialidad(query.toUpperCase(), especialidadFiltro.getIdEspecialidad());
                if(items.isEmpty())
                    items = subEspecialidadRepository.completeSubEspecialidad(null, especialidadFiltro.getIdEspecialidad());
            }
            return items;
        } else
        {
            return null;
        }
    }

    public List completeProcedimiento(String query)
    {
        if(subEspecialidadFiltro.getIdSubEspecialidad() != null)
        {
            procedimientoFiltro.setSubEspecialidad(subEspecialidadFiltro);
            List items = new ArrayList();
            query = query.trim();
            if(query.length() > 0)
            {
                items = procedimientoRepository.completeProcedimiento(query.toUpperCase(), subEspecialidadFiltro.getIdSubEspecialidad());
                if(items.isEmpty())
                    items = procedimientoRepository.completeProcedimiento(null, subEspecialidadFiltro.getIdSubEspecialidad());
            }
            return items;
        } else
        {
            return null;
        }
    }

    public List completeEnfermedad(String query)
    {
        List items = new ArrayList();
        query = query.trim();
        Enfermedad filtro = new Enfermedad();
        filtro.setIdEnfermedad(query);
        filtro.setNombreEnfermedad(query);
        if(query.length() > 0)
            items = enfermedadRepository.findByFilterNoCap(filtro);
        return items;
    }

    public void limpiarFiltrosPorEspecialidad(SelectEvent event)
    {
        especialidadFiltro = (Especialidad)event.getObject();
        subEspecialidadFiltro = new SubEspecialidad();
        procedimientoFiltro = new Procedimiento();
        cargarPreguntasEspecialidad();
    }

    public void limpiarFiltrosPorSubEspecialidad(SelectEvent event)
    {
        subEspecialidadFiltro = (SubEspecialidad)event.getObject();
        procedimientoFiltro = new Procedimiento();
        cargarPreguntasSubEspecialidad();
    }

    public void verificarPersonaSeleccionada(ActionEvent actionEvent)
    {
        if(personaSeleccionada == null)
        {
            addMessage("Debe seleccionar un paciente", FacesMessage.SEVERITY_WARN, new Object[0]);
            setCorrecto(false);
        } else
        if(personaSeleccionada.getIdPersona() == null)
        {
            addMessage("Debe seleccionar un paciente", FacesMessage.SEVERITY_WARN, new Object[0]);
            setCorrecto(false);
        } else
        if(personaSeleccionada.getPaciente() == null)
        {
            addMessage("La persona seleccionada se encuentra asignada como paciente, por favor contacte a su administrador", FacesMessage.SEVERITY_WARN, new Object[0]);
            setCorrecto(false);
        } else
        if(personaSeleccionada.getPaciente().getHistoria() == null)
        {
            addMessage("La persona seleccionada no tiene asignada una historia cl\355nica, por favor contacte a su administrador", FacesMessage.SEVERITY_WARN, new Object[0]);
            setCorrecto(false);
        } else
        if(!separacion)
        {
            String dialogName = getParam(actionEvent, "dialogName");
            executeModal(dialogName, true);
        } else
        {
            setCorrecto(true);
            atencion.setFechaAtencion(DateUtils.addDays(new Date(), 0));
        }
    }

    public void verificarTrabajoSeleccionado(ActionEvent actionEvent)
    {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map sessionMap = externalContext.getSessionMap();
        if(trabajoDTO == null)
        {
            addMessage("Debe seleccionar una especialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
            sessionMap.put("mostrarDialogTrabajo", Boolean.valueOf(false));
        } else
        if(trabajoDTO.getIdTrabajo() == null)
        {
            addMessage("Debe seleccionar un especialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
            sessionMap.put("mostrarDialogTrabajo", Boolean.valueOf(false));
        } else
        {
            executeModal("seleccionarTrabajoDialog", false);
            sessionMap.put("mostrarDialogTrabajo", Boolean.valueOf(true));
        }
    }

    public void cargarPreguntasGenerales(ActionEvent event)
    {
        if(getUsuario().isEsLaboratorista())
        {
            if(trabajoDTO == null)
            {
                List aux = new ArrayList(getUsuario().getEmpleado().getConjuntoTrabajos());
                trabajoDTO = (TrabajoDTO)aux.get(0);
            }
            especialidadFiltro = (Especialidad)especialidadRepository.find(trabajoDTO.getEspecialidad().getIdEspecialidad());
            setSelectedIndex(new Integer(0));
            setMostrarTabGeneral(false);
            setMostrarTabEspecialidad(false);
            setMostrarTabSubEspecialidad(true);
            setMostrarTabProcedimiento(false);
        }
        if(preguntasGenerales.size() <= 0)
        {
            PreguntaDTO preguntaFiltro = new PreguntaDTO();
            preguntaFiltro.setEstadoPregunta("A");
            preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
            preguntasGenerales = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
            RespuestaDTO respuesta;
            for(Iterator i$ = preguntasGenerales.iterator(); i$.hasNext(); respuestasGenerales.add(respuesta))
            {
                PreguntaDTO preguntaGeneral = (PreguntaDTO)i$.next();
                respuesta = new RespuestaDTO();
                respuesta.setDescripcionPregunta(preguntaGeneral.getDescripcionPregunta());
            }

        }
    }

    private void cargarPreguntasEspecialidad()
    {
        PreguntaDTO preguntaFiltro = new PreguntaDTO();
        preguntaFiltro.setEstadoPregunta("A");
        preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
        preguntaFiltro.setEspecialidad((EspecialidadDTO)mapperService.map(especialidadFiltro, infraestructura/dto/EspecialidadDTO));
        preguntasEspecialidad = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
        RespuestaDTO respuesta;
        for(Iterator i$ = preguntasEspecialidad.iterator(); i$.hasNext(); respuestasEspecialidad.add(respuesta))
        {
            PreguntaDTO preguntaEspecialidad = (PreguntaDTO)i$.next();
            respuesta = new RespuestaDTO();
            respuesta.setDescripcionPregunta(preguntaEspecialidad.getDescripcionPregunta());
        }

        setMostrarTabEspecialidad(true);
    }

    private void cargarPreguntasSubEspecialidad()
    {
        PreguntaDTO preguntaFiltro = new PreguntaDTO();
        preguntaFiltro.setEstadoPregunta("A");
        preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
        preguntaFiltro.setSubEspecialidad((SubEspecialidadDTO)mapperService.map(subEspecialidadFiltro, infraestructura/dto/SubEspecialidadDTO));
        preguntasSubEspecialidad = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
        RespuestaDTO respuesta;
        for(Iterator i$ = preguntasSubEspecialidad.iterator(); i$.hasNext(); respuestasSubEspecialidad.add(respuesta))
        {
            PreguntaDTO preguntaSubEspecialidad = (PreguntaDTO)i$.next();
            respuesta = new RespuestaDTO();
            respuesta.setDescripcionPregunta(preguntaSubEspecialidad.getDescripcionPregunta());
        }

        setMostrarTabSubEspecialidad(true);
    }

    public void cargarPreguntasProcedimiento(SelectEvent event)
    {
        procedimientoFiltro = (Procedimiento)event.getObject();
        PreguntaDTO preguntaFiltro = new PreguntaDTO();
        preguntaFiltro.setEstadoPregunta("A");
        preguntaFiltro.setQuienPregunta(getUsuario().getEmpleado().getTipoEmpleado());
        preguntaFiltro.setProcedimiento((ProcedimientoDTO)mapperService.map(procedimientoFiltro, infraestructura/dto/ProcedimientoDTO));
        preguntasProcedimiento = preguntaService.findByFilter(preguntaFiltro, 0, 0, false);
        RespuestaDTO respuesta;
        for(Iterator i$ = preguntasProcedimiento.iterator(); i$.hasNext(); respuestasProcedimiento.add(respuesta))
        {
            PreguntaDTO preguntaProcedimiento = (PreguntaDTO)i$.next();
            respuesta = new RespuestaDTO();
            respuesta.setDescripcionPregunta(preguntaProcedimiento.getDescripcionPregunta());
        }

        setMostrarTabProcedimiento(true);
    }

    public void agregarEnfermedad(SelectEvent event)
    {
        enfermedadFiltro = new Enfermedad();
        Enfermedad enfermedad = (Enfermedad)event.getObject();
        for(Iterator i$ = listaEnfermedades.iterator(); i$.hasNext();)
        {
            Enfermedad enf = (Enfermedad)i$.next();
            if(enf.getIdEnfermedad().equals(enfermedad.getIdEnfermedad()))
            {
                addMessage("La enfermedad ya ha sido ingresada", FacesMessage.SEVERITY_WARN, new Object[0]);
                return;
            }
        }

        enfermedadSeleccionada = enfermedad;
        listaEnfermedades.add(enfermedad);
    }

    public void quitarEnfermedad(ActionEvent actionEvent)
    {
        if(enfermedadSeleccionada == null)
        {
            addMessage("Seleccionar la enfermedad que desea borrar", FacesMessage.SEVERITY_WARN, new Object[0]);
        } else
        {
            listaEnfermedades.remove(enfermedadSeleccionada);
            enfermedadSeleccionada = new Enfermedad();
        }
    }

    public void setarSeleccionado(SelectEvent event)
    {
        personaSeleccionada = (PersonaDTO)event.getObject();
    }

    public void setarSeleccionado2(SelectEvent event)
    {
        trabajoDTO = (TrabajoDTO)event.getObject();
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        Map sessionMap = externalContext.getSessionMap();
        if(trabajoDTO.getEspecialidad().getNombreEspecialidad().substring(0, 6).toLowerCase().equals("cardio"))
            sessionMap.put("mostrarBoton", Boolean.valueOf(true));
        else
            sessionMap.put("mostrarBoton", Boolean.valueOf(false));
    }

    public void setarSeleccionado3(AjaxBehaviorEvent event)
    {
        SelectOneListbox obj = (SelectOneListbox)event.getSource();
        idPersonaSeleccionada = (Integer)obj.getValue();
        if(idPersonaSeleccionada == null || idPersonaSeleccionada.intValue() == -1)
        {
            return;
        } else
        {
            PersonaDTO pfiltro = new PersonaDTO();
            pfiltro.setIdPersona(idPersonaSeleccionada);
            setPersonaSeleccionada((PersonaDTO)personaService.findByFilter(pfiltro).get(0));
            atencion.setFechaAtencion(DateUtils.addDays(new Date(), 0));
            return;
        }
    }

    public void inicializaFiltros()
    {
        especialidadFiltro = new Especialidad();
        subEspecialidadFiltro = new SubEspecialidad();
        procedimientoFiltro = new Procedimiento();
        atencion = new AtencionDTO();
    }

    public Integer getIdPersonaSeleccionada()
    {
        return idPersonaSeleccionada;
    }

    public void setIdPersonaSeleccionada(Integer idPersonaSeleccionada)
    {
        this.idPersonaSeleccionada = idPersonaSeleccionada;
    }

    public Especialidad getEspecialidadFiltro()
    {
        return especialidadFiltro;
    }

    public void setEspecialidadFiltro(Especialidad especialidadFiltro)
    {
        this.especialidadFiltro = especialidadFiltro;
    }

    public SubEspecialidad getSubEspecialidadFiltro()
    {
        return subEspecialidadFiltro;
    }

    public void setSubEspecialidadFiltro(SubEspecialidad subEspecialidadFiltro)
    {
        this.subEspecialidadFiltro = subEspecialidadFiltro;
    }

    public Procedimiento getProcedimientoFiltro()
    {
        return procedimientoFiltro;
    }

    public void setProcedimientoFiltro(Procedimiento procedimientoFiltro)
    {
        this.procedimientoFiltro = procedimientoFiltro;
    }

    public PersonaDTO getPersonaSeleccionada()
    {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(PersonaDTO personaSeleccionada)
    {
        this.personaSeleccionada = personaSeleccionada;
    }

    public boolean isMostrarTabGeneral()
    {
        return mostrarTabGeneral;
    }

    public void setMostrarTabGeneral(boolean mostrarTabGeneral)
    {
        this.mostrarTabGeneral = mostrarTabGeneral;
    }

    public boolean isMostrarTabEspecialidad()
    {
        return mostrarTabEspecialidad;
    }

    public void setMostrarTabEspecialidad(boolean mostrarTabEspecialidad)
    {
        this.mostrarTabEspecialidad = mostrarTabEspecialidad;
    }

    public boolean isMostrarTabSubEspecialidad()
    {
        return mostrarTabSubEspecialidad;
    }

    public void setMostrarTabSubEspecialidad(boolean mostrarTabSubEspecialidad)
    {
        this.mostrarTabSubEspecialidad = mostrarTabSubEspecialidad;
    }

    public boolean isMostrarTabProcedimiento()
    {
        return mostrarTabProcedimiento;
    }

    public void setMostrarTabProcedimiento(boolean mostrarTabProcedimiento)
    {
        this.mostrarTabProcedimiento = mostrarTabProcedimiento;
    }

    public List getPreguntasGenerales()
    {
        return preguntasGenerales;
    }

    public void setPreguntasGenerales(List preguntasGenerales)
    {
        this.preguntasGenerales = preguntasGenerales;
    }

    public List getPreguntasEspecialidad()
    {
        return preguntasEspecialidad;
    }

    public void setPreguntasEspecialidad(List preguntasEspecialidad)
    {
        this.preguntasEspecialidad = preguntasEspecialidad;
    }

    public List getPreguntasSubEspecialidad()
    {
        return preguntasSubEspecialidad;
    }

    public void setPreguntasSubEspecialidad(List preguntasSubEspecialidad)
    {
        this.preguntasSubEspecialidad = preguntasSubEspecialidad;
    }

    public List getPreguntasProcedimiento()
    {
        return preguntasProcedimiento;
    }

    public void setPreguntasProcedimiento(List preguntasProcedimiento)
    {
        this.preguntasProcedimiento = preguntasProcedimiento;
    }

    public List getRespuestasGenerales()
    {
        return respuestasGenerales;
    }

    public void setRespuestasGenerales(List respuestasGenerales)
    {
        this.respuestasGenerales = respuestasGenerales;
    }

    public List getRespuestasEspecialidad()
    {
        return respuestasEspecialidad;
    }

    public void setRespuestasEspecialidad(List respuestasEspecialidad)
    {
        this.respuestasEspecialidad = respuestasEspecialidad;
    }

    public List getRespuestasSubEspecialidad()
    {
        return respuestasSubEspecialidad;
    }

    public void setRespuestasSubEspecialidad(List respuestasSubEspecialidad)
    {
        this.respuestasSubEspecialidad = respuestasSubEspecialidad;
    }

    public List getRespuestasProcedimiento()
    {
        return respuestasProcedimiento;
    }

    public void setRespuestasProcedimiento(List respuestasProcedimiento)
    {
        this.respuestasProcedimiento = respuestasProcedimiento;
    }

    public TipoRespuestaPreguntaEnum getTipoRespuestaEnum()
    {
        return tipoRespuestaEnum;
    }

    public void setTipoRespuestaEnum(TipoRespuestaPreguntaEnum tipoRespuestaEnum)
    {
        this.tipoRespuestaEnum = tipoRespuestaEnum;
    }

    public TrabajoDTO getTrabajoDTO()
    {
        return trabajoDTO;
    }

    public void setTrabajoDTO(TrabajoDTO trabajoDTO)
    {
        this.trabajoDTO = trabajoDTO;
    }

    public AtencionDTO getAtencion()
    {
        return atencion;
    }

    public void setAtencion(AtencionDTO atencion)
    {
        this.atencion = atencion;
    }

    public Enfermedad getEnfermedadFiltro()
    {
        return enfermedadFiltro;
    }

    public void setEnfermedadFiltro(Enfermedad enfermedadFiltro)
    {
        this.enfermedadFiltro = new Enfermedad();
    }

    public Enfermedad getEnfermedadSeleccionada()
    {
        return enfermedadSeleccionada;
    }

    public void setEnfermedadSeleccionada(Enfermedad enfermedadSeleccionada)
    {
        this.enfermedadSeleccionada = enfermedadSeleccionada;
    }

    public List getListaEnfermedades()
    {
        return listaEnfermedades;
    }

    public void setListaEnfermedades(List listaEnfermedades)
    {
        this.listaEnfermedades = listaEnfermedades;
    }

    public Integer getSelectedIndex()
    {
        return selectedIndex;
    }

    public void setSelectedIndex(Integer selectedIndex)
    {
        this.selectedIndex = selectedIndex;
    }

    public Date getFechaLuego()
    {
        return fechaLuego;
    }

    public void setFechaLuego(Date fechaLuego)
    {
        this.fechaLuego = fechaLuego;
    }

    public boolean isSeparacion()
    {
        return separacion;
    }

    public void setSeparacion(boolean separacion)
    {
        this.separacion = separacion;
    }

    public boolean isCorrecto()
    {
        return correcto;
    }

    public void setCorrecto(boolean correcto)
    {
        this.correcto = correcto;
    }

    @Autowired
    ApplicationBean applicationBean;
    @Autowired
    AtencionService atencionService;
    @Autowired
    EspecialidadService especialidadService;
    @Autowired
    SubEspecialidadService subEspecialidadService;
    @Autowired
    ProcedimientoService procedimientoService;
    @Autowired
    PreguntaService preguntaService;
    @Autowired
    FormulaService formulaService;
    @Autowired
    PersonaService personaService;
    @Autowired
    DTOMapperService mapperService;
    @Autowired
    EspecialidadRepository especialidadRepository;
    @Autowired
    SubEspecialidadRepository subEspecialidadRepository;
    @Autowired
    ProcedimientoRepository procedimientoRepository;
    @Autowired
    EnfermedadRepository enfermedadRepository;
    private Especialidad especialidadFiltro;
    private SubEspecialidad subEspecialidadFiltro;
    private Procedimiento procedimientoFiltro;
    private PersonaDTO personaSeleccionada;
    private Integer idPersonaSeleccionada;
    private boolean mostrarTabGeneral;
    private boolean mostrarTabEspecialidad;
    private boolean mostrarTabSubEspecialidad;
    private boolean mostrarTabProcedimiento;
    private List preguntasGenerales;
    private List preguntasEspecialidad;
    private List preguntasSubEspecialidad;
    private List preguntasProcedimiento;
    private List respuestasGenerales;
    private List respuestasEspecialidad;
    private List respuestasSubEspecialidad;
    private List respuestasProcedimiento;
    private TipoRespuestaPreguntaEnum tipoRespuestaEnum;
    private TrabajoDTO trabajoDTO;
    AtencionDTO atencion;
    Enfermedad enfermedadFiltro;
    Enfermedad enfermedadSeleccionada;
    List listaEnfermedades;
    private Integer selectedIndex;
    private Date fechaLuego;
    private boolean separacion;
    private boolean correcto;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/beans/AtencionJSFBean);

}
