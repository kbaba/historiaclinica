// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:33:23 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpleadosJSFBean.java

package paquete.beans;

import infraestructura.dto.*;
import infraestructura.enums.*;
import infraestructura.exception.ApplicationException;
import java.io.PrintStream;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.event.ActionEvent;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import paquete.lazyModel.EmpleadoLazyModel;
import paquete.repositorios.*;
import paquete.service.*;
import util.FindCrudBeanBase;
import util.Validaciones;

@ManagedBean
@Scope(value="session")
@Component
public class EmpleadosJSFBean extends FindCrudBeanBase
{

    public EmpleadosJSFBean()
    {
        listaEstadosEmpleado = new ArrayList();
        listaSexoPersonaEnum = new ArrayList();
        listaQuienPreguntaEnum = new ArrayList();
        listaEstadoTrabajoEnum = new ArrayList();
        listaTrabajosDetail = new ArrayList();
        trabajoSeleccionadoDetail = new TrabajoDTO();
        listaEspecialidadPersist = new ArrayList();
        especialidadSeleccionadaPersist = new EspecialidadDTO();
        listaIdsEspecialidades = new ArrayList();
        listaBorrarIdsEspecialidades = new ArrayList();
    }

    @PostConstruct
    public void init()
    {
        trabajoFiltro = new TrabajoDTO();
        listaEstadosEmpleado = EstadoEmpleadoEnum.findAll();
        listaSexoPersonaEnum = SexoPersonaEnum.findAll();
        listaQuienPreguntaEnum = QuienPreguntaEnum.findAll();
        listaEstadoTrabajoEnum = EstadoTrabajoEnum.findAll();
        listaPais = paisRepository.getAllPaises();
        listaTrabajosNuevoEmpleado = new ArrayList();
        inicializaPersonaSeleccionada();
        inicializaFiltro();
        inicializaNuevaPersona();
        inicializaLocalizacion();
        inicializaEspecialidad();
        personaSeleccionada.setDepartamentoPersona(new DepartamentoDTO());
        personaSeleccionada.setProvinciaPersona(provinciaSeleccionada);
        personaSeleccionada.setDistritoPersona(distritoSeleccionado);
        personaLazyModel = new EmpleadoLazyModel(personaFiltro, personaService, true);
        mostrarDetalle = true;
        nuevoEstadoPersonaSeleccionada = "-1";
        validar = new Validaciones();
    }

    public void setCursorTableNull()
    {
    }

    public void onNewRecord(ActionEvent actionevent)
    {
    }

    public void onFind(ActionEvent actionevent)
    {
    }

    public void onClean(ActionEvent actionEvent)
    {
        personaLazyModel = new EmpleadoLazyModel(personaFiltro, personaService, false);
        inicializaFiltro();
        inicializaPersonaSeleccionada();
        inicializaEspecialidad();
        listaIdsEspecialidades.clear();
        listaEspecialidadPersist.clear();
    }

    public void onPersist(ActionEvent actionevent)
    {
    }

    public void onDelete(ActionEvent actionevent)
    {
    }

    protected void onDialogClose(CloseEvent closeevent)
    {
    }

    public void actualizaData(ActionEvent actionEvent)
    {
        String mensaje[] = {
            ""
        };
        boolean estaOk = validarDatosPersona(personaSeleccionada, mensaje);
        if(!estaOk)
        {
            addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        } else
        {
            personaSeleccionada.getEmpleado().setEstadoEmpleado(personaSeleccionada.getEmpleado().getEstadoEmpleadoEnum().getCode());
            personaSeleccionada.setIdResidenciaDistrito(idCompuestoDistritoPersonaSeleccionada);
            personaSeleccionada.setSexoPersona(personaSeleccionada.getSexoPersonaEnum().getCode());
            personaSeleccionada.getEmpleado().setTipoEmpleado(personaSeleccionada.getEmpleado().getQuienPreguntaEnum().getCode());
            personaSeleccionada.setRol(determinaTipoRolPersona(personaSeleccionada.getEmpleado().getTipoEmpleado()));
            personaSeleccionada = personaService.guardaDevuelvePersona(personaSeleccionada);
            remueveIdsTemporalesListaTrabajoDetail();
            actualizaEstadoDeTrabajos();
            trabajoService.eliminaActualizaCreaTrabajos(personaSeleccionada.getEmpleado().getIdEmpleado(), listaTrabajosDetail, listaBorrarIdsEspecialidades);
            addMessage("Se actualiz\363 de manera correcta la informaci\363n del empleado", FacesMessage.SEVERITY_INFO, new Object[0]);
            inicializaPersonaSeleccionada();
            ocultaDialogModificar();
            inicializaLocalizacion();
            inicializaEspecialidad();
            executeModal("detailDialog", false);
            realizaBusqueda();
            return;
        }
    }

    public void guardaEmpleado(ActionEvent actionEvent)
    {
        String mensaje[] = {
            ""
        };
        boolean estaOk = validarDatosPersona(nuevaPersona, mensaje);
        if(!estaOk)
        {
            addMessage(mensaje[0], FacesMessage.SEVERITY_ERROR, new Object[0]);
            return;
        }
        nuevaPersona.setSexoPersona(nuevaPersona.getSexoPersonaEnum().getCode());
        nuevaPersona.getEmpleado().setTipoEmpleado(nuevaPersona.getEmpleado().getQuienPreguntaEnum().getCode());
        nuevaPersona.setRol(determinaTipoRolPersona(nuevaPersona.getEmpleado().getTipoEmpleado()));
        nuevaPersona = personaService.guardaDevuelvePersona(nuevaPersona);
        TrabajoDTO nuevoTrabajo;
        for(Iterator i$ = listaEspecialidadPersist.iterator(); i$.hasNext(); listaTrabajosNuevoEmpleado.add(nuevoTrabajo))
        {
            EspecialidadDTO especialidad = (EspecialidadDTO)i$.next();
            nuevoTrabajo = new TrabajoDTO();
            especialidad.addTrabajo(nuevoTrabajo);
            nuevaPersona.getEmpleado().agregarTrabajo(nuevoTrabajo);
        }

        trabajoRepository.persistirTrabajoQueryDTO(listaTrabajosNuevoEmpleado);
        addMessage("Se guard\363 de manera correcta la informaci\363n del empleado", FacesMessage.SEVERITY_INFO, new Object[0]);
        inicializaNuevaPersona();
        inicializaLocalizacion();
        inicializaEspecialidad();
        executeModal("persistDialog", false);
        realizaBusqueda();
        listaEspecialidadPersist.clear();
        listaIdsEspecialidades.clear();
    }

    public void realizaBusqueda()
    {
        if(personaFiltro.getEmpleado().getEstadoEmpleadoEnum() == null)
            personaFiltro.getEmpleado().setEstadoEmpleado("");
        else
            personaFiltro.getEmpleado().setEstadoEmpleado(personaFiltro.getEmpleado().getEstadoEmpleadoEnum().getCode());
        if(personaFiltro.getSexoPersonaEnum() == null)
            personaFiltro.setSexoPersona("");
        else
            personaFiltro.setSexoPersona(personaFiltro.getSexoPersonaEnum().getCode());
        if(especialidadSeleccionadaId != null && especialidadSeleccionadaId.intValue() != -1)
        {
            EspecialidadDTO especialidad = especialidadService.find(especialidadSeleccionadaId);
            trabajoFiltro = new TrabajoDTO();
            trabajoFiltro.setEspecialidad(especialidad);
            personaFiltro.getEmpleado().vaciaAgregaTrabajo(trabajoFiltro);
        }
        if(personaFiltro.getEmpleado().getQuienPreguntaEnum() != null)
            personaFiltro.getEmpleado().setTipoEmpleado(personaFiltro.getEmpleado().getQuienPreguntaEnum().getCode());
        else
            personaFiltro.getEmpleado().setTipoEmpleado("");
        personaLazyModel = new EmpleadoLazyModel(personaFiltro, personaService, true);
    }

    public void cargaDetallesDeEmpleado(ActionEvent event)
    {
        listaBorrarIdsEspecialidades.clear();
        listaTrabajosNuevoEmpleado.clear();
        listaTrabajosDetail.clear();
        if(personaSeleccionada == null || StringUtils.isBlank(personaSeleccionada.getDocumentoPersona()))
        {
            addMessage("Debe seleccionar antes un empleado", FacesMessage.SEVERITY_WARN, new Object[0]);
            mostrarDetalle = false;
            executeModal("detailDialog", mostrarDetalle);
            return;
        } else
        {
            String idCompuesto = personaSeleccionada.getIdResidenciaDistrito();
            List lista = separaIdsCompuesto(idCompuesto, ",");
            String idPais = (String)lista.get(0);
            String idDepartamento = (String)lista.get(1);
            String idProvincia = (String)lista.get(2);
            String idDistrito = (String)lista.get(3);
            listaDepartamento = departamentoRepository.findByPaisId(idPais);
            listaProvincia = provinciaRepository.findByDepartamento(idPais, idDepartamento);
            listaDistrito = distritoRepository.findByProvincia(idPais, idDepartamento, idProvincia);
            idCompuestoDepartamentoPersonaSeleccionada = (new StringBuilder()).append(idPais).append(",").append(idDepartamento).toString();
            idCompuestoProvinciaPersonaSeleccionada = (new StringBuilder()).append(idPais).append(",").append(idDepartamento).append(",").append(idProvincia).toString();
            idCompuestoDistritoPersonaSeleccionada = (new StringBuilder()).append(idPais).append(",").append(idDepartamento).append(",").append(idProvincia).append(",").append(idDistrito).toString();
            listaIdsEspecialidades.clear();
            listaTrabajosDetail = trabajoService.getTrabajoEmpleado(personaSeleccionada.getEmpleado().getIdEmpleado());
            listaIdsEspecialidades = getIdEspecialidadListadeTrabajos(listaTrabajosDetail);
            mostrarDetalle = true;
            executeModal("detailDialog", mostrarDetalle);
            showNewDialog(event);
            return;
        }
    }

    private void inicializaEspecialidad()
    {
        listaEspecialidad = especialidadService.getAll();
        especialidadSeleccionadaId = new Integer(-1);
        especialidadSeleccionadaIdPersist = new Integer(-1);
    }

    private void inicializaPersonaSeleccionada()
    {
        personaSeleccionada = new PersonaDTO();
        empleadoSeleccionado = new EmpleadoDTO();
        personaSeleccionada.setEmpleado(empleadoSeleccionado);
        personaSeleccionada.setTipoDocumentoPersona("1");
        listaTrabajosNuevoEmpleado = new ArrayList();
        listaEspecialidadPersist = new ArrayList();
        listaTrabajosDetail = new ArrayList();
        especialidadSeleccionadaIdPersist = new Integer(-1);
        listaIdsEspecialidades.clear();
        listaBorrarIdsEspecialidades.clear();
        listaTrabajosNuevoEmpleado.clear();
        especialidadSeleccionadaIdPersist = new Integer(-1);
    }

    private void inicializaLocalizacion()
    {
        idCompuestoDepartamento = "";
        idCompuestoProvincia = "";
        idCompuestoDistrito = "";
        idCompuestoDepartamentoPersonaSeleccionada = "";
        idCompuestoProvinciaPersonaSeleccionada = "";
        idCompuestoDistritoPersonaSeleccionada = "";
        provinciaSeleccionada = new ProvinciaDTO();
        distritoSeleccionado = new DistritoDTO();
        listaDepartamento = new ArrayList();
        listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void inicializaFiltro()
    {
        personaFiltro = new PersonaDTO();
        empleadoFiltro = new EmpleadoDTO();
        empleadoFiltro.setEstadoEmpleado("A");
        trabajoFiltro = new TrabajoDTO();
        empleadoFiltro.agregarTrabajo(trabajoFiltro);
        personaFiltro.setEmpleado(empleadoFiltro);
        especialidadSeleccionadaIdPersist = new Integer(-1);
    }

    public void inicializaNuevaPersona()
    {
        nuevaPersona = new PersonaDTO();
        nuevoEmpleado = new EmpleadoDTO();
        nuevoEmpleado.setEstadoEmpleado("A");
        nuevaPersona.setEmpleado(nuevoEmpleado);
        listaTrabajosNuevoEmpleado.clear();
        listaEspecialidadPersist.clear();
        listaTrabajosDetail.clear();
        especialidadSeleccionadaIdPersist = new Integer(-1);
        listaIdsEspecialidades.clear();
        listaBorrarIdsEspecialidades.clear();
        listaTrabajosNuevoEmpleado.clear();
    }

    private boolean validarDatosPersona(PersonaDTO persona, String mensaje[])
    {
        String nombres = persona.getNombrePersona();
        String apeP = persona.getApellidoPaternoPersona();
        String apeM = persona.getApellidoMaternoPersona();
        String documento = persona.getDocumentoPersona();
        if(!validar.chekeoPalabrasSinNFAmero(nombres))
        {
            mensaje[0] = "Para \"nombre\" de persona solo est\341n permitidos caracteres de aA-zZ, no n\372meros, ni s\355mbolos. Campo obligatorio";
            return false;
        }
        if(!validar.chekeoPalabrasSinNFAmero(apeP))
        {
            mensaje[0] = "Para \"apellido paterno\" de persona solo est\341n permitidos caracteres de aA-zZ, no n\372meros, ni s\355mbolos. Campo obligatorio";
            return false;
        }
        if(!validar.chekeoPalabrasSinNFAmero(apeM))
        {
            mensaje[0] = "Para \"apellido materno\" de persona solo est\341n permitidos caracteres de aA-zZ, no n\372meros, ni s\355mbolos. Campo obligatorio";
            return false;
        }
        if(!validar.chekeoNumero(persona.getTipoDocumentoPersona()))
        {
            mensaje[0] = (new StringBuilder()).append("\"Tipo de Documento\" de persona Campo obligatorio: ").append(persona.getTipoDocumentoPersona()).toString();
            return false;
        }
        if(!esMenorIgual(persona.getFechaNacimientoPersona()))
        {
            mensaje[0] = "La fecha de nacimiento seleccionada debe ser menor o igual a la actual";
            return false;
        }
        if(persona.getTipoDocumentoPersona().equals("1"))
        {
            if(!validar.chekeoNumero(documento))
            {
                mensaje[0] = "Para \"DNI\" de persona solo est\341n permitidos caracteres num\351ricos.Campo obligatorio";
                return false;
            }
            if(!validar.chekeoPalabraCantidadCaracteres(documento, 8))
            {
                mensaje[0] = "Para \"DNI\" de persona la cantidad de caracteres debe ser exactamente 08.Campo obligatorio";
                return false;
            }
        }
        if(persona.getIdPersona() != null)
        {
            if(personaService.esDocumentoRepetido(documento, persona.getIdPersona().intValue(), persona.getTipoDocumentoPersona()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Documento\" de persona. Documento ya en uso.";
                return false;
            }
            if(personaService.esUsuarioRepetido(persona.getUsuario(), persona.getIdPersona().intValue()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Usuario\" de persona. Usuario ya en uso.";
                return false;
            }
        } else
        {
            if(personaService.esDocumentoRepetido(documento, persona.getTipoDocumentoPersona()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Documento\" de persona. Documento ya en uso.";
                return false;
            }
            if(personaService.esUsuarioRepetido(persona.getUsuario()))
            {
                mensaje[0] = "Error ya existe un registro en el sistema con ese \"Usuario\" de persona. Usuario ya en uso.";
                return false;
            }
        }
        if(persona.getSexoPersonaEnum() == null)
        {
            mensaje[0] = "\"Sexo\" de la persona  es un dato necesario.";
            return false;
        }
        if(StringUtils.isBlank(persona.getIdResidenciaDistrito()))
        {
            mensaje[0] = "\"Distrito de residencia\" de la persona  es un dato necesario.";
            return false;
        }
        if(StringUtils.isBlank(persona.getDireccionPersona()))
        {
            mensaje[0] = "\"Direcci\363n\" de persona es un dato necesario.";
            return false;
        }
        if(persona.getEmpleado().getQuienPreguntaEnum() == null)
        {
            mensaje[0] = "\"Tipo de Empleado\" de la persona  es un dato necesario.";
            return false;
        }
        if(persona.getIdPersona() == null)
        {
            if(listaEspecialidadPersist.isEmpty())
            {
                mensaje[0] = "Debe especificar por lo menos una especialidad laboral.";
                return false;
            }
        } else
        if(listaTrabajosDetail.isEmpty())
        {
            mensaje[0] = "Debe especificar por lo menos una especialidad laboral.";
            return false;
        }
        return true;
    }

    public void manejaCambioDepartamentoDetail()
    {
        List compuesto = separaIdsCompuesto(idCompuestoDepartamentoPersonaSeleccionada, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId))
            listaProvincia = provinciaRepository.findByDepartamento(idPais, departamentoId);
        else
            listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void detailManejaCambioPais()
    {
        String idPais = personaSeleccionada.getIdPaisNacionalidad();
        depurar((new StringBuilder()).append("el id de pais es: ").append(idPais).toString());
        if(StringUtils.isNotBlank(idPais))
        {
            listaDepartamento = departamentoRepository.findByPaisId(idPais);
            depurar((new StringBuilder()).append("tam\361o de departamentos es: ").append(listaDepartamento.size()).toString());
        } else
        {
            listaDepartamento = new ArrayList();
        }
        listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void manejaCambioProvinciaDetail()
    {
        List compuesto = separaIdsCompuesto(idCompuestoProvinciaPersonaSeleccionada, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        String provinciaId = (String)compuesto.get(2);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId) && StringUtils.isNotBlank(provinciaId))
            listaDistrito = distritoRepository.findByProvincia(idPais, departamentoId, provinciaId);
        else
            listaDistrito = new ArrayList();
    }

    public void manejaCambioDepartamento()
    {
        List compuesto = separaIdsCompuesto(idCompuestoDepartamento, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId))
            listaProvincia = provinciaRepository.findByDepartamento(idPais, departamentoId);
        else
            listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void manejaCambioProvincia()
    {
        List compuesto = separaIdsCompuesto(idCompuestoProvincia, ",");
        String idPais = (String)compuesto.get(0);
        String departamentoId = (String)compuesto.get(1);
        String provinciaId = (String)compuesto.get(2);
        if(StringUtils.isNotBlank(idPais) && StringUtils.isNotBlank(departamentoId) && StringUtils.isNotBlank(provinciaId))
            listaDistrito = distritoRepository.findByProvincia(idPais, departamentoId, provinciaId);
        else
            listaDistrito = new ArrayList();
    }

    public void persistManejaCambioPais()
    {
        String idPais = nuevaPersona.getIdPaisNacionalidad();
        depurar((new StringBuilder()).append("el id de pais es: ").append(idPais).toString());
        if(StringUtils.isNotBlank(idPais))
        {
            listaDepartamento = departamentoRepository.findByPaisId(idPais);
            depurar((new StringBuilder()).append("tam\361o de departamentos es: ").append(listaDepartamento.size()).toString());
        } else
        {
            listaDepartamento = new ArrayList();
        }
        listaProvincia = new ArrayList();
        listaDistrito = new ArrayList();
    }

    public void toIndex(ActionEvent e)
    {
        redireccionar("../index.jsf");
    }

    private boolean esMenorIgual(Date fecha)
    {
        Calendar hoy = Calendar.getInstance();
        Date actual = hoy.getTime();
        int diff = actual.compareTo(fecha);
        return diff > 0 || diff == 0;
    }

    public void depurar(String mensaje)
    {
        System.out.println((new StringBuilder()).append("####").append(mensaje).toString());
    }

    private void ocultaDialogModificar()
    {
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute("detailDialog.hide()");
    }

    public List separaIdsCompuesto(String compuesto, String separador)
    {
        List lista = new ArrayList();
        for(StringTokenizer st = new StringTokenizer(compuesto, separador); st.hasMoreTokens(); lista.add(st.nextToken()));
        return lista;
    }

    public void addEspecialidadPersist(ActionEvent event)
    {
        if(especialidadSeleccionadaIdPersist == null || especialidadSeleccionadaIdPersist.intValue() == -1)
        {
            addMessage("Seleccione alguna especialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        }
        if(listaIdsEspecialidades.contains(especialidadSeleccionadaIdPersist))
        {
            addMessage("Ya se agreg\363 dicha especialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        }
        try
        {
            EspecialidadDTO especialidad = especialidadService.find(especialidadSeleccionadaIdPersist);
            listaEspecialidadPersist.add(especialidad);
            listaIdsEspecialidades.add(especialidadSeleccionadaIdPersist);
        }
        catch(ApplicationException e)
        {
            addMessage(e.getMessage(), FacesMessage.SEVERITY_WARN, e.getParameters());
        }
        return;
    }

    public void addEspecialidadDetail(ActionEvent event)
    {
        if(especialidadSeleccionadaIdPersist == null || especialidadSeleccionadaIdPersist.intValue() == -1)
        {
            addMessage("Seleccione alguna especialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        }
        if(listaIdsEspecialidades.contains(especialidadSeleccionadaIdPersist))
        {
            addMessage("Ya se agreg\363 dicha especialidad", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        } else
        {
            EspecialidadDTO especialidad = especialidadService.find(especialidadSeleccionadaIdPersist);
            TrabajoDTO trabajo = new TrabajoDTO();
            trabajo.setIdTrabajo(especialidadSeleccionadaId);
            trabajo.setEmpleado(personaSeleccionada.getEmpleado());
            trabajo.setEspecialidad(especialidad);
            trabajo.setEstadoTrabajo("A");
            trabajo.setPonerleIdNulo(trabajo.NULO_SI);
            listaTrabajosDetail.add(trabajo);
            listaIdsEspecialidades.add(especialidadSeleccionadaIdPersist);
            return;
        }
    }

    public void removeEspecialidadPersist(ActionEvent event)
    {
        if(especialidadSeleccionadaPersist == null)
        {
            addMessage("Seleccionar el trabajo que desea borrar", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        } else
        {
            listaEspecialidadPersist.remove(especialidadSeleccionadaPersist);
            listaIdsEspecialidades.remove(especialidadSeleccionadaPersist.getIdEspecialidad());
            especialidadSeleccionadaPersist = new EspecialidadDTO();
            addMessage("Se borr\363 la asociaci\363n de especialidad con empleado", FacesMessage.SEVERITY_INFO, new Object[0]);
            return;
        }
    }

    public void removeEspecialidadDetail(ActionEvent event)
    {
        if(trabajoSeleccionadoDetail == null)
        {
            addMessage("Seleccionar el trabajo que desea borrar", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        }
        if(!atencionRepository.esTrabajoBorrable(trabajoSeleccionadoDetail.getIdTrabajo()))
        {
            addMessage("No se puede borrar este trabajo, contiene atenciones registradas. Se recomienda entonces dejarlo con estado inactivo", FacesMessage.SEVERITY_WARN, new Object[0]);
            return;
        } else
        {
            listaIdsEspecialidades.remove(trabajoSeleccionadoDetail.getEspecialidad().getIdEspecialidad());
            listaBorrarIdsEspecialidades.add(trabajoSeleccionadoDetail.getEspecialidad().getIdEspecialidad());
            listaTrabajosDetail.remove(trabajoSeleccionadoDetail);
            addMessage("Se borr\363 la asociaci\363n de especialidad con empleado", FacesMessage.SEVERITY_INFO, new Object[0]);
            return;
        }
    }

    public List getListaEstadoTrabajoEnum()
    {
        return listaEstadoTrabajoEnum;
    }

    public void setListaEstadoTrabajoEnum(List listaEstadoTrabajoEnum)
    {
        this.listaEstadoTrabajoEnum = listaEstadoTrabajoEnum;
    }

    private List getIdEspecialidadLista(List listaEspecialidadPersist)
    {
        List lista = new ArrayList();
        EspecialidadDTO esp;
        for(Iterator i$ = listaEspecialidadPersist.iterator(); i$.hasNext(); lista.add(esp.getIdEspecialidad()))
            esp = (EspecialidadDTO)i$.next();

        return lista;
    }

    public EspecialidadDTO getEspecialidadSeleccionadaPersist()
    {
        return especialidadSeleccionadaPersist;
    }

    public void setEspecialidadSeleccionadaPersist(EspecialidadDTO especialidadSeleccionadaPersist)
    {
        this.especialidadSeleccionadaPersist = especialidadSeleccionadaPersist;
    }

    public Integer getEspecialidadSeleccionadaIdPersist()
    {
        return especialidadSeleccionadaIdPersist;
    }

    public void setEspecialidadSeleccionadaIdPersist(Integer especialidadSeleccionadaIdPersist)
    {
        this.especialidadSeleccionadaIdPersist = especialidadSeleccionadaIdPersist;
    }

    public List getListaEspecialidadPersist()
    {
        return listaEspecialidadPersist;
    }

    public void setListaEspecialidadPersist(List listaEspecialidadPersist)
    {
        this.listaEspecialidadPersist = listaEspecialidadPersist;
    }

    public List getListaTrabajosNuevoEmpleado()
    {
        return listaTrabajosNuevoEmpleado;
    }

    public void setListaTrabajosNuevoEmpleado(List listaTrabajosNuevoEmpleado)
    {
        this.listaTrabajosNuevoEmpleado = listaTrabajosNuevoEmpleado;
    }

    public List getListaQuienPreguntaEnum()
    {
        return listaQuienPreguntaEnum;
    }

    public void setListaQuienPreguntaEnum(List listaQuienPreguntaEnum)
    {
        this.listaQuienPreguntaEnum = listaQuienPreguntaEnum;
    }

    public TrabajoDTO getTrabajoFiltro()
    {
        return trabajoFiltro;
    }

    public TrabajoDTO getTrabajoSeleccionadoDetail()
    {
        return trabajoSeleccionadoDetail;
    }

    public void setTrabajoSeleccionadoDetail(TrabajoDTO trabajoSeleccionadoDetail)
    {
        this.trabajoSeleccionadoDetail = trabajoSeleccionadoDetail;
    }

    public void setTrabajoFiltro(TrabajoDTO trabajoFiltro)
    {
        this.trabajoFiltro = trabajoFiltro;
    }

    public List getListaTrabajosDetail()
    {
        return listaTrabajosDetail;
    }

    public void setListaTrabajosDetail(List listaTrabajosDetail)
    {
        this.listaTrabajosDetail = listaTrabajosDetail;
    }

    public List getListaEstadosEmpleado()
    {
        return listaEstadosEmpleado;
    }

    public void setListaEstadosEmpleado(List listaEstadosEmpleado)
    {
        this.listaEstadosEmpleado = listaEstadosEmpleado;
    }

    public List getListaEspecialidad()
    {
        return listaEspecialidad;
    }

    public void setListaEspecialidad(List listaEspecialidad)
    {
        this.listaEspecialidad = listaEspecialidad;
    }

    public Integer getEspecialidadSeleccionadaId()
    {
        return especialidadSeleccionadaId;
    }

    public void setEspecialidadSeleccionadaId(Integer especialidadSeleccionadaId)
    {
        this.especialidadSeleccionadaId = especialidadSeleccionadaId;
    }

    public Integer getSubEspecialidadSeleccionadaId()
    {
        return subEspecialidadSeleccionadaId;
    }

    public void setSubEspecialidadSeleccionadaId(Integer subEspecialidadSeleccionadaId)
    {
        this.subEspecialidadSeleccionadaId = subEspecialidadSeleccionadaId;
    }

    public Integer getProcedimientoSeleccionadoId()
    {
        return procedimientoSeleccionadoId;
    }

    public void setProcedimientoSeleccionadoId(Integer procedimientoSeleccionadoId)
    {
        this.procedimientoSeleccionadoId = procedimientoSeleccionadoId;
    }

    public String getNuevoEstadoPersonaSeleccionada()
    {
        return nuevoEstadoPersonaSeleccionada;
    }

    public void setNuevoEstadoPersonaSeleccionada(String nuevoEstadoPersonaSeleccionada)
    {
        this.nuevoEstadoPersonaSeleccionada = nuevoEstadoPersonaSeleccionada;
    }

    public boolean isMostrarDetalle()
    {
        return mostrarDetalle;
    }

    public List getListaSexoPersonaEnum()
    {
        return listaSexoPersonaEnum;
    }

    public void setListaSexoPersonaEnum(List listaSexoPersonaEnum)
    {
        this.listaSexoPersonaEnum = listaSexoPersonaEnum;
    }

    public void setMostrarDetalle(boolean mostrarDetalle)
    {
        this.mostrarDetalle = mostrarDetalle;
    }

    public String getIdCompuestoDistrito()
    {
        return idCompuestoDistrito;
    }

    public void setIdCompuestoDistrito(String idCompuestoDistrito)
    {
        this.idCompuestoDistrito = idCompuestoDistrito;
    }

    public void setarSeleccionado(SelectEvent event)
    {
        personaSeleccionada = (PersonaDTO)event.getObject();
    }

    public String getIdCompuestoProvincia()
    {
        return idCompuestoProvincia;
    }

    public void setIdCompuestoProvincia(String idCompuestoProvincia)
    {
        this.idCompuestoProvincia = idCompuestoProvincia;
    }

    public PersonaDTO getPersonaFiltro()
    {
        return personaFiltro;
    }

    public void setPersonaFiltro(PersonaDTO personaFiltro)
    {
        this.personaFiltro = personaFiltro;
    }

    public List getListaDepartamento()
    {
        return listaDepartamento;
    }

    public String getIdCompuestoDepartamento()
    {
        return idCompuestoDepartamento;
    }

    public void setIdCompuestoDepartamento(String idCompuestoDepartamento)
    {
        this.idCompuestoDepartamento = idCompuestoDepartamento;
    }

    public void setListaDepartamento(List listaDepartamento)
    {
        this.listaDepartamento = listaDepartamento;
    }

    public List getListaDistrito()
    {
        return listaDistrito;
    }

    public void setListaDistrito(List listaDistrito)
    {
        this.listaDistrito = listaDistrito;
    }

    public List getListaProvincia()
    {
        return listaProvincia;
    }

    public void setListaProvincia(List listaProvincia)
    {
        this.listaProvincia = listaProvincia;
    }

    public String getIdCompuestoDepartamentoPersonaSeleccionada()
    {
        return idCompuestoDepartamentoPersonaSeleccionada;
    }

    public void setIdCompuestoDepartamentoPersonaSeleccionada(String idCompuestoDepartamentoPersonaSeleccionada)
    {
        this.idCompuestoDepartamentoPersonaSeleccionada = idCompuestoDepartamentoPersonaSeleccionada;
    }

    public String getIdCompuestoDistritoPersonaSeleccionada()
    {
        return idCompuestoDistritoPersonaSeleccionada;
    }

    public void setIdCompuestoDistritoPersonaSeleccionada(String idCompuestoDistritoPersonaSeleccionada)
    {
        this.idCompuestoDistritoPersonaSeleccionada = idCompuestoDistritoPersonaSeleccionada;
    }

    public String getIdCompuestoProvinciaPersonaSeleccionada()
    {
        return idCompuestoProvinciaPersonaSeleccionada;
    }

    public void setIdCompuestoProvinciaPersonaSeleccionada(String idCompuestoProvinciaPersonaSeleccionada)
    {
        this.idCompuestoProvinciaPersonaSeleccionada = idCompuestoProvinciaPersonaSeleccionada;
    }

    public PersonaDTO getPersonaSeleccionada()
    {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(PersonaDTO personaSeleccionada)
    {
        this.personaSeleccionada = personaSeleccionada;
    }

    public List getListaPais()
    {
        return listaPais;
    }

    public void setListaPais(List listaPais)
    {
        this.listaPais = listaPais;
    }

    public PersonaDTO getNuevaPersona()
    {
        return nuevaPersona;
    }

    public void setNuevaPersona(PersonaDTO nuevaPersona)
    {
        this.nuevaPersona = nuevaPersona;
    }

    public DistritoDTO getDistritoSeleccionado()
    {
        return distritoSeleccionado;
    }

    public void setDistritoSeleccionado(DistritoDTO distritoSeleccionado)
    {
        this.distritoSeleccionado = distritoSeleccionado;
    }

    public ProvinciaDTO getProvinciaSeleccionada()
    {
        return provinciaSeleccionada;
    }

    public void setProvinciaSeleccionada(ProvinciaDTO provinciaSeleccionada)
    {
        this.provinciaSeleccionada = provinciaSeleccionada;
    }

    public LazyDataModel getPersonaLazyModel()
    {
        return personaLazyModel;
    }

    public void setPersonaLazyModel(LazyDataModel personaLazyModel)
    {
        this.personaLazyModel = personaLazyModel;
    }

    public Date getHoy()
    {
        Calendar calendar1 = Calendar.getInstance();
        hoy = calendar1.getTime();
        return hoy;
    }

    public void setHoy(Date hoy)
    {
        this.hoy = hoy;
    }

    public EmpleadoDTO getEmpleadoFiltro()
    {
        return empleadoFiltro;
    }

    public void setEmpleadoFiltro(EmpleadoDTO empleadoFiltro)
    {
        this.empleadoFiltro = empleadoFiltro;
    }

    public EmpleadoDTO getNuevoEmpleado()
    {
        return nuevoEmpleado;
    }

    public void setNuevoEmpleado(EmpleadoDTO nuevoEmpleado)
    {
        this.nuevoEmpleado = nuevoEmpleado;
    }

    public EmpleadoDTO getEmpleadoSeleccionado()
    {
        return empleadoSeleccionado;
    }

    public void setEmpleadoSeleccionado(EmpleadoDTO empleadoSeleccionado)
    {
        this.empleadoSeleccionado = empleadoSeleccionado;
    }

    private RolDTO determinaTipoRolPersona(String tipoEmpleado)
    {
        RolDTO rol = new RolDTO();
        if(tipoEmpleado.equalsIgnoreCase("T"))
        {
            rol = rolService.findById(Integer.valueOf(2));
            return rol;
        }
        if(tipoEmpleado.equalsIgnoreCase("L"))
        {
            rol = rolService.findById(Integer.valueOf(3));
            return rol;
        }
        if(tipoEmpleado.equalsIgnoreCase("M"))
        {
            rol = rolService.findById(Integer.valueOf(4));
            return rol;
        } else
        {
            return null;
        }
    }

    private List getIdEspecialidadListadeTrabajos(List listaTrabajosDetail)
    {
        List lista = new ArrayList();
        TrabajoDTO entrada;
        for(Iterator i$ = listaTrabajosDetail.iterator(); i$.hasNext(); lista.add(entrada.getEspecialidad().getIdEspecialidad()))
            entrada = (TrabajoDTO)i$.next();

        return lista;
    }

    private void remueveIdsTemporalesListaTrabajoDetail()
    {
        TrabajoDTO temp = new TrabajoDTO();
        for(int i = 0; i < listaTrabajosDetail.size(); i++)
            if(((TrabajoDTO)listaTrabajosDetail.get(i)).getPonerleIdNulo().equalsIgnoreCase(temp.NULO_SI))
                ((TrabajoDTO)listaTrabajosDetail.get(i)).setIdTrabajo(null);

    }

    private void actualizaEstadoDeTrabajos()
    {
        for(int i = 0; i < listaTrabajosDetail.size(); i++)
            ((TrabajoDTO)listaTrabajosDetail.get(i)).setEstadoTrabajo(((TrabajoDTO)listaTrabajosDetail.get(i)).getEstadoTrabajoEnum().getCode());

    }

    @Autowired
    DataSource dataSource;
    @Autowired
    PersonaService personaService;
    @Autowired
    PacienteService pacienteService;
    @Autowired
    TrabajoService trabajoService;
    @Autowired
    PaisRepository paisRepository;
    @Autowired
    EspecialidadService especialidadService;
    @Autowired
    SubEspecialidadService subEspecialidadService;
    @Autowired
    ProcedimientoService procedimientoService;
    @Autowired
    RolService rolService;
    @Autowired
    DepartamentoRepository departamentoRepository;
    @Autowired
    ProvinciaRepository provinciaRepository;
    @Autowired
    AtencionRepository atencionRepository;
    @Autowired
    DistritoRepository distritoRepository;
    @Autowired
    TrabajoRepository trabajoRepository;
    private PersonaDTO personaFiltro;
    private PersonaDTO personaSeleccionada;
    private PersonaDTO nuevaPersona;
    private EmpleadoDTO empleadoFiltro;
    private EmpleadoDTO nuevoEmpleado;
    private TrabajoDTO trabajoFiltro;
    private String idCompuestoDepartamento;
    private String idCompuestoProvincia;
    private String idCompuestoDistrito;
    private String idCompuestoDepartamentoPersonaSeleccionada;
    private String idCompuestoProvinciaPersonaSeleccionada;
    private String idCompuestoDistritoPersonaSeleccionada;
    private LazyDataModel personaLazyModel;
    private boolean mostrarDetalle;
    private List listaEstadosEmpleado;
    private List listaSexoPersonaEnum;
    private List listaQuienPreguntaEnum;
    private List listaEstadoTrabajoEnum;
    private String nuevoEstadoPersonaSeleccionada;
    private List listaEspecialidad;
    private Integer especialidadSeleccionadaId;
    private Integer subEspecialidadSeleccionadaId;
    private Integer procedimientoSeleccionadoId;
    private ProvinciaDTO provinciaSeleccionada;
    private DistritoDTO distritoSeleccionado;
    private EmpleadoDTO empleadoSeleccionado;
    private List listaTrabajosDetail;
    private TrabajoDTO trabajoSeleccionadoDetail;
    private List listaTrabajosNuevoEmpleado;
    private List listaEspecialidadPersist;
    private Integer especialidadSeleccionadaIdPersist;
    private EspecialidadDTO especialidadSeleccionadaPersist;
    private List listaIdsEspecialidades;
    private List listaBorrarIdsEspecialidades;
    private List listaPais;
    private List listaDepartamento;
    private List listaProvincia;
    private List listaDistrito;
    private Date hoy;
    private Validaciones validar;
}
