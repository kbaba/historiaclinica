// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:12 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Opcion.java

package paquete.entities;

import java.io.Serializable;

// Referenced classes of package paquete.entities:
//            Menu, Rol

@Entity
@Table(name="opcion")
@Table(appliesTo="opcion", indexes={@Index(name="opcion_rol_index", columnNames={"rol_id"}), @Index(name="opcion_menu_index", columnNames={"menu_id"})})
public class Opcion
    implements Serializable
{

    public Opcion()
    {
    }

    public Integer getIdOpcion()
    {
        return idOpcion;
    }

    public void setIdOpcion(Integer id)
    {
        idOpcion = id;
    }

    public String getNombreOpcion()
    {
        return nombreOpcion;
    }

    public void setNombreOpcion(String nombreOpcion)
    {
        this.nombreOpcion = nombreOpcion;
    }

    public String getDescripcionOpcion()
    {
        return descripcionOpcion;
    }

    public void setDescripcionOpcion(String descripcionOpcion)
    {
        this.descripcionOpcion = descripcionOpcion;
    }

    public Integer getPosicionOpcion()
    {
        return posicionOpcion;
    }

    public void setPosicionOpcion(Integer posicionOpcion)
    {
        this.posicionOpcion = posicionOpcion;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public Menu getMenu()
    {
        return menu;
    }

    public void setMenu(Menu menu)
    {
        this.menu = menu;
    }

    public Boolean getMostrarEnMenu()
    {
        return mostrarEnMenu;
    }

    public void setMostrarEnMenu(Boolean mostrarEnMenu)
    {
        this.mostrarEnMenu = mostrarEnMenu;
    }

    public Rol getRol()
    {
        return rol;
    }

    public void setRol(Rol rol)
    {
        this.rol = rol;
    }

    public Boolean getMostrarEnMobile()
    {
        return mostrarEnMobile;
    }

    public void setMostrarEnMobile(Boolean mostrarEnMobile)
    {
        this.mostrarEnMobile = mostrarEnMobile;
    }

    public int hashCode()
    {
        int hash = 7;
        hash = 11 * hash + (idOpcion == null ? 0 : idOpcion.hashCode());
        hash = 11 * hash + (nombreOpcion == null ? 0 : nombreOpcion.hashCode());
        hash = 11 * hash + (descripcionOpcion == null ? 0 : descripcionOpcion.hashCode());
        hash = 11 * hash + (posicionOpcion == null ? 0 : posicionOpcion.hashCode());
        hash = 11 * hash + (url == null ? 0 : url.hashCode());
        hash = 11 * hash + (mostrarEnMenu == null ? 0 : mostrarEnMenu.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Opcion other = (Opcion)obj;
        if(idOpcion != other.idOpcion && (idOpcion == null || !idOpcion.equals(other.idOpcion)))
            return false;
        if(nombreOpcion != null ? !nombreOpcion.equals(other.nombreOpcion) : other.nombreOpcion != null)
            return false;
        if(descripcionOpcion != null ? !descripcionOpcion.equals(other.descripcionOpcion) : other.descripcionOpcion != null)
            return false;
        if(posicionOpcion != other.posicionOpcion && (posicionOpcion == null || !posicionOpcion.equals(other.posicionOpcion)))
            return false;
        if(url != null ? !url.equals(other.url) : other.url != null)
            return false;
        return mostrarEnMenu == other.mostrarEnMenu || mostrarEnMenu != null && mostrarEnMenu.equals(other.mostrarEnMenu);
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_opcion", unique=0x00000001, nullable=0x00000000)
    private Integer idOpcion;
    @Column(name="nombre_opcion", nullable=0x00000000, length=0x00000064)
    private String nombreOpcion;
    @Column(name="descripcion_opcion", nullable=0x00000000, length=0x00000064)
    private String descripcionOpcion;
    @Column(name="posicion_opcion", nullable=0x00000000, length=0x00000002)
    private Integer posicionOpcion;
    @Column(name="url", nullable=0x00000000, length=0x00000064)
    private String url;
    @ManyToOne(fetch=javax.persistence.FetchType.EAGER)
    @JoinColumn(name="menu_id", nullable=0x00000000)
    @Fetch(value=org.hibernate.annotations.FetchMode.JOIN)
    private Menu menu;
    @Column(name="mostrar_en_menu", nullable=0x00000000, length=0x00000001)
    private Boolean mostrarEnMenu;
    @Column(name="mostrar_en_mobile", nullable=0x00000000, length=0x00000001)
    private Boolean mostrarEnMobile;
    @ManyToOne(fetch=javax.persistence.FetchType.EAGER)
    @JoinColumn(name="rol_id", nullable=0x00000000)
    private Rol rol;
}
