// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:17 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Paciente.java

package paquete.entities;

import java.io.Serializable;

// Referenced classes of package paquete.entities:
//            Historia, Persona

@Entity
@Table(name="paciente")
@Table(appliesTo="paciente", indexes={@Index(name="persona_index", columnNames={"persona_id"})})
public class Paciente
    implements Serializable
{

    public Paciente()
    {
    }

    public Integer getIdPaciente()
    {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente)
    {
        this.idPaciente = idPaciente;
    }

    public String getEstadoPaciente()
    {
        return estadoPaciente;
    }

    public void setEstadoPaciente(String estadoPaciente)
    {
        this.estadoPaciente = estadoPaciente;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public Historia getHistoria()
    {
        return historia;
    }

    public void setHistoria(Historia historia)
    {
        if(historia != null)
            historia.setPaciente(this);
        this.historia = historia;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Paciente other = (Paciente)obj;
        if(idPaciente != other.idPaciente && (idPaciente == null || !idPaciente.equals(other.idPaciente)))
            return false;
        if(persona != other.persona && (persona == null || !persona.equals(other.persona)))
            return false;
        if(estadoPaciente != null ? !estadoPaciente.equals(other.estadoPaciente) : other.estadoPaciente != null)
            return false;
        return historia == other.historia || historia != null && historia.equals(other.historia);
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_paciente", unique=0x00000001, nullable=0x00000000)
    private Integer idPaciente;
    @OneToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="persona_id", nullable=0x00000000)
    @ForeignKey(name="fk_persona")
    private Persona persona;
    @Column(name="estado_paciente", nullable=0x00000000, length=0x00000001)
    private String estadoPaciente;
    @OneToOne(cascade={javax.persistence.CascadeType.ALL}, fetch=javax.persistence.FetchType.LAZY, mappedBy="paciente")
    private Historia historia;
}
