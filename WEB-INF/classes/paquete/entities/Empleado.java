// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:34:47 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Empleado.java

package paquete.entities;

import java.io.Serializable;
import java.util.*;

// Referenced classes of package paquete.entities:
//            Trabajo, Persona

@Entity
@Table(name="empleado")
@Table(appliesTo="empleado", indexes={@Index(name="persona_empleado_index", columnNames={"persona_id"})})
public class Empleado
    implements Serializable
{

    public Empleado()
    {
        trabajos = new HashSet();
    }

    public Integer getIdEmpleado()
    {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idTrabajador)
    {
        idEmpleado = idTrabajador;
    }

    public String getTipoEmpleado()
    {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(String tipoTrabajador)
    {
        tipoEmpleado = tipoTrabajador;
    }

    public String getEstadoEmpleado()
    {
        return estadoEmpleado;
    }

    public void setEstadoEmpleado(String estadoTrabajador)
    {
        estadoEmpleado = estadoTrabajador;
    }

    public Persona getPersona()
    {
        return persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public Set getTrabajos()
    {
        return trabajos;
    }

    public void setTrabajos(Set trabajos)
    {
        this.trabajos = trabajos;
    }

    public void agregarTrabajo(Trabajo trabajo)
    {
        trabajo.setEmpleado(this);
        trabajos.add(trabajo);
    }

    public void agregarTrabajos(List trabajo)
    {
        Trabajo trab;
        for(Iterator i$ = trabajo.iterator(); i$.hasNext(); agregarTrabajo(trab))
            trab = (Trabajo)i$.next();

    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Empleado other = (Empleado)obj;
        if(idEmpleado != other.idEmpleado && (idEmpleado == null || !idEmpleado.equals(other.idEmpleado)))
            return false;
        if(tipoEmpleado != null ? !tipoEmpleado.equals(other.tipoEmpleado) : other.tipoEmpleado != null)
            return false;
        return estadoEmpleado != null ? estadoEmpleado.equals(other.estadoEmpleado) : other.estadoEmpleado == null;
    }

    public int hashCode()
    {
        int hash = 5;
        hash = 31 * hash + (idEmpleado == null ? 0 : idEmpleado.hashCode());
        hash = 31 * hash + (tipoEmpleado == null ? 0 : tipoEmpleado.hashCode());
        hash = 31 * hash + (estadoEmpleado == null ? 0 : estadoEmpleado.hashCode());
        return hash;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_empleado", unique=0x00000001, nullable=0x00000000)
    private Integer idEmpleado;
    @OneToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="persona_id", nullable=0x00000000)
    @ForeignKey(name="fk_persona_empleado")
    private Persona persona;
    @Column(name="tipo_empleado", nullable=0x00000000, length=0x00000001)
    private String tipoEmpleado;
    @Column(name="estado_empleado", nullable=0x00000000, length=0x00000001)
    private String estadoEmpleado;
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="empleado", fetch=javax.persistence.FetchType.EAGER)
    private Set trabajos;
}
