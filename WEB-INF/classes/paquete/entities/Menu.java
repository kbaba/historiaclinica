// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:09 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Menu.java

package paquete.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="menu")
public class Menu
    implements Serializable
{

    public Menu()
    {
        opciones = new HashSet();
    }

    public Integer getIdMenu()
    {
        return idMenu;
    }

    public void setIdMenu(Integer idMenu)
    {
        this.idMenu = idMenu;
    }

    public String getNombreMenu()
    {
        return nombreMenu;
    }

    public void setNombreMenu(String nombreMenu)
    {
        this.nombreMenu = nombreMenu;
    }

    public Integer getPosicionMenu()
    {
        return posicionMenu;
    }

    public void setPosicionMenu(Integer posicionMenu)
    {
        this.posicionMenu = posicionMenu;
    }

    public Set getOpciones()
    {
        return opciones;
    }

    public void setOpciones(Set opciones)
    {
        this.opciones = opciones;
    }

    public int hashCode()
    {
        int hash = 5;
        hash = 17 * hash + (idMenu == null ? 0 : idMenu.hashCode());
        hash = 17 * hash + (nombreMenu == null ? 0 : nombreMenu.hashCode());
        hash = 17 * hash + (posicionMenu == null ? 0 : posicionMenu.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Menu other = (Menu)obj;
        if(idMenu != null ? !idMenu.equals(other.idMenu) : other.idMenu != null)
            return false;
        if(nombreMenu != null ? !nombreMenu.equals(other.nombreMenu) : other.nombreMenu != null)
            return false;
        return posicionMenu != null ? posicionMenu.equals(other.posicionMenu) : other.posicionMenu == null;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_menu", unique=0x00000001, nullable=0x00000000)
    private Integer idMenu;
    @Column(name="nombre_menu", nullable=0x00000000, length=0x0000002D)
    private String nombreMenu;
    @Column(name="posicion_menu", length=0x00000002)
    private Integer posicionMenu;
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, orphanRemoval=0x00000001, mappedBy="menu")
    @BatchSize(size=0x0000000A)
    private Set opciones;
}
