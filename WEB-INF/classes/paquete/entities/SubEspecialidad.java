// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:46 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidad.java

package paquete.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

// Referenced classes of package paquete.entities:
//            Procedimiento, Pregunta, Especialidad

@Entity
@Table(name="sub_especialidad")
@Table(appliesTo="sub_especialidad", indexes={@Index(name="especialidad_index", columnNames={"especialidad_id"})})
public class SubEspecialidad
    implements Serializable
{

    public SubEspecialidad()
    {
        procedimientos = new HashSet();
        preguntas = new HashSet();
    }

    public Integer getIdSubEspecialidad()
    {
        return idSubEspecialidad;
    }

    public void setIdSubEspecialidad(Integer idSubEspecialidad)
    {
        this.idSubEspecialidad = idSubEspecialidad;
    }

    public String getNombreSubEspecialidad()
    {
        return nombreSubEspecialidad;
    }

    public void setNombreSubEspecialidad(String nombreSubEspecialidad)
    {
        this.nombreSubEspecialidad = nombreSubEspecialidad;
    }

    public Especialidad getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad)
    {
        this.especialidad = especialidad;
    }

    public Set getProcedimientos()
    {
        return procedimientos;
    }

    public void setProcedimientos(Set procedimientos)
    {
        this.procedimientos = procedimientos;
    }

    public String getEstadoSubEspecialidad()
    {
        return estadoSubEspecialidad;
    }

    public void setEstadoSubEspecialidad(String estadoSubEspecialidad)
    {
        this.estadoSubEspecialidad = estadoSubEspecialidad;
    }

    public Set getPreguntas()
    {
        return preguntas;
    }

    public void setPreguntas(Set preguntas)
    {
        this.preguntas = preguntas;
    }

    public void agregarProcedimientos(Procedimiento procedimiento)
    {
        procedimiento.setSubEspecialidad(this);
        procedimientos.add(procedimiento);
    }

    public void agregarPregunta(Pregunta pregunta)
    {
        pregunta.setSubEspecialidad(this);
        preguntas.add(pregunta);
    }

    public int hashCode()
    {
        int hash = 5;
        hash = 17 * hash + (idSubEspecialidad == null ? 0 : idSubEspecialidad.hashCode());
        hash = 17 * hash + (nombreSubEspecialidad == null ? 0 : nombreSubEspecialidad.hashCode());
        hash = 17 * hash + (estadoSubEspecialidad == null ? 0 : estadoSubEspecialidad.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
        {
            return false;
        } else
        {
            SubEspecialidad other = (SubEspecialidad)obj;
            return true;
        }
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_sub_especialidad", unique=0x00000001, nullable=0x00000000)
    private Integer idSubEspecialidad;
    @Column(name="nombre_sub_especialidad", nullable=0x00000000, length=0x00000064)
    private String nombreSubEspecialidad;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="especialidad_id", nullable=0x00000000)
    @ForeignKey(name="fk_especialidad")
    private Especialidad especialidad;
    @Column(name="estado_sub_especialidad", nullable=0x00000000, length=0x00000001)
    private String estadoSubEspecialidad;
    @BatchSize(size=0x00000014)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="subEspecialidad", orphanRemoval=0x00000001, fetch=javax.persistence.FetchType.LAZY)
    private Set procedimientos;
    @BatchSize(size=0x00000014)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="subEspecialidad", fetch=javax.persistence.FetchType.LAZY)
    private Set preguntas;
}
