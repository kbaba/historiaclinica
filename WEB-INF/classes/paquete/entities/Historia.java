// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:06 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Historia.java

package paquete.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

// Referenced classes of package paquete.entities:
//            Atencion, Paciente

@Entity
@Table(name="historia")
@Table(appliesTo="historia", indexes={@Index(name="paciente_index", columnNames={"paciente_id"})})
public class Historia
    implements Serializable
{

    public Historia()
    {
        atenciones = new HashSet();
    }

    public Integer getIdHistoria()
    {
        return idHistoria;
    }

    public void setIdHistoria(Integer idHistoria)
    {
        this.idHistoria = idHistoria;
    }

    public Paciente getPaciente()
    {
        return paciente;
    }

    public void setPaciente(Paciente paciente)
    {
        this.paciente = paciente;
    }

    public Set getAtenciones()
    {
        return atenciones;
    }

    public void setAtenciones(Set atenciones)
    {
        this.atenciones = atenciones;
    }

    public transient void agregarAtencion(Atencion atenciones[])
    {
        Atencion arr$[] = atenciones;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            Atencion atencion = arr$[i$];
            atencion.setHistoria(this);
            this.atenciones.add(atencion);
        }

    }

    public int hashCode()
    {
        int hash = 3;
        hash = 97 * hash + (idHistoria == null ? 0 : idHistoria.hashCode());
        hash = 97 * hash + (paciente == null ? 0 : paciente.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Historia other = (Historia)obj;
        if(idHistoria != other.idHistoria && (idHistoria == null || !idHistoria.equals(other.idHistoria)))
            return false;
        return paciente == other.paciente || paciente != null && paciente.equals(other.paciente);
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_historia", unique=0x00000001, nullable=0x00000000)
    private Integer idHistoria;
    @OneToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="paciente_id", nullable=0x00000000)
    @ForeignKey(name="fk_paciente")
    private Paciente paciente;
    @BatchSize(size=0x00000032)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="historia", orphanRemoval=0x00000001, fetch=javax.persistence.FetchType.LAZY)
    private Set atenciones;
}
