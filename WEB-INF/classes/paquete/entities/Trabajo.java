// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:51 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Trabajo.java

package paquete.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

// Referenced classes of package paquete.entities:
//            Atencion, Especialidad, Empleado

@Entity
@Table(name="trabajo")
public class Trabajo
    implements Serializable
{

    public Trabajo()
    {
        atenciones = new HashSet();
    }

    public Integer getIdTrabajo()
    {
        return idTrabajo;
    }

    public void setIdTrabajo(Integer idTrabajo)
    {
        this.idTrabajo = idTrabajo;
    }

    public Especialidad getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad)
    {
        this.especialidad = especialidad;
    }

    public Empleado getEmpleado()
    {
        return empleado;
    }

    public void setEmpleado(Empleado empleado)
    {
        this.empleado = empleado;
    }

    public Set getAtenciones()
    {
        return atenciones;
    }

    public void setAtenciones(Set atencion)
    {
        atenciones = atencion;
    }

    public void agregarAtencion(Atencion atencion)
    {
        atencion.setTrabajo(this);
        atenciones.add(atencion);
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
        {
            return false;
        } else
        {
            Trabajo other = (Trabajo)obj;
            return true;
        }
    }

    public String getEstadoTrabajo()
    {
        return estadoTrabajo;
    }

    public void setEstadoTrabajo(String estadoTrabajo)
    {
        this.estadoTrabajo = estadoTrabajo;
    }

    public int hashCode()
    {
        int hash = 5;
        hash = 41 * hash + (idTrabajo == null ? 0 : idTrabajo.hashCode());
        return hash;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_trabajo", unique=0x00000001, nullable=0x00000000)
    private Integer idTrabajo;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="especialidad_id", nullable=0x00000000)
    @ForeignKey(name="fk_trabajo_especialidad")
    private Especialidad especialidad;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="empleado_id", nullable=0x00000000)
    @ForeignKey(name="fk_trabajo_empleado")
    private Empleado empleado;
    @BatchSize(size=0x00000064)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="trabajo", fetch=javax.persistence.FetchType.LAZY)
    private Set atenciones;
    @Column(name="estado_trabajo", nullable=0x00000000, length=0x00000001)
    private String estadoTrabajo;
}
