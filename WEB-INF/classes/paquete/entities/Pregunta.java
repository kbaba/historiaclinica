// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:28 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Pregunta.java

package paquete.entities;

import java.io.Serializable;
import java.util.*;

// Referenced classes of package paquete.entities:
//            Alternativa, Especialidad, SubEspecialidad, Procedimiento

@Entity
@Table(name="pregunta")
@Table(appliesTo="pregunta", indexes={@Index(name="pregunta_especialidad_index", columnNames={"especialidad_id"}), @Index(name="pregunta_sub_especialidad_index", columnNames={"sub_especialidad_id"}), @Index(name="pregunta_procedimiento_index", columnNames={"procedimiento_id"})})
public class Pregunta
    implements Serializable
{

    public Pregunta()
    {
        alternativas = new HashSet();
    }

    public Integer getIdPregunta()
    {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta)
    {
        this.idPregunta = idPregunta;
    }

    public String getDescripcionPregunta()
    {
        return descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta)
    {
        this.descripcionPregunta = descripcionPregunta;
    }

    public String getTipoPregunta()
    {
        return tipoPregunta;
    }

    public void setTipoPregunta(String tipoPregunta)
    {
        this.tipoPregunta = tipoPregunta;
    }

    public String getTipoRespuesta()
    {
        return tipoRespuesta;
    }

    public void setTipoRespuesta(String tipoRespuesta)
    {
        this.tipoRespuesta = tipoRespuesta;
    }

    public String getQuienPregunta()
    {
        return quienPregunta;
    }

    public void setQuienPregunta(String quienPregunta)
    {
        this.quienPregunta = quienPregunta;
    }

    public String getEstadoPregunta()
    {
        return estadoPregunta;
    }

    public void setEstadoPregunta(String estadoPregunta)
    {
        this.estadoPregunta = estadoPregunta;
    }

    public Procedimiento getProcedimiento()
    {
        return procedimiento;
    }

    public void setProcedimiento(Procedimiento procedimiento)
    {
        this.procedimiento = procedimiento;
    }

    public Set getAlternativas()
    {
        return alternativas;
    }

    public void setAlternativas(Set alternativas)
    {
        this.alternativas = alternativas;
    }

    public Especialidad getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad)
    {
        this.especialidad = especialidad;
    }

    public SubEspecialidad getSubEspecialidad()
    {
        return subEspecialidad;
    }

    public void setSubEspecialidad(SubEspecialidad subEspecialidad)
    {
        this.subEspecialidad = subEspecialidad;
    }

    public void agregarAlternativa(Alternativa alternativa)
    {
        alternativa.setPregunta(this);
        alternativas.add(alternativa);
    }

    public void addAlternativas(Set alternativas)
    {
        Alternativa entrada;
        for(Iterator i$ = alternativas.iterator(); i$.hasNext(); agregarAlternativa(entrada))
            entrada = (Alternativa)i$.next();

    }

    public int hashCode()
    {
        int hash = 3;
        hash = 97 * hash + (idPregunta == null ? 0 : idPregunta.hashCode());
        hash = 97 * hash + (descripcionPregunta == null ? 0 : descripcionPregunta.hashCode());
        hash = 97 * hash + (tipoPregunta == null ? 0 : tipoPregunta.hashCode());
        hash = 97 * hash + (tipoRespuesta == null ? 0 : tipoRespuesta.hashCode());
        hash = 97 * hash + (quienPregunta == null ? 0 : quienPregunta.hashCode());
        hash = 97 * hash + (estadoPregunta == null ? 0 : estadoPregunta.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Pregunta other = (Pregunta)obj;
        if(idPregunta != other.idPregunta && (idPregunta == null || !idPregunta.equals(other.idPregunta)))
            return false;
        if(descripcionPregunta != null ? !descripcionPregunta.equals(other.descripcionPregunta) : other.descripcionPregunta != null)
            return false;
        if(tipoPregunta != null ? !tipoPregunta.equals(other.tipoPregunta) : other.tipoPregunta != null)
            return false;
        if(tipoRespuesta != null ? !tipoRespuesta.equals(other.tipoRespuesta) : other.tipoRespuesta != null)
            return false;
        if(quienPregunta != null ? !quienPregunta.equals(other.quienPregunta) : other.quienPregunta != null)
            return false;
        return estadoPregunta != null ? estadoPregunta.equals(other.estadoPregunta) : other.estadoPregunta == null;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_pregunta", unique=0x00000001, nullable=0x00000000)
    private Integer idPregunta;
    @Column(name="descripcion_pregunta", nullable=0x00000000, length=0x00000096)
    private String descripcionPregunta;
    @Column(name="tipo_pregunta", nullable=0x00000000, length=0x00000001)
    private String tipoPregunta;
    @Column(name="tipo_respuesta", nullable=0x00000000, length=0x00000001)
    private String tipoRespuesta;
    @Column(name="quien_pregunta", nullable=0x00000000, length=0x0000002D)
    private String quienPregunta;
    @Column(name="estado_pregunta", nullable=0x00000000, length=0x00000001)
    private String estadoPregunta;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="especialidad_id", nullable=0x00000001)
    @ForeignKey(name="fk_pregunta_especialidad")
    private Especialidad especialidad;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="sub_especialidad_id", nullable=0x00000001)
    @ForeignKey(name="fk_pregunta_sub_especialidad")
    private SubEspecialidad subEspecialidad;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="procedimiento_id", nullable=0x00000001)
    @ForeignKey(name="fk_pregunta_procedimiento")
    private Procedimiento procedimiento;
    @BatchSize(size=0x0000000A)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="pregunta", orphanRemoval=0x00000001, fetch=javax.persistence.FetchType.EAGER)
    private Set alternativas;
}
