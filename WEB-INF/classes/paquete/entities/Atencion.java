// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:34:30 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Atencion.java

package paquete.entities;

import java.io.Serializable;
import java.util.*;

// Referenced classes of package paquete.entities:
//            Respuesta, Historia, Trabajo, Enfermedad

@Entity
@Table(name="atencion")
@Table(appliesTo="atencion", indexes={@Index(name="historia_index", columnNames={"historia_id"}), @Index(name="trabajo_index", columnNames={"trabajo_id"})})
public class Atencion
    implements Serializable
{

    public Atencion()
    {
        respuestas = new HashSet();
        enfermedades = new HashSet();
    }

    public Integer getIdAtencion()
    {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion)
    {
        this.idAtencion = idAtencion;
    }

    public Date getFechaAtencion()
    {
        return fechaAtencion;
    }

    public void setFechaAtencion(Date fechaAtencion)
    {
        this.fechaAtencion = fechaAtencion;
    }

    public String getDiagnosticoAtencion()
    {
        return diagnosticoAtencion;
    }

    public void setDiagnosticoAtencion(String diagnosticoAtencion)
    {
        this.diagnosticoAtencion = diagnosticoAtencion;
    }

    public Date getFechaAproxSiguienteAtencion()
    {
        return fechaAproxSiguienteAtencion;
    }

    public void setFechaAproxSiguienteAtencion(Date fechaAproxSiguienteAtencion)
    {
        this.fechaAproxSiguienteAtencion = fechaAproxSiguienteAtencion;
    }

    public Set getRespuestas()
    {
        return respuestas;
    }

    public void setRespuestas(Set respuestas)
    {
        this.respuestas = respuestas;
    }

    public Historia getHistoria()
    {
        return historia;
    }

    public void setHistoria(Historia historia)
    {
        this.historia = historia;
    }

    public Set getEnfermedades()
    {
        return enfermedades;
    }

    public void setEnfermedades(Set enfermedades)
    {
        this.enfermedades = enfermedades;
    }

    public Trabajo getTrabajo()
    {
        return trabajo;
    }

    public void setTrabajo(Trabajo trabajo)
    {
        this.trabajo = trabajo;
    }

    public transient void agregarRespuestas(Respuesta respuestas[])
    {
        Respuesta arr$[] = respuestas;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            Respuesta respuesta = arr$[i$];
            respuesta.setAtencion(this);
            this.respuestas.add(respuesta);
        }

    }

    public transient void agregarEnfermedades(Enfermedad enfermedades[])
    {
        Enfermedad arr$[] = enfermedades;
        int len$ = arr$.length;
        for(int i$ = 0; i$ < len$; i$++)
        {
            Enfermedad enfermedad = arr$[i$];
            this.enfermedades.add(enfermedad);
        }

    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_atencion", unique=0x00000001, nullable=0x00000000)
    private Integer idAtencion;
    @Temporal(value=javax.persistence.TemporalType.DATE)
    @Column(name="fecha_atencion", nullable=0x00000000, length=0x0000000A)
    private Date fechaAtencion;
    @Column(name="diagnostico_atencion", length=0x0000FFFF, nullable=0x00000001)
    private String diagnosticoAtencion;
    @Temporal(value=javax.persistence.TemporalType.DATE)
    @Column(name="fecha_aprox_siguiente_atencion", length=0x0000000A, nullable=0x00000001)
    private Date fechaAproxSiguienteAtencion;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="historia_id", nullable=0x00000000)
    @ForeignKey(name="fk_historia")
    private Historia historia;
    @BatchSize(size=0x00000032)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="atencion", orphanRemoval=0x00000001, fetch=javax.persistence.FetchType.LAZY)
    private Set respuestas;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="trabajo_id", nullable=0x00000000)
    @ForeignKey(name="fk_atencion_trabajo")
    private Trabajo trabajo;
    @BatchSize(size=0x0000000A)
    @ManyToMany(fetch=javax.persistence.FetchType.LAZY, cascade={javax.persistence.CascadeType.ALL})
    @JoinTable(name="atencion_enfermedad", joinColumns={@JoinColumn(name="atencion_id", nullable=0x00000000, updatable=0x00000000)}, inverseJoinColumns={@JoinColumn(name="enfermedad_id", nullable=0x00000000, updatable=0x00000000)})
    public Set enfermedades;
}
