// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:42 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Rol.java

package paquete.entities;

import java.io.Serializable;
import java.util.*;

// Referenced classes of package paquete.entities:
//            Opcion

@Entity
@Table(name="rol")
public class Rol
    implements Serializable
{

    public Rol()
    {
        options = new HashSet();
    }

    public Integer getIdRol()
    {
        return idRol;
    }

    public void setIdRol(Integer idRol)
    {
        this.idRol = idRol;
    }

    public String getNombreRol()
    {
        return nombreRol;
    }

    public void setNombreRol(String nombreRol)
    {
        this.nombreRol = nombreRol;
    }

    public Set getOptions()
    {
        return options;
    }

    public void setOptions(Set options)
    {
        this.options = options;
    }

    public void addOpciones(Set options)
    {
        Opcion opcion;
        for(Iterator i$ = options.iterator(); i$.hasNext(); addOpcion(opcion))
            opcion = (Opcion)i$.next();

    }

    public void addOpcion(Opcion opcion)
    {
        opcion.setRol(this);
        options.add(opcion);
    }

    public int hashCode()
    {
        int hash = 7;
        hash = 83 * hash + (idRol == null ? 0 : idRol.hashCode());
        hash = 83 * hash + (nombreRol == null ? 0 : nombreRol.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Rol other = (Rol)obj;
        if(idRol != null ? !idRol.equals(other.idRol) : other.idRol != null)
            return false;
        return nombreRol != null ? nombreRol.equals(other.nombreRol) : other.nombreRol == null;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_rol", unique=0x00000001, nullable=0x00000000)
    private Integer idRol;
    @Column(name="nombre_rol", nullable=0x00000000, length=0x0000002D)
    private String nombreRol;
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="rol")
    @BatchSize(size=0x0000000A)
    @OrderBy(value="posicionOpcion ASC")
    private Set options;
}
