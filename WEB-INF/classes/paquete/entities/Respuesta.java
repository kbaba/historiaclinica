// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:35 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Respuesta.java

package paquete.entities;

import java.io.Serializable;

// Referenced classes of package paquete.entities:
//            Atencion, Especialidad, SubEspecialidad, Procedimiento

@Entity
@Table(name="respuesta")
@Table(appliesTo="respuesta", indexes={@Index(name="atencion_index", columnNames={"atencion_id"})})
public class Respuesta
    implements Serializable
{

    public Respuesta()
    {
    }

    public Integer getIdRespuesta()
    {
        return idRespuesta;
    }

    public void setIdRespuesta(Integer idRespuesta)
    {
        this.idRespuesta = idRespuesta;
    }

    public String getDescripcionPregunta()
    {
        return descripcionPregunta;
    }

    public void setDescripcionPregunta(String descripcionPregunta)
    {
        this.descripcionPregunta = descripcionPregunta;
    }

    public String getDetalleRespuesta()
    {
        return detalleRespuesta;
    }

    public void setDetalleRespuesta(String detalleRespuesta)
    {
        this.detalleRespuesta = detalleRespuesta;
    }

    public Atencion getAtencion()
    {
        return atencion;
    }

    public void setAtencion(Atencion atencion)
    {
        this.atencion = atencion;
    }

    public Especialidad getEspecialidad()
    {
        return especialidad;
    }

    public void setEspecialidad(Especialidad especialidad)
    {
        this.especialidad = especialidad;
    }

    public SubEspecialidad getSubEspecialidad()
    {
        return subEspecialidad;
    }

    public void setSubEspecialidad(SubEspecialidad subEspecialidad)
    {
        this.subEspecialidad = subEspecialidad;
    }

    public Procedimiento getProcedimiento()
    {
        return procedimiento;
    }

    public void setProcedimiento(Procedimiento procedimiento)
    {
        this.procedimiento = procedimiento;
    }

    public int hashCode()
    {
        int hash = 7;
        hash = 71 * hash + (idRespuesta == null ? 0 : idRespuesta.hashCode());
        hash = 71 * hash + (descripcionPregunta == null ? 0 : descripcionPregunta.hashCode());
        hash = 71 * hash + (detalleRespuesta == null ? 0 : detalleRespuesta.hashCode());
        hash = 71 * hash + (atencion == null ? 0 : atencion.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Respuesta other = (Respuesta)obj;
        if(idRespuesta != other.idRespuesta && (idRespuesta == null || !idRespuesta.equals(other.idRespuesta)))
            return false;
        if(descripcionPregunta != null ? !descripcionPregunta.equals(other.descripcionPregunta) : other.descripcionPregunta != null)
            return false;
        return detalleRespuesta != null ? detalleRespuesta.equals(other.detalleRespuesta) : other.detalleRespuesta == null;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_respuesta", unique=0x00000001, nullable=0x00000000)
    private Integer idRespuesta;
    @Column(name="descripcion_pregunta", nullable=0x00000000, length=0x00000096)
    private String descripcionPregunta;
    @Column(name="detalle_respuesta", nullable=0x00000000, length=0x00000096)
    private String detalleRespuesta;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="atencion_id", nullable=0x00000000)
    @ForeignKey(name="fk_atencion")
    private Atencion atencion;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="especialidad_id", nullable=0x00000001)
    private Especialidad especialidad;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="sub_especialidad_id", nullable=0x00000001)
    private SubEspecialidad subEspecialidad;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="procedimiento_id", nullable=0x00000001)
    private Procedimiento procedimiento;
}
