// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:34:56 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Enfermedad.java

package paquete.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="enfermedad")
public class Enfermedad
    implements Serializable
{

    public Enfermedad()
    {
        Atenciones = new HashSet();
    }

    public Enfermedad(String idEnfermedad, String nombreEnfermedad)
    {
        Atenciones = new HashSet();
        this.idEnfermedad = idEnfermedad;
        this.nombreEnfermedad = nombreEnfermedad;
    }

    public Enfermedad(String idEnfermedad, String nombreEnfermedad, String grupoEnfermedad)
    {
        Atenciones = new HashSet();
        this.idEnfermedad = idEnfermedad;
        this.nombreEnfermedad = nombreEnfermedad;
        this.grupoEnfermedad = grupoEnfermedad;
    }

    public String getIdEnfermedad()
    {
        return idEnfermedad;
    }

    public void setIdEnfermedad(String idEnfermedad)
    {
        this.idEnfermedad = idEnfermedad;
    }

    public String getNombreEnfermedad()
    {
        return nombreEnfermedad;
    }

    public void setNombreEnfermedad(String nombreEnfermedad)
    {
        this.nombreEnfermedad = nombreEnfermedad;
    }

    public String getGrupoEnfermedad()
    {
        return grupoEnfermedad;
    }

    public void setGrupoEnfermedad(String grupoEnfermedad)
    {
        this.grupoEnfermedad = grupoEnfermedad;
    }

    public Set getAtenciones()
    {
        return Atenciones;
    }

    public void setAtenciones(Set Atenciones)
    {
        this.Atenciones = Atenciones;
    }

    public Boolean getEsCapitulo()
    {
        return esCapitulo;
    }

    public void setEsCapitulo(Boolean esCapitulo)
    {
        this.esCapitulo = esCapitulo;
    }

    @Id
    @Column(name="id_enfermedad", unique=0x00000001, nullable=0x00000000, length=0x0000000B)
    private String idEnfermedad;
    @Column(name="nombre_enfermedad", nullable=0x00000000, length=0x00000096)
    private String nombreEnfermedad;
    @Column(name="grupo_enfermedad", length=0x0000000F)
    private String grupoEnfermedad;
    @Column(name="es_capitulo", length=0x00000001)
    private Boolean esCapitulo;
    @BatchSize(size=0x0000000A)
    @ManyToMany(cascade={javax.persistence.CascadeType.ALL}, fetch=javax.persistence.FetchType.EAGER, mappedBy="enfermedades")
    public Set Atenciones;
}
