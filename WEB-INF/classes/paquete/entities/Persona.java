// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:22 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Persona.java

package paquete.entities;

import java.io.Serializable;
import java.util.Date;

// Referenced classes of package paquete.entities:
//            Paciente, Empleado, Rol

@Entity
@Table(name="persona")
@Table(appliesTo="persona", indexes={@Index(name="persona_pais_index", columnNames={"nacionalidad_pais_id"}), @Index(name="persona_rol_index", columnNames={"rol_id"})})
public class Persona
    implements Serializable
{

    public Persona()
    {
    }

    public Integer getIdPersona()
    {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona)
    {
        this.idPersona = idPersona;
    }

    public String getTipoDocumentoPersona()
    {
        return tipoDocumentoPersona;
    }

    public void setTipoDocumentoPersona(String tipoDocumentoPersona)
    {
        this.tipoDocumentoPersona = tipoDocumentoPersona;
    }

    public String getDocumentoPersona()
    {
        return documentoPersona;
    }

    public void setDocumentoPersona(String documentoPersona)
    {
        this.documentoPersona = documentoPersona;
    }

    public String getNombrePersona()
    {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona)
    {
        this.nombrePersona = nombrePersona;
    }

    public String getApellidoPaternoPersona()
    {
        return apellidoPaternoPersona;
    }

    public void setApellidoPaternoPersona(String apellidoPaternoPersona)
    {
        this.apellidoPaternoPersona = apellidoPaternoPersona;
    }

    public String getApellidoMaternoPersona()
    {
        return apellidoMaternoPersona;
    }

    public void setApellidoMaternoPersona(String apellidoMaternoPersona)
    {
        this.apellidoMaternoPersona = apellidoMaternoPersona;
    }

    public Date getFechaNacimientoPersona()
    {
        return fechaNacimientoPersona;
    }

    public void setFechaNacimientoPersona(Date fechaNacimientoPersona)
    {
        this.fechaNacimientoPersona = fechaNacimientoPersona;
    }

    public String getTelefonoFijoPersona()
    {
        return telefonoFijoPersona;
    }

    public void setTelefonoFijoPersona(String telefonoFijoPersona)
    {
        this.telefonoFijoPersona = telefonoFijoPersona;
    }

    public String getTelefonoCelularPersona()
    {
        return telefonoCelularPersona;
    }

    public void setTelefonoCelularPersona(String telefonoCelularPersona)
    {
        this.telefonoCelularPersona = telefonoCelularPersona;
    }

    public String getEmailPersona()
    {
        return emailPersona;
    }

    public void setEmailPersona(String emailPersona)
    {
        this.emailPersona = emailPersona;
    }

    public String getDireccionPersona()
    {
        return direccionPersona;
    }

    public void setDireccionPersona(String direccionPersona)
    {
        this.direccionPersona = direccionPersona;
    }

    public String getIdResidenciaDistrito()
    {
        return idResidenciaDistrito;
    }

    public void setIdResidenciaDistrito(String idResidenciaDistrito)
    {
        this.idResidenciaDistrito = idResidenciaDistrito;
    }

    public Paciente getPaciente()
    {
        return paciente;
    }

    public void setPaciente(Paciente paciente)
    {
        if(paciente != null)
            paciente.setPersona(this);
        this.paciente = paciente;
    }

    public Empleado getEmpleado()
    {
        return empleado;
    }

    public void setEmpleado(Empleado empleado)
    {
        if(empleado != null)
            empleado.setPersona(this);
        this.empleado = empleado;
    }

    public String getIdPaisNacionalidad()
    {
        return idPaisNacionalidad;
    }

    public void setIdPaisNacionalidad(String idPaisNacionalidad)
    {
        this.idPaisNacionalidad = idPaisNacionalidad;
    }

    public Integer getCantidadPersonasViviendaPersona()
    {
        return cantidadPersonasViviendaPersona;
    }

    public void setCantidadPersonasViviendaPersona(Integer cantidadPersonasViviendaPersona)
    {
        this.cantidadPersonasViviendaPersona = cantidadPersonasViviendaPersona;
    }

    public Integer getDormitoriosViviendaPersona()
    {
        return dormitoriosViviendaPersona;
    }

    public void setDormitoriosViviendaPersona(Integer dormitoriosViviendaPersona)
    {
        this.dormitoriosViviendaPersona = dormitoriosViviendaPersona;
    }

    public String getEstatutoSocialPersona()
    {
        return estatutoSocialPersona;
    }

    public void setEstatutoSocialPersona(String estatutoSocialPersona)
    {
        this.estatutoSocialPersona = estatutoSocialPersona;
    }

    public String getMaterialViviendaPersona()
    {
        return materialViviendaPersona;
    }

    public void setMaterialViviendaPersona(String materialViviendaPersona)
    {
        this.materialViviendaPersona = materialViviendaPersona;
    }

    public String getPertenenciaViviendaPersona()
    {
        return pertenenciaViviendaPersona;
    }

    public void setPertenenciaViviendaPersona(String pertenenciaViviendaPersona)
    {
        this.pertenenciaViviendaPersona = pertenenciaViviendaPersona;
    }

    public String getTipoResidenciaPersona()
    {
        return tipoResidenciaPersona;
    }

    public void setTipoResidenciaPersona(String tipoResidenciaPersona)
    {
        this.tipoResidenciaPersona = tipoResidenciaPersona;
    }

    public Rol getRol()
    {
        return rol;
    }

    public void setRol(Rol rol)
    {
        this.rol = rol;
    }

    public String getSexoPersona()
    {
        return sexoPersona;
    }

    public void setSexoPersona(String sexoPersona)
    {
        this.sexoPersona = sexoPersona;
    }

    public String getUsuario()
    {
        return usuario;
    }

    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }

    public String getClave()
    {
        return clave;
    }

    public void setClave(String clave)
    {
        this.clave = clave;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_persona")
    private Integer idPersona;
    @Column(name="tipo_documento_persona", length=0x00000001)
    private String tipoDocumentoPersona;
    @Column(name="documento_persona", length=0x00000014)
    private String documentoPersona;
    @Column(name="nacionalidad_pais_id", nullable=0x00000000, length=0x00000003)
    private String idPaisNacionalidad;
    @Column(name="nombre_persona", nullable=0x00000000, length=0x00000037)
    private String nombrePersona;
    @Column(name="apellido_paterno_persona", nullable=0x00000000, length=0x0000002D)
    private String apellidoPaternoPersona;
    @Column(name="apellido_materno_persona", nullable=0x00000000, length=0x0000002D)
    private String apellidoMaternoPersona;
    @Temporal(value=javax.persistence.TemporalType.DATE)
    @Column(name="fecha_nacimiento_persona", nullable=0x00000000, length=0x0000000A)
    private Date fechaNacimientoPersona;
    @Column(name="telefono_fijo_persona", length=0x00000014)
    private String telefonoFijoPersona;
    @Column(name="telefono_celular_persona", length=0x00000014)
    private String telefonoCelularPersona;
    @Column(name="email_persona", length=0x00000032)
    private String emailPersona;
    @Column(name="direccion_persona", length=0x0000012C)
    private String direccionPersona;
    @Column(name="residencia_distrito_id", length=0x0000000F)
    private String idResidenciaDistrito;
    @OneToOne(cascade={javax.persistence.CascadeType.ALL}, fetch=javax.persistence.FetchType.LAZY, mappedBy="persona")
    private Paciente paciente;
    @OneToOne(cascade={javax.persistence.CascadeType.ALL}, fetch=javax.persistence.FetchType.EAGER, mappedBy="persona")
    private Empleado empleado;
    @Column(name="tipo_residencia_persona", length=0x00000001)
    private String tipoResidenciaPersona;
    @Column(name="dormitorios_vivienda_persona")
    private Integer dormitoriosViviendaPersona;
    @Column(name="cantidad_personas_vivienda_persona")
    private Integer cantidadPersonasViviendaPersona;
    @Column(name="pertenencia_vivienda_persona", length=0x00000001)
    private String pertenenciaViviendaPersona;
    @Column(name="material_vivienda_persona", length=0x00000001)
    private String materialViviendaPersona;
    @Column(name="estatuto_social_persona", length=0x00000001)
    private String estatutoSocialPersona;
    @Column(name="sexo_persona", length=0x00000001)
    private String sexoPersona;
    @ManyToOne
    @JoinColumn(name="rol_id", nullable=0x00000001)
    @ForeignKey(name="fk_persona_rol")
    private Rol rol;
    @Column(name="usuario", length=0x0000002D)
    private String usuario;
    @Column(name="clave", length=0x000000C8)
    private String clave;
}
