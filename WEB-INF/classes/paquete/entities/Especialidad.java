// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:01 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Especialidad.java

package paquete.entities;

import java.io.Serializable;
import java.util.*;

// Referenced classes of package paquete.entities:
//            Trabajo, SubEspecialidad, Pregunta

@Entity
@Table(name="especialidad")
public class Especialidad
    implements Serializable
{

    public Especialidad()
    {
        subEspecialidades = new HashSet();
        preguntas = new HashSet();
        trabajos = new HashSet();
    }

    public Especialidad(String nombreEspecialidad, String estadoEspecialidad)
    {
        subEspecialidades = new HashSet();
        preguntas = new HashSet();
        trabajos = new HashSet();
        this.nombreEspecialidad = nombreEspecialidad;
        this.estadoEspecialidad = estadoEspecialidad;
    }

    public Integer getIdEspecialidad()
    {
        return idEspecialidad;
    }

    public void setIdEspecialidad(Integer idEspecialidad)
    {
        this.idEspecialidad = idEspecialidad;
    }

    public String getNombreEspecialidad()
    {
        return nombreEspecialidad;
    }

    public void setNombreEspecialidad(String nombreEspecialidad)
    {
        this.nombreEspecialidad = nombreEspecialidad;
    }

    public String getEstadoEspecialidad()
    {
        return estadoEspecialidad;
    }

    public void setEstadoEspecialidad(String estadoEspecialidad)
    {
        this.estadoEspecialidad = estadoEspecialidad;
    }

    public Set getSubEspecialidades()
    {
        return subEspecialidades;
    }

    public void setSubEspecialidades(Set subEspecialidades)
    {
        this.subEspecialidades = subEspecialidades;
    }

    public Set getPreguntas()
    {
        return preguntas;
    }

    public void setPreguntas(Set preguntas)
    {
        this.preguntas = preguntas;
    }

    public void agregarSubEspecialidad(SubEspecialidad subEspecialidad)
    {
        subEspecialidad.setEspecialidad(this);
        subEspecialidades.add(subEspecialidad);
    }

    public void agregarPregunta(Pregunta pregunta)
    {
        pregunta.setEspecialidad(this);
        preguntas.add(pregunta);
    }

    public Set getTrabajos()
    {
        return trabajos;
    }

    public void setTrabajos(Set trabajos)
    {
        this.trabajos = trabajos;
    }

    public void addTrabajo(Trabajo trabajo)
    {
        if(trabajo != null)
            trabajo.setEspecialidad(this);
        trabajos.add(trabajo);
    }

    public void addTrabajos(List trabajos)
    {
        Trabajo trabajo;
        for(Iterator i$ = trabajos.iterator(); i$.hasNext(); addTrabajo(trabajo))
            trabajo = (Trabajo)i$.next();

    }

    public int hashCode()
    {
        int hash = 5;
        hash = 83 * hash + (idEspecialidad == null ? 0 : idEspecialidad.hashCode());
        hash = 83 * hash + (nombreEspecialidad == null ? 0 : nombreEspecialidad.hashCode());
        hash = 83 * hash + (estadoEspecialidad == null ? 0 : estadoEspecialidad.hashCode());
        return hash;
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
        {
            return false;
        } else
        {
            Especialidad other = (Especialidad)obj;
            return true;
        }
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_especialidad", unique=0x00000001, nullable=0x00000000)
    private Integer idEspecialidad;
    @Column(name="nombre_especialidad", nullable=0x00000000, length=0x00000064)
    private String nombreEspecialidad;
    @Column(name="estado_especialidad", nullable=0x00000000, length=0x00000001)
    private String estadoEspecialidad;
    @BatchSize(size=0x00000014)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="especialidad", fetch=javax.persistence.FetchType.LAZY)
    private Set subEspecialidades;
    @BatchSize(size=0x00000064)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="especialidad", fetch=javax.persistence.FetchType.LAZY)
    private Set preguntas;
    @BatchSize(size=0x00000014)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="especialidad", orphanRemoval=0x00000001, fetch=javax.persistence.FetchType.LAZY)
    private Set trabajos;
}
