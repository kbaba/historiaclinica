// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:34:24 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Alternativa.java

package paquete.entities;

import java.io.Serializable;

// Referenced classes of package paquete.entities:
//            Pregunta

@Entity
@Table(name="alternativa")
@Table(appliesTo="alternativa", indexes={@Index(name="pregunta_index", columnNames={"pregunta_id"})})
public class Alternativa
    implements Serializable
{

    public Alternativa()
    {
    }

    public Integer getIdAlternativa()
    {
        return idAlternativa;
    }

    public void setIdAlternativa(Integer idAlternativa)
    {
        this.idAlternativa = idAlternativa;
    }

    public String getDescripcionAlternativa()
    {
        return descripcionAlternativa;
    }

    public void setDescripcionAlternativa(String descripcionAlternativa)
    {
        this.descripcionAlternativa = descripcionAlternativa;
    }

    public String getEstadoAlternativa()
    {
        return estadoAlternativa;
    }

    public void setEstadoAlternativa(String estadoAlternativa)
    {
        this.estadoAlternativa = estadoAlternativa;
    }

    public Pregunta getPregunta()
    {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta)
    {
        this.pregunta = pregunta;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_alternativa", unique=0x00000001, nullable=0x00000000)
    private Integer idAlternativa;
    @Column(name="descripcion_alternativa", nullable=0x00000000, length=0x0000002D)
    private String descripcionAlternativa;
    @ManyToOne(fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="pregunta_id", nullable=0x00000001)
    @ForeignKey(name="fk_pregunta")
    private Pregunta pregunta;
    @Column(name="estado_alternativa", nullable=0x00000000, length=0x00000001)
    private String estadoAlternativa;
}
