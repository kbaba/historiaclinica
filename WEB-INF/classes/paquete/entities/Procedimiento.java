// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:35:32 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Procedimiento.java

package paquete.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

// Referenced classes of package paquete.entities:
//            Pregunta, SubEspecialidad

@Entity
@Table(name="procedimiento")
@Table(appliesTo="procedimiento", indexes={@Index(name="sub_especialidad_index", columnNames={"sub_especialidad_id"})})
public class Procedimiento
    implements Serializable
{

    public Procedimiento()
    {
        preguntas = new HashSet();
    }

    public Integer getIdProcedimiento()
    {
        return idProcedimiento;
    }

    public void setIdProcedimiento(Integer idProcedimiento)
    {
        this.idProcedimiento = idProcedimiento;
    }

    public String getNombreProcedimiento()
    {
        return nombreProcedimiento;
    }

    public void setNombreProcedimiento(String nombreProcedimiento)
    {
        this.nombreProcedimiento = nombreProcedimiento;
    }

    public SubEspecialidad getSubEspecialidad()
    {
        return subEspecialidad;
    }

    public void setSubEspecialidad(SubEspecialidad subEspecialidad)
    {
        this.subEspecialidad = subEspecialidad;
    }

    public String getEstadoProcedimiento()
    {
        return estadoProcedimiento;
    }

    public void setEstadoProcedimiento(String estadoProcedimiento)
    {
        this.estadoProcedimiento = estadoProcedimiento;
    }

    public Set getPreguntas()
    {
        return preguntas;
    }

    public void setPreguntas(Set preguntas)
    {
        this.preguntas = preguntas;
    }

    public void agregarPregunta(Pregunta pregunta)
    {
        pregunta.setProcedimiento(this);
        preguntas.add(pregunta);
    }

    public boolean equals(Object obj)
    {
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Procedimiento other = (Procedimiento)obj;
        if(idProcedimiento != other.idProcedimiento && (idProcedimiento == null || !idProcedimiento.equals(other.idProcedimiento)))
            return false;
        if(nombreProcedimiento != null ? !nombreProcedimiento.equals(other.nombreProcedimiento) : other.nombreProcedimiento != null)
            return false;
        return estadoProcedimiento != null ? estadoProcedimiento.equals(other.estadoProcedimiento) : other.estadoProcedimiento == null;
    }

    public int hashCode()
    {
        int hash = 7;
        hash = 67 * hash + (idProcedimiento == null ? 0 : idProcedimiento.hashCode());
        hash = 67 * hash + (nombreProcedimiento == null ? 0 : nombreProcedimiento.hashCode());
        hash = 67 * hash + (estadoProcedimiento == null ? 0 : estadoProcedimiento.hashCode());
        return hash;
    }

    @Id
    @GeneratedValue(strategy=javax.persistence.GenerationType.IDENTITY)
    @Column(name="id_procedimiento", unique=0x00000001, nullable=0x00000000)
    private Integer idProcedimiento;
    @Column(name="nombre_procedimiento", nullable=0x00000000, length=0x00000064)
    private String nombreProcedimiento;
    @ManyToOne(cascade={javax.persistence.CascadeType.ALL}, fetch=javax.persistence.FetchType.LAZY)
    @JoinColumn(name="sub_especialidad_id", nullable=0x00000000)
    @ForeignKey(name="fk_sub_especialidad")
    private SubEspecialidad subEspecialidad;
    @Column(name="estado_procedimiento", nullable=0x00000000, length=0x00000001)
    private String estadoProcedimiento;
    @BatchSize(size=0x00000064)
    @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="procedimiento", orphanRemoval=0x00000001, fetch=javax.persistence.FetchType.LAZY)
    private Set preguntas;
}
