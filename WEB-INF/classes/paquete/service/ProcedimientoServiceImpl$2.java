// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:03 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoServiceImpl.java

package paquete.service;

import infraestructura.dto.ProcedimientoDTO;
import java.util.Comparator;

// Referenced classes of package paquete.service:
//            ProcedimientoServiceImpl

class ProcedimientoServiceImpl$2
    implements Comparator
{

    public int compare(ProcedimientoDTO o1, ProcedimientoDTO o2)
    {
        return o1.getNombreProcedimiento().compareTo(o2.getNombreProcedimiento());
    }

    public volatile int compare(Object x0, Object x1)
    {
        return compare((ProcedimientoDTO)x0, (ProcedimientoDTO)x1);
    }

    final ProcedimientoServiceImpl this$0;

    ProcedimientoServiceImpl$2()
    {
        this$0 = ProcedimientoServiceImpl.this;
        super();
    }
}
