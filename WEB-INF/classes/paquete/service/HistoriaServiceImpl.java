// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:44:59 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriaServiceImpl.java

package paquete.service;

import infraestructura.dto.HistoriaDTO;
import infraestructura.mapper.DTOMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Historia;
import paquete.repositorios.HistoriaRepository;
import paquete.repositorios.PersonaRepository;

// Referenced classes of package paquete.service:
//            HistoriaService

@Service
@Component
public class HistoriaServiceImpl
    implements HistoriaService
{

    public HistoriaServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE PERSONA *****************\n");
    }

    public HistoriaDTO find(Integer idHistoria)
    {
        Historia dao = (Historia)historiaRepository.find(idHistoria);
        HistoriaDTO dto = (HistoriaDTO)mapperService.map(dao, infraestructura/dto/HistoriaDTO);
        return dto;
    }

    @Transactional
    public void guardarHistoria(HistoriaDTO historiaDTO)
    {
        Historia historia = (Historia)mapperService.map(historiaDTO, paquete/entities/Historia);
        if(historia.getIdHistoria() == null)
            historiaRepository.persist(new Historia[] {
                historia
            });
        else
            historiaRepository.merge(historia);
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/HistoriaServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private HistoriaRepository historiaRepository;

}
