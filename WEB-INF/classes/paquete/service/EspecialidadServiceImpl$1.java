// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:22 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EspecialidadServiceImpl.java

package paquete.service;

import infraestructura.dto.EspecialidadDTO;
import java.util.Comparator;

// Referenced classes of package paquete.service:
//            EspecialidadServiceImpl

class EspecialidadServiceImpl$1
    implements Comparator
{

    public int compare(EspecialidadDTO o1, EspecialidadDTO o2)
    {
        return o1.getNombreEspecialidad().compareTo(o2.getNombreEspecialidad());
    }

    public volatile int compare(Object x0, Object x1)
    {
        return compare((EspecialidadDTO)x0, (EspecialidadDTO)x1);
    }

    final EspecialidadServiceImpl this$0;

    EspecialidadServiceImpl$1()
    {
        this$0 = EspecialidadServiceImpl.this;
        super();
    }
}
