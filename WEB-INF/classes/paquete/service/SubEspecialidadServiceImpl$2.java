// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:39 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadServiceImpl.java

package paquete.service;

import infraestructura.dto.SubEspecialidadDTO;
import java.util.Comparator;

// Referenced classes of package paquete.service:
//            SubEspecialidadServiceImpl

class SubEspecialidadServiceImpl$2
    implements Comparator
{

    public int compare(SubEspecialidadDTO o1, SubEspecialidadDTO o2)
    {
        return o1.getNombreSubEspecialidad().compareTo(o2.getNombreSubEspecialidad());
    }

    public volatile int compare(Object x0, Object x1)
    {
        return compare((SubEspecialidadDTO)x0, (SubEspecialidadDTO)x1);
    }

    final SubEspecialidadServiceImpl this$0;

    SubEspecialidadServiceImpl$2()
    {
        this$0 = SubEspecialidadServiceImpl.this;
        super();
    }
}
