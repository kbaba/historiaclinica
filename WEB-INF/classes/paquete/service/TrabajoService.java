// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:48 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TrabajoService.java

package paquete.service;

import infraestructura.dto.TrabajoDTO;
import java.util.List;

public interface TrabajoService
{

    public abstract TrabajoDTO find(Integer integer);

    public abstract List getTrabajoEmpleado(Integer integer);

    public abstract List getEspecialidadEmpleado(Integer integer);

    public abstract List getIntegerEspecialidadEmpleado(Integer integer);

    public abstract void guardarTrabajo(TrabajoDTO trabajodto);

    public abstract boolean esTrabajoBorrable(Integer integer, Integer integer1);

    public abstract void eliminaActualizaCreaTrabajos(Integer integer, List list, List list1);
}
