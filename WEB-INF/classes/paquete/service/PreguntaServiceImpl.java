// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:45:38 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntaServiceImpl.java

package paquete.service;

import infraestructura.dto.AlternativaDTO;
import infraestructura.dto.PreguntaDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import paquete.entities.Alternativa;
import paquete.entities.Pregunta;
import paquete.repositorios.EspecialidadRepository;
import paquete.repositorios.PreguntaRepository;

// Referenced classes of package paquete.service:
//            PreguntaService, EspecialidadService, SubEspecialidadService

@Service
@Component
public class PreguntaServiceImpl
    implements PreguntaService
{

    public PreguntaServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    @Transactional
    public void guardarPregunta(PreguntaDTO preguntaDTO)
    {
        Pregunta pregunta = (Pregunta)mapperService.map(preguntaDTO, paquete/entities/Pregunta);
        Set alternativasDTO = preguntaDTO.getConjuntoAlternativas();
        AlternativaDTO alDTO;
        for(Iterator i$ = alternativasDTO.iterator(); i$.hasNext(); pregunta.agregarAlternativa((Alternativa)mapperService.map(alDTO, paquete/entities/Alternativa)))
            alDTO = (AlternativaDTO)i$.next();

        if(pregunta.getIdPregunta() == null)
            preguntaRepository.persist(new Pregunta[] {
                pregunta
            });
        else
            preguntaRepository.merge(pregunta);
    }

    public List findByFilter(PreguntaDTO preguntaDTO, int indiceInicial, int indiceFinal, boolean limitarResultados)
    {
        Pregunta pregunta = (Pregunta)mapperService.map(preguntaDTO, paquete/entities/Pregunta);
        List resultados = preguntaRepository.findByFilter(pregunta, indiceInicial, indiceFinal, limitarResultados);
        List result = new ArrayList();
        PreguntaDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            Pregunta entity = (Pregunta)i$.next();
            dto = (PreguntaDTO)mapperService.map(entity, infraestructura/dto/PreguntaDTO);
            if(entity.getAlternativas().isEmpty())
                continue;
            Alternativa alter;
            for(Iterator i$ = entity.getAlternativas().iterator(); i$.hasNext(); dto.addAlternativa((AlternativaDTO)mapperService.map(alter, infraestructura/dto/AlternativaDTO)))
                alter = (Alternativa)i$.next();

        }

        return result;
    }

    public PreguntaDTO findById(Integer preguntaId)
    {
        Pregunta pregunta = (Pregunta)preguntaRepository.find(preguntaId);
        PreguntaDTO dto = (PreguntaDTO)mapperService.map(pregunta, infraestructura/dto/PreguntaDTO);
        return dto;
    }

    public void eliminarPregunta(PreguntaDTO pregunta)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int cuentaRegistrosFilterLazy(PreguntaDTO filtro)
    {
        Pregunta pregunta = (Pregunta)mapperService.map(filtro, paquete/entities/Pregunta);
        return preguntaRepository.cuentaRegistrosFilterLazy(pregunta);
    }

    public int cuentaRegistrosFilterLazy(PreguntaDTO filtro, AlternativaDTO alternativaDTO)
    {
        Pregunta pregunta = (Pregunta)mapperService.map(filtro, paquete/entities/Pregunta);
        Alternativa alternativa = (Alternativa)mapperService.map(alternativaDTO, paquete/entities/Alternativa);
        return preguntaRepository.cuentaRegistrosFilterLazy(pregunta, alternativa);
    }

    public List findByFilter(PreguntaDTO preguntaDTO, AlternativaDTO alternativaDTO, int indiceInicial, int indiceFinal)
    {
        Pregunta pregunta = (Pregunta)mapperService.map(preguntaDTO, paquete/entities/Pregunta);
        Alternativa alternativa = (Alternativa)mapperService.map(alternativaDTO, paquete/entities/Alternativa);
        List resultados = preguntaRepository.findByFilter(pregunta, alternativa, indiceInicial, indiceFinal);
        List result = new ArrayList();
        PreguntaDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            Pregunta entity = (Pregunta)i$.next();
            dto = (PreguntaDTO)mapperService.map(entity, infraestructura/dto/PreguntaDTO);
            if(entity.getAlternativas().isEmpty())
                continue;
            Alternativa alter;
            for(Iterator i$ = entity.getAlternativas().iterator(); i$.hasNext(); dto.addAlternativa((AlternativaDTO)mapperService.map(alter, infraestructura/dto/AlternativaDTO)))
                alter = (Alternativa)i$.next();

        }

        return result;
    }

    public boolean esPreguntaRepetida(String descripcion, int idPregunta)
    {
        return preguntaRepository.esPreguntaRepetida(descripcion, idPregunta);
    }

    public boolean esPreguntaRepetida(String descripcion)
    {
        return preguntaRepository.esPreguntaRepetida(descripcion);
    }

    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private PreguntaRepository preguntaRepository;
    @Autowired
    private EspecialidadRepository especialidadRepository;
    @Autowired
    private EspecialidadService especialidadService;
    @Autowired
    private SubEspecialidadService subEspecialidadService;
}
