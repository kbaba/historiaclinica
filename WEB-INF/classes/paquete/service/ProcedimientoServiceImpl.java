// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:08 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoServiceImpl.java

package paquete.service;

import infraestructura.dto.ProcedimientoDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Procedimiento;
import paquete.repositorios.ProcedimientoRepository;

// Referenced classes of package paquete.service:
//            ProcedimientoService

@Service
@Component
public class ProcedimientoServiceImpl
    implements ProcedimientoService
{

    public ProcedimientoServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE ENFERMEDAD *****************\n");
    }

    @Transactional
    public void guardarProcedimiento(ProcedimientoDTO procedimientoDTO)
    {
        Procedimiento procedimiento = (Procedimiento)mapperService.map(procedimientoDTO, paquete/entities/Procedimiento);
        if(procedimiento.getIdProcedimiento() == null)
            procedimientoRepository.persist(new Procedimiento[] {
                procedimiento
            });
        else
            procedimientoRepository.merge(procedimiento);
    }

    public List findByFilter(ProcedimientoDTO filtro)
    {
        Procedimiento procedimiento = (Procedimiento)mapperService.map(filtro, paquete/entities/Procedimiento);
        List resultados = procedimientoRepository.findByFilter(procedimiento);
        Collections.sort(resultados, new Comparator() {

            public int compare(Procedimiento proc1, Procedimiento proc2)
            {
                return proc1.getNombreProcedimiento().compareTo(proc2.getNombreProcedimiento());
            }

            public volatile int compare(Object x0, Object x1)
            {
                return compare((Procedimiento)x0, (Procedimiento)x1);
            }

            final ProcedimientoServiceImpl this$0;

            
            {
                this$0 = ProcedimientoServiceImpl.this;
                super();
            }
        }
);
        List result = new ArrayList();
        ProcedimientoDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            Procedimiento entity = (Procedimiento)i$.next();
            dto = (ProcedimientoDTO)mapperService.map(entity, infraestructura/dto/ProcedimientoDTO);
        }

        return result;
    }

    public boolean existeProcedimientoRepetido(String nombre, String idEspecialidad)
    {
        return procedimientoRepository.existeProcedimientoRepetido(nombre, idEspecialidad);
    }

    public boolean existeProcedimientoRepetido(String nombre)
    {
        return procedimientoRepository.existeProcedimientoRepetido(nombre);
    }

    public ProcedimientoDTO findById(int id)
    {
        Procedimiento dao = (Procedimiento)procedimientoRepository.find(new Integer(id));
        ProcedimientoDTO dto = (ProcedimientoDTO)mapperService.map(dao, infraestructura/dto/ProcedimientoDTO);
        return dto;
    }

    public List findBySubEspecialidad(Integer id)
    {
        List resultados = procedimientoRepository.findBySubEspecialidad(id);
        List result = new ArrayList();
        ProcedimientoDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            Procedimiento entity = (Procedimiento)i$.next();
            dto = (ProcedimientoDTO)mapperService.map(entity, infraestructura/dto/ProcedimientoDTO);
        }

        Collections.sort(result, new Comparator() {

            public int compare(ProcedimientoDTO o1, ProcedimientoDTO o2)
            {
                return o1.getNombreProcedimiento().compareTo(o2.getNombreProcedimiento());
            }

            public volatile int compare(Object x0, Object x1)
            {
                return compare((ProcedimientoDTO)x0, (ProcedimientoDTO)x1);
            }

            final ProcedimientoServiceImpl this$0;

            
            {
                this$0 = ProcedimientoServiceImpl.this;
                super();
            }
        }
);
        return result;
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/ProcedimientoServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private ProcedimientoRepository procedimientoRepository;

}
