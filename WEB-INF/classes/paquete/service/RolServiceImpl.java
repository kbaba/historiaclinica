// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:19 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   RolServiceImpl.java

package paquete.service;

import infraestructura.dto.RolDTO;
import infraestructura.mapper.DTOMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Rol;
import paquete.repositorios.RolRepository;

// Referenced classes of package paquete.service:
//            RolService

@Service
@Component
public class RolServiceImpl
    implements RolService
{

    public RolServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    public RolDTO findById(Integer idRol)
    {
        RolDTO rol = new RolDTO();
        Rol rolDAO = (Rol)rolRepository.find(idRol);
        rol = (RolDTO)mapperService.map(rolDAO, infraestructura/dto/RolDTO);
        return rol;
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/RolServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private RolRepository rolRepository;

}
