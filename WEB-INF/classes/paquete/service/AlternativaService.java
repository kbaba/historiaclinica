// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:41:50 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AlternativaService.java

package paquete.service;

import infraestructura.dto.AlternativaDTO;
import java.util.List;

public interface AlternativaService
{

    public abstract List getAlternativasXPreguntaId(int i);

    public abstract AlternativaDTO find(int i);

    public abstract void borraRegistroAlternativa(int i);
}
