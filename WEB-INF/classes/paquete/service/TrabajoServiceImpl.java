// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:47:05 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TrabajoServiceImpl.java

package paquete.service;

import infraestructura.dto.EspecialidadDTO;
import infraestructura.dto.TrabajoDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Trabajo;
import paquete.repositorios.PersonaRepository;
import paquete.repositorios.TrabajoRepository;

// Referenced classes of package paquete.service:
//            TrabajoService

@Service
@Component
public class TrabajoServiceImpl
    implements TrabajoService
{

    public TrabajoServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE PERSONA *****************\n");
    }

    public TrabajoDTO find(Integer idTrabajo)
    {
        Trabajo dao = (Trabajo)trabajoRepository.find(idTrabajo);
        TrabajoDTO dto = (TrabajoDTO)mapperService.map(dao, infraestructura/dto/TrabajoDTO);
        return dto;
    }

    @Transactional
    public void guardarTrabajo(TrabajoDTO trabajoDTO)
    {
        Trabajo trabajo = (Trabajo)mapperService.map(trabajoDTO, paquete/entities/Trabajo);
        if(trabajo.getIdTrabajo() == null)
            trabajoRepository.persist(new Trabajo[] {
                trabajo
            });
        else
            trabajoRepository.merge(trabajo);
    }

    public List getTrabajoEmpleado(Integer idEmpleado)
    {
        List trabajos = new ArrayList();
        List daos = trabajoRepository.getTrabajoEmpleado(idEmpleado);
        Trabajo dao;
        for(Iterator i$ = daos.iterator(); i$.hasNext(); trabajos.add(mapperService.map(dao, infraestructura/dto/TrabajoDTO)))
            dao = (Trabajo)i$.next();

        return trabajos;
    }

    public List getEspecialidadEmpleado(Integer idEmpleado)
    {
        List listaEspecialidades = new ArrayList();
        List trabajos = getTrabajoEmpleado(idEmpleado);
        TrabajoDTO tra;
        for(Iterator i$ = trabajos.iterator(); i$.hasNext(); listaEspecialidades.add(tra.getEspecialidad()))
            tra = (TrabajoDTO)i$.next();

        return listaEspecialidades;
    }

    public List getIntegerEspecialidadEmpleado(Integer idEmpleado)
    {
        List listaInteger = new ArrayList();
        List trabajos = getTrabajoEmpleado(idEmpleado);
        TrabajoDTO tra;
        for(Iterator i$ = trabajos.iterator(); i$.hasNext(); listaInteger.add(tra.getEspecialidad().getIdEspecialidad()))
            tra = (TrabajoDTO)i$.next();

        return listaInteger;
    }

    public boolean esTrabajoBorrable(Integer idEspecialidad, Integer idEmpleado)
    {
        return trabajoRepository.esTrabajoBorrable(idEspecialidad, idEmpleado);
    }

    @Transactional
    public void eliminaActualizaCreaTrabajos(Integer idEmpleado, List trabajosList, List idEspecialidadesList)
    {
        String query = "";
        String qElimina = trabajoRepository.getQueryEliminar(idEmpleado, idEspecialidadesList);
        String qActualiza = trabajoRepository.getQueryActualizar(idEmpleado, trabajosList);
        String qCrea = trabajoRepository.getQueryCrear(idEmpleado, trabajosList);
        query = (new StringBuilder()).append(query).append(qElimina).append(qActualiza).append(qCrea).toString();
        List queriesList = separaQueries(query, ";");
        trabajoRepository.ejecutarInstruccion(queriesList);
    }

    public List separaQueries(String compuesto, String separador)
    {
        List lista = new ArrayList();
        for(StringTokenizer st = new StringTokenizer(compuesto, separador); st.hasMoreTokens(); lista.add(st.nextToken()));
        return lista;
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/TrabajoServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private TrabajoRepository trabajoRepository;

}
