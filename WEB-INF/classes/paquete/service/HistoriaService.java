// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:53 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriaService.java

package paquete.service;

import infraestructura.dto.HistoriaDTO;

public interface HistoriaService
{

    public abstract HistoriaDTO find(Integer integer);

    public abstract void guardarHistoria(HistoriaDTO historiadto);
}
