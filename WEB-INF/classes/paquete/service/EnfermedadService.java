// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:12 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadService.java

package paquete.service;

import infraestructura.dto.EnfermedadDTO;
import java.util.List;
import paquete.entities.Enfermedad;

public interface EnfermedadService
{

    public abstract void guardarEnfermedad(EnfermedadDTO enfermedaddto);

    public abstract List findByFilter(EnfermedadDTO enfermedaddto, Enfermedad enfermedad, int i, int j);

    public abstract int cuentaRegistros(EnfermedadDTO enfermedaddto, Enfermedad enfermedad, int i, int j);
}
