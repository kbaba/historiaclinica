// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:04 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpleadoService.java

package paquete.service;

import infraestructura.dto.*;
import java.util.List;

public interface EmpleadoService
{

    public abstract void eliminarTrabajadorEspecialidad(EmpleadoDTO empleadodto, EspecialidadDTO especialidaddto);

    public abstract void eliminarTrabajador(PersonaDTO personadto);

    public abstract void eliminarTrabajadorEspecialidad(int i, int j);

    public abstract List traeTrabajosDeEmpleado(int i);
}
