// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:19 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EspecialidadService.java

package paquete.service;

import infraestructura.dto.EspecialidadDTO;
import java.util.List;
import paquete.entities.Especialidad;

public interface EspecialidadService
{

    public abstract void guardarEspecialidad(EspecialidadDTO especialidaddto);

    public abstract boolean existeEspecialidadRepetida(String s, String s1);

    public abstract boolean existeEspecialidadRepetida(String s);

    public abstract List buscarEspecialidad(Especialidad especialidad);

    public abstract List getAll();

    public abstract EspecialidadDTO find(Integer integer);
}
