// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:31 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadServiceImpl.java

package paquete.service;

import java.util.Comparator;
import paquete.entities.SubEspecialidad;

// Referenced classes of package paquete.service:
//            SubEspecialidadServiceImpl

class SubEspecialidadServiceImpl$1
    implements Comparator
{

    public int compare(SubEspecialidad subEspecialidad1, SubEspecialidad subEspecialidad2)
    {
        return subEspecialidad1.getNombreSubEspecialidad().compareTo(subEspecialidad2.getNombreSubEspecialidad());
    }

    public volatile int compare(Object x0, Object x1)
    {
        return compare((SubEspecialidad)x0, (SubEspecialidad)x1);
    }

    final SubEspecialidadServiceImpl this$0;

    SubEspecialidadServiceImpl$1()
    {
        this$0 = SubEspecialidadServiceImpl.this;
        super();
    }
}
