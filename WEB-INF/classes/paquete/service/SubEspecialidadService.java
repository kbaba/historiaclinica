// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:25 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadService.java

package paquete.service;

import infraestructura.dto.SubEspecialidadDTO;
import java.util.List;

public interface SubEspecialidadService
{

    public abstract void guardarSubEspecialidad(SubEspecialidadDTO subespecialidaddto);

    public abstract List buscarSubEspecialidad(SubEspecialidadDTO subespecialidaddto);

    public abstract SubEspecialidadDTO findById(Integer integer);

    public abstract boolean existeSubEspecialidadRepetida(String s, String s1);

    public abstract boolean existeSubEspecialidadRepetida(String s);

    public abstract List findByEspecialidadId(Integer integer);
}
