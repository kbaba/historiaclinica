// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:45:24 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PersonaServiceImpl.java

package paquete.service;

import infraestructura.dto.PersonaDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Persona;
import paquete.repositorios.*;

// Referenced classes of package paquete.service:
//            PersonaService

@Service
@Component
public class PersonaServiceImpl
    implements PersonaService
{

    public PersonaServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE PERSONA *****************\n");
    }

    @Transactional
    public void guardarPersona(PersonaDTO personaDTO)
    {
        Persona persona = (Persona)mapperService.map(personaDTO, paquete/entities/Persona);
        if(persona.getIdPersona() == null)
            personaRepository.persist(new Persona[] {
                persona
            });
        else
            personaRepository.merge(persona);
    }

    @Transactional
    public PersonaDTO guardaDevuelvePersona(PersonaDTO personaDTO)
    {
        Persona persona = (Persona)mapperService.map(personaDTO, paquete/entities/Persona);
        if(persona.getIdPersona() == null)
            personaRepository.persist(new Persona[] {
                persona
            });
        else
            personaRepository.merge(persona);
        return (PersonaDTO)mapperService.map(persona, infraestructura/dto/PersonaDTO);
    }

    @Transactional
    public List findByFilter(PersonaDTO filtro)
    {
        Persona persona = (Persona)mapperService.map(filtro, paquete/entities/Persona);
        List resultados = personaRepository.findByFilter(persona);
        List result = new ArrayList();
        PersonaDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            Persona entity = (Persona)i$.next();
            dto = (PersonaDTO)mapperService.map(entity, infraestructura/dto/PersonaDTO);
        }

        return result;
    }

    public int cuentaRegistrosFilterLazy(PersonaDTO filtro)
    {
        Persona persona = (Persona)mapperService.map(filtro, paquete/entities/Persona);
        return personaRepository.cuentaRegistrosFilterLazy(persona);
    }

    @Transactional
    public List findByFilterLazy(PersonaDTO filtro, int indiceInicial, int indiceFinal)
    {
        Persona persona = (Persona)mapperService.map(filtro, paquete/entities/Persona);
        List resultados = personaRepository.findByFilterLazy(persona, indiceInicial, indiceFinal);
        List result = new ArrayList();
        for(Iterator i$ = resultados.iterator(); i$.hasNext();)
        {
            Persona entity = (Persona)i$.next();
            try
            {
                PersonaDTO dto = (PersonaDTO)mapperService.map(entity, infraestructura/dto/PersonaDTO);
                result.add(dto);
                LOG.info((new StringBuilder()).append("\n\n SE MAPEO CON \311XITO EL ").append(entity.getNombrePersona()).append(" ").append(entity.getApellidoPaternoPersona()).toString());
            }
            catch(Exception ex)
            {
                LOG.error(ex.getMessage());
                LOG.error(ex.getLocalizedMessage());
            }
        }

        return result;
    }

    @Transactional
    public List findByFilterLazyEmpleado(PersonaDTO filtro, int indiceInicial, int indiceFinal)
    {
        Persona persona = (Persona)mapperService.map(filtro, paquete/entities/Persona);
        List resultados = personaRepository.findByFilterLazyEmpleado(persona, indiceInicial, indiceFinal);
        List result = new ArrayList();
        PersonaDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            Persona entity = (Persona)i$.next();
            dto = (PersonaDTO)mapperService.map(entity, infraestructura/dto/PersonaDTO);
        }

        return result;
    }

    public boolean esDocumentoRepetido(String documento, int idPersona, String tipoDocumento)
    {
        return personaRepository.esDocumentoRepetido(documento, idPersona, tipoDocumento);
    }

    public boolean esDocumentoRepetido(String documento, String tipoDocumento)
    {
        return personaRepository.esDocumentoRepetido(documento, tipoDocumento);
    }

    public boolean esUsuarioRepetido(String usuario, int idPersona)
    {
        return personaRepository.esUsuarioRepetido(usuario, idPersona);
    }

    public boolean esUsuarioRepetido(String usuario)
    {
        return personaRepository.esUsuarioRepetido(usuario);
    }

    public void eliminarPersona(PersonaDTO persona)
    {
        if(persona.getEmpleado() != null)
            trabajadorRepository.eliminarTrabajador(persona.getEmpleado());
        if(persona.getPaciente() != null)
            pacienteRepository.eliminarPaciente(persona.getPaciente());
    }

    public String determinaEstatutoSocial(PersonaDTO filtro)
    {
        int i = random(0, 2);
        return social[i];
    }

    public static int random(int min, int max)
    {
        int a = min + (int)(Math.random() * (double)((max - min) + 1));
        return a;
    }

    public int cuentaRegistrosFilterLazyEmpleado(PersonaDTO filtro)
    {
        Persona persona = (Persona)mapperService.map(filtro, paquete/entities/Persona);
        return personaRepository.cuentaRegistrosFilterLazyEmpleado(persona);
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/PersonaServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private PacienteRepository pacienteRepository;
    @Autowired
    private EmpleadoRepository trabajadorRepository;
    private String social[] = {
        "A", "B", "C"
    };

}
