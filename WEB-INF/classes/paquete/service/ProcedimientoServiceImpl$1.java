// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:45:53 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoServiceImpl.java

package paquete.service;

import java.util.Comparator;
import paquete.entities.Procedimiento;

// Referenced classes of package paquete.service:
//            ProcedimientoServiceImpl

class ProcedimientoServiceImpl$1
    implements Comparator
{

    public int compare(Procedimiento proc1, Procedimiento proc2)
    {
        return proc1.getNombreProcedimiento().compareTo(proc2.getNombreProcedimiento());
    }

    public volatile int compare(Object x0, Object x1)
    {
        return compare((Procedimiento)x0, (Procedimiento)x1);
    }

    final ProcedimientoServiceImpl this$0;

    ProcedimientoServiceImpl$1()
    {
        this$0 = ProcedimientoServiceImpl.this;
        super();
    }
}
