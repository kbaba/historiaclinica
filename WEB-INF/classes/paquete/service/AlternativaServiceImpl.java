// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:41:53 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AlternativaServiceImpl.java

package paquete.service;

import infraestructura.dto.AlternativaDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Alternativa;
import paquete.repositorios.AlternativaRepository;

// Referenced classes of package paquete.service:
//            AlternativaService

@Service
@Component
public class AlternativaServiceImpl
    implements AlternativaService
{

    public AlternativaServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    public List getAlternativasXPreguntaId(int preguntaId)
    {
        List resultados = alternativaRepository.getAlternativasXPreguntaId(preguntaId);
        List result = new ArrayList();
        AlternativaDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            Alternativa entity = (Alternativa)i$.next();
            dto = (AlternativaDTO)mapperService.map(entity, infraestructura/dto/AlternativaDTO);
        }

        return result;
    }

    public void borraRegistroAlternativa(int alternativaId)
    {
        alternativaRepository.borraRegistroAlternativa(alternativaId);
    }

    public AlternativaDTO find(int alternativaId)
    {
        Alternativa dao = (Alternativa)alternativaRepository.find(Integer.valueOf(alternativaId));
        AlternativaDTO dto = (AlternativaDTO)mapperService.map(dao, infraestructura/dto/AlternativaDTO);
        return dto;
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/AlternativaServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private AlternativaRepository alternativaRepository;

}
