// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:45:18 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PersonaService.java

package paquete.service;

import infraestructura.dto.PersonaDTO;
import java.util.List;

public interface PersonaService
{

    public abstract void guardarPersona(PersonaDTO personadto);

    public abstract boolean esDocumentoRepetido(String s, int i, String s1);

    public abstract boolean esDocumentoRepetido(String s, String s1);

    public abstract boolean esUsuarioRepetido(String s, int i);

    public abstract boolean esUsuarioRepetido(String s);

    public abstract void eliminarPersona(PersonaDTO personadto);

    public abstract List findByFilter(PersonaDTO personadto);

    public abstract List findByFilterLazy(PersonaDTO personadto, int i, int j);

    public abstract List findByFilterLazyEmpleado(PersonaDTO personadto, int i, int j);

    public abstract int cuentaRegistrosFilterLazy(PersonaDTO personadto);

    public abstract int cuentaRegistrosFilterLazyEmpleado(PersonaDTO personadto);

    public abstract String determinaEstatutoSocial(PersonaDTO personadto);

    public abstract PersonaDTO guardaDevuelvePersona(PersonaDTO personadto);
}
