// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:45:32 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntaService.java

package paquete.service;

import infraestructura.dto.AlternativaDTO;
import infraestructura.dto.PreguntaDTO;
import java.util.List;

public interface PreguntaService
{

    public abstract void guardarPregunta(PreguntaDTO preguntadto);

    public abstract List findByFilter(PreguntaDTO preguntadto, int i, int j, boolean flag);

    public abstract PreguntaDTO findById(Integer integer);

    public abstract List findByFilter(PreguntaDTO preguntadto, AlternativaDTO alternativadto, int i, int j);

    public abstract void eliminarPregunta(PreguntaDTO preguntadto);

    public abstract int cuentaRegistrosFilterLazy(PreguntaDTO preguntadto);

    public abstract int cuentaRegistrosFilterLazy(PreguntaDTO preguntadto, AlternativaDTO alternativadto);

    public abstract boolean esPreguntaRepetida(String s, int i);

    public abstract boolean esPreguntaRepetida(String s);
}
