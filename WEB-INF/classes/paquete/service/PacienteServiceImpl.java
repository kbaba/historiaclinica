// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:45:12 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PacienteServiceImpl.java

package paquete.service;

import infraestructura.dto.PersonaDTO;
import infraestructura.mapper.DTOMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.repositorios.PacienteRepository;
import paquete.repositorios.PersonaRepository;

// Referenced classes of package paquete.service:
//            PacienteService

@Service
@Component
public class PacienteServiceImpl
    implements PacienteService
{

    public PacienteServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE PERSONA *****************\n");
    }

    public void eliminarPaciente(PersonaDTO persona)
    {
        if(persona.getPaciente() == null)
        {
            return;
        } else
        {
            pacienteRepository.eliminarPaciente(persona.getPaciente());
            return;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/PacienteServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private PacienteRepository pacienteRepository;

}
