// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:46:43 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadServiceImpl.java

package paquete.service;

import infraestructura.dto.ProcedimientoDTO;
import infraestructura.dto.SubEspecialidadDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import paquete.entities.Procedimiento;
import paquete.entities.SubEspecialidad;
import paquete.repositorios.SubEspecialidadRepository;

// Referenced classes of package paquete.service:
//            SubEspecialidadService

@Service
@Component
public class SubEspecialidadServiceImpl
    implements SubEspecialidadService
{

    public SubEspecialidadServiceImpl()
    {
    }

    @Transactional
    public void guardarSubEspecialidad(SubEspecialidadDTO subEspecialidadDTO)
    {
        SubEspecialidad subEspecialidad = (SubEspecialidad)mapperService.map(subEspecialidadDTO, paquete/entities/SubEspecialidad);
        ProcedimientoDTO dto;
        for(Iterator i$ = subEspecialidadDTO.getConjuntoProcedimientos().iterator(); i$.hasNext(); subEspecialidad.agregarProcedimientos((Procedimiento)mapperService.map(dto, paquete/entities/Procedimiento)))
            dto = (ProcedimientoDTO)i$.next();

        subEspecialidadRepository.persistirSubEspecialidad(subEspecialidad);
    }

    public List buscarSubEspecialidad(SubEspecialidadDTO subEspecialidadFiltro)
    {
        SubEspecialidad subEspecialidad = (SubEspecialidad)mapperService.map(subEspecialidadFiltro, paquete/entities/SubEspecialidad);
        List resultados = subEspecialidadRepository.findByFilter(subEspecialidad);
        Collections.sort(resultados, new Comparator() {

            public int compare(SubEspecialidad subEspecialidad1, SubEspecialidad subEspecialidad2)
            {
                return subEspecialidad1.getNombreSubEspecialidad().compareTo(subEspecialidad2.getNombreSubEspecialidad());
            }

            public volatile int compare(Object x0, Object x1)
            {
                return compare((SubEspecialidad)x0, (SubEspecialidad)x1);
            }

            final SubEspecialidadServiceImpl this$0;

            
            {
                this$0 = SubEspecialidadServiceImpl.this;
                super();
            }
        }
);
        List result = new ArrayList();
        SubEspecialidadDTO subEspecialidadDTO;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(subEspecialidadDTO))
        {
            SubEspecialidad subEspecialidadResult = (SubEspecialidad)i$.next();
            subEspecialidadDTO = (SubEspecialidadDTO)mapperService.map(subEspecialidadResult, infraestructura/dto/SubEspecialidadDTO);
            Integer numeroProcedimnientos = Integer.valueOf(subEspecialidadResult.getProcedimientos().size());
            subEspecialidadDTO.setNumeroProcedimnientos(numeroProcedimnientos.intValue() != 0 ? numeroProcedimnientos.intValue() <= 1 ? (new StringBuilder()).append(numeroProcedimnientos.toString()).append(" procedimiento").toString() : (new StringBuilder()).append(numeroProcedimnientos.toString()).append(" procedimientos").toString() : "No posee procedimientos");
        }

        return result;
    }

    public SubEspecialidadDTO findById(Integer id)
    {
        SubEspecialidad subEspecialidad = (SubEspecialidad)subEspecialidadRepository.find(id);
        SubEspecialidadDTO subEspecialidadDTO = (SubEspecialidadDTO)mapperService.map(subEspecialidad, infraestructura/dto/SubEspecialidadDTO);
        return subEspecialidadDTO;
    }

    public boolean existeSubEspecialidadRepetida(String nombre, String idSubEspecialidad)
    {
        return subEspecialidadRepository.existeSubEspecialidadRepetida(nombre, idSubEspecialidad);
    }

    public boolean existeSubEspecialidadRepetida(String nombre)
    {
        return subEspecialidadRepository.existeSubEspecialidadRepetida(nombre);
    }

    public List findByEspecialidadId(Integer idEspecialidad)
    {
        List resultados = subEspecialidadRepository.findByEspecialidadId(idEspecialidad);
        List result = new ArrayList();
        SubEspecialidadDTO dto;
        for(Iterator i$ = resultados.iterator(); i$.hasNext(); result.add(dto))
        {
            SubEspecialidad entity = (SubEspecialidad)i$.next();
            dto = (SubEspecialidadDTO)mapperService.map(entity, infraestructura/dto/SubEspecialidadDTO);
        }

        Collections.sort(result, new Comparator() {

            public int compare(SubEspecialidadDTO o1, SubEspecialidadDTO o2)
            {
                return o1.getNombreSubEspecialidad().compareTo(o2.getNombreSubEspecialidad());
            }

            public volatile int compare(Object x0, Object x1)
            {
                return compare((SubEspecialidadDTO)x0, (SubEspecialidadDTO)x1);
            }

            final SubEspecialidadServiceImpl this$0;

            
            {
                this$0 = SubEspecialidadServiceImpl.this;
                super();
            }
        }
);
        return result;
    }

    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private SubEspecialidadRepository subEspecialidadRepository;
}
