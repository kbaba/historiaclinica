// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:48 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FormulaServiceImpl.java

package paquete.service;

import infraestructura.mapper.DTOMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.repositorios.FormulaRepository;
import paquete.repositorios.HistoriaRepository;

// Referenced classes of package paquete.service:
//            FormulaService

@Service
@Component
public class FormulaServiceImpl
    implements FormulaService
{

    public FormulaServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    public Object findValorRespuesta(String descripcionPregunta, Integer idPaciente)
    {
        return formulaRepository.findValorRespuesta(descripcionPregunta, idPaciente);
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/FormulaServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private FormulaRepository formulaRepository;
    @Autowired
    private HistoriaRepository historiaRepository;

}
