// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:16 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadServiceImpl.java

package paquete.service;

import infraestructura.dto.EnfermedadDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Enfermedad;
import paquete.repositorios.EnfermedadRepository;

// Referenced classes of package paquete.service:
//            EnfermedadService

@Service
@Component
public class EnfermedadServiceImpl
    implements EnfermedadService
{

    public EnfermedadServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE ENFERMEDAD *****************\n");
    }

    @Transactional
    public void guardarEnfermedad(EnfermedadDTO enfermedadDTO)
    {
        Enfermedad enfermedad = (Enfermedad)mapperService.map(enfermedadDTO, paquete/entities/Enfermedad);
        if(enfermedad.getIdEnfermedad() == null)
            enfermedadRepository.persist(new Enfermedad[] {
                enfermedad
            });
        else
            enfermedadRepository.merge(enfermedad);
    }

    public List findByFilter(EnfermedadDTO filtro, Enfermedad capituloFiltro, int indiceInicial, int indiceFinal)
    {
        Enfermedad enfermedad = (Enfermedad)mapperService.map(filtro, paquete/entities/Enfermedad);
        List resultados = enfermedadRepository.findByFilter(enfermedad, capituloFiltro, indiceInicial, indiceFinal);
        return resultados;
    }

    public int cuentaRegistros(EnfermedadDTO enfermedadFiltro, Enfermedad capituloFiltro, int startingAt, int maxPerPage)
    {
        return enfermedadRepository.cuentaRegistros(enfermedadFiltro, capituloFiltro);
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/EnfermedadServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private EnfermedadRepository enfermedadRepository;

}
