// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:00 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AtencionServiceImpl.java

package paquete.service;

import infraestructura.dto.*;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.*;
import paquete.repositorios.*;

// Referenced classes of package paquete.service:
//            AtencionService, EspecialidadService, SubEspecialidadService, ProcedimientoService

@Service
@Component
public class AtencionServiceImpl
    implements AtencionService
{

    public AtencionServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE PERSONA *****************\n");
    }

    public static List separaIdsCompuesto(String compuesto, String separador)
    {
        List lista = new ArrayList();
        for(StringTokenizer st = new StringTokenizer(compuesto, separador); st.hasMoreTokens(); lista.add(st.nextToken()));
        return lista;
    }

    @Transactional
    public AtencionDTO persistOrMerge(AtencionDTO atencion)
    {
        Atencion dao = (Atencion)mapperService.map(atencion, paquete/entities/Atencion);
        RespuestaDTO respuestaDTO;
        for(Iterator i$ = atencion.getConjuntoRespuestas().iterator(); i$.hasNext(); dao.agregarRespuestas(new Respuesta[] {
    (Respuesta)mapperService.map(respuestaDTO, paquete/entities/Respuesta)
}))
            respuestaDTO = (RespuestaDTO)i$.next();

        EnfermedadDTO enfermedadDTO;
        for(Iterator i$ = atencion.getConjuntoEnfermedades().iterator(); i$.hasNext(); dao.agregarEnfermedades(new Enfermedad[] {
    (Enfermedad)mapperService.map(enfermedadDTO, paquete/entities/Enfermedad)
}))
            enfermedadDTO = (EnfermedadDTO)i$.next();

        if(dao.getIdAtencion() == null)
            atencionRepository.persist(new Atencion[] {
                dao
            });
        else
            atencionRepository.merge(dao);
        AtencionDTO dto = (AtencionDTO)mapperService.map(dao, infraestructura/dto/AtencionDTO);
        Enfermedad enfermedad;
        for(Iterator i$ = dao.getEnfermedades().iterator(); i$.hasNext(); dto.agregarEnfermedades(new EnfermedadDTO[] {
    (EnfermedadDTO)mapperService.map(enfermedad, infraestructura/dto/EnfermedadDTO)
}))
            enfermedad = (Enfermedad)i$.next();

        Respuesta respuesta;
        for(Iterator i$ = dao.getRespuestas().iterator(); i$.hasNext(); dto.agregarRespuestas(new RespuestaDTO[] {
    (RespuestaDTO)mapperService.map(respuesta, infraestructura/dto/RespuestaDTO)
}))
            respuesta = (Respuesta)i$.next();

        return dto;
    }

    public List traeAtencionesHistoria(Integer idHistoria, String limite)
    {
        List listaAtenciones = atencionRepository.traeAtencionesHistoria(idHistoria, limite);
        TrabajoDTO temp = new TrabajoDTO();
        for(int i = 0; i < listaAtenciones.size(); i++)
        {
            temp = trabajoRepository.getTrabajoAtencion(((AtencionDTO)listaAtenciones.get(i)).getIdAtencion());
            temp.setEmpleado((EmpleadoDTO)mapperService.map(empleadoRepository.find(temp.getEmpleadoId()), infraestructura/dto/EmpleadoDTO));
            temp.setEspecialidad((EspecialidadDTO)mapperService.map(especialidadService.find(temp.getEspecialidadId()), infraestructura/dto/EspecialidadDTO));
            ((AtencionDTO)listaAtenciones.get(i)).setTrabajo(temp);
        }

        return listaAtenciones;
    }

    public List traeRespuestasDeAtencion(Integer idAtencion)
    {
        List respuestas = atencionRepository.traeRespuestasDeAtencion(idAtencion);
        for(int i = 0; i < respuestas.size(); i++)
        {
            RespuestaDTO temp = (RespuestaDTO)respuestas.get(i);
            List detalleRespuestasSueltas = separaIdsCompuesto(temp.getDetalleRespuesta(), ";");
            String especialidadDerivado = determinaEspecialidadDerivado(temp);
            ((RespuestaDTO)respuestas.get(i)).setListaRespuestas(detalleRespuestasSueltas);
            ((RespuestaDTO)respuestas.get(i)).setEspecialidadDerivados(especialidadDerivado);
        }

        return respuestas;
    }

    private String determinaEspecialidadDerivado(RespuestaDTO temp)
    {
        String resultado = "Pregunta General";
        Integer esp = temp.getEspecialidadId();
        Integer sub = temp.getSubEspecialidadId();
        Integer pro = temp.getProcedimientoId();
        if(esp == null && sub == null && pro == null)
            return "Pregunta general";
        if(esp != null && esp.intValue() > 0)
        {
            EspecialidadDTO especialidad = especialidadService.find(esp);
            return (new StringBuilder()).append("Pregunta ").append(especialidad.getNombreEspecialidad()).toString();
        }
        if(sub != null && sub.intValue() > 0)
        {
            SubEspecialidadDTO subEspecialidad = subEspecialidadService.findById(sub);
            return (new StringBuilder()).append("Pregunta ").append(subEspecialidad.getNombreSubEspecialidad()).toString();
        }
        if(pro != null && pro.intValue() > 0)
        {
            ProcedimientoDTO procedimiento = procedimientoService.findById(pro.intValue());
            return (new StringBuilder()).append("Pregunta ").append(procedimiento.getNombreProcedimiento()).toString();
        } else
        {
            return resultado;
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/AtencionServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private AtencionRepository atencionRepository;
    @Autowired
    private TrabajoRepository trabajoRepository;
    @Autowired
    private EmpleadoRepository empleadoRepository;
    @Autowired
    private EspecialidadService especialidadService;
    @Autowired
    private SubEspecialidadService subEspecialidadService;
    @Autowired
    private ProcedimientoService procedimientoService;

}
