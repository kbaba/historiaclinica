// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:45:45 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoService.java

package paquete.service;

import infraestructura.dto.ProcedimientoDTO;
import java.util.List;

public interface ProcedimientoService
{

    public abstract void guardarProcedimiento(ProcedimientoDTO procedimientodto);

    public abstract boolean existeProcedimientoRepetido(String s, String s1);

    public abstract boolean existeProcedimientoRepetido(String s);

    public abstract List findByFilter(ProcedimientoDTO procedimientodto);

    public abstract ProcedimientoDTO findById(int i);

    public abstract List findBySubEspecialidad(Integer integer);
}
