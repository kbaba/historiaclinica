// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:39 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadisticaServiceImpl.java

package paquete.service;

import infraestructura.mapper.DTOMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.repositorios.EstadisticaRepository;

// Referenced classes of package paquete.service:
//            EstadisticaService

@Service
@Component
public class EstadisticaServiceImpl
    implements EstadisticaService
{

    public EstadisticaServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/EstadisticaServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private EstadisticaRepository estadisticaRepository;

}
