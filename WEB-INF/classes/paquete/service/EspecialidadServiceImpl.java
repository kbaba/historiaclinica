// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:29 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EspecialidadServiceImpl.java

package paquete.service;

import infraestructura.dto.EspecialidadDTO;
import infraestructura.dto.SubEspecialidadDTO;
import infraestructura.mapper.DTOMapperService;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Especialidad;
import paquete.entities.SubEspecialidad;
import paquete.repositorios.EspecialidadRepository;

// Referenced classes of package paquete.service:
//            EspecialidadService

@Service
@Component
public class EspecialidadServiceImpl
    implements EspecialidadService
{

    public EspecialidadServiceImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO SERVICE DE AREA *****************\n");
    }

    @Transactional
    public void guardarEspecialidad(EspecialidadDTO especialidadDTO)
    {
        Especialidad especialidad = (Especialidad)mapperService.map(especialidadDTO, paquete/entities/Especialidad);
        SubEspecialidadDTO subEspecialidadDTO;
        for(Iterator i$ = especialidadDTO.getConjuntoSubEspecialidades().iterator(); i$.hasNext(); especialidad.agregarSubEspecialidad((SubEspecialidad)mapperService.map(subEspecialidadDTO, paquete/entities/SubEspecialidad)))
            subEspecialidadDTO = (SubEspecialidadDTO)i$.next();

        especialidadRepository.persistEspecialidad(especialidad);
    }

    public List buscarEspecialidad(Especialidad especialidad)
    {
        List lista = especialidadRepository.findByFilter(especialidad);
        List listaResultante = new ArrayList();
        EspecialidadDTO dto;
        for(Iterator i$ = lista.iterator(); i$.hasNext(); listaResultante.add(dto))
        {
            Especialidad especialidadResult = (Especialidad)i$.next();
            dto = (EspecialidadDTO)mapperService.map(especialidadResult, infraestructura/dto/EspecialidadDTO);
            List numeroDeHijos = especialidadRepository.buscarHijosPorEspecialidad(dto);
            dto.setNumeroDeSubEspecialidades(numeroDeHijos.size() <= 0 ? "0" : (String)numeroDeHijos.get(0));
            dto.setNumeroDeProcedimientos(numeroDeHijos.size() <= 1 ? "0" : (String)numeroDeHijos.get(1));
        }

        return listaResultante;
    }

    public boolean existeEspecialidadRepetida(String nombre, String idEspecialidad)
    {
        return especialidadRepository.esNombreRepetido(nombre, idEspecialidad);
    }

    public boolean existeEspecialidadRepetida(String nombre)
    {
        return especialidadRepository.esNombreRepetido(nombre);
    }

    public List getAll()
    {
        List lista = especialidadRepository.findAll();
        List listaResultante = new ArrayList();
        EspecialidadDTO dto;
        for(Iterator i$ = lista.iterator(); i$.hasNext(); listaResultante.add(dto))
        {
            Especialidad especialidadResult = (Especialidad)i$.next();
            dto = (EspecialidadDTO)mapperService.map(especialidadResult, infraestructura/dto/EspecialidadDTO);
            List numeroDeHijos = especialidadRepository.buscarHijosPorEspecialidad(dto);
            dto.setNumeroDeSubEspecialidades(numeroDeHijos.size() <= 0 ? "0" : (String)numeroDeHijos.get(0));
            dto.setNumeroDeProcedimientos(numeroDeHijos.size() <= 1 ? "0" : (String)numeroDeHijos.get(1));
        }

        Collections.sort(listaResultante, new Comparator() {

            public int compare(EspecialidadDTO o1, EspecialidadDTO o2)
            {
                return o1.getNombreEspecialidad().compareTo(o2.getNombreEspecialidad());
            }

            public volatile int compare(Object x0, Object x1)
            {
                return compare((EspecialidadDTO)x0, (EspecialidadDTO)x1);
            }

            final EspecialidadServiceImpl this$0;

            
            {
                this$0 = EspecialidadServiceImpl.this;
                super();
            }
        }
);
        return listaResultante;
    }

    public EspecialidadDTO find(Integer especialidadId)
    {
        Especialidad dao = (Especialidad)especialidadRepository.find(especialidadId);
        EspecialidadDTO dto = (EspecialidadDTO)mapperService.map(dao, infraestructura/dto/EspecialidadDTO);
        return dto;
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/EspecialidadServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private EspecialidadRepository especialidadRepository;

}
