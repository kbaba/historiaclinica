// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:42:08 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpleadoServiceImpl.java

package paquete.service;

import infraestructura.dto.*;
import infraestructura.mapper.DTOMapperService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.repositorios.EmpleadoRepository;
import paquete.repositorios.PersonaRepository;

// Referenced classes of package paquete.service:
//            EmpleadoService

@Service
@Component
public class EmpleadoServiceImpl
    implements EmpleadoService
{

    public EmpleadoServiceImpl()
    {
    }

    @Transactional
    public void eliminarTrabajadorEspecialidad(EmpleadoDTO trabajador, EspecialidadDTO especialidad)
    {
        empleadoRepository.eliminarTrabajadorEspecialidad(trabajador, especialidad);
    }

    @Transactional
    public void eliminarTrabajadorEspecialidad(int idTrabajador, int idEspecialidad)
    {
        empleadoRepository.eliminarTrabajadorEspecialidad(idTrabajador, idEspecialidad);
    }

    public void eliminarTrabajador(PersonaDTO persona)
    {
        if(persona.getEmpleado() == null)
        {
            return;
        } else
        {
            empleadoRepository.eliminarTrabajador(persona.getEmpleado());
            return;
        }
    }

    public List traeTrabajosDeEmpleado(int idEmpleado)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/service/EmpleadoServiceImpl);
    @Autowired
    private DTOMapperService mapperService;
    @Autowired
    private PersonaRepository personaRepository;
    @Autowired
    private EmpleadoRepository empleadoRepository;

}
