// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:34:18 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   CronDepuradorSeparaciones.java

package paquete.crons;

import infraestructura.dto.ApplicationDTO;
import infraestructura.dto.UsuarioDTO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.beans.ApplicationBean;

@Service
public class CronDepuradorSeparaciones
{

    public CronDepuradorSeparaciones()
    {
    }

    public void depurarSeparaciones()
        throws Exception
    {
        Date horarioActual = new Date();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        logger.info((new StringBuilder()).append("\n\n\n Verificando separaciones a la hora ").append(dateFormat.format(horarioActual)).toString());
        Map separaciones = applicationBean.getSeparaciones();
        Iterator i$ = applicationBean.getSeparaciones().entrySet().iterator();
        do
        {
            if(!i$.hasNext())
                break;
            java.util.Map.Entry entry = (java.util.Map.Entry)i$.next();
            if(!((ApplicationDTO)entry.getValue()).getSeEncuentraEnAtencion().booleanValue())
            {
                Date horarioSeparacion = ((ApplicationDTO)entry.getValue()).getHoraSeparacion();
                long difference = horarioActual.getTime() - horarioSeparacion.getTime();
                int minutes = (int)((difference / 60000L) % 60L);
                if(minutes >= 10)
                {
                    applicationBean.getSeparaciones().remove(entry.getKey());
                    logger.info((new StringBuilder()).append("\n Se ha eliminado la separaci\363n del doctor ").append(((ApplicationDTO)entry.getValue()).getEmpleado().getApellido()).append("\n").toString());
                }
            }
        } while(true);
    }

    private static final Logger logger = LoggerFactory.getLogger(paquete/crons/CronDepuradorSeparaciones);
    @Autowired
    private ApplicationBean applicationBean;

}
