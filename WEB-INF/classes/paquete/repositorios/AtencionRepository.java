// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:23 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AtencionRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import java.util.List;
import paquete.entities.Atencion;

public interface AtencionRepository
    extends GenericDAO
{

    public abstract void guardarAtencion(Atencion atencion);

    public abstract boolean esTrabajoBorrable(Integer integer, Integer integer1);

    public abstract boolean esTrabajoBorrable(Integer integer);

    public abstract List traeAtencionesHistoria(Integer integer, String s);

    public abstract List traeRespuestasDeAtencion(Integer integer);
}
