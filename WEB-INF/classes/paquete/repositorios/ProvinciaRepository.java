// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:40:51 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProvinciaRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.ProvinciaDTO;
import java.util.List;

public interface ProvinciaRepository
    extends GenericDAO
{

    public abstract List findByDepartamento(String s, String s1);

    public abstract ProvinciaDTO findByCode(String s, String s1, String s2);
}
