// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:41:15 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   RolRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.persistence.BaseDAO;
import paquete.entities.Rol;

// Referenced classes of package paquete.repositorios:
//            RolRepository

@Repository
public class RolRepositoryImpl extends BaseDAO
    implements RolRepository
{

    public RolRepositoryImpl()
    {
    }

    public Rol findBy(String code)
    {
        Search s = new Search();
        s.addFilterEqual("idRol", code);
        return (Rol)searchUnique(s);
    }
}
