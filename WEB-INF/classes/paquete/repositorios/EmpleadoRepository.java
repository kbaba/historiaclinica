// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:46 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpleadoRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.EmpleadoDTO;
import infraestructura.dto.EspecialidadDTO;

public interface EmpleadoRepository
    extends GenericDAO
{

    public abstract void eliminarTrabajadorEspecialidad(EmpleadoDTO empleadodto, EspecialidadDTO especialidaddto);

    public abstract void eliminarTrabajadorEspecialidad(int i, int j);

    public abstract void eliminarTrabajador(EmpleadoDTO empleadodto);
}
