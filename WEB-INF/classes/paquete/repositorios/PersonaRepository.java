// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:39:55 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PersonaRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.PersonaDTO;
import java.util.List;
import paquete.entities.Persona;

public interface PersonaRepository
    extends GenericDAO
{

    public abstract void persistirPersona(Persona persona);

    public abstract void eliminarPersona(PersonaDTO personadto);

    public abstract boolean esDocumentoRepetido(String s, int i, String s1);

    public abstract boolean esDocumentoRepetido(String s, String s1);

    public abstract List findByFilter(Persona persona);

    public abstract List findByFilterLazy(Persona persona, int i, int j);

    public abstract List findByFilterLazyEmpleado(Persona persona, int i, int j);

    public abstract int cuentaRegistrosFilterLazy(Persona persona);

    public abstract int cuentaRegistrosFilterLazyEmpleado(Persona persona);

    public abstract boolean esUsuarioRepetido(String s, int i);

    public abstract boolean esUsuarioRepetido(String s);

    public abstract Persona acceder(String s, String s1);
}
