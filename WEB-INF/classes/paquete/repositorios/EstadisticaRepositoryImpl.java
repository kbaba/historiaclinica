// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:37:33 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EstadisticaRepositoryImpl.java

package paquete.repositorios;

import infraestructura.mapper.DTOMapperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Referenced classes of package paquete.repositorios:
//            EstadisticaRepository

@Repository
public class EstadisticaRepositoryImpl
    implements EstadisticaRepository
{

    public EstadisticaRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/EstadisticaRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
