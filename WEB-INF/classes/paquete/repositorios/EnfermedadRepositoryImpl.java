// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:58 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.dto.EnfermedadDTO;
import infraestructura.mapper.DTOMapperService;
import infraestructura.mapper.EnfermedadRowMapper;
import infraestructura.mapper.GrupoEnfermedadRowMapper;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import paquete.entities.Enfermedad;

// Referenced classes of package paquete.repositorios:
//            EnfermedadRepository

@Repository
public class EnfermedadRepositoryImpl extends BaseDAO
    implements EnfermedadRepository
{

    public EnfermedadRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE ENFERMEDAD *****************\n");
    }

    public void persistirEnfermedad(Enfermedad enfermedad)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List findByFilter(Enfermedad filtro, Enfermedad capituloFiltro, int indiceInicial, int indiceFinal)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select ");
        builder.append("a.id_enfermedad as idCapitulo, ");
        builder.append("a.nombre_enfermedad as nombreCapitulo, ");
        builder.append("b.id_enfermedad as idGrupo, ");
        builder.append("b.nombre_enfermedad as nombreGrupo,  ");
        builder.append("c.id_enfermedad as idSubGrupo, ");
        builder.append("c.nombre_enfermedad as nombreSubGrupo, ");
        builder.append("d.id_enfermedad as idEnfermedad, ");
        builder.append("d.nombre_Enfermedad as nombreEnfermedad ");
        builder.append("from ");
        builder.append("enfermedad a inner join ");
        builder.append("enfermedad b on ");
        builder.append("a.grupo_enfermedad=b.grupo_enfermedad ");
        builder.append("and a.es_capitulo=1 inner join enfermedad c on ");
        builder.append("b.id_enfermedad=c.grupo_enfermedad inner join enfermedad d on ");
        builder.append("c.id_enfermedad = LEFT(d.id_enfermedad, 3) ");
        if(StringUtils.isNotBlank(filtro.getIdEnfermedad()))
            builder.append("and d.id_enfermedad like '%").append(filtro.getIdEnfermedad()).append("%'");
        if(StringUtils.isNotBlank(filtro.getNombreEnfermedad()))
            builder.append("and d.nombre_enfermedad like '%").append(filtro.getNombreEnfermedad()).append("%'");
        if(capituloFiltro != null && StringUtils.isNotBlank(capituloFiltro.getIdEnfermedad()))
            builder.append("and a.id_enfermedad ='").append(capituloFiltro.getIdEnfermedad()).append("'");
        builder.append("limit ").append(indiceInicial).append(", ").append(indiceFinal);
        List results = new ArrayList();
        results = getJdbcTemplate().query(builder.toString(), new GrupoEnfermedadRowMapper());
        return results;
    }

    private JdbcTemplate getJdbcTemplate()
    {
        return new JdbcTemplate(dataSource);
    }

    public int cuentaRegistros(EnfermedadDTO enfermedadFiltro, Enfermedad capituloFiltro)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(*) ");
        builder.append("from ");
        builder.append("enfermedad a inner join ");
        builder.append("enfermedad b on ");
        builder.append("a.grupo_enfermedad=b.grupo_enfermedad ");
        builder.append("and a.es_capitulo=1 inner join enfermedad c on ");
        builder.append("b.id_enfermedad=c.grupo_enfermedad inner join enfermedad d on ");
        builder.append("c.id_enfermedad = LEFT(d.id_enfermedad, 3) ");
        if(StringUtils.isNotBlank(enfermedadFiltro.getIdEnfermedad()))
            builder.append("and d.id_enfermedad like '%").append(enfermedadFiltro.getIdEnfermedad()).append("%'");
        if(StringUtils.isNotBlank(enfermedadFiltro.getNombreEnfermedad()))
            builder.append("and d.nombre_enfermedad like '%").append(enfermedadFiltro.getNombreEnfermedad()).append("%'");
        if(capituloFiltro != null && StringUtils.isNotBlank(capituloFiltro.getIdEnfermedad()))
            builder.append("and a.id_enfermedad ='").append(capituloFiltro.getIdEnfermedad()).append("'");
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue();
    }

    public List findByFilter(Enfermedad filtro)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT  `id_enfermedad` ,  `nombre_enfermedad` ,  `grupo_enfermedad`,`es_capitulo` ");
        builder.append("FROM enfermedad WHERE id_enfermedad=id_enfermedad and es_capitulo=1 ");
        if(StringUtils.isNotBlank(filtro.getIdEnfermedad()))
            builder.append(" and LOWER(id_enfermedad) LIKE '%").append(filtro.getIdEnfermedad().toLowerCase()).append("%' and es_capitulo=1");
        if(StringUtils.isNotBlank(filtro.getNombreEnfermedad()))
            builder.append(" OR  LOWER(nombre_enfermedad) like '%").append(filtro.getNombreEnfermedad().toLowerCase()).append("%' and es_capitulo=1");
        List results = new ArrayList();
        results = getJdbcTemplate().query(builder.toString(), new EnfermedadRowMapper());
        if(results.isEmpty())
        {
            builder.delete(152, builder.length());
            results = getJdbcTemplate().query(builder.toString(), new EnfermedadRowMapper());
        }
        return results;
    }

    public List findByFilterNoCap(Enfermedad filtro)
    {
        Search parameter = new Search();
        if(StringUtils.isNotBlank(filtro.getNombreEnfermedad()))
            parameter.addFilterILike("nombreEnfermedad", (new StringBuilder()).append("%").append(filtro.getNombreEnfermedad().toLowerCase()).append("%").toString());
        return search(parameter);
    }

    public List cargarGruposCapitulos(String idEnfermedad)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM `enfermedad` WHERE  grupo_enfermedad=").append(idEnfermedad);
        builder.append(" and es_capitulo=0");
        Query query = em().createNativeQuery(builder.toString());
        List countersAsBigDecimal = query.getResultList();
        return countersAsBigDecimal;
    }

    public List traeEnfermedadesAtencion(Integer idAtencion)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT E.nombre_enfermedad \"nombreEnfermedad\", E.id_enfermedad \"idEnfermedad\" from enfermedad E ");
        builder.append(" inner join atencion_enfermedad A ");
        builder.append(" WHERE E.id_enfermedad= A.enfermedad_id ");
        builder.append(" AND A.atencion_id =").append(idAtencion);
        org.hibernate.Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(builder.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/EnfermedadDTO));
        List lista = query.list();
        return lista;
    }

    @Autowired
    DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/EnfermedadRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
