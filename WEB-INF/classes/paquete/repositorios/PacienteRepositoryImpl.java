// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:39:30 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PacienteRepositoryImpl.java

package paquete.repositorios;

import infraestructura.dto.PacienteDTO;
import infraestructura.dto.PersonaDTO;
import infraestructura.mapper.DTOMapperService;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import javax.persistence.EntityManager;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Referenced classes of package paquete.repositorios:
//            PersonaRepositoryImpl, PacienteRepository

@Repository
public class PacienteRepositoryImpl extends BaseDAO
    implements PacienteRepository
{

    public PacienteRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE PERSONA /*****************\n");
    }

    @Transactional
    public int cuentaPacientesConIDersona(int id, String estado)
    {
        String sqlQuery = (new StringBuilder()).append(" select count(*) from paciente where persona_id ='").append(id).append("' AND paciente_estado='").append(estado).append("';").toString();
        Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(sqlQuery).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/PersonaDTO));
        Object salida = query.uniqueResult();
        int cont = ((Integer)salida).intValue();
        return cont;
    }

    @Transactional
    public void eliminarPaciente(PacienteDTO paciente)
    {
        String strQuery = (new StringBuilder()).append("UPDATE paciente SET estado_paciente='I' WHERE id_paciente='").append(paciente.getIdPaciente().intValue()).append("'").toString();
        em().createNativeQuery(strQuery).executeUpdate();
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/PersonaRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
