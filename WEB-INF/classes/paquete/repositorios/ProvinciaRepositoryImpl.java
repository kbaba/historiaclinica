// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:40:59 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProvinciaRepositoryImpl.java

package paquete.repositorios;

import infraestructura.dto.ProvinciaDTO;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

// Referenced classes of package paquete.repositorios:
//            ProvinciaRepository

@Repository
public class ProvinciaRepositoryImpl extends BaseDAO
    implements ProvinciaRepository
{

    public ProvinciaRepositoryImpl()
    {
    }

    public List findByDepartamento(String pais_id, String departamento_id)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" C.pais_id \"paisId\",");
        sql.append(" C.departamento_id \"departamentoId\", ");
        sql.append(" C.id_provincia \"idProvincia\", ");
        sql.append(" C.nombre_provincia \"nombreProvincia\" ");
        sql.append(" FROM PROVINCIA C   ");
        if(StringUtils.isNotBlank(pais_id))
            sql.append("WHERE C.pais_id= '").append(pais_id).append("'");
        if(StringUtils.isNotBlank(departamento_id))
        {
            sql.append(" AND ");
            sql.append(" C.departamento_id= '").append(departamento_id).append("'");
        }
        sql.append(" order by C.nombre_provincia");
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/ProvinciaDTO));
        items = query.list();
        return items;
    }

    public ProvinciaDTO findByCode(String pais_id, String departamento_id, String id_provincia)
    {
        List state = null;
        ProvinciaDTO nuevoProvinciaDTO = new ProvinciaDTO();
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" C.pais_id \"paisId\",");
        sql.append(" C.departamento_id \"departamentoId\", ");
        sql.append(" C.id_provincia \"idProvincia\", ");
        sql.append(" C.nombre_provincia \"nombreProvincia\" ");
        sql.append(" FROM PROVINCIA C   ");
        if(StringUtils.isNotBlank(pais_id))
            sql.append("WHERE C.pais_id= '").append(pais_id).append("'");
        if(StringUtils.isNotBlank(departamento_id))
        {
            sql.append(" AND ");
            sql.append(" C.departamento_id= '").append(departamento_id).append("'");
        }
        if(StringUtils.isNotBlank(id_provincia))
        {
            sql.append(" AND ");
            sql.append(" C.id_provincia = '").append(id_provincia).append("'");
        }
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/ProvinciaDTO));
        state = query.list();
        ProvinciaDTO as;
        for(Iterator i$ = state.iterator(); i$.hasNext(); nuevoProvinciaDTO.setNombreProvincia(as.getNombreProvincia()))
        {
            as = (ProvinciaDTO)i$.next();
            nuevoProvinciaDTO.setPaisId(as.getPaisId());
            nuevoProvinciaDTO.setDepartamentoId(as.getDepartamentoId());
            nuevoProvinciaDTO.setIdProvincia(as.getIdProvincia());
        }

        return nuevoProvinciaDTO;
    }
}
