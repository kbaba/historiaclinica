// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:38:49 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   MenuRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import paquete.entities.Menu;

public interface MenuRepository
    extends GenericDAO
{

    public abstract Menu findBy(String s);
}
