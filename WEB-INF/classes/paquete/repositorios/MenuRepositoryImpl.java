// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:39:01 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   MenuRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.persistence.BaseDAO;
import paquete.entities.Menu;

// Referenced classes of package paquete.repositorios:
//            MenuRepository

@Repository
public class MenuRepositoryImpl extends BaseDAO
    implements MenuRepository
{

    public MenuRepositoryImpl()
    {
    }

    public Menu findBy(String name)
    {
        Search s = new Search();
        s.addFilterEqual("idMenu", name);
        return (Menu)searchUnique(s);
    }
}
