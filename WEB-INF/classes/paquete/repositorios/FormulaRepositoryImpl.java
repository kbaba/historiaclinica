// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:38:28 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FormulaRepositoryImpl.java

package paquete.repositorios;

import infraestructura.mapper.DTOMapperService;
import infraestructura.persistence.BaseDAO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Referenced classes of package paquete.repositorios:
//            FormulaRepository, AlternativaRepository, ProcedimientoRepository, SubEspecialidadRepository, 
//            EspecialidadRepository

@Repository
public class FormulaRepositoryImpl extends BaseDAO
    implements FormulaRepository
{

    public FormulaRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    public Object findValorRespuesta(String descripcionPregunta, Integer idPaciente)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT  R.detalle_respuesta from respuesta R inner join atencion A inner join historia H ");
        builder.append(" ON H.paciente_id= H.paciente_id");
        if(idPaciente != null)
            builder.append(" AND H.paciente_id= ").append(idPaciente.intValue()).append(" ");
        builder.append(" AND H.id_historia=A.historia_id ");
        builder.append(" AND A.id_atencion= R.atencion_id ");
        if(StringUtils.isNotBlank(descripcionPregunta))
            builder.append(" AND R.descripcion_pregunta like '%").append(descripcionPregunta).append("%' ");
        builder.append(" order by R.id_respuesta DESC ");
        builder.append(" limit 1 ");
        Query query = em().createNativeQuery(builder.toString());
        if(query.getResultList().size() == 0)
        {
            return null;
        } else
        {
            Object result = query.getSingleResult();
            return result;
        }
    }

    @Autowired
    AlternativaRepository alternativaRepository;
    @Autowired
    ProcedimientoRepository procedimientoRepository;
    @Autowired
    SubEspecialidadRepository subEspecialidadRepository;
    @Autowired
    EspecialidadRepository especialidadRepository;
    @Autowired
    DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/FormulaRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
