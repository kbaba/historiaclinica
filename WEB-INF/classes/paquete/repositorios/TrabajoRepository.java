// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:41:29 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TrabajoRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.TrabajoDTO;
import java.util.List;
import paquete.entities.Trabajo;

public interface TrabajoRepository
    extends GenericDAO
{

    public abstract void persistirTrabajo(Trabajo trabajo);

    public abstract List getTrabajoEmpleado(Integer integer);

    public abstract TrabajoDTO getTrabajoAtencion(Integer integer);

    public abstract void persistirTrabajoQuery(List list);

    public abstract void persistirTrabajoQueryDTO(List list);

    public abstract void eliminarTrabajosQueryDTO(List list, Integer integer);

    public abstract boolean esTrabajoBorrable(Integer integer, Integer integer1);

    public abstract void ejecutarInstruccion(String s);

    public abstract void ejecutarInstruccion(List list);

    public abstract boolean existeTrabajo(Integer integer, Integer integer1);

    public abstract String getQueryEliminar(Integer integer, List list);

    public abstract String getQueryActualizar(Integer integer, List list);

    public abstract String getQueryCrear(Integer integer, List list);
}
