// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:40:18 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntaRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import java.util.List;
import paquete.entities.Alternativa;
import paquete.entities.Pregunta;

public interface PreguntaRepository
    extends GenericDAO
{

    public abstract void guardarPregunta(Pregunta pregunta);

    public abstract List findByFilter(Pregunta pregunta, int i, int j, boolean flag);

    public abstract List findByFilter(Pregunta pregunta, Alternativa alternativa, int i, int j);

    public abstract int cuentaRegistrosFilterLazy(Pregunta pregunta);

    public abstract int cuentaRegistrosFilterLazy(Pregunta pregunta, Alternativa alternativa);

    public abstract void eliminarPregunta(Pregunta pregunta);

    public abstract boolean esPreguntaRepetida(String s, int i);

    public abstract boolean esPreguntaRepetida(String s);
}
