// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:16 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AlternativaRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import java.util.List;

public interface AlternativaRepository
    extends GenericDAO
{

    public abstract List getAlternativasXPreguntaId(int i);

    public abstract void borraRegistroAlternativa(int i);
}
