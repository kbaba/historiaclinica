// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:32 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   DepartamentoRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.DepartamentoDTO;
import java.util.List;

public interface DepartamentoRepository
    extends GenericDAO
{

    public abstract List findByName(String s);

    public abstract List findByPaisId(String s);

    public abstract DepartamentoDTO findByCode(String s, String s1);
}
