// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:40:03 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PersonaRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.dto.PersonaDTO;
import infraestructura.mapper.DTOMapperService;
import infraestructura.mapper.EmpleadoRowMapper;
import infraestructura.persistence.BaseDAO;
import java.math.BigInteger;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import paquete.entities.*;

// Referenced classes of package paquete.repositorios:
//            PersonaRepository, ProcedimientoRepository, SubEspecialidadRepository, EspecialidadRepository

@Repository
public class PersonaRepositoryImpl extends BaseDAO
    implements PersonaRepository
{

    public PersonaRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE PERSONA /*****************\n");
    }

    public void persistirPersona(Persona persona)
    {
        if(persona.getIdPersona() == null)
            em().persist(persona);
        else
            em().merge(persona);
    }

    @Transactional
    public List findByFilter(Persona filtro)
    {
        Search parameter = new Search();
        if(filtro.getIdPersona() != null)
            parameter.addFilterEqual("idPersona", filtro.getIdPersona());
        if(filtro.getTipoDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getTipoDocumentoPersona()))
            parameter.addFilterEqual("tipoDocumentoPersona", filtro.getTipoDocumentoPersona());
        if(filtro.getDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getDocumentoPersona()))
            parameter.addFilterEqual("documentoPersona", filtro.getDocumentoPersona());
        if(filtro.getNombrePersona() != null && StringUtils.isNotBlank(filtro.getNombrePersona()))
            parameter.addFilterLike("nombrePersona", (new StringBuilder()).append("%").append(filtro.getNombrePersona()).append("%").toString());
        if(filtro.getApellidoPaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoPaternoPersona()))
            parameter.addFilterLike("apellidoPaternoPersona", (new StringBuilder()).append("%").append(filtro.getApellidoPaternoPersona()).append("%").toString());
        if(filtro.getApellidoMaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoMaternoPersona()))
            parameter.addFilterLike("apellidoMaternoPersona", (new StringBuilder()).append("%").append(filtro.getApellidoMaternoPersona()).append("%").toString());
        if(filtro.getDireccionPersona() != null && StringUtils.isNotBlank(filtro.getDireccionPersona()))
            parameter.addFilterLike("direccionPersona", (new StringBuilder()).append("%").append(filtro.getDireccionPersona()).append("%").toString());
        if(filtro.getFechaNacimientoPersona() != null && StringUtils.isNotBlank(filtro.getFechaNacimientoPersona().toString()))
            parameter.addFilterEqual("fechaNacimientoPersona", filtro.getFechaNacimientoPersona());
        if(filtro.getTipoResidenciaPersona() != null && StringUtils.isNotBlank(filtro.getTipoResidenciaPersona()))
            parameter.addFilterEqual("tipoResidenciaPersona", filtro.getTipoResidenciaPersona());
        if(filtro.getDormitoriosViviendaPersona() != null && StringUtils.isNotBlank(filtro.getDormitoriosViviendaPersona().toString()))
            parameter.addFilterEqual("dormitoriosViviendaPersona", filtro.getDormitoriosViviendaPersona());
        if(filtro.getCantidadPersonasViviendaPersona() != null && StringUtils.isNotBlank(filtro.getCantidadPersonasViviendaPersona().toString()))
            parameter.addFilterEqual("cantidadPersonasViviendaPersona", filtro.getCantidadPersonasViviendaPersona());
        if(filtro.getPertenenciaViviendaPersona() != null && StringUtils.isNotBlank(filtro.getPertenenciaViviendaPersona()))
            parameter.addFilterEqual("pertenenciaViviendaPersona", filtro.getPertenenciaViviendaPersona());
        if(filtro.getMaterialViviendaPersona() != null && StringUtils.isNotBlank(filtro.getMaterialViviendaPersona()))
            parameter.addFilterEqual("materialViviendaPersona", filtro.getMaterialViviendaPersona());
        if(filtro.getEstatutoSocialPersona() != null && StringUtils.isNotBlank(filtro.getEstatutoSocialPersona()))
            parameter.addFilterEqual("estatutoSocialPersona", filtro.getEstatutoSocialPersona());
        if(filtro.getSexoPersona() != null && StringUtils.isNotBlank(filtro.getSexoPersona()))
            parameter.addFilterEqual("sexoPersona", filtro.getSexoPersona());
        if(filtro.getPaciente() != null)
        {
            if(StringUtils.isEmpty(filtro.getPaciente().getEstadoPaciente()))
                parameter.addFilterLike("paciente.estadoPaciente", "%%");
            else
            if(filtro.getPaciente().getEstadoPaciente() != null && StringUtils.isNotBlank(filtro.getPaciente().getEstadoPaciente()))
                parameter.addFilterEqual("paciente.estadoPaciente", filtro.getPaciente().getEstadoPaciente());
            if(filtro.getPaciente().getIdPaciente() != null)
                parameter.addFilterEqual("paciente.idPaciente", filtro.getPaciente().getIdPaciente());
        }
        return search(parameter);
    }

    @Transactional
    public List findByFilterLazy(Persona filtro, int indiceInicial, int indiceFinal)
    {
        Search parameter = new Search();
        if(filtro.getIdPersona() != null)
            parameter.addFilterEqual("idPersona", filtro.getIdPersona());
        if(filtro.getTipoDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getTipoDocumentoPersona()))
            parameter.addFilterEqual("tipoDocumentoPersona", filtro.getTipoDocumentoPersona());
        if(filtro.getDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getDocumentoPersona()))
            parameter.addFilterEqual("documentoPersona", filtro.getDocumentoPersona());
        if(filtro.getNombrePersona() != null && StringUtils.isNotBlank(filtro.getNombrePersona()))
            parameter.addFilterLike("nombrePersona", (new StringBuilder()).append("%").append(filtro.getNombrePersona()).append("%").toString());
        if(filtro.getApellidoPaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoPaternoPersona()))
            parameter.addFilterLike("apellidoPaternoPersona", (new StringBuilder()).append("%").append(filtro.getApellidoPaternoPersona()).append("%").toString());
        if(filtro.getApellidoMaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoMaternoPersona()))
            parameter.addFilterLike("apellidoMaternoPersona", (new StringBuilder()).append("%").append(filtro.getApellidoMaternoPersona()).append("%").toString());
        if(filtro.getDireccionPersona() != null && StringUtils.isNotBlank(filtro.getDireccionPersona()))
            parameter.addFilterLike("direccionPersona", (new StringBuilder()).append("%").append(filtro.getDireccionPersona()).append("%").toString());
        if(filtro.getFechaNacimientoPersona() != null && StringUtils.isNotBlank(filtro.getFechaNacimientoPersona().toString()))
            parameter.addFilterEqual("fechaNacimientoPersona", filtro.getFechaNacimientoPersona());
        if(filtro.getTipoResidenciaPersona() != null && StringUtils.isNotBlank(filtro.getTipoResidenciaPersona()))
            parameter.addFilterEqual("tipoResidenciaPersona", filtro.getTipoResidenciaPersona());
        if(filtro.getDormitoriosViviendaPersona() != null && StringUtils.isNotBlank(filtro.getDormitoriosViviendaPersona().toString()))
            parameter.addFilterEqual("dormitoriosViviendaPersona", filtro.getDormitoriosViviendaPersona());
        if(filtro.getCantidadPersonasViviendaPersona() != null && StringUtils.isNotBlank(filtro.getCantidadPersonasViviendaPersona().toString()))
            parameter.addFilterEqual("cantidadPersonasViviendaPersona", filtro.getCantidadPersonasViviendaPersona());
        if(filtro.getPertenenciaViviendaPersona() != null && StringUtils.isNotBlank(filtro.getPertenenciaViviendaPersona()))
            parameter.addFilterEqual("pertenenciaViviendaPersona", filtro.getPertenenciaViviendaPersona());
        if(filtro.getMaterialViviendaPersona() != null && StringUtils.isNotBlank(filtro.getMaterialViviendaPersona()))
            parameter.addFilterEqual("materialViviendaPersona", filtro.getMaterialViviendaPersona());
        if(filtro.getEstatutoSocialPersona() != null && StringUtils.isNotBlank(filtro.getEstatutoSocialPersona()))
            parameter.addFilterEqual("estatutoSocialPersona", filtro.getEstatutoSocialPersona());
        if(filtro.getSexoPersona() != null && StringUtils.isNotBlank(filtro.getSexoPersona()))
            parameter.addFilterEqual("sexoPersona", filtro.getSexoPersona());
        if(filtro.getPaciente() != null)
        {
            if(StringUtils.isEmpty(filtro.getPaciente().getEstadoPaciente()))
                parameter.addFilterLike("paciente.estadoPaciente", "%%");
            else
            if(filtro.getPaciente().getEstadoPaciente() != null && StringUtils.isNotBlank(filtro.getPaciente().getEstadoPaciente()))
                parameter.addFilterEqual("paciente.estadoPaciente", filtro.getPaciente().getEstadoPaciente());
            if(filtro.getPaciente().getIdPaciente() != null)
                parameter.addFilterEqual("paciente.idPaciente", filtro.getPaciente().getIdPaciente());
        }
        parameter.setFirstResult(indiceInicial);
        parameter.setMaxResults(indiceFinal);
        return search(parameter);
    }

    @Transactional
    public boolean esDocumentoRepetido(String documento, int idPersona, String tipoDocumento)
    {
        Search parameter = new Search();
        parameter.addFilterNotEqual("idPersona", Integer.valueOf(idPersona));
        parameter.addFilterEqual("tipoDocumentoPersona", tipoDocumento);
        parameter.addFilterEqual("documentoPersona", documento);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public boolean esUsuarioRepetido(String usuario, int idPersona)
    {
        Search parameter = new Search();
        parameter.addFilterNotEqual("idPersona", Integer.valueOf(idPersona));
        parameter.addFilterEqual("usuario", usuario);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public boolean esUsuarioRepetido(String usuario)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("usuario", usuario);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public boolean esDocumentoRepetido(String documento, String tipoDocumento)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("tipoDocumentoPersona", tipoDocumento);
        parameter.addFilterEqual("documentoPersona", documento);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    public void eliminarPersona(PersonaDTO persona)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int cuentaRegistrosFilterLazy(Persona filtro)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(*) ");
        builder.append("from ");
        builder.append("persona a inner join ");
        builder.append("paciente b on ");
        builder.append("a.id_persona=b.persona_id ");
        if(filtro.getIdPersona() != null)
            builder.append(" AND id_persona='").append(filtro.getIdPersona()).append("' ");
        if(filtro.getTipoDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getTipoDocumentoPersona()))
            builder.append(" AND tipo_documento_persona='").append(filtro.getTipoDocumentoPersona()).append("' ");
        if(filtro.getDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getDocumentoPersona()))
            builder.append(" AND documento_persona='").append(filtro.getDocumentoPersona()).append("' ");
        if(filtro.getNombrePersona() != null && StringUtils.isNotBlank(filtro.getNombrePersona()))
            builder.append(" AND nombre_persona like '%").append(filtro.getNombrePersona()).append("%' ");
        if(filtro.getApellidoPaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoPaternoPersona()))
            builder.append(" AND apellido_paterno_persona like '%").append(filtro.getApellidoPaternoPersona()).append("%' ");
        if(filtro.getApellidoMaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoMaternoPersona()))
            builder.append(" AND apellido_materno_persona like '%").append(filtro.getApellidoMaternoPersona()).append("%' ");
        if(filtro.getDireccionPersona() != null && StringUtils.isNotBlank(filtro.getDireccionPersona()))
            builder.append(" AND direccion_persona like '%").append(filtro.getDireccionPersona()).append("%' ");
        if(filtro.getFechaNacimientoPersona() != null && StringUtils.isNotBlank(filtro.getFechaNacimientoPersona().toString()))
            builder.append(" AND fecha_nacimiento_persona ='").append(filtro.getFechaNacimientoPersona()).append("' ");
        if(filtro.getTipoResidenciaPersona() != null && StringUtils.isNotBlank(filtro.getTipoResidenciaPersona()))
            builder.append(" AND tipo_residencia_persona ='").append(filtro.getTipoResidenciaPersona()).append("' ");
        if(filtro.getDormitoriosViviendaPersona() != null && StringUtils.isNotBlank(filtro.getDormitoriosViviendaPersona().toString()))
            builder.append(" AND domitorios_vivienda_persona ='").append(filtro.getDormitoriosViviendaPersona()).append("' ");
        if(filtro.getCantidadPersonasViviendaPersona() != null && StringUtils.isNotBlank(filtro.getCantidadPersonasViviendaPersona().toString()))
            builder.append(" AND cantidad_personas_vivienda_persona ='").append(filtro.getCantidadPersonasViviendaPersona()).append("' ");
        if(filtro.getPertenenciaViviendaPersona() != null && StringUtils.isNotBlank(filtro.getPertenenciaViviendaPersona()))
            builder.append(" AND pertenencia_vivienda_persona='").append(filtro.getPertenenciaViviendaPersona()).append("' ");
        if(filtro.getMaterialViviendaPersona() != null && StringUtils.isNotBlank(filtro.getMaterialViviendaPersona()))
            builder.append(" AND material_vivienda_persona='").append(filtro.getMaterialViviendaPersona()).append("' ");
        if(filtro.getEstatutoSocialPersona() != null && StringUtils.isNotBlank(filtro.getEstatutoSocialPersona()))
            builder.append(" AND estatuto_social_persona='").append(filtro.getEstatutoSocialPersona()).append("' ");
        if(filtro.getSexoPersona() != null && StringUtils.isNotBlank(filtro.getSexoPersona()))
            builder.append(" AND sexo_persona='").append(filtro.getSexoPersona()).append("' ");
        if(filtro.getPaciente() != null)
        {
            if(StringUtils.isEmpty(filtro.getPaciente().getEstadoPaciente()))
                builder.append(" AND b.estado_paciente like '%%' ");
            else
            if(filtro.getPaciente().getEstadoPaciente() != null && StringUtils.isNotBlank(filtro.getPaciente().getEstadoPaciente()))
                builder.append(" AND b.estado_paciente ='").append(filtro.getPaciente().getEstadoPaciente()).append("' ");
            if(filtro.getPaciente().getIdPaciente() != null)
                builder.append(" AND b.id_paciente ='").append(filtro.getPaciente().getIdPaciente()).append("' ");
        }
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue();
    }

    private String getFechaString(Date fechaNacimientoPersona)
    {
        int y = 1900 + fechaNacimientoPersona.getYear();
        int m = fechaNacimientoPersona.getMonth() + 1;
        int d = fechaNacimientoPersona.getDate();
        String year = "";
        String month = "";
        String date = "";
        if(m < 10)
            month = (new StringBuilder()).append("0").append(m).toString();
        else
            month = (new StringBuilder()).append(m).append("").toString();
        if(d < 10)
            date = (new StringBuilder()).append("0").append(d).toString();
        else
            date = (new StringBuilder()).append("").append(d).toString();
        year = (new StringBuilder()).append("").append(y).toString();
        return (new StringBuilder()).append(year).append("-").append(month).append("-").append(date).toString();
    }

    public List findByFilterLazyEmpleado(Persona filtro, int min, int max)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select DISTINCT  ");
        builder.append(" P.id_persona, ");
        builder.append(" P.tipo_documento_persona, ");
        builder.append(" P.documento_persona, ");
        builder.append(" P.nacionalidad_pais_id, ");
        builder.append(" P.nombre_persona, ");
        builder.append(" P.apellido_paterno_persona, ");
        builder.append(" P.apellido_materno_persona, ");
        builder.append(" P.fecha_nacimiento_persona, ");
        builder.append(" P.telefono_fijo_persona, ");
        builder.append(" P.telefono_celular_persona, ");
        builder.append(" P.email_persona, ");
        builder.append(" P.direccion_persona, ");
        builder.append(" P.residencia_distrito_id, ");
        builder.append(" P.sexo_persona, ");
        builder.append(" P.usuario, ");
        builder.append(" P.clave, ");
        builder.append(" E.id_empleado, ");
        builder.append(" E.tipo_empleado, ");
        builder.append(" E.estado_empleado ");
        builder.append("from ");
        builder.append("persona P inner join ");
        builder.append("empleado E inner join trabajo T  ");
        builder.append(" ON P.id_persona=E.persona_id ");
        builder.append(" AND E.id_empleado=T.empleado_id ");
        if(filtro.getIdPersona() != null)
            builder.append(" AND P.id_persona='").append(filtro.getIdPersona()).append("' ");
        if(filtro.getTipoDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getTipoDocumentoPersona()))
            builder.append(" AND P.tipo_documento_persona='").append(filtro.getTipoDocumentoPersona()).append("' ");
        if(filtro.getDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getDocumentoPersona()))
            builder.append(" AND P.documento_persona='").append(filtro.getDocumentoPersona()).append("' ");
        if(filtro.getNombrePersona() != null && StringUtils.isNotBlank(filtro.getNombrePersona()))
            builder.append(" AND P.nombre_persona like '%").append(filtro.getNombrePersona()).append("%' ");
        if(filtro.getApellidoPaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoPaternoPersona()))
            builder.append(" AND P.apellido_paterno_persona like '%").append(filtro.getApellidoPaternoPersona()).append("%' ");
        if(filtro.getApellidoMaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoMaternoPersona()))
            builder.append(" AND P.apellido_materno_persona like '%").append(filtro.getApellidoMaternoPersona()).append("%' ");
        if(filtro.getDireccionPersona() != null && StringUtils.isNotBlank(filtro.getDireccionPersona()))
            builder.append(" AND P.direccion_persona like '%").append(filtro.getDireccionPersona()).append("%' ");
        if(filtro.getFechaNacimientoPersona() != null && StringUtils.isNotBlank(filtro.getFechaNacimientoPersona().toString()))
            builder.append(" AND P.fecha_nacimiento_persona ='").append(filtro.getFechaNacimientoPersona()).append("' ");
        if(filtro.getSexoPersona() != null && StringUtils.isNotBlank(filtro.getSexoPersona()))
            builder.append(" AND P.sexo_persona='").append(filtro.getSexoPersona()).append("' ");
        if(filtro.getEmpleado() != null)
        {
            if(StringUtils.isEmpty(filtro.getEmpleado().getEstadoEmpleado()))
                builder.append(" AND E.estado_empleado like '%%' ");
            else
            if(filtro.getEmpleado().getEstadoEmpleado() != null && StringUtils.isNotBlank(filtro.getEmpleado().getEstadoEmpleado()))
                builder.append(" AND E.estado_empleado='").append(filtro.getEmpleado().getEstadoEmpleado()).append("' ");
            if(filtro.getEmpleado().getIdEmpleado() != null)
                builder.append(" AND E.id_empleado='").append(filtro.getEmpleado().getIdEmpleado()).append("' ");
            if(filtro.getEmpleado().getTipoEmpleado() != null && StringUtils.isNotBlank(filtro.getEmpleado().getTipoEmpleado()))
                builder.append(" AND E.tipo_empleado='").append(filtro.getEmpleado().getTipoEmpleado()).append("' ");
            if(!filtro.getEmpleado().getTrabajos().isEmpty())
            {
                Set trabajos = filtro.getEmpleado().getTrabajos();
                Iterator i$ = trabajos.iterator();
                do
                {
                    if(!i$.hasNext())
                        break;
                    Trabajo entrada = (Trabajo)i$.next();
                    if(entrada.getEspecialidad() != null && entrada.getEspecialidad().getIdEspecialidad() != null)
                        builder.append(" AND T.especialidad_id='").append(entrada.getEspecialidad().getIdEspecialidad()).append("' ");
                } while(true);
            }
        }
        List results = new ArrayList();
        EmpleadoRowMapper rowmapper = new EmpleadoRowMapper();
        rowmapper.setProcedimientoRepository(procedimientoRepository);
        rowmapper.setSubEspecialidadRepository(subEspecialidadRepository);
        rowmapper.setEspecialidadRepository(especialidadRepository);
        results = getJdbcTemplate().query(builder.toString(), rowmapper);
        return results;
    }

    public int cuentaRegistrosFilterLazyEmpleado(Persona filtro)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select count( DISTINCT P.id_persona )");
        builder.append(" FROM ");
        builder.append(" persona P inner join ");
        builder.append(" empleado E inner join trabajo T on ");
        builder.append(" P.id_persona=E.persona_id ");
        builder.append(" AND E.id_empleado=T.empleado_id ");
        if(filtro.getIdPersona() != null)
            builder.append(" AND P.id_persona='").append(filtro.getIdPersona()).append("' ");
        if(filtro.getTipoDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getTipoDocumentoPersona()))
            builder.append(" AND P.tipo_documento_persona='").append(filtro.getTipoDocumentoPersona()).append("' ");
        if(filtro.getDocumentoPersona() != null && StringUtils.isNotBlank(filtro.getDocumentoPersona()))
            builder.append(" AND P.documento_persona='").append(filtro.getDocumentoPersona()).append("' ");
        if(filtro.getNombrePersona() != null && StringUtils.isNotBlank(filtro.getNombrePersona()))
            builder.append(" AND P.nombre_persona like '%").append(filtro.getNombrePersona()).append("%' ");
        if(filtro.getApellidoPaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoPaternoPersona()))
            builder.append(" AND P.apellido_paterno_persona like '%").append(filtro.getApellidoPaternoPersona()).append("%' ");
        if(filtro.getApellidoMaternoPersona() != null && StringUtils.isNotBlank(filtro.getApellidoMaternoPersona()))
            builder.append(" AND P.apellido_materno_persona like '%").append(filtro.getApellidoMaternoPersona()).append("%' ");
        if(filtro.getDireccionPersona() != null && StringUtils.isNotBlank(filtro.getDireccionPersona()))
            builder.append(" AND P.direccion_persona like '%").append(filtro.getDireccionPersona()).append("%' ");
        if(filtro.getFechaNacimientoPersona() != null && StringUtils.isNotBlank(filtro.getFechaNacimientoPersona().toString()))
            builder.append(" AND P.fecha_nacimiento_persona ='").append(filtro.getFechaNacimientoPersona()).append("' ");
        if(filtro.getSexoPersona() != null && StringUtils.isNotBlank(filtro.getSexoPersona()))
            builder.append(" AND P.sexo_persona='").append(filtro.getSexoPersona()).append("' ");
        if(filtro.getEmpleado() != null)
        {
            if(StringUtils.isEmpty(filtro.getEmpleado().getEstadoEmpleado()))
                builder.append(" AND E.estado_empleado like '%%' ");
            else
            if(filtro.getEmpleado().getEstadoEmpleado() != null && StringUtils.isNotBlank(filtro.getEmpleado().getEstadoEmpleado()))
                builder.append(" AND E.estado_empleado='").append(filtro.getEmpleado().getEstadoEmpleado()).append("' ");
            if(filtro.getEmpleado().getIdEmpleado() != null)
                builder.append(" AND E.id_empleado='").append(filtro.getEmpleado().getIdEmpleado()).append("' ");
            if(filtro.getEmpleado().getTipoEmpleado() != null && StringUtils.isNotBlank(filtro.getEmpleado().getTipoEmpleado()))
                builder.append(" AND E.tipo_empleado='").append(filtro.getEmpleado().getTipoEmpleado()).append("' ");
            if(!filtro.getEmpleado().getTrabajos().isEmpty())
            {
                Set trabajos = filtro.getEmpleado().getTrabajos();
                Iterator i$ = trabajos.iterator();
                do
                {
                    if(!i$.hasNext())
                        break;
                    Trabajo entrada = (Trabajo)i$.next();
                    if(entrada.getEspecialidad() != null && entrada.getEspecialidad().getIdEspecialidad() != null)
                        builder.append(" AND T.especialidad_id='").append(entrada.getEspecialidad().getIdEspecialidad()).append("' ");
                } while(true);
            }
        }
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue();
    }

    public Persona acceder(String login, String password)
    {
        Search s = new Search();
        s.addFilterEqual("usuario", login);
        s.addFilterEqual("clave", password);
        return (Persona)searchUnique(s);
    }

    private JdbcTemplate getJdbcTemplate()
    {
        return new JdbcTemplate(dataSource);
    }

    @Autowired
    DataSource dataSource;
    @Autowired
    ProcedimientoRepository procedimientoRepository;
    @Autowired
    SubEspecialidadRepository subEspecialidadRepository;
    @Autowired
    EspecialidadRepository especialidadRepository;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/PersonaRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
