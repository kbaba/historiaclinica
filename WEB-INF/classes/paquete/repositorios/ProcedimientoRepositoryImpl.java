// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:40:44 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.mapper.DTOMapperService;
import infraestructura.mapper.ProcedimientoRowMapper;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import paquete.entities.Procedimiento;
import paquete.entities.SubEspecialidad;

// Referenced classes of package paquete.repositorios:
//            ProcedimientoRepository

@Repository
public class ProcedimientoRepositoryImpl extends BaseDAO
    implements ProcedimientoRepository
{

    public ProcedimientoRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE ENFERMEDAD *****************\n");
    }

    public void persistirProcedimiento(Procedimiento procedimiento)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List findByFilter(Procedimiento filtro)
    {
        Search parameter = new Search();
        if(filtro.getIdProcedimiento() != null)
            parameter.addFilterEqual("idProcedimiento", filtro.getIdProcedimiento());
        if(filtro.getNombreProcedimiento() != null && StringUtils.isNotBlank(filtro.getNombreProcedimiento()))
            parameter.addFilterLike("nombreProcedimiento", (new StringBuilder()).append("%").append(filtro.getNombreProcedimiento()).append("%").toString());
        if(filtro.getSubEspecialidad() != null)
            parameter.addFilterEqual("subEspecialidad.idSubEspecialidad", filtro.getSubEspecialidad().getIdSubEspecialidad());
        if(filtro.getEstadoProcedimiento() != null && StringUtils.isNotBlank(filtro.getEstadoProcedimiento()))
            parameter.addFilterEqual("estadoProcedimiento", filtro.getEstadoProcedimiento());
        return search(parameter);
    }

    @Transactional
    public boolean existeProcedimientoRepetido(String nombre)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("nombreProcedimiento", nombre);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public boolean existeProcedimientoRepetido(String nombre, String idProcedimiento)
    {
        Search parameter = new Search();
        parameter.addFilterNotEqual("idProcedimiento", idProcedimiento);
        parameter.addFilterEqual("nombreProcedimiento", nombre);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public List findBySubEspecialidad(Integer id)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" P.id_procedimiento\"idProcedimiento\",");
        sql.append(" P.nombre_procedimiento \"nombreProcedimiento\", ");
        sql.append(" P.estado_procedimiento\"estadoProcedimiento\" ");
        sql.append(" FROM PROCEDIMIENTO p ");
        sql.append(" WHERE ");
        sql.append(" sub_especialidad_id='").append(id).append("'");
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(paquete/entities/Procedimiento));
        items = query.list();
        return items;
    }

    public List completeProcedimiento(String nombreProcedimiento, Integer idSubEspecialidad)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select * from procedimiento where id_procedimiento=id_procedimiento ");
        if(StringUtils.isNotBlank(nombreProcedimiento))
            builder.append("and lower(nombre_procedimiento) like '%").append(nombreProcedimiento).append("%' ");
        if(idSubEspecialidad != null && idSubEspecialidad.intValue() != 0)
            builder.append("and sub_especialidad_id =").append(idSubEspecialidad);
        builder.append(" order by nombre_procedimiento");
        List results = new ArrayList();
        results = getJdbcTemplate().query(builder.toString(), new ProcedimientoRowMapper());
        return results;
    }

    private JdbcTemplate getJdbcTemplate()
    {
        return new JdbcTemplate(dataSource);
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/ProcedimientoRepositoryImpl);
    @Autowired
    DataSource dataSource;
    @Autowired
    private DTOMapperService mapperService;

}
