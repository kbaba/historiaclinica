// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:37:04 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EspecialidadRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.EspecialidadDTO;
import java.util.List;
import paquete.entities.Especialidad;

public interface EspecialidadRepository
    extends GenericDAO
{

    public abstract void persistEspecialidad(Especialidad especialidad);

    public abstract List findByFilter(Especialidad especialidad);

    public abstract List completeEspecialidad(String s);

    public abstract List buscarHijosPorEspecialidad(EspecialidadDTO especialidaddto);

    public abstract boolean esNombreRepetido(String s, String s1);

    public abstract boolean esNombreRepetido(String s);
}
