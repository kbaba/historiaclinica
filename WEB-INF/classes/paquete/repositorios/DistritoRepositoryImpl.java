// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:43 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   DistritoRepositoryImpl.java

package paquete.repositorios;

import infraestructura.dto.DistritoDTO;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

// Referenced classes of package paquete.repositorios:
//            DistritoRepository

@Repository
public class DistritoRepositoryImpl extends BaseDAO
    implements DistritoRepository
{

    public DistritoRepositoryImpl()
    {
    }

    public List findByProvincia(String pais_id, String departamento_id, String provincia_id)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" C.pais_id \"paisId\",");
        sql.append(" C.departamento_id \"departamentoId\", ");
        sql.append(" C.provincia_id \"provinciaId\", ");
        sql.append(" C.id_distrito\"idDistrito\", ");
        sql.append(" C.nombre_distrito \"nombreDistrito\" ");
        sql.append(" FROM DISTRITO C ");
        if(StringUtils.isNotBlank(pais_id))
            sql.append("WHERE C.pais_id= '").append(pais_id).append("'");
        if(StringUtils.isNotBlank(departamento_id))
        {
            sql.append(" AND ");
            sql.append(" C.departamento_id= '").append(departamento_id).append("'");
        }
        if(StringUtils.isNotBlank(provincia_id))
        {
            sql.append(" AND ");
            sql.append(" C.provincia_id = '").append(provincia_id).append("'");
        }
        sql.append(" order by C.nombre_distrito ");
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/DistritoDTO));
        items = query.list();
        return items;
    }

    public DistritoDTO findByCode(String pais_id, String departamento_id, String provincia_id, String id_distrito)
    {
        List state = null;
        DistritoDTO nuevoProvinceDTO = new DistritoDTO();
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" C.pais_id \"paisId\",");
        sql.append(" C.departamento_id \"departamentoId\", ");
        sql.append(" C.provincia_id \"provinciaId\", ");
        sql.append(" C.id_distrito\"idDistrito\", ");
        sql.append(" C.nombre_distrito \"nombreDistrito\" ");
        sql.append(" FROM DISTRITO C ");
        if(StringUtils.isNotBlank(pais_id))
            sql.append("WHERE C.pais_id= '").append(pais_id).append("'");
        if(StringUtils.isNotBlank(departamento_id))
        {
            sql.append(" AND ");
            sql.append(" C.departamento_id= '").append(departamento_id).append("'");
        }
        if(StringUtils.isNotBlank(provincia_id))
        {
            sql.append(" AND ");
            sql.append(" C.provincia_id = '").append(provincia_id).append("'");
        }
        if(StringUtils.isNotBlank(id_distrito))
        {
            sql.append(" AND ");
            sql.append(" C.id_distrito= '").append(id_distrito).append("'");
        }
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/DistritoDTO));
        state = query.list();
        DistritoDTO as;
        for(Iterator i$ = state.iterator(); i$.hasNext(); nuevoProvinceDTO.setNombreDistrito(as.getNombreDistrito()))
        {
            as = (DistritoDTO)i$.next();
            nuevoProvinceDTO.setPaisId(as.getPaisId());
            nuevoProvinceDTO.setDepartamentoId(as.getDepartamentoId());
            nuevoProvinceDTO.setProvinciaId(as.getProvinciaId());
            nuevoProvinceDTO.setIdDistrito(as.getIdDistrito());
        }

        return nuevoProvinceDTO;
    }
}
