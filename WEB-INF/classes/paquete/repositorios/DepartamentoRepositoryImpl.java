// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:36 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   DepartamentoRepositoryImpl.java

package paquete.repositorios;

import infraestructura.dto.DepartamentoDTO;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;

// Referenced classes of package paquete.repositorios:
//            DepartamentoRepository

@Repository
public class DepartamentoRepositoryImpl extends BaseDAO
    implements DepartamentoRepository
{

    public DepartamentoRepositoryImpl()
    {
    }

    @Transactional
    public List findByName(String name)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" A.pais_id \"paisId\",");
        sql.append(" A.id_departamento\"idDepartamento\", ");
        sql.append(" A.nombre_departamento\"nombreDepartamento\" ");
        sql.append(" FROM DEPARTAMENTO A ");
        if(StringUtils.isNotBlank(name))
        {
            sql.append(" WHERE ");
            sql.append(" UPPER(A.nombre_departamento) LIKE '%").append(name.toUpperCase()).append("%'");
        }
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/DepartamentoDTO));
        items = query.list();
        return items;
    }

    @Transactional
    public DepartamentoDTO findByCode(String pais_id, String id_departamento)
    {
        List state = null;
        DepartamentoDTO nuevoDepartamentoDTO = new DepartamentoDTO();
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" A.pais_id \"paisId\",");
        sql.append(" A.id_departamento\"idDepartamento\", ");
        sql.append(" A.nombre_departamento\"nombreDepartamento\" ");
        sql.append(" FROM DEPARTAMENTO A ");
        if(StringUtils.isNotBlank(pais_id))
        {
            sql.append(" WHERE A.pais_id = '").append(pais_id).append("' ");
            sql.append(" AND A.id_departamento = '").append(id_departamento).append("' ");
        }
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/DepartamentoDTO));
        state = query.list();
        DepartamentoDTO as;
        for(Iterator i$ = state.iterator(); i$.hasNext(); nuevoDepartamentoDTO.setNombreDepartamento(as.getNombreDepartamento()))
        {
            as = (DepartamentoDTO)i$.next();
            nuevoDepartamentoDTO.setPaisId(as.getPaisId());
            nuevoDepartamentoDTO.setIdDepartamento(as.getIdDepartamento());
        }

        return nuevoDepartamentoDTO;
    }

    @Transactional
    public List findByPaisId(String idPais)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" A.pais_id \"paisId\",");
        sql.append(" A.id_departamento\"idDepartamento\", ");
        sql.append(" A.nombre_departamento\"nombreDepartamento\" ");
        sql.append(" FROM DEPARTAMENTO A ");
        if(StringUtils.isNotBlank(idPais))
        {
            sql.append(" WHERE ");
            sql.append(" A.pais_id ='").append(idPais).append("'");
        }
        sql.append(" order by A.nombre_departamento");
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/DepartamentoDTO));
        items = query.list();
        return items;
    }
}
