// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:38:41 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   HistoriaRepositoryImpl.java

package paquete.repositorios;

import infraestructura.dto.HistoriaDTO;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.hibernate.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Referenced classes of package paquete.repositorios:
//            HistoriaRepository, SubEspecialidadRepository

@Repository
public class HistoriaRepositoryImpl extends BaseDAO
    implements HistoriaRepository
{

    public HistoriaRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE AREA /*****************\n");
    }

    public HistoriaDTO traeHistoriaPorPersonaId(Integer idPersona)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" select ");
        builder.append(" H.id_historia \"idHistoria\", ");
        builder.append(" H.paciente_id \"pacienteId\" ");
        builder.append(" from historia H ");
        builder.append(" inner join persona P ");
        builder.append(" inner join paciente A ");
        builder.append(" WHERE P.id_persona= A.persona_id ");
        builder.append(" AND A.id_paciente= H.paciente_id ");
        builder.append(" AND P.id_persona=").append(idPersona);
        Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(builder.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/HistoriaDTO));
        HistoriaDTO historia = (HistoriaDTO)query.uniqueResult();
        return historia;
    }

    @Autowired
    DataSource dataSource;
    @Autowired
    SubEspecialidadRepository subAreaRepository;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/HistoriaRepositoryImpl);

}
