// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:40:24 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PreguntaRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.mapper.DTOMapperService;
import infraestructura.mapper.PreguntaRowMapper;
import infraestructura.persistence.BaseDAO;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import paquete.entities.*;

// Referenced classes of package paquete.repositorios:
//            PreguntaRepository, AlternativaRepository, ProcedimientoRepository, SubEspecialidadRepository, 
//            EspecialidadRepository

@Repository
public class PreguntaRepositoryImpl extends BaseDAO
    implements PreguntaRepository
{

    public PreguntaRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE PERSONA /*****************\n");
    }

    @Transactional
    public void guardarPregunta(Pregunta pregunta)
    {
        if(pregunta.getIdPregunta() == null)
            em().persist(pregunta);
        else
            em().merge(pregunta);
    }

    @Transactional
    public List findByFilter(Pregunta filtro, int indiceInicial, int indiceFinal, boolean limitarResultados)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select  ");
        builder.append(" P.id_pregunta, ");
        builder.append(" P.descripcion_pregunta, ");
        builder.append(" P.tipo_pregunta, ");
        builder.append(" P.tipo_respuesta, ");
        builder.append(" P.procedimiento_id, ");
        builder.append(" P.sub_especialidad_id, ");
        builder.append(" P.especialidad_id, ");
        builder.append(" P.quien_pregunta, ");
        builder.append(" P.estado_pregunta  ");
        builder.append(" from PREGUNTA  P ");
        builder.append(" where P.id_pregunta=P.id_pregunta");
        if(filtro.getIdPregunta() != null)
            builder.append(" AND P.id_pregunta='").append(filtro.getIdPregunta()).append("' ");
        if(filtro.getDescripcionPregunta() != null && StringUtils.isNotBlank(filtro.getDescripcionPregunta()))
            builder.append(" AND P.descripcion_pregunta like '%").append(filtro.getDescripcionPregunta()).append("%' ");
        if(filtro.getTipoPregunta() != null && StringUtils.isNotBlank(filtro.getTipoPregunta()))
            builder.append(" AND P.tipo_pregunta='").append(filtro.getTipoPregunta()).append("' ");
        if(filtro.getTipoRespuesta() != null && StringUtils.isNotBlank(filtro.getTipoRespuesta()))
            builder.append(" AND P.tipo_respuesta='").append(filtro.getTipoRespuesta()).append("' ");
        if(filtro.getQuienPregunta() != null && StringUtils.isNotBlank(filtro.getQuienPregunta()))
            builder.append(" AND P.quien_pregunta='").append(filtro.getQuienPregunta()).append("' ");
        if(filtro.getProcedimiento() != null)
        {
            if(filtro.getProcedimiento().getIdProcedimiento() != null)
                builder.append(" AND P.procedimiento_id='").append(filtro.getProcedimiento().getIdProcedimiento()).append("' ");
        } else
        {
            builder.append(" AND P.procedimiento_id is NULL ");
        }
        if(filtro.getSubEspecialidad() != null)
        {
            if(filtro.getSubEspecialidad().getIdSubEspecialidad() != null)
                builder.append(" AND P.sub_especialidad_id='").append(filtro.getSubEspecialidad().getIdSubEspecialidad()).append("' ");
        } else
        {
            builder.append(" AND P.sub_especialidad_id is NULL ");
        }
        if(filtro.getEspecialidad() != null)
        {
            if(filtro.getEspecialidad().getIdEspecialidad() != null)
                builder.append(" AND P.especialidad_id='").append(filtro.getEspecialidad().getIdEspecialidad()).append("' ");
        } else
        {
            builder.append(" AND P.especialidad_id is NULL ");
        }
        if(filtro.getEstadoPregunta() != null && StringUtils.isNotBlank(filtro.getEstadoPregunta()))
            builder.append(" AND P.estado_pregunta='").append(filtro.getEstadoPregunta()).append("' ");
        if(limitarResultados)
            builder.append(" LIMIT ").append(indiceInicial).append(",  ").append(indiceFinal);
        List results = new ArrayList();
        PreguntaRowMapper rowmapper = new PreguntaRowMapper();
        rowmapper.setAlternativaRepository(alternativaRepository);
        rowmapper.setProcedimientoRepository(procedimientoRepository);
        rowmapper.setSubEspecialidadRepository(subEspecialidadRepository);
        rowmapper.setEspecialidadRepository(especialidadRepository);
        results = getJdbcTemplate().query(builder.toString(), rowmapper);
        return results;
    }

    public int cuentaRegistrosFilterLazy(Pregunta filtro)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(id_pregunta) ");
        builder.append(" from pregunta as P ");
        builder.append(" WHERE  P.id_pregunta = P.id_pregunta");
        if(filtro.getIdPregunta() != null)
            builder.append((new StringBuilder()).append(" AND P.id_pregunta='").append(filtro.getIdPregunta()).append("' ").toString());
        if(filtro.getDescripcionPregunta() != null && StringUtils.isNotBlank(filtro.getDescripcionPregunta()))
            builder.append((new StringBuilder()).append(" AND P.descripcion_pregunta like '%").append(filtro.getDescripcionPregunta()).append("%' ").toString());
        if(filtro.getTipoPregunta() != null && StringUtils.isNotBlank(filtro.getTipoPregunta()))
            builder.append((new StringBuilder()).append(" AND P.tipo_pregunta='").append(filtro.getTipoPregunta()).append("' ").toString());
        if(filtro.getTipoRespuesta() != null && StringUtils.isNotBlank(filtro.getTipoRespuesta()))
            builder.append((new StringBuilder()).append(" AND P.tipo_respuesta='").append(filtro.getTipoRespuesta()).append("' ").toString());
        if(filtro.getQuienPregunta() != null && StringUtils.isNotBlank(filtro.getQuienPregunta()))
            builder.append((new StringBuilder()).append(" AND P.quien_pregunta='").append(filtro.getQuienPregunta()).append("' ").toString());
        if(filtro.getProcedimiento() != null)
        {
            if(filtro.getProcedimiento().getIdProcedimiento() != null)
                builder.append((new StringBuilder()).append(" AND P.procedimiento_id='").append(filtro.getProcedimiento().getIdProcedimiento()).append("' ").toString());
        } else
        if(filtro.getSubEspecialidad() != null)
        {
            if(filtro.getSubEspecialidad().getIdSubEspecialidad() != null)
                builder.append((new StringBuilder()).append(" AND P.sub_especialidad_id='").append(filtro.getSubEspecialidad().getIdSubEspecialidad()).append("' ").toString());
        } else
        if(filtro.getEspecialidad() != null && filtro.getEspecialidad().getIdEspecialidad() != null)
            builder.append((new StringBuilder()).append(" AND P.especialidad_id='").append(filtro.getEspecialidad().getIdEspecialidad()).append("' ").toString());
        if(filtro.getEstadoPregunta() != null && StringUtils.isNotBlank(filtro.getEstadoPregunta()))
            builder.append((new StringBuilder()).append(" AND P.estado_pregunta='").append(filtro.getEstadoPregunta()).append("' ").toString());
        if(filtro.getProcedimiento() == null && filtro.getSubEspecialidad() == null && filtro.getEspecialidad() == null)
        {
            builder.append(" AND P.procedimiento_id is NULL ");
            builder.append(" AND P.sub_especialidad_id is NULL ");
            builder.append(" AND P.especialidad_id is NULL ");
        }
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue();
    }

    public int cuentaRegistrosFilterLazy(Pregunta filtro, Alternativa alternativa)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select count( DISTINCT P.id_pregunta ) ");
        builder.append(" from PREGUNTA  P ");
        builder.append(" inner join  ALTERNATIVA A");
        builder.append(" where P.id_pregunta=A.pregunta_id");
        if(filtro.getIdPregunta() != null)
            builder.append((new StringBuilder()).append(" AND P.id_pregunta='").append(filtro.getIdPregunta()).append("' ").toString());
        if(filtro.getDescripcionPregunta() != null && StringUtils.isNotBlank(filtro.getDescripcionPregunta()))
            builder.append((new StringBuilder()).append(" AND P.descripcion_pregunta like '%").append(filtro.getDescripcionPregunta()).append("%' ").toString());
        if(filtro.getTipoPregunta() != null && StringUtils.isNotBlank(filtro.getTipoPregunta()))
            builder.append((new StringBuilder()).append(" AND P.tipo_pregunta='").append(filtro.getTipoPregunta()).append("' ").toString());
        if(filtro.getTipoRespuesta() != null && StringUtils.isNotBlank(filtro.getTipoRespuesta()))
            builder.append((new StringBuilder()).append(" AND P.tipo_respuesta='").append(filtro.getTipoRespuesta()).append("' ").toString());
        if(filtro.getQuienPregunta() != null && StringUtils.isNotBlank(filtro.getQuienPregunta()))
            builder.append((new StringBuilder()).append(" AND P.quien_pregunta='").append(filtro.getQuienPregunta()).append("' ").toString());
        if(filtro.getProcedimiento() != null && filtro.getProcedimiento().getIdProcedimiento() != null)
            builder.append((new StringBuilder()).append(" AND P.procedimiento_id='").append(filtro.getProcedimiento().getIdProcedimiento()).append("' ").toString());
        if(filtro.getEspecialidad() != null && filtro.getEspecialidad().getIdEspecialidad() != null)
            builder.append((new StringBuilder()).append(" AND P.especialidad_id='").append(filtro.getEspecialidad().getIdEspecialidad()).append("' ").toString());
        if(filtro.getSubEspecialidad() != null && filtro.getSubEspecialidad().getIdSubEspecialidad() != null)
            builder.append((new StringBuilder()).append(" AND P.sub_especialidad_id='").append(filtro.getSubEspecialidad().getIdSubEspecialidad()).append("' ").toString());
        if(filtro.getEstadoPregunta() != null && StringUtils.isNotBlank(filtro.getEstadoPregunta()))
            builder.append((new StringBuilder()).append(" AND P.estado_pregunta='").append(filtro.getEstadoPregunta()).append("' ").toString());
        if(filtro.getProcedimiento() == null && filtro.getSubEspecialidad() == null && filtro.getEspecialidad() == null)
        {
            builder.append(" AND P.procedimiento_id is NULL ");
            builder.append(" AND P.sub_especialidad_id is NULL ");
            builder.append(" AND P.especialidad_id is NULL ");
        }
        if(alternativa.getDescripcionAlternativa() != null && StringUtils.isNotBlank(alternativa.getDescripcionAlternativa()))
            builder.append((new StringBuilder()).append(" AND A.descripcion_alternativa like '%").append(alternativa.getDescripcionAlternativa()).append("%' ").toString());
        if(alternativa.getEstadoAlternativa() != null && StringUtils.isNotBlank(alternativa.getEstadoAlternativa()))
            builder.append((new StringBuilder()).append(" AND A.estado_alternativa='").append(alternativa.getEstadoAlternativa()).append("' ").toString());
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue();
    }

    public void eliminarPregunta(Pregunta pregunta)
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List findByFilter(Pregunta filtro, Alternativa alternativa, int indiceInicial, int indiceFinal)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select DISTINCT ");
        builder.append(" P.id_pregunta, ");
        builder.append(" P.descripcion_pregunta, ");
        builder.append(" P.tipo_pregunta, ");
        builder.append(" P.tipo_respuesta, ");
        builder.append(" P.procedimiento_id, ");
        builder.append(" P.sub_especialidad_id, ");
        builder.append(" P.especialidad_id, ");
        builder.append(" P.quien_pregunta, ");
        builder.append(" P.estado_pregunta  ");
        builder.append(" from PREGUNTA  P ");
        builder.append(" inner join  ALTERNATIVA A");
        builder.append(" where P.id_pregunta=A.pregunta_id");
        if(filtro.getIdPregunta() != null)
            builder.append((new StringBuilder()).append(" AND P.id_pregunta='").append(filtro.getIdPregunta()).append("' ").toString());
        if(filtro.getDescripcionPregunta() != null && StringUtils.isNotBlank(filtro.getDescripcionPregunta()))
            builder.append((new StringBuilder()).append(" AND P.descripcion_pregunta like '%").append(filtro.getDescripcionPregunta()).append("%' ").toString());
        if(filtro.getTipoPregunta() != null && StringUtils.isNotBlank(filtro.getTipoPregunta()))
            builder.append((new StringBuilder()).append(" AND P.tipo_pregunta='").append(filtro.getTipoPregunta()).append("' ").toString());
        if(filtro.getTipoRespuesta() != null && StringUtils.isNotBlank(filtro.getTipoRespuesta()))
            builder.append((new StringBuilder()).append(" AND P.tipo_respuesta='").append(filtro.getTipoRespuesta()).append("' ").toString());
        if(filtro.getQuienPregunta() != null && StringUtils.isNotBlank(filtro.getQuienPregunta()))
            builder.append((new StringBuilder()).append(" AND P.quien_pregunta='").append(filtro.getQuienPregunta()).append("' ").toString());
        if(filtro.getProcedimiento() != null && filtro.getProcedimiento().getIdProcedimiento() != null)
            builder.append((new StringBuilder()).append(" AND P.procedimiento_id='").append(filtro.getProcedimiento().getIdProcedimiento()).append("' ").toString());
        if(filtro.getEspecialidad() != null && filtro.getEspecialidad().getIdEspecialidad() != null)
            builder.append((new StringBuilder()).append(" AND P.especialidad_id='").append(filtro.getEspecialidad().getIdEspecialidad()).append("' ").toString());
        if(filtro.getSubEspecialidad() != null && filtro.getSubEspecialidad().getIdSubEspecialidad() != null)
            builder.append((new StringBuilder()).append(" AND P.sub_especialidad_id='").append(filtro.getSubEspecialidad().getIdSubEspecialidad()).append("' ").toString());
        if(filtro.getEstadoPregunta() != null && StringUtils.isNotBlank(filtro.getEstadoPregunta()))
            builder.append((new StringBuilder()).append(" AND P.estado_pregunta='").append(filtro.getEstadoPregunta()).append("' ").toString());
        if(filtro.getProcedimiento() == null && filtro.getSubEspecialidad() == null && filtro.getEspecialidad() == null)
        {
            builder.append(" AND P.procedimiento_id is NULL ");
            builder.append(" AND P.sub_especialidad_id is NULL ");
            builder.append(" AND P.especialidad_id is NULL ");
        }
        if(alternativa.getDescripcionAlternativa() != null && StringUtils.isNotBlank(alternativa.getDescripcionAlternativa()))
            builder.append((new StringBuilder()).append(" AND A.descripcion_alternativa like '%").append(alternativa.getDescripcionAlternativa()).append("%' ").toString());
        if(alternativa.getEstadoAlternativa() != null && StringUtils.isNotBlank(alternativa.getEstadoAlternativa()))
            builder.append((new StringBuilder()).append(" AND A.estado_alternativa='").append(alternativa.getEstadoAlternativa()).append("' ").toString());
        builder.append((new StringBuilder()).append(" LIMIT ").append(indiceInicial).append(",  ").append(indiceFinal).toString());
        List results = new ArrayList();
        PreguntaRowMapper rowmapper = new PreguntaRowMapper();
        rowmapper.setAlternativaRepository(alternativaRepository);
        rowmapper.setProcedimientoRepository(procedimientoRepository);
        rowmapper.setSubEspecialidadRepository(subEspecialidadRepository);
        rowmapper.setEspecialidadRepository(especialidadRepository);
        results = getJdbcTemplate().query(builder.toString(), rowmapper);
        return results;
    }

    private JdbcTemplate getJdbcTemplate()
    {
        return new JdbcTemplate(dataSource);
    }

    public boolean esPreguntaRepetida(String descripcion, int idPregunta)
    {
        Search parameter = new Search();
        parameter.addFilterNotEqual("idPregunta", Integer.valueOf(idPregunta));
        parameter.addFilterEqual("descripcionPregunta", descripcion);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    public boolean esPreguntaRepetida(String descripcion)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("descripcionPregunta", descripcion);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Autowired
    AlternativaRepository alternativaRepository;
    @Autowired
    ProcedimientoRepository procedimientoRepository;
    @Autowired
    SubEspecialidadRepository subEspecialidadRepository;
    @Autowired
    EspecialidadRepository especialidadRepository;
    @Autowired
    DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/PreguntaRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
