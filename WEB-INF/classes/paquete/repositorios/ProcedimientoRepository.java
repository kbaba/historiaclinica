// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:40:36 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import java.util.List;
import paquete.entities.Procedimiento;

public interface ProcedimientoRepository
    extends GenericDAO
{

    public abstract void persistirProcedimiento(Procedimiento procedimiento);

    public abstract List findByFilter(Procedimiento procedimiento);

    public abstract boolean existeProcedimientoRepetido(String s, String s1);

    public abstract boolean existeProcedimientoRepetido(String s);

    public abstract List findBySubEspecialidad(Integer integer);

    public abstract List completeProcedimiento(String s, Integer integer);
}
