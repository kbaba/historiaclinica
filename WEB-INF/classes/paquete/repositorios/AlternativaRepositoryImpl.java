// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:20 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AlternativaRepositoryImpl.java

package paquete.repositorios;

import infraestructura.mapper.DTOMapperService;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.util.List;
import javax.persistence.EntityManager;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Alternativa;

// Referenced classes of package paquete.repositorios:
//            AlternativaRepository

@Repository
public class AlternativaRepositoryImpl extends BaseDAO
    implements AlternativaRepository
{

    public AlternativaRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE PERSONA /*****************\n");
    }

    public List getAlternativasXPreguntaId(int preguntaId)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" A.id_alternativa \"idAlternativa\",");
        sql.append(" A.descripcion_alternativa \"descripcionAlternativa\", ");
        sql.append(" A.estado_alternativa \"estadoAlternativa\" ");
        sql.append(" FROM Alternativa A ");
        sql.append("WHERE A.pregunta_id= '").append(preguntaId).append("'");
        sql.append(" order by A.descripcion_alternativa ");
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(paquete/entities/Alternativa));
        items = query.list();
        return items;
    }

    @Transactional
    public void borraRegistroAlternativa(int alternativaId)
    {
        StringBuffer sql = new StringBuffer();
        sql.append((new StringBuilder()).append(" delete from ALTERNATIVA where id_alternativa='").append(alternativaId).append("'").toString());
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString());
        query.executeUpdate();
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/AlternativaRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
