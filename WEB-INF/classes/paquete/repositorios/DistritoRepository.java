// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:39 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   DistritoRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.DistritoDTO;
import java.util.List;

public interface DistritoRepository
    extends GenericDAO
{

    public abstract List findByProvincia(String s, String s1, String s2);

    public abstract DistritoDTO findByCode(String s, String s1, String s2, String s3);
}
