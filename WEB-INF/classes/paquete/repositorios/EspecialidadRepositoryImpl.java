// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:37:15 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EspecialidadRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.dto.EspecialidadDTO;
import infraestructura.mapper.EspecialidadRowMapper;
import infraestructura.persistence.BaseDAO;
import java.math.BigInteger;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import paquete.entities.Especialidad;

// Referenced classes of package paquete.repositorios:
//            EspecialidadRepository, SubEspecialidadRepository

@Repository
public class EspecialidadRepositoryImpl extends BaseDAO
    implements EspecialidadRepository
{

    public EspecialidadRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE AREA /*****************\n");
    }

    public void persistEspecialidad(Especialidad especialidad)
    {
        if(especialidad.getIdEspecialidad() == null)
            em().persist(especialidad);
        else
            em().merge(especialidad);
    }

    public List findByFilter(Especialidad especialidad)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT  `id_especialidad` ,  `nombre_especialidad` ,  `estado_especialidad` ");
        builder.append("FROM especialidad WHERE id_especialidad=id_especialidad ");
        if(StringUtils.isNotBlank(especialidad.getNombreEspecialidad()))
            builder.append("and LOWER( nombre_especialidada ) LIKE '%").append(especialidad.getNombreEspecialidad()).append("%'");
        if(StringUtils.isNotBlank(especialidad.getEstadoEspecialidad()))
            builder.append("and estado_especialidad='").append(especialidad.getEstadoEspecialidad()).append("'");
        builder.append(" order by nombre_especialidad");
        List results = new ArrayList();
        results = getJdbcTemplate().query(builder.toString(), new EspecialidadRowMapper());
        return results;
    }

    private JdbcTemplate getJdbcTemplate()
    {
        return new JdbcTemplate(dataSource);
    }

    public List completeEspecialidad(String nombreEspecialidad)
    {
        StringBuilder builder = new StringBuilder();
        if(StringUtils.isNotBlank(nombreEspecialidad))
            builder.append("select * from especialidad where lower(nombre_especialidad) like '%").append(nombreEspecialidad).append("%' order by nombre_especialidad");
        else
            builder.append("select * from especialidad order by nombre_especialidad");
        List results = new ArrayList();
        results = getJdbcTemplate().query(builder.toString(), new EspecialidadRowMapper());
        return results;
    }

    public List buscarHijosPorEspecialidad(EspecialidadDTO dto)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select count(*) from sub_especialidad where especialidad_id=").append(dto.getIdEspecialidad());
        builder.append(" union all select count(*) from procedimiento p inner join sub_especialidad se where se.id_sub_especialidad=p.sub_especialidad_id ");
        builder.append(" and se.especialidad_id=").append(dto.getIdEspecialidad());
        Query query = em().createNativeQuery(builder.toString());
        List result = query.getResultList();
        List numeroDeHijos = new ArrayList();
        BigInteger hijo;
        for(Iterator i$ = result.iterator(); i$.hasNext(); numeroDeHijos.add(hijo.toString()))
            hijo = (BigInteger)i$.next();

        return numeroDeHijos;
    }

    @Transactional
    public boolean esNombreRepetido(String nombreEspecialidad, String idEspecialidad)
    {
        Search parameter = new Search();
        parameter.addFilterNotEqual("idEspecialidad", idEspecialidad);
        parameter.addFilterEqual("nombreEspecialidad", nombreEspecialidad);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public boolean esNombreRepetido(String nombre)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("nombreEspecialidad", nombre);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Autowired
    DataSource dataSource;
    @Autowired
    SubEspecialidadRepository subAreaRepository;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/EspecialidadRepositoryImpl);

}
