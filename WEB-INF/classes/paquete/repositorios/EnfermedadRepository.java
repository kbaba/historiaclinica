// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:55 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import infraestructura.dto.EnfermedadDTO;
import java.util.List;
import paquete.entities.Enfermedad;

public interface EnfermedadRepository
    extends GenericDAO
{

    public abstract void persistirEnfermedad(Enfermedad enfermedad);

    public abstract List findByFilter(Enfermedad enfermedad, Enfermedad enfermedad1, int i, int j);

    public abstract List findByFilter(Enfermedad enfermedad);

    public abstract List findByFilterNoCap(Enfermedad enfermedad);

    public abstract List cargarGruposCapitulos(String s);

    public abstract int cuentaRegistros(EnfermedadDTO enfermedaddto, Enfermedad enfermedad);

    public abstract List traeEnfermedadesAtencion(Integer integer);
}
