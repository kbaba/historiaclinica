// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:39:48 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PaisRepositoryImpl.java

package paquete.repositorios;

import infraestructura.dto.PaisDTO;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.io.PrintStream;
import java.util.List;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.hibernate.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Referenced classes of package paquete.repositorios:
//            PaisRepository

@Repository
public class PaisRepositoryImpl extends BaseDAO
    implements PaisRepository
{

    public PaisRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE AREA /*****************\n");
    }

    public List getAllPaises()
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" P.id_pais \"idPais\" , ");
        sql.append(" P.nombre_pais  \"nombrePais\" ");
        sql.append(" FROM pais as P order by p.nombre_pais");
        System.out.println(sql);
        Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/PaisDTO));
        items = query.list();
        return items;
    }

    public PaisDTO getPais(String idPais)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" P.id_pais \"idPais\" , ");
        sql.append(" P.nombre_pais  \"nombrePais\" ");
        sql.append(" FROM pais as P ");
        sql.append(" WHERE id_pais ='").append(idPais).append("';");
        System.out.println(sql);
        Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/PaisDTO));
        items = query.list();
        return (PaisDTO)items.get(0);
    }

    @Autowired
    DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/PaisRepositoryImpl);

}
