// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:41:19 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadRepository.java

package paquete.repositorios;

import com.googlecode.genericdao.dao.jpa.GenericDAO;
import java.util.List;
import paquete.entities.SubEspecialidad;

public interface SubEspecialidadRepository
    extends GenericDAO
{

    public abstract void persistirSubEspecialidad(SubEspecialidad subespecialidad);

    public abstract List findByFilter(SubEspecialidad subespecialidad);

    public abstract List completeSubEspecialidad(String s, Integer integer);

    public abstract boolean existeSubEspecialidadRepetida(String s, String s1);

    public abstract boolean existeSubEspecialidadRepetida(String s);

    public abstract List findByEspecialidadId(Integer integer);
}
