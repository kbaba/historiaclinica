// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:41:25 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   SubEspecialidadRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.mapper.SubEspecialidadRowMapper;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import paquete.entities.Especialidad;
import paquete.entities.SubEspecialidad;

// Referenced classes of package paquete.repositorios:
//            SubEspecialidadRepository

@Repository
public class SubEspecialidadRepositoryImpl extends BaseDAO
    implements SubEspecialidadRepository
{

    public SubEspecialidadRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE AREA /*****************\n");
    }

    public void persistirSubEspecialidad(SubEspecialidad subEspecialidad)
    {
        if(subEspecialidad.getIdSubEspecialidad() == null)
            em().persist(subEspecialidad);
        else
            em().merge(subEspecialidad);
    }

    public List findByFilter(SubEspecialidad subEspecialidad)
    {
        Search parameter = new Search();
        if(StringUtils.isNotBlank(subEspecialidad.getNombreSubEspecialidad()))
            parameter.addFilterILike("nombreSubEspecialidad", (new StringBuilder()).append("%").append(subEspecialidad.getNombreSubEspecialidad()).append("%").toString());
        if(StringUtils.isNotBlank(subEspecialidad.getEstadoSubEspecialidad()))
            parameter.addFilterEqual("estadoSubEspecialidad", subEspecialidad.getEstadoSubEspecialidad());
        if(StringUtils.isNotBlank(subEspecialidad.getEspecialidad().getNombreEspecialidad()))
            parameter.addFilterEqual("especialidad.nombreEspecialidad", subEspecialidad.getEspecialidad().getNombreEspecialidad());
        if(subEspecialidad.getEspecialidad().getIdEspecialidad() != null)
            parameter.addFilterEqual("especialidad.idEspecialidad", subEspecialidad.getEspecialidad().getIdEspecialidad());
        return search(parameter);
    }

    public List completeSubEspecialidad(String nombreSubEspecialidad, Integer idEspecialidad)
    {
        StringBuilder builder = new StringBuilder();
        builder.append("select * from sub_especialidad where id_sub_especialidad=id_sub_especialidad ");
        if(StringUtils.isNotBlank(nombreSubEspecialidad))
            builder.append("and lower(nombre_sub_especialidad) like '%").append(nombreSubEspecialidad).append("%' ");
        if(idEspecialidad != null && idEspecialidad.intValue() != 0)
            builder.append("and especialidad_id =").append(idEspecialidad);
        builder.append(" order by nombre_sub_especialidad");
        List results = new ArrayList();
        results = getJdbcTemplate().query(builder.toString(), new SubEspecialidadRowMapper());
        return results;
    }

    private JdbcTemplate getJdbcTemplate()
    {
        return new JdbcTemplate(dataSource);
    }

    @Transactional
    public boolean existeSubEspecialidadRepetida(String nombre)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("nombreSubEspecialidad", nombre);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public boolean existeSubEspecialidadRepetida(String nombre, String idSubEspecialidad)
    {
        Search parameter = new Search();
        parameter.addFilterNotEqual("idSubEspecialidad", idSubEspecialidad);
        parameter.addFilterEqual("nombreSubEspecialidad", nombre);
        List lista = search(parameter);
        return lista.size() > 0;
    }

    @Transactional
    public List findByEspecialidadId(Integer id)
    {
        List items = null;
        StringBuffer sql = new StringBuffer();
        sql.append(" SELECT ");
        sql.append(" S.id_sub_especialidad\"idSubEspecialidad\",");
        sql.append(" S.nombre_sub_especialidad \"nombreSubEspecialidad\", ");
        sql.append(" S.estado_sub_especialidad\"estadoSubEspecialidad\" ");
        sql.append(" FROM SUB_ESPECIALIDAD S ");
        sql.append(" WHERE ");
        sql.append(" especialidad_id='").append(id).append("'");
        Query query = ((Session)em().unwrap(org/hibernate/classic/Session)).createSQLQuery(sql.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(paquete/entities/SubEspecialidad));
        items = query.list();
        return items;
    }

    @Autowired
    DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/SubEspecialidadRepositoryImpl);

}
