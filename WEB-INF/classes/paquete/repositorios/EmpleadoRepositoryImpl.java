// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:51 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EmpleadoRepositoryImpl.java

package paquete.repositorios;

import infraestructura.dto.EmpleadoDTO;
import infraestructura.dto.EspecialidadDTO;
import infraestructura.mapper.DTOMapperService;
import infraestructura.persistence.BaseDAO;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Referenced classes of package paquete.repositorios:
//            PersonaRepositoryImpl, EmpleadoRepository

@Repository
public class EmpleadoRepositoryImpl extends BaseDAO
    implements EmpleadoRepository
{

    public EmpleadoRepositoryImpl()
    {
    }

    @Transactional
    public void eliminarTrabajadorEspecialidad(EmpleadoDTO empleadodto, EspecialidadDTO especialidaddto)
    {
    }

    @Transactional
    public void eliminarTrabajadorEspecialidad(int i, int j)
    {
    }

    @Transactional
    public void eliminarTrabajador(EmpleadoDTO empleado)
    {
        String strQuery = (new StringBuilder()).append("UPDATE empleado SET estado_empleado='I' WHERE id_empleado='").append(empleado.getIdEmpleado().intValue()).append("'").toString();
        em().createNativeQuery(strQuery).executeUpdate();
    }

    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/PersonaRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
