// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:41:36 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TrabajoRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.Sort;
import infraestructura.dto.EmpleadoDTO;
import infraestructura.dto.EspecialidadDTO;
import infraestructura.dto.TrabajoDTO;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.sql.DataSource;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Empleado;
import paquete.entities.Especialidad;
import paquete.entities.Trabajo;

// Referenced classes of package paquete.repositorios:
//            TrabajoRepository, SubEspecialidadRepository

@Repository
public class TrabajoRepositoryImpl extends BaseDAO
    implements TrabajoRepository
{

    public TrabajoRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
    }

    public void persistirTrabajo(Trabajo trabajo)
    {
        if(trabajo.getIdTrabajo() == null)
            em().persist(trabajo);
        else
            em().merge(trabajo);
    }

    @Transactional
    public void persistirTrabajoQuery(List trabajos)
    {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < trabajos.size(); i++)
        {
            builder.append("INSERT INTO TRABAJO (especialidad_id, empleado_id) VALUES ");
            builder.append("(").append(((Trabajo)trabajos.get(i)).getEspecialidad().getIdEspecialidad()).append(", ");
            builder.append(((Trabajo)trabajos.get(i)).getEmpleado().getIdEmpleado()).append(");");
        }

        em().createNativeQuery(builder.toString()).executeUpdate();
    }

    @Transactional
    public void persistirTrabajoQueryDTO(List trabajos)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" INSERT INTO TRABAJO (especialidad_id, estado_trabajo,empleado_id) VALUES ");
        for(int i = 0; i < trabajos.size(); i++)
        {
            if(esTrabajoRepetido((TrabajoDTO)trabajos.get(i)))
                continue;
            builder.append(" (").append(((TrabajoDTO)trabajos.get(i)).getEspecialidad().getIdEspecialidad()).append(", ");
            builder.append(" 'A' ,");
            if(i == trabajos.size() - 1)
                builder.append(((TrabajoDTO)trabajos.get(i)).getEmpleado().getIdEmpleado()).append(") ");
            else
                builder.append(((TrabajoDTO)trabajos.get(i)).getEmpleado().getIdEmpleado()).append("), ");
        }

        em().createNativeQuery(builder.toString()).executeUpdate();
    }

    public List getTrabajoEmpleado(Integer idEmpleado)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("empleado.idEmpleado", idEmpleado);
        parameter.addSort(Sort.asc("estadoTrabajo"));
        return search(parameter);
    }

    public boolean esTrabajoBorrable(Integer idEspecialidad, Integer idEmpleado)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" select count (*) from  trabajo T ");
        builder.append(" inner join atencion A ");
        builder.append(" where A.trabajo_id=T.id_trabajo");
        builder.append(" and T.especialidad_id='").append(idEspecialidad).append("' ");
        builder.append(" and T.empleado_id= '").append(idEmpleado).append("' ");
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue() == 0;
    }

    private boolean esTrabajoRepetido(TrabajoDTO trabajo)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" select count(*) ");
        builder.append(" from ");
        builder.append(" trabajo where ");
        builder.append(" id_trabajo>0 ");
        if(trabajo.getEmpleado() != null && trabajo.getEmpleado().getIdEmpleado() != null)
            builder.append(" AND empleado_id='").append(trabajo.getEmpleado().getIdEmpleado()).append("' ");
        if(trabajo.getEspecialidad() != null && trabajo.getEspecialidad().getIdEspecialidad() != null)
            builder.append(" AND especialidad_id='").append(trabajo.getEspecialidad().getIdEspecialidad()).append("' ");
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue() > 0;
    }

    @Transactional
    public void eliminarTrabajosQueryDTO(List ids, Integer idEmpleado)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" delete from trabajo where empleado_id='").append(idEmpleado).append("'");
        builder.append(" AND (");
        for(int i = 0; i < ids.size(); i++)
            if(i == ids.size() - 1)
                builder.append(" especialidad_id='").append(ids.get(i)).append("');");
            else
                builder.append(" especialidad_id='").append(ids.get(i)).append("' OR ");

        Query query = em().createNativeQuery(builder.toString());
        query.executeUpdate();
    }

    public TrabajoDTO getTrabajoAtencion(Integer idAtencion)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" select ");
        builder.append(" T.id_trabajo \"idTrabajo\", ");
        builder.append(" T.especialidad_id \"especialidadId\" , ");
        builder.append("T.empleado_id  \"empleadoId\" ");
        builder.append(" from trabajo T inner join atencion A  ");
        builder.append(" where T.id_trabajo=A.trabajo_id  ");
        builder.append(" AND A.id_atencion=").append(idAtencion);
        org.hibernate.Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(builder.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/TrabajoDTO));
        TrabajoDTO trabajoDTO = (TrabajoDTO)query.uniqueResult();
        return trabajoDTO;
    }

    @Transactional
    public void ejecutarInstruccion(String sql)
    {
        em().createNativeQuery(sql).executeUpdate();
    }

    @Transactional
    public void ejecutarInstruccion(List sqlsList)
    {
        for(int i = 0; i < sqlsList.size(); i++)
            ejecutarInstruccion((String)sqlsList.get(i));

    }

    public boolean existeTrabajo(Integer idEmpleado, Integer idEspecialidad)
    {
        StringBuilder builder = new StringBuilder();
        builder.append(" select count(*) ");
        builder.append(" from ");
        builder.append(" trabajo where ");
        builder.append(" id_trabajo>0 ");
        builder.append(" AND empleado_id='").append(idEmpleado.intValue()).append("' ");
        builder.append(" AND especialidad_id='").append(idEspecialidad.intValue()).append("' ");
        Query query = em().createNativeQuery(builder.toString());
        BigInteger result = (BigInteger)query.getSingleResult();
        return result.intValue() > 0;
    }

    public String getQueryEliminar(Integer idEmpleado, List idEspecialidadesList)
    {
        String salida = "";
        for(int i = 0; i < idEspecialidadesList.size(); i++)
            salida = (new StringBuilder()).append(salida).append("DELETE FROM TRABAJO WHERE empleado_id='").append(idEmpleado.intValue()).append("' ").append(" AND especialidad_id='").append(((Integer)idEspecialidadesList.get(i)).intValue()).append("';").toString();

        return salida;
    }

    public String getQueryActualizar(Integer idEmpleado, List trabajosList)
    {
        String salida = "";
        Integer idEspecialidad = Integer.valueOf(-1);
        for(int i = 0; i < trabajosList.size(); i++)
        {
            idEspecialidad = ((TrabajoDTO)trabajosList.get(i)).getEspecialidad().getIdEspecialidad();
            if(existeTrabajo(idEmpleado, idEspecialidad))
                salida = (new StringBuilder()).append(salida).append(" UPDATE trabajo  SET estado_trabajo ='").append(((TrabajoDTO)trabajosList.get(i)).getEstadoTrabajo()).append("' ").append(" WHERE empleado_id ='").append(idEmpleado.intValue()).append("' ").append(" AND especialidad_id='").append(idEspecialidad.intValue()).append("' ;").toString();
        }

        return salida;
    }

    public String getQueryCrear(Integer idEmpleado, List trabajosList)
    {
        StringBuilder salida = new StringBuilder();
        Integer idEspecialidad = Integer.valueOf(-1);
        for(int i = 0; i < trabajosList.size(); i++)
        {
            idEspecialidad = ((TrabajoDTO)trabajosList.get(i)).getEspecialidad().getIdEspecialidad();
            if(!existeTrabajo(idEmpleado, idEspecialidad))
                salida.append("INSERT INTO trabajo (empleado_id,especialidad_id, estado_trabajo) VALUES ").append(" ('").append(idEmpleado.intValue()).append("', '").append(idEspecialidad.intValue()).append("', '").append(((TrabajoDTO)trabajosList.get(i)).getEstadoTrabajo()).append("');");
        }

        return salida.toString();
    }

    @Autowired
    DataSource dataSource;
    @Autowired
    SubEspecialidadRepository subAreaRepository;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/TrabajoRepositoryImpl);

}
