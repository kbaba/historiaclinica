// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:36:27 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   AtencionRepositoryImpl.java

package paquete.repositorios;

import com.googlecode.genericdao.search.Search;
import infraestructura.dto.AtencionDTO;
import infraestructura.dto.RespuestaDTO;
import infraestructura.mapper.DTOMapperService;
import infraestructura.persistence.BaseDAO;
import infraestructura.persistence.JpaHbmBeanResultTransformer;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.hibernate.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paquete.entities.Atencion;

// Referenced classes of package paquete.repositorios:
//            AtencionRepository, AlternativaRepository, ProcedimientoRepository, SubEspecialidadRepository, 
//            EspecialidadRepository

@Repository
public class AtencionRepositoryImpl extends BaseDAO
    implements AtencionRepository
{

    public AtencionRepositoryImpl()
    {
    }

    @PostConstruct
    public void init()
    {
        LOG.info("\n/***************** INICIALIZANDO REPOSITORY DE PERSONA /*****************\n");
    }

    @Transactional
    public void guardarAtencion(Atencion atencion)
    {
        if(atencion.getIdAtencion() == null)
            em().persist(atencion);
        else
            em().merge(atencion);
    }

    public boolean esTrabajoBorrable(Integer idEspecialidad, Integer idEmpleado)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("trabajo.especialidad.idEspecialidad", idEspecialidad);
        parameter.addFilterEqual("trabajo.empleado.idEmpleado", idEmpleado);
        long cont = search(parameter).size();
        return cont == 0L;
    }

    public boolean esTrabajoBorrable(Integer idTrabajo)
    {
        Search parameter = new Search();
        parameter.addFilterEqual("trabajo.idTrabajo", idTrabajo);
        long cont = search(parameter).size();
        return cont == 0L;
    }

    public List traeAtencionesHistoria(Integer idHistoria, String limite)
    {
        List lista = new ArrayList();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT A.id_atencion \"idAtencion\", ");
        builder.append(" A.fecha_atencion\"fechaAtencion\", ");
        builder.append(" A.diagnostico_atencion\"diagnosticoAtencion\", ");
        builder.append(" A.fecha_aprox_siguiente_atencion \"fechaAproxSiguienteAtencion\" ");
        builder.append(" from atencion A inner join historia H ");
        builder.append(" where A.historia_id=H.id_historia ");
        builder.append(" AND H.id_historia=").append(idHistoria);
        builder.append(" order by A.id_atencion DESC ");
        if(!limite.equalsIgnoreCase("TODO"))
            builder.append("LIMIT 0 ,").append(limite);
        Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(builder.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/AtencionDTO));
        lista = query.list();
        return lista;
    }

    public List traeRespuestasDeAtencion(Integer idAtencion)
    {
        List lista = new ArrayList();
        StringBuilder builder = new StringBuilder();
        builder.append(" SELECT R.id_respuesta \"idRespuesta\", ");
        builder.append(" R.descripcion_pregunta\"descripcionPregunta\", ");
        builder.append(" R.detalle_respuesta\"detalleRespuesta\", ");
        builder.append(" R.especialidad_id \"especialidadId\", ");
        builder.append(" R.sub_especialidad_id \"subEspecialidadId\", ");
        builder.append(" R.procedimiento_id \"procedimientoId\" ");
        builder.append(" from respuesta R inner join atencion A ");
        builder.append(" where R.atencion_id = A.id_atencion ");
        builder.append(" AND A.id_atencion = ").append(idAtencion);
        builder.append(" order by A.id_atencion DESC ");
        Query query = ((Session)em().unwrap(org/hibernate/Session)).createSQLQuery(builder.toString()).setResultTransformer(new JpaHbmBeanResultTransformer(infraestructura/dto/RespuestaDTO));
        lista = query.list();
        return lista;
    }

    @Autowired
    AlternativaRepository alternativaRepository;
    @Autowired
    ProcedimientoRepository procedimientoRepository;
    @Autowired
    SubEspecialidadRepository subEspecialidadRepository;
    @Autowired
    EspecialidadRepository especialidadRepository;
    @Autowired
    DataSource dataSource;
    private static final Logger LOG = LoggerFactory.getLogger(paquete/repositorios/AtencionRepositoryImpl);
    @Autowired
    private DTOMapperService mapperService;

}
