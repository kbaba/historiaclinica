// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:41 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ProcedimientoConverter.java

package jsf.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import paquete.repositorios.ProcedimientoRepository;

@Component
@FacesConverter(value="jsf.converter.AreaConverter")
public class ProcedimientoConverter
    implements Converter
{

    public ProcedimientoConverter()
    {
    }

    public Object getAsObject(FacesContext context, UIComponent component, String value)
        throws ConverterException
    {
        if(value == null || value.equals(""))
            return null;
        Integer id = null;
        try
        {
            id = Integer.valueOf(Integer.parseInt(value));
        }
        catch(NumberFormatException e)
        {
            return null;
        }
        return procedimientoRepository.find(id);
    }

    public String getAsString(FacesContext context, UIComponent component, Object value)
        throws ConverterException
    {
        if(value == null || value.equals(""))
            return null;
        else
            return value.toString();
    }

    public ProcedimientoRepository getEspecialidadRepository()
    {
        return procedimientoRepository;
    }

    public void setEspecialidadRepository(ProcedimientoRepository especialidadRepository)
    {
        procedimientoRepository = especialidadRepository;
    }

    @Autowired
    protected ProcedimientoRepository procedimientoRepository;
}
