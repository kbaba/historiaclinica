// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:32:33 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   EnfermedadConverter.java

package jsf.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import paquete.repositorios.EnfermedadRepository;

@Component
@FacesConverter(value="jsf.converter.EnfermedadConverter")
public class EnfermedadConverter
    implements Converter
{

    public EnfermedadConverter()
    {
    }

    public Object getAsObject(FacesContext context, UIComponent component, String value)
        throws ConverterException
    {
        if(value == null || value.equals(""))
            return null;
        else
            return enfermedadRepository.find(value);
    }

    public String getAsString(FacesContext context, UIComponent component, Object value)
        throws ConverterException
    {
        if(value == null || value.equals(""))
            return null;
        else
            return value.toString();
    }

    public EnfermedadRepository getEnfermedadRepository()
    {
        return enfermedadRepository;
    }

    public void setAEnfermedadRepository(EnfermedadRepository enfermedadRepository)
    {
        this.enfermedadRepository = enfermedadRepository;
    }

    @Autowired
    protected EnfermedadRepository enfermedadRepository;
}
