// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:50:04 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ResourceProperties.java

package util;

import java.io.IOException;
import java.util.Properties;

public class ResourceProperties
{

    public ResourceProperties()
    {
    }

    public static Properties getResourcePorperties(String resourcePath)
        throws IOException
    {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Properties props = new Properties();
        props.load(classLoader.getResourceAsStream(resourcePath));
        return props;
    }
}
