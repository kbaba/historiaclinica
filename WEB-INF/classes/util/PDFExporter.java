// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:52 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PDFExporter.java

package util;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package util:
//            Exporter

public class PDFExporter extends Exporter
{

    public PDFExporter()
    {
    }

    public void export(FacesContext context, List columns, List values, String filename, String encodingType)
        throws IOException
    {
        try
        {
            Document document = new Document();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PdfWriter.getInstance(document, baos);
            if(!document.isOpen())
                document.open();
            PdfPTable pdfTable = exportPDFTable(context, columns, values, encodingType);
            document.add(pdfTable);
            document.close();
            writePDFToResponse((HttpServletResponse)context.getExternalContext().getResponse(), baos, filename);
        }
        catch(DocumentException e)
        {
            throw new IOException(e.getMessage());
        }
    }

    private PdfPTable exportPDFTable(FacesContext context, List columns, List values, String encoding)
    {
        int numberOfColumns = columns.size();
        PdfPTable pdfTable = new PdfPTable(numberOfColumns);
        Font font = FontFactory.getFont("Times", encoding);
        Font headerFont = FontFactory.getFont("Times", encoding, 12F, 1);
        addFacetColumns(pdfTable, columns, headerFont);
        exportAll(context, columns, values, pdfTable, font);
        return pdfTable;
    }

    private void exportAll(FacesContext context, List columns, List values, PdfPTable pdfTable, Font font)
    {
        int numberOfColumns = columns.size();
        for(int row = 0; row < values.size(); row++)
        {
            for(int col = 0; col < numberOfColumns; col++)
                addColumnValue(pdfTable, (String)((Map)values.get(row)).get(columns.get(col)), col, font);

        }

    }

    private void addColumnValue(PdfPTable pdfTable, String value, int index, Font font)
    {
        if(value == null)
            value = "";
        pdfTable.addCell(new Paragraph(value, font));
    }

    private void addFacetColumns(PdfPTable pdfTable, List columns, Font font)
    {
        for(int i = 0; i < columns.size(); i++)
        {
            String column = (String)columns.get(i);
            addColumnValue(pdfTable, column, font);
        }

    }

    private void addColumnValue(PdfPTable pdfTable, String value, Font font)
    {
        if(value == null)
            value = "";
        pdfTable.addCell(new Paragraph(value, font));
    }

    private void writePDFToResponse(HttpServletResponse response, ByteArrayOutputStream baos, String fileName)
        throws IOException, DocumentException
    {
        response.setContentType("application/pdf");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment;filename=").append(fileName).append(".pdf").toString());
        response.setContentLength(baos.size());
        ServletOutputStream out = response.getOutputStream();
        baos.writeTo(out);
        out.flush();
    }
}
