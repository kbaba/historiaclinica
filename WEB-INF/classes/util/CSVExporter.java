// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:05 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   CSVExporter.java

package util;

import java.io.*;
import java.util.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package util:
//            Exporter

public class CSVExporter extends Exporter
{

    public CSVExporter()
    {
    }

    public void export(FacesContext context, List columns, List values, String filename, String encodingType)
        throws IOException
    {
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
        response.setContentType("text/csv");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment;filename=").append(filename).append(".csv").toString());
        java.io.OutputStream os = response.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, encodingType);
        PrintWriter writer = new PrintWriter(osw);
        addFacetColumns(writer, columns);
        addFacetValues(writer, columns, values);
        writer.flush();
        writer.close();
        response.getOutputStream().flush();
    }

    private void addFacetValues(PrintWriter writer, List columns, List values)
        throws IOException
    {
label0:
        for(Iterator i$ = values.iterator(); i$.hasNext(); writer.write("\n"))
        {
            Map value = (Map)i$.next();
            Iterator iterator = columns.iterator();
            do
            {
                if(!iterator.hasNext())
                    continue label0;
                String val = (String)value.get(iterator.next());
                if(val == null)
                    val = "";
                writer.write((new StringBuilder()).append("\"").append(val).append("\"").toString());
                if(iterator.hasNext())
                    writer.write(",");
            } while(true);
        }

    }

    private void addFacetColumns(PrintWriter writer, List columns)
        throws IOException
    {
        Iterator iterator = columns.iterator();
        do
        {
            if(!iterator.hasNext())
                break;
            writer.write((new StringBuilder()).append("\"").append((String)iterator.next()).append("\"").toString());
            if(iterator.hasNext())
                writer.write(",");
        } while(true);
        writer.write("\n");
    }
}
