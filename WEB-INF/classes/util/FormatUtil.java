// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:41 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FormatUtil.java

package util;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.*;
import java.util.Calendar;
import java.util.Date;

public class FormatUtil
{

    public FormatUtil()
    {
    }

    public static Date formatShortDate(Date fecha)
    {
        if(fecha == null)
        {
            return null;
        } else
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fecha);
            calendar.set(10, 0);
            calendar.set(12, 0);
            calendar.set(13, 0);
            calendar.set(14, 0);
            return calendar.getTime();
        }
    }

    public static Date formatShortNextDate(Date fecha)
    {
        if(fecha == null)
        {
            return null;
        } else
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(fecha);
            calendar.add(6, 1);
            calendar.set(10, 0);
            calendar.set(12, 0);
            calendar.set(13, 0);
            calendar.set(14, 0);
            return calendar.getTime();
        }
    }

    public static String format(Date fecha, String pattern)
    {
        if(fecha == null)
        {
            return null;
        } else
        {
            SimpleDateFormat formatter = new SimpleDateFormat(pattern);
            return formatter.format(fecha);
        }
    }

    public static String capitalizeFirstChar(String word)
    {
        return (new StringBuilder()).append(word.substring(0, 1).toUpperCase()).append(word.substring(1).toLowerCase()).toString();
    }

    public static boolean validateLength(String text, int length)
    {
        return text == null || text.length() <= length;
    }

    public static boolean validateLength(BigDecimal amount)
    {
        return amount == null || amount.precision() <= 15;
    }

    public static BigDecimal format(BigDecimal amount)
    {
        if(amount == null)
            return null;
        else
            return amount.setScale(4, 4);
    }

    public static Date getDateTime(Object value)
    {
        try
        {
            return toDateTime(value);
        }
        catch(ParseException pe)
        {
            pe.printStackTrace();
        }
        return null;
    }

    public static Date toDateTime(Object value)
        throws ParseException
    {
        if(value == null)
            return null;
        if(value instanceof Date)
            return (Date)value;
        if(value instanceof String)
        {
            if("".equals((String)value))
                return null;
            else
                return IN_DATETIME_FORMAT.parse((String)value);
        } else
        {
            return IN_DATETIME_FORMAT.parse(value.toString());
        }
    }

    public Date fechaMenosMins(Date fec, int minutos)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(fec.getTime());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        Calendar _tmp = cal;
        cal.add(12, -minutos);
        System.out.println((new StringBuilder()).append("getFecha(sdf.format(cal.getTime()));: ").append(getFecha(sdf.format(cal.getTime()))).toString());
        return getFecha(sdf.format(cal.getTime()));
    }

    public Date getFecha(String fecha)
    {
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
            Date d = sdf.parse(fecha);
            return d;
        }
        catch(ParseException e)
        {
            e.getMessage();
        }
        return null;
    }

    public static final String FORMATO_FECHA = "yyyyMMdd";
    public static final String FORMATO_FECHA_LONG = "dd/MM/yyyy";
    public static final String FORMATO_FECHA_SHORT = "dd/MM";
    public static final String FORMATO_FECHA_SEPARADOR = "dd/MM/yyyy";
    public static DateFormat IN_DATETIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm a");

}
