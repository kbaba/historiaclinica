// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:50:15 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   TemplateExcelExporter.java

package util;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;

// Referenced classes of package util:
//            ApplicationProperties

public class TemplateExcelExporter
{

    public TemplateExcelExporter()
    {
    }

    public void export(FacesContext context, String templateFileName)
        throws IOException
    {
        ServletContext servletContext = (ServletContext)context.getExternalContext().getContext();
        String fullPath = servletContext.getRealPath((new StringBuilder()).append(ApplicationProperties.getExcelTemplatePath()).append(templateFileName).append(".xls").toString());
        String fileName = (new StringBuilder()).append(templateFileName).append(System.currentTimeMillis()).toString();
        FileInputStream fileInputStream = new FileInputStream(fullPath);
        POIFSFileSystem fsFileSystem = new POIFSFileSystem(fileInputStream);
        wb = new HSSFWorkbook(fsFileSystem);
        sheet = wb.getSheetAt(0);
    }

    public void addRow(int rowIndex)
    {
        row = sheet.createRow(rowIndex);
    }

    public void setCellValue(int rowIndex, int colIndex, String value)
    {
        Row row = sheet.getRow(rowIndex);
        Cell cell = row.getCell(colIndex);
        if(cell == null)
            cell = row.createCell(colIndex);
        if(value == null)
            value = "";
        cell.setCellValue(new HSSFRichTextString(value));
    }

    public void setCellValue(int rowIndex, int colIndex, BigDecimal value)
    {
        String aux = value != null ? String.valueOf(value) : "";
        setCellValue(rowIndex, colIndex, value);
    }

    public void addColumnValue(int index, String value)
    {
        Cell cell = row.createCell(index);
        if(value == null)
            value = "";
        cell.setCellStyle(defaultCellStyle());
        cell.setCellValue(new HSSFRichTextString(value));
    }

    public void addColumnValue(int index, BigDecimal value)
    {
        String aux = value != null ? String.valueOf(value) : "";
        addColumnValue(index, aux);
    }

    public void addColumnValue(int index, Long value)
    {
        String aux = value != null ? String.valueOf(value) : "";
        addColumnValue(index, aux);
    }

    public void writeExcelToResponse(HttpServletResponse response, String filename)
        throws IOException
    {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment;filename=").append(filename).append(".xls").toString());
        wb.write(response.getOutputStream());
    }

    private HSSFCellStyle defaultCellStyle()
    {
        HSSFCellStyle style = wb.createCellStyle();
        style.setBorderBottom((short)1);
        style.setBorderTop((short)1);
        style.setBorderLeft((short)1);
        style.setBorderRight((short)1);
        return style;
    }

    private Row row;
    private Sheet sheet;
    private HSSFWorkbook wb;
}
