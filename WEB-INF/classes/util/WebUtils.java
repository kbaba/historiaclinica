// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:50:28 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   WebUtils.java

package util;

import java.io.IOException;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class WebUtils
{

    public WebUtils()
    {
    }

    public static HttpSession getSession()
    {
        return (HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true);
    }

    public static HttpServletResponse getResponse()
    {
        return (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
    }

    public static void renderFile(byte bytes[], String filename)
        throws IOException
    {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = getResponse();
        response.reset();
        response.setContentType("application/octet-stream");
        response.setContentLength(bytes.length);
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment; filename=\"").append(filename).append("\"").toString());
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0L);
        ServletOutputStream ouputStream = response.getOutputStream();
        ouputStream.write(bytes, 0, bytes.length);
        ouputStream.flush();
        ouputStream.close();
        context.responseComplete();
    }
}
