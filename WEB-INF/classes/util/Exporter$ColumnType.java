// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:24 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Exporter.java

package util;


// Referenced classes of package util:
//            Exporter

protected static final class Exporter$ColumnType extends Enum
{

    public static Exporter$ColumnType[] values()
    {
        return (Exporter$ColumnType[])$VALUES.clone();
    }

    public static Exporter$ColumnType valueOf(String name)
    {
        return (Exporter$ColumnType)Enum.valueOf(util/Exporter$ColumnType, name);
    }

    public String facet()
    {
        return facet;
    }

    public String toString()
    {
        return facet;
    }

    public static final Exporter$ColumnType HEADER;
    public static final Exporter$ColumnType FOOTER;
    private final String facet;
    private static final Exporter$ColumnType $VALUES[];

    static 
    {
        HEADER = new Exporter$ColumnType("HEADER", 0, "header");
        FOOTER = new Exporter$ColumnType("FOOTER", 1, "footer");
        $VALUES = (new Exporter$ColumnType[] {
            HEADER, FOOTER
        });
    }

    private Exporter$ColumnType(String s, int i, String facet)
    {
        super(s, i);
        this.facet = facet;
    }
}
