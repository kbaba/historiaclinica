// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:29 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Exporter.java

package util;

import java.io.IOException;
import java.util.*;

import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.component.*;
import javax.faces.component.html.HtmlCommandLink;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import org.primefaces.component.column.Column;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.treetable.TreeTable;

// Referenced classes of package util:
//            CSVExporter, ExcelExporter, PDFExporter, XMLExporter

public abstract class Exporter
{
    protected static final class ColumnType extends Enum
    {
        public static ColumnType[] values()
        {
            return (ColumnType[])$VALUES.clone();
        }

        public static ColumnType valueOf(String name)
        {
            int util;
			return (ColumnType)Enum.valueOf(util/Exporter$ColumnType, name);
        }

        public String facet()
        {
            return facet;
        }

        public String toString()
        {
            return facet;
        }

        public static final ColumnType HEADER;
        public static final ColumnType FOOTER;
        private final String facet;
        private static final ColumnType $VALUES[];

        static 
        {
            HEADER = new ColumnType("HEADER", 0, "header");
            FOOTER = new ColumnType("FOOTER", 1, "footer");
            $VALUES = (new ColumnType[] {
                HEADER, FOOTER
            });
        }

        private ColumnType(String s, int i, String facet)
        {
            super(s, i);
            this.facet = facet;
        }
    }


    public Exporter()
    {
    }

    public static Exporter generateExporter(String fileType)
    {
        Exporter exporter = null;
        if("csv".equals(fileType))
            exporter = new CSVExporter();
        else
        if("xls".equals(fileType))
            exporter = new ExcelExporter();
        else
        if("pdf".equals(fileType))
            exporter = new PDFExporter();
        else
        if("xml".equals(fileType))
            exporter = new XMLExporter();
        return exporter;
    }

    public abstract void export(FacesContext facescontext, List list, List list1, String s, String s1)
        throws IOException;

    public List getColumnsToPrint(String tableName)
    {
        List columnsToPrint = new ArrayList();
        List uiComponents = null;
        UIComponent component = findComponent(tableName);
        if(component != null)
        {
            if(component instanceof DataTable)
            {
                DataTable dataTable = (DataTable)findComponent(tableName);
                uiComponents = dataTable.getChildren();
            } else
            if(component instanceof TreeTable)
            {
                TreeTable treeTable = (TreeTable)findComponent(tableName);
                uiComponents = treeTable.getChildren();
            }
            List uiColumns = getColumnsToExport(uiComponents);
            addFacetColumns(uiColumns, columnsToPrint, ColumnType.HEADER);
        }
        return columnsToPrint;
    }

    protected List getColumnsToExport(List children)
    {
        List columns = new ArrayList();
        Iterator i$ = children.iterator();
        do
        {
            if(!i$.hasNext())
                break;
            UIComponent child = (UIComponent)i$.next();
            if((child instanceof Column) || (child instanceof UIColumn))
            {
                UIColumn column = (UIColumn)child;
                columns.add(column);
            }
        } while(true);
        return columns;
    }

    private void addFacetColumns(List columns, List values, ColumnType columnType)
    {
        for(Iterator iterator = columns.iterator(); iterator.hasNext(); values.add(addColumnValue(((UIColumn)iterator.next()).getFacet(columnType.facet()))));
    }

    private String addColumnValue(UIComponent component)
    {
        return component != null ? exportValue(FacesContext.getCurrentInstance(), component) : "";
    }

    /**
     * @param context
     * @param component
     * @return
     */
    protected String exportValue(FacesContext context, UIComponent component)
    {
        if(component instanceof HtmlCommandLink)
        {
            HtmlCommandLink link = (HtmlCommandLink)component;
            Object value = link.getValue();
            if(value != null)
                return String.valueOf(value);
            for(Iterator<UIComponent> i$ = link.getChildren().iterator(); i$.hasNext();)
            {
                UIComponent child = (UIComponent)i$.next();
                if(child instanceof ValueHolder)
                    return exportValue(context, child);
            }

            return null;
        } else
        {
            if(component instanceof ValueHolder)
            {
                if(component instanceof EditableValueHolder)
                {
                    Object submittedValue = ((EditableValueHolder)component).getSubmittedValue();
                    if(submittedValue != null)
                        return submittedValue.toString();
                }
                ValueHolder valueHolder = (ValueHolder)component;
                Object value = valueHolder.getValue();
                if(value == null)
                    return "";
                if(valueHolder.getConverter() != null)
                    return valueHolder.getConverter().getAsString(context, component, value);
                ValueExpression expr = component.getValueExpression("value");
                if(expr != null)
                {
                    Class valueType = expr.getType(context.getELContext());
                    if(valueType != null)
                    {
                        Converter converterForType = context.getApplication().createConverter(valueType);
                        if(converterForType != null)
                            return converterForType.getAsString(context, component, value);
                    }
                }
                return value.toString();
            }
            String value = component.toString();
            if(value != null)
                return value.trim();
            else
                return "";
        }
    }

    protected Object getSelectedRowTable(String tableName)
    {
        Object selectedObject = null;
        UIComponent component = findComponent(tableName);
        if(component instanceof DataTable)
        {
            DataTable dataTable = (DataTable)findComponent(tableName);
            selectedObject = dataTable.getSelection();
        } else
        if(component instanceof TreeTable)
        {
            TreeTable treeTable = (TreeTable)findComponent(tableName);
            selectedObject = treeTable.getSelection();
        }
        return selectedObject;
    }

    protected UIComponent findComponent(String id)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        javax.faces.component.UIViewRoot root = context.getViewRoot();
        return findComponent(((UIComponent) (root)), id);
    }

    private UIComponent findComponent(UIComponent c, String id)
    {
        if(id.equals(c.getId()))
            return c;
        for(Iterator kids = c.getFacetsAndChildren(); kids.hasNext();)
        {
            UIComponent found = findComponent((UIComponent)kids.next(), id);
            if(found != null)
                return found;
        }

        return null;
    }
}
