// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:02 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ApplicationProperties.java

package util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Referenced classes of package util:
//            ResourceProperties

public class ApplicationProperties
{

    public ApplicationProperties()
    {
    }

    public static Properties getAppPorperties()
    {
        if(props == null)
            try
            {
                props = ResourceProperties.getResourcePorperties(appResourcePath);
            }
            catch(IOException e)
            {
                logger.error((new StringBuilder()).append("Error al leer el archivo de propiedades ").append(appResourcePath).toString(), e);
            }
        return props;
    }

    public static String getEmailFrom()
    {
        return getAppPorperties().getProperty("mail.adm.from");
    }

    public static String getEmailErrorTo()
    {
        return getAppPorperties().getProperty("mail.error.to");
    }

    public static String getExcelTemplatePath()
    {
        return getAppPorperties().getProperty("report.excel.template.path");
    }

    public static String getSmtpHost()
    {
        return getAppPorperties().getProperty("mail.smtp.host");
    }

    public static String getSmtpHostPort()
    {
        return getAppPorperties().getProperty("mail.smtp.port");
    }

    public static String getSmtpHostAuthFlag()
    {
        return getAppPorperties().getProperty("mail.smtp.auth");
    }

    public static String getSmtpHostStart()
    {
        return getAppPorperties().getProperty("mail.smtp.starttls");
    }

    public static String getSellAccountCode()
    {
        return getAppPorperties().getProperty("sell.account.code");
    }

    public static String getSalaryAccountCode()
    {
        return getAppPorperties().getProperty("salary.account.code");
    }

    public static String getCommisionAccountCode()
    {
        return getAppPorperties().getProperty("commision.account.code");
    }

    public static BigDecimal getFactorIndicator()
    {
        String factor = getAppPorperties().getProperty("egyp.factor.indicator");
        return new BigDecimal(factor);
    }

    private static final Logger logger = LoggerFactory.getLogger(util/ApplicationProperties);
    public static final String SCHEMA_DATABASE = "";
    public static final String SCHEMA_HISTORY_SEQUENCE = "HIBERNATE_H_SEQUENCE";
    public static final String SCHEMA_REAL_SEQUENCE = "HIBERNATE_R_SEQUENCE";
    public static final String SCHEMA_BUDGET_SEQUENCE = "HIBERNATE_B_SEQUENCE";
    public static final String SCHEMA_TMPL_SEQUENCE = "HIBERNATE_TMPL_SEQUENCE";
    private static Properties props = null;
    private static String appResourcePath = "com/eckerd/budget/application.properties";

}
