package util;

import java.io.*;
import java.util.List;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

// Referenced classes of package util:
//            Exporter

public class XMLExporter extends Exporter
{

    public XMLExporter()
    {
    }

    public void export(FacesContext context, List columns, List values, String filename, String encodingType)
        throws IOException
    {
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
        response.setContentType("text/xml");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment;filename=").append(filename).append(".xml").toString());
        java.io.OutputStream os = response.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, encodingType);
        PrintWriter writer = new PrintWriter(osw);
        writer.write("<?xml version=\"1.0\"?>\n");
        writer.write("<table>\n");
        exportAll(columns, values, writer);
        writer.write("</table>");
        writer.flush();
        writer.close();
        response.getOutputStream().flush();
    }

    private void exportAll(List columns, List values, PrintWriter writer)
        throws IOException
    {
        String var = "row";
        int rowCount = values.size();
        for(int i = 0; i < rowCount; i++)
        {
            writer.write((new StringBuilder()).append("\t<").append(var).append(">\n").toString());
            addColumnValues(writer, columns, (Map)values.get(i));
            writer.write((new StringBuilder()).append("\t</").append(var).append(">\n").toString());
        }

    }

    private void addColumnValues(PrintWriter writer, List columns, Map values)
        throws IOException
    {
        for(int i = 0; i < columns.size(); i++)
        {
            String header = (String)columns.get(i);
            addColumnValue(writer, (String)values.get(header), header);
        }

    }

    private void addColumnValue(PrintWriter writer, String value, String header)
        throws IOException
    {
        String tag = header.toLowerCase();
        writer.write((new StringBuilder()).append("\t\t<").append(tag).append(">").toString());
        if(value == null)
            value = "";
        writer.write(value);
        writer.write((new StringBuilder()).append("</").append(tag).append(">\n").toString());
    }
}
