// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:58 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   PoolConexiones.java

package util;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.*;
import javax.sql.DataSource;

public class PoolConexiones
{

    public PoolConexiones()
    {
    }

    public Connection getConexion()
    {
        try
        {
            Context c = new InitialContext();
            Connection connection = datasource.getConnection();
            if(connection == null)
                throw new SQLException("Error al establecer la conexion.");
            else
                return connection;
        }
        catch(NamingException e)
        {
            e.getMessage();
            return null;
        }
        catch(SQLException w)
        {
            w.getMessage();
        }
        return null;
    }

    @Autowired
    DataSource datasource;
}
