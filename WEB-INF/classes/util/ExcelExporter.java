// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:19 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   ExcelExporter.java

package util;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

// Referenced classes of package util:
//            Exporter

public class ExcelExporter extends Exporter
{

    public ExcelExporter()
    {
    }

    public void export(FacesContext context, List columns, List values, String filename, String encodingType)
        throws IOException
    {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet();
        addFacetColumns(sheet, columns, 0);
        exportAll(columns, values, sheet);
        writeExcelToResponse((HttpServletResponse)context.getExternalContext().getResponse(), wb, filename);
    }

    protected void exportAll(List columns, List values, Sheet sheet)
    {
        for(int rowIndex = 0; rowIndex < values.size(); rowIndex++)
        {
            Map value = (Map)values.get(rowIndex);
            Row rowHeader = sheet.createRow(rowIndex + 1);
            for(int colIndex = 0; colIndex < columns.size(); colIndex++)
            {
                String val = (String)value.get(columns.get(colIndex));
                addColumnValue(rowHeader, val, colIndex);
            }

        }

    }

    protected void addFacetColumns(Sheet sheet, List columns, int rowIndex)
    {
        Row rowHeader = sheet.createRow(rowIndex);
        for(int i = 0; i < columns.size(); i++)
            addColumnValue(rowHeader, (String)columns.get(i), i);

    }

    protected void addColumnValue(Row row, String value, int index)
    {
        Cell cell = row.createCell(index);
        if(value == null)
            value = "";
        cell.setCellValue(new HSSFRichTextString(value));
    }

    protected void writeExcelToResponse(HttpServletResponse response, Workbook generatedExcel, String filename)
        throws IOException
    {
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Expires", "0");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setHeader("Content-disposition", (new StringBuilder()).append("attachment;filename=").append(filename).append(".xls").toString());
        generatedExcel.write(response.getOutputStream());
    }
}
