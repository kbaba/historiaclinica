// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:13 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Envio_Correo.java

package util;

import java.io.PrintStream;
import java.sql.*;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.*;
import javax.sql.DataSource;

public class Envio_Correo
{

    public Envio_Correo()
    {
        PUERTO = "";
        HOST = "";
        USUARIO = "";
        CLAVE = "";
        DOMINIO = "";
        EMAIL_QUE_ENVIA = "";
        try
        {
            Connection conn = null;
            String CONNECTION_URL = "jdbc:mysql://172.17.8.73:3306/info_mobile?user=remoto&password=remoto";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(CONNECTION_URL);
            String strQuery = "SELECT * FROM correo c where c.nid_correo = 1";
            PreparedStatement st = conn.prepareStatement(strQuery);
            for(ResultSet rs = st.executeQuery(); rs.next();)
            {
                PUERTO = rs.getString("puerto");
                HOST = rs.getString("host");
                USUARIO = rs.getString("usuario");
                CLAVE = rs.getString("clave");
                DOMINIO = rs.getString("dominio");
                EMAIL_QUE_ENVIA = (new StringBuilder()).append(USUARIO).append(DOMINIO).toString();
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void enviarCorreo(String email, String data[])
    {
        try
        {
            Multipart multipart = new MimeMultipart();
            BodyPart messageBodyPart = new MimeBodyPart();
            Properties props = new Properties();
            props.put("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.host", HOST);
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.starttls.required", "true");
            props.setProperty("mail.smtp.user", EMAIL_QUE_ENVIA);
            props.setProperty("mail.smtp.auth", "true");
            props.put("mail.smtp.host", HOST);
            props.put("mail.smtp.starttls.required", "true");
            String smtp = HOST;
            if(smtp.indexOf(HOST) >= 0)
            {
                props.setProperty("mail.smtp.socketFactory.fallback", "false");
                props.setProperty("mail.smtp.port", PUERTO);
                props.setProperty("mail.smtp.socketFactory.port", PUERTO);
            }
            if(smtp.equals("smtp.gmail.com"))
            {
                props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
                props.setProperty("mail.smtp.socketFactory.fallback", "false");
                props.setProperty("mail.smtp.port", PUERTO);
                props.setProperty("mail.smtp.socketFactory.port", PUERTO);
            }
            Session session = Session.getDefaultInstance(props);
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(EMAIL_QUE_ENVIA));
            message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(email));
            message.setSubject("INFO-MOBILE URP Alarma.");
            String contenido = contenidoHTML(data);
            messageBodyPart.setContent(contenido, "text/html");
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            Transport t = session.getTransport("smtp");
            t.connect(HOST, USUARIO, CLAVE);
            t.sendMessage(message, message.getAllRecipients());
            System.out.println("Se envio el correo Electr\363nico!");
            t.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public String contenidoHTML(String data[])
    {
        String contenido = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 500px;\">".concat("<tbody>").concat("<tr>").concat("<td colspan=\"2\" style = \"vertical-align:middle;\">").concat("<span style=\"font-size:10px;\"><a href=\"\"><img alt=\"LOGO\" src=\"http://img109.imageshack.us/img109/6844/iconpequeno.jpg\" style=\"height: 50px; width: 50px;\" /></a>&nbsp;<strong><span style=\"font-family:arial,helvetica,sans-serif;\"><span style=\"font-size: 12px;\"><span style=\"color: rgb(0, 128, 0);\">INFO MOBILE</span></span></span></strong></span></td>").concat("<td colspan=\"2\">").concat((new StringBuilder()).append("<span style=\"color:#008000;\">Fecha:</span>&nbsp; ").append(data[0]).append("</td>").toString()).concat("</tr>").concat("<tr>").concat("<td>").concat((new StringBuilder()).append("<span style=\"color:#008000;\">Alarma enviada :</span>").append(data[1]).append("</td>").toString()).concat("<td>").concat((new StringBuilder()).append("<span style=\"color:#008000;\">Alumno:</span>&nbsp;").append(data[2]).append("</td>").toString()).concat("<td>").concat((new StringBuilder()).append("<span style=\"color:#008000;\">Evento:</span>&nbsp;").append(data[3]).append("</td>").toString()).concat("</tr>").concat("<tr>").concat("<td colspan=\"3\" rowspan=\"1\">").concat((new StringBuilder()).append("<span style=\"color:#008000;\">Nota:</span>&nbsp;").append(data[4]).append("</td>").toString()).concat("</tr>").concat("</tbody>").concat("</table>");
        return contenido;
    }

    public void enviarSMS(String celular, String operador)
    {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.host", HOST);
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.starttls.required", "true");
        props.setProperty("mail.smtp.user", EMAIL_QUE_ENVIA);
        props.setProperty("mail.smtp.auth", "true");
        props.put("mail.smtp.host", HOST);
        props.put("mail.smtp.starttls.required", "true");
        String smtp = HOST;
        if(smtp.indexOf(HOST) >= 0)
        {
            props.setProperty("mail.smtp.socketFactory.fallback", "false");
            props.setProperty("mail.smtp.port", PUERTO);
            props.setProperty("mail.smtp.socketFactory.port", PUERTO);
        }
        Session session = Session.getDefaultInstance(props);
        try
        {
            if(operador.equals("nextel"))
                celular = (new StringBuilder()).append(celular).append("@nextelmensajes.com.pe").toString();
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(celular));
            message.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(celular));
            message.setSubject("PRUEBA CELULAR");
            message.setText("MENSAJE");
            Transport.send(message);
            System.out.println("Se envio el SMS!");
        }
        catch(MessagingException mex)
        {
            mex.printStackTrace();
        }
    }

    @Autowired
    DataSource datasource;
    private String PUERTO;
    private String HOST;
    private String USUARIO;
    private String CLAVE;
    private String DOMINIO;
    private String EMAIL_QUE_ENVIA;
}
