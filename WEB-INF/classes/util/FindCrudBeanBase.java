// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:49:32 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FindCrudBeanBase.java

package util;

import infraestructura.dto.UsuarioDTO;
import infraestructura.exception.ApplicationException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import javax.faces.application.Application;
import javax.faces.application.FacesMessage;
import javax.faces.component.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.treetable.TreeTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import security.HistoriasClinicasAuthenticationToken;
import security.HistoriasClinicasSecurityInfo;

public abstract class FindCrudBeanBase
{

    public FindCrudBeanBase()
    {
        noRecordFoundMsg = "label_no_records_found1";
        noSelectedRowMsg = "noSelectedRowMsg";
        successfullyUpdateMsg = "successfullyUpdateMsg";
        successfullyDeleteMsg = "successfullyDeleteMsg";
        invalidUploadedFile = "invalidUploadedFile";
        successfullyUploadedMesg = "successfullyUploadedMesg";
        successfullyCreatedMsg = "successfullyCreatedMsg";
        resourceBundleName = "messages";
        rowsPerPage = Long.valueOf(25L);
        name = "";
        noRecordFound = false;
        success = true;
    }

    @PostConstruct
    private void getUser()
    {
        LOG.info("Recuperando el usuario de la sesi\363n para que sea com\372n a todas las p\341ginas");
        HistoriasClinicasAuthenticationToken authentication = (HistoriasClinicasAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
        String login = (String)authentication.getPrincipal();
        String password = (String)authentication.getCredentials();
        boolean mobile = authentication.isMobile();
        usuario = historiasClinicasSecurityInfo.obtenerUsuario(login, password, mobile);
    }

    public abstract void setCursorTableNull();

    public abstract void onNewRecord(ActionEvent actionevent);

    public abstract void onFind(ActionEvent actionevent);

    public abstract void onClean(ActionEvent actionevent);

    public abstract void onPersist(ActionEvent actionevent);

    public abstract void onDelete(ActionEvent actionevent);

    protected abstract void onDialogClose(CloseEvent closeevent);

    public void cleanAction(ActionEvent actionEvent)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        resetDataTable(root);
        onClean(actionEvent);
        setCursorTableNull();
        resetForm(actionEvent.getComponent().getNamingContainer());
    }

    public void handleDialogClose(CloseEvent event)
    {
        LOG.debug("Handling dialog close in handleDialogClose()");
        onDialogClose(event);
        resetForm(event.getComponent());
    }

    public void showNewDialog(ActionEvent actionEvent)
    {
        onNewRecord(actionEvent);
    }

    public void showEditDialog(ActionEvent actionEvent)
    {
        String tableName = (String)actionEvent.getComponent().getAttributes().get("tableName");
        if(tableName != null && getSelectedRowTable(tableName) == null)
        {
            addMessage(noSelectedRowMsg, FacesMessage.SEVERITY_WARN, new Object[0]);
        } else
        {
            onShowEditDialog(actionEvent);
            String dialogName = (String)actionEvent.getComponent().getAttributes().get("dialogName");
            executeModal(dialogName, true);
        }
    }

    public void closeEditDialog(ActionEvent actionEvent)
    {
        String dialogName = (String)actionEvent.getComponent().getAttributes().get("dialogName");
        if(dialogName != null)
            executeModal(dialogName, false);
        onFind(actionEvent);
    }

    public void showDeleteDialog(ActionEvent actionEvent)
    {
        String tableName = (String)actionEvent.getComponent().getAttributes().get("tableName");
        String dialogName = (String)actionEvent.getComponent().getAttributes().get("dialogName");
        if(tableName != null && getSelectedRowTable(tableName) == null)
            addMessage(noSelectedRowMsg, FacesMessage.SEVERITY_WARN, new Object[0]);
        else
            executeModal(dialogName, true);
    }

    public void showViewDialog(ActionEvent actionEvent)
    {
        String tableName = (String)actionEvent.getComponent().getAttributes().get("tableName");
        String dialogName = (String)actionEvent.getComponent().getAttributes().get("dialogName");
        if(tableName != null && getSelectedRowTable(tableName) == null)
        {
            addMessage(noSelectedRowMsg, FacesMessage.SEVERITY_WARN, new Object[0]);
        } else
        {
            onShowViewDialog(actionEvent);
            executeModal(dialogName, true);
        }
    }

    public void showSaveDialog(ActionEvent actionEvent)
    {
        String dialogName = (String)actionEvent.getComponent().getAttributes().get("dialogName");
        onShowSaveDialog(actionEvent);
        executeModal(dialogName, true);
    }

    public void onShowSaveDialog(ActionEvent actionevent)
    {
    }

    public void onShowEditDialog(ActionEvent actionevent)
    {
    }

    public void onShowViewDialog(ActionEvent actionevent)
    {
    }

    @Transactional
    public void findAction(ActionEvent actionEvent)
    {
        noRecordFound = false;
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        resetDataTable(root);
        setCursorTableNull();
        try
        {
            onFind(actionEvent);
            if(noRecordFound)
                addMessage(noRecordFoundMsg, FacesMessage.SEVERITY_WARN, new Object[0]);
        }
        catch(ApplicationException e)
        {
            noRecordFound = true;
            addMessage(e.getMessage(), FacesMessage.SEVERITY_ERROR, e.getParameters());
        }
    }

    @Transactional
    public void persistAction(ActionEvent actionEvent)
    {
        success = true;
        String dialogName = (String)actionEvent.getComponent().getAttributes().get("dialogName");
        try
        {
            onPersist(actionEvent);
        }
        catch(ApplicationException e)
        {
            success = false;
            addMessage(e.getMessage(), FacesMessage.SEVERITY_ERROR, e.getParameters());
        }
        if(success)
        {
            if(dialogName != null)
                executeModal(dialogName, false);
            addMessage(successfullyUpdateMsg, new Object[0]);
            onFind(actionEvent);
        }
    }

    @Transactional
    public void deleteAction(ActionEvent actionEvent)
    {
        success = true;
        onDelete(actionEvent);
        if(success)
        {
            addMessage(successfullyDeleteMsg, new Object[0]);
            setCursorTableNull();
            onFind(actionEvent);
        }
    }

    public void onPrint(ActionEvent actionevent, List list, List list1)
    {
    }

    protected void resetForm(UIComponent form)
    {
        UIComponent uic;
        for(Iterator i$ = form.getChildren().iterator(); i$.hasNext(); resetForm(uic))
        {
            uic = (UIComponent)i$.next();
            if(uic instanceof UIData)
            {
                UIData uiDataComponent = (UIData)uic;
                uiDataComponent.setRowIndex(-1);
            }
            if(uic instanceof EditableValueHolder)
            {
                EditableValueHolder evh = (EditableValueHolder)uic;
                evh.resetValue();
            }
        }

    }

    protected void resetDataTable(UIComponent uiComponent)
    {
        UIComponent uic;
        for(Iterator i$ = uiComponent.getChildren().iterator(); i$.hasNext(); resetDataTable(uic))
        {
            uic = (UIComponent)i$.next();
            if(uic instanceof DataTable)
            {
                DataTable dataTable = (DataTable)uic;
                dataTable.reset();
            }
        }

    }

    protected UIComponent findComponent(String id)
    {
        FacesContext context = FacesContext.getCurrentInstance();
        UIViewRoot root = context.getViewRoot();
        return findComponent(((UIComponent) (root)), id);
    }

    private UIComponent findComponent(UIComponent c, String id)
    {
        if(id.equals(c.getId()))
            return c;
        for(Iterator kids = c.getFacetsAndChildren(); kids.hasNext();)
        {
            UIComponent found = findComponent((UIComponent)kids.next(), id);
            if(found != null)
                return found;
        }

        return null;
    }

    protected Object getSelectedRowTable(String tableName)
    {
        Object selectedObject = null;
        UIComponent component = findComponent(tableName);
        String selectionMode = null;
        String selectionColMode = null;
        if(component instanceof DataTable)
        {
            DataTable dataTable = (DataTable)findComponent(tableName);
            selectedObject = dataTable.getSelection();
            selectionMode = dataTable.getSelectionMode();
            selectionColMode = dataTable.getColumnSelectionMode();
        } else
        if(component instanceof TreeTable)
        {
            TreeTable treeTable = (TreeTable)findComponent(tableName);
            selectedObject = treeTable.getSelection();
            selectionMode = treeTable.getSelectionMode();
        }
        if(("multiple".equals(selectionMode) || "multiple".equals(selectionColMode)) && ((Object[])(Object[])selectedObject).length == 0)
            return null;
        else
            return selectedObject;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isSuccess()
    {
        return success;
    }

    protected void setSuccess(boolean success)
    {
        this.success = success;
    }

    protected ResourceBundle getResourceBundle()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ResourceBundle bundle = facesContext.getApplication().getResourceBundle(facesContext, resourceBundleName);
        return bundle;
    }

    protected transient void addMessage(String key, Object parameters[])
    {
        addMessage(key, null, parameters);
    }

    protected transient void addMessage(String key, javax.faces.application.FacesMessage.Severity severity, Object parameters[])
    {
        String text = getMessage(key, parameters);
        FacesMessage facesMessage;
        if(severity == null)
            facesMessage = new FacesMessage("", text);
        else
            facesMessage = new FacesMessage(severity, "", text);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, facesMessage);
    }

    protected transient String getMessage(String key, Object parameters[])
    {
        String text = null;
        try
        {
            text = getResourceBundle().getString(key);
        }
        catch(MissingResourceException e)
        {
            text = key;
        }
        if(parameters != null && parameters.length > 0)
        {
            MessageFormat messageFormat = new MessageFormat(text);
            text = messageFormat.format(((Object) (parameters)));
        }
        return text;
    }

    protected void executeModal(String dialogName, boolean show)
    {
        RequestContext context = RequestContext.getCurrentInstance();
        if(show)
            context.execute((new StringBuilder()).append(dialogName).append(".show()").toString());
        else
            context.execute((new StringBuilder()).append(dialogName).append(".hide()").toString());
    }

    public void redireccionar(String url)
    {
        try
        {
            FacesContext.getCurrentInstance().getExternalContext().redirect(url);
        }
        catch(Exception e)
        {
            e.getMessage();
        }
    }

    protected void navigateTo(String url)
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest)facesContext.getExternalContext().getRequest();
        try
        {
            String page = (new StringBuilder()).append(request.getContextPath()).append("/pages/").append(url).toString();
            FacesContext.getCurrentInstance().getExternalContext().redirect(page);
        }
        catch(IOException e)
        {
            throw new ApplicationException("Error General", e);
        }
    }

    protected String getParam(ActionEvent actionEvent, String parameter)
    {
        String result = (String)actionEvent.getComponent().getAttributes().get(parameter);
        return result;
    }

    protected String getParam(SelectEvent event, String parameter)
    {
        String result = (String)event.getComponent().getAttributes().get(parameter);
        return result;
    }

    public UploadedFile getFile()
    {
        return file;
    }

    public void setFile(UploadedFile file)
    {
        this.file = file;
    }

    public boolean isNoRecordFound()
    {
        return noRecordFound;
    }

    public void setNoRecordFound(boolean noRecordFound)
    {
        this.noRecordFound = noRecordFound;
    }

    public Long getRowsPerPage()
    {
        return rowsPerPage;
    }

    public void setRowsPerPage(Long rowsPerPage)
    {
        this.rowsPerPage = rowsPerPage;
    }

    public UsuarioDTO getUsuario()
    {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario)
    {
        this.usuario = usuario;
    }

    private static final Logger LOG = LoggerFactory.getLogger(util/FindCrudBeanBase);
    protected static final String SAME_PAGE = null;
    protected String noRecordFoundMsg;
    protected String noSelectedRowMsg;
    protected String successfullyUpdateMsg;
    protected String successfullyDeleteMsg;
    protected String invalidUploadedFile;
    protected String successfullyUploadedMesg;
    protected String successfullyCreatedMsg;
    private String resourceBundleName;
    private Long rowsPerPage;
    private String name;
    private UploadedFile file;
    private boolean noRecordFound;
    private boolean success;
    protected String dialogNameASE;
    protected UsuarioDTO usuario;
    @Autowired
    HistoriasClinicasSecurityInfo historiasClinicasSecurityInfo;

}
