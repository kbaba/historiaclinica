// Decompiled by DJ v3.11.11.95 Copyright 2009 Atanas Neshkov  Date: 29/04/2013 02:50:22 p.m.
// Home Page: http://members.fortunecity.com/neshkov/dj.html  http://www.neshkov.com/dj.html - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Validaciones.java

package util;


public class Validaciones
{

    public Validaciones()
    {
    }

    public boolean chekeoNumero(String s)
    {
        if(s.isEmpty())
            return false;
        for(int i = 0; i < s.length(); i++)
        {
            char ch = s.charAt(i);
            if(!Character.isDigit(ch))
                return false;
        }

        return true;
    }

    public boolean esPunto(char ch)
    {
        return ch == '.';
    }

    public boolean chekeoDouble(String s)
    {
        if(s.isEmpty())
            return false;
        for(int i = 0; i < s.length(); i++)
        {
            char ch = s.charAt(i);
            if(!Character.isDigit(ch) && !esPunto(ch))
                return false;
        }

        return true;
    }

    public boolean chekeoPalabraSinEspacio(String s)
    {
        String cadena = s;
        if(cadena.isEmpty())
            return false;
        for(int i = 0; i < cadena.length(); i++)
        {
            char ch = cadena.charAt(i);
            if(!Character.isLetterOrDigit(ch))
                return false;
        }

        return true;
    }

    public boolean esEspacio(char ch)
    {
        return ch == ' ';
    }

    public boolean chekeoPalabraCantidadCaracteres(String s, int cantidadDeseada)
    {
        return s.length() == cantidadDeseada;
    }

    public boolean chekeoPalabras(String s)
    {
        String cadena = s;
        if(cadena.isEmpty())
            return false;
        for(int i = 0; i < cadena.length(); i++)
        {
            char ch = cadena.charAt(i);
            if(!Character.isLetterOrDigit(ch) && !esEspacio(ch))
                return false;
        }

        return true;
    }

    public boolean chekeoPalabrasSinNFAmero(String s)
    {
        String cadena = s;
        if(cadena.isEmpty())
            return false;
        for(int i = 0; i < cadena.length(); i++)
        {
            char ch = cadena.charAt(i);
            if(!Character.isLetter(ch) && !esEspacio(ch))
                return false;
        }

        return true;
    }

    public boolean estaEnIntervalo(int numero, int min, int max)
    {
        return numero > min && numero < max;
    }
}
